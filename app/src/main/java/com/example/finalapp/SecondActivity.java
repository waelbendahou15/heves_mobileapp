package com.example.finalapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.webkit.WebView;
import android.widget.Button;
//import android.widget.Toolbar;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.navigation.NavigationView;

public class SecondActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;

    ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigationView);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.menu_Open, R.string.close_menu);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSupportActionBar(toolbar);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                final int id = item.getItemId();
                if(id == R.id.nav_destination)
                {
                    Log.i("MENU_DRAWER_TAG", "Destination item is clicked");
                    /// close it
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else if (id == R.id.nav_spydermap) {
                    Log.i("MENU_DRAWER_TAG", "User SpyderMap item is clicked");
                    /// close it
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else if (id ==R.id.nav_calculate) {
                    Log.i("MENU_DRAWER_TAG", "Calculate item is clicked");
                    /// close it
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else if (id ==R.id.nav_statistics) {
                    Log.i("MENU_DRAWER_TAG", "Statistics item is clicked");
                    /// close it
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else if (id ==R.id.nav_notifications) {
                    Log.i("MENU_DRAWER_TAG", "Notifications item is clicked");
                    /// close it
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else if (id ==R.id.nav_carsettings)
                {
                    Log.i("MENU_DRAWER_TAG", "Car settings item is clicked");
                    /// close it
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else
                {
                    return true;
                }
                return true;
            }
        });


    }
}

