
<body id="mainPage">

	<header id="entete">
		<a href="index.html"><img src="images/logoV3R.png" style="margin-top: 10px" /></a>
		<h1 id="title" style="color: rgb(4, 81, 178); text-align: center; margin-left: 100px">
			<i><font face="verdana">D&eacute;monstrateur calculs &eacute;nerg&eacute;tiques</font></i>
		</h1>
	</header>

	<section id="subsec1">
		<div id="gifV3R">
			<img src="images/V3R.gif" height="90%" width="90%">
		</div>

		<nav>
			<ul class="menuVertical">
				<li><a onclick="gotoPage('${pageContext.request.contextPath}/');" style="cursor: pointer;">Accueil</a></li>
				<li><a class="active" onclick="gotoPage('${pageContext.request.contextPath}/contact');" style="cursor: pointer;">Contacts</a></li>
			</ul>
		</nav>
		<div id="links">
			<div id="reseauSoc">
				<a href="https://fr-fr.facebook.com/AltranFrance/" target="_blank">
					<img src="images/fb.png" height="100%">
				</a>
			</div>
			<div id="reseauSoc">
				<a href="https://twitter.com/altranfr?lang=fr" target="_blank">
					<img src="images/Twitter.png" height="100%">
				</a>
			</div>
			<div id="reseauSoc">
				<a href="https://fr.linkedin.com/company/altran-" target="_blank">
					<img src="images/LinkedIn.png" height="100%">
				</a>
			</div>
			<div id="reseauSoc">
				<a href="http://fr.viadeo.com/fr/company/altran-france" target="_blank"> 
					<img src="images/Viadeo.png" height="100%">
				</a>
			</div>
		</div>
	</section>

	<section id="overview">
		<div id="contactsSec">
			<table id="categories">
				<tr>
					<th style="background-color: white;">EQUIPE</th>
				</tr>
				<tr>
					<td>v1.1</td>
				</tr>
				<tr>
					<td>Jean-Sebastien BREST(Docteur)</td>
				</tr>
				<tr>
					<td><a href="mailto:jean-sebastien.brest@altran.com">jean-sebastien.brest@altran.com</a></td>
				</tr>
				<tr>
					<td>Pierrick BERTHOLDT (D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:pierrick.bertholdt@altran.com">pierrick.bertholdt@altran.com</a></td>
				</tr>
				<tr>
					<td>Theo RIZO (Coordinateur)</td>
				</tr>
				<tr>
					<td><a href="mailto:theo.rizo@altran.com">theo.rizo@altran.com</a></td>
				</tr>
				<tr>
					<td>Thomas PERRATON (D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:thomas.perraton@altran.com">thomas.perraton@altran.com</a></td>
				</tr>
				<tr>
					<td>v2.0</td>
				</tr>
				<tr>
					<td>Souhail BEN AFIA(Docteur)</td>
				</tr>
				<tr>
					<td><a href="mailto:souhail.benafia@altran.com">souhail.benafia@altran.com</a></td>
				</tr>
				<tr>
					<td>Anthony HERN(D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:anthony.hern@altran.com">anthony.hern@altran.com</a></td>
				</tr>
				<tr>
					<td>Jean LAPRESLE(D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:jean.lapresle@altran.com">jean.lapresle@altran.com</a></td>
				</tr>
				<tr>
					<td>Florian SA�Z(D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:florian.saiz@altran.com">florian.saiz@altran.com</a></td>
				</tr>
				<tr>
					<td>Cyril CAILLAUD(D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:cyril.caillaud@altran.com">cyril.caillaud@altran.com</a></td>
				</tr>
				<tr>
					<td>Luc AUDIS (D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:luc.audis@altran.com">luc.audis@altran.com</a></td>
				</tr>
				<tr>
					<td>v3.0</td>
				</tr>
				<tr>
					<td>Mohamed JAAFAR (Docteur)</td>
				</tr>
				<tr>
					<td><a href="mailto:mohamed.jaafar@altran.com">mohamed.jaafar@altran.com</a></td>
				</tr>
				<tr>
					<td>Maxence GALOPIN(D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:maxence.galopin@altran.com">maxence.galopin@altran.com</a></td>
				</tr>
				<tr>
					<td>Huijian HUANG(D&eacute;velopeur)</td>
				</tr>
				<tr>
					<td><a href="mailto:huijian.huang@altran.com">huijian.huang@altran.com</a></td>
				</tr>
			
				
				
				
			</table>
		</div>
	</section>
</body>
