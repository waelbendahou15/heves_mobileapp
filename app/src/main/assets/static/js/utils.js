window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(231,233,237)'
};

window.randomScalingFactor = function() {
	return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
}

var submitRun = false;

function postAjax(data, url, successCallback, failureCallback){
	submitRun = true;
	
	$.ajax(
		{
			type : "POST",
			url : url,
			data : data,
			success : function(response) {
				if (response != "success"){
					if (response._status === 1){
			           	alert(response._successMessage.join('<br>'));
	                } else {
	                	//alert(response._warningMessage.join('<br>'));
	                	alert(response);
	                }
				}

				$("#body").removeClass("page-loading");
				submitRun=false;
				
				if (successCallback) successCallback(response);
			},
			error: function(response){
                if (response != "failure"){
	                if (response.responseJSON != null && response.responseJSON != ""){
	                	var msgErreur = "<b>L'application a détecté " + response.responseJSON._errorMessage.length + " erreur(s)</b> : <br>" + response.responseJSON._errorMessage.join('<br>');
	                	alert(msgErreur);
	                } else if (response._errorMessage != null && response._errorMessage != ""){
	                	var msgErreur = "<b>L'application a détecté " + response._errorMessage.length + " erreur(s)</b> : <br>" + response._errorMessage.join('<br>');
	                	alert(msgErreur);
	                } else {
	                	alert("Une erreur est survenue (code : " + response.status + ") : " + response);
	                }
                }

				submitRun=false;
                
                if (failureCallback) failureCallback(response);
			}
		}
	);
}
