
//---------------------------------------------------------------------------------- TODO Variables -------------------------------------------------------------------------------------

var dataConsoPacUTBM;
var dataConsoPacUTBM_CS = new Array();                   // creation
															// d'un
															// tableau
															// pour
															// pouvoir
															// tracer
															// les
															// graphes

var CS=false;
var boolExp = false;

// --------------
// Variables
// generales
// Google
/**
 * Actualisation automatique de la carte
 */
google.maps.visualRefresh = true;
/**
 * Initialisation lors du chargement de la fenetre
 */
google.maps.event.addDomListener(window, 'load', initialize);
/**
 * Chargement des packages Google
 */
google.load('visualization', '1', {packages: ['corechart','table']});


/**
 * Geocoder permettant de transformer les adresses en latitude/longitude
 */
var geocoderService = new google.maps.Geocoder();

/**
 * Map sur laquelle on effectue les requetes et on affiche les trajets
 */
var map;
/**
 * Graphique pour l'affichage de l'elevation (VE, VT et VH)
 */
var chartVE;
var chartVT;
var chartVH;
/**
 * Graphique pour l'affichage du taux de charge
 */
var chartcharge;
/**
 * Graphique pour l'affichage de la map cse
 */
var chartCse;
/**
 * Fenetre pour l'affichage d'information sur la carte
 */
var infowindow = new google.maps.InfoWindow();
/**
 * Marker sur la carte en fonction de la position de la souris sur le graphique
 */
var mousemarker = null;
/**
 * Latitude et longitude lors d'un clic droit de la souris sur la carte
 */
var rightclicklatlng;
/**
 * Calque pour l'affichage du traffic
 */
var trafficLayer = new google.maps.TrafficLayer();	

// --------------
// Variables
// d'itineraire
// Google
/**
 * Objet pour les requetes d'itineraire Google API
 */
var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions = {
	draggable: true
});
/**
 * Objet pour les requetes d'itineraire Google API
 */
var directionsService = new google.maps.DirectionsService();
/**
 * Resultat de la requete d'itineraire Google API
 */
var directionsresult;

/**
 * Objet pour les requetes d'elevation Google API
 */
var elevator;
/**
 * Points intermediaire par lesquels on impose le passage
 */
var waypoints = [];
/**
 * Comprend l'ensemble des polylines du trajet chaque element correspond a une etape globale du trajet
 */
var polylines = [];
/**
 * Point de depart de la requete de trajet
 */
var start;
/**
 * Point d'arrivee de la requete de trajet
 */
var end;
/**
 * Type de conduite (voiture, a pied, en velo)
 */
var mode;
/**
 * Boolean faux si le trajet se fait dans le sens choisi par l'utilisateur et vrai si on inverse le sens du trajet
 */
var inverse;
/**
 * Mise en memoire du point de depart du trajet de l'utilisateur
 */
var departure_place;
/**
 * Mise en memoire du point d'arrivee du trajet de l'utilisateur
 */
var arrival_place;
/**
 * Duree discretisee du parcours
 */
var overview_duration = [];
/**
 * Distance discretisee du parcours
 */
var overview_distance = [];
/**
 * Sommes de la discretisation du parcours
 */
var overview_summed_durations = [];
/**
 * Objet pour l'autocomplete du depart de Google API
 */
var autocomplete_departure;
/**
 * Objet pour l'autocomplete de l'arrivee de Google API
 */
var autocomplete_arrival;
/**
 * Marker pour le deplacement sur la carte lors de la selection du depart et de l'arrivee
 */
var marker;
/**
 * Distance totale du trajet choisi par l'utilisateur
 */
var distanceTotaleRoute;

// --------------
// Variables
// d'elevation
/**
 * Tableau contenant les elevations
 */
var elevations = [];
/**
 * Status de la requete Vrai si la requete s'est deroulee normalement et faux sinon
 */
var elevation_status;
/**
 * Gradient pour l'affichage d'un gradient orange sur la courbe
 */
var gradient_orange;
/**
 * Gradient pour l'affichage d'un gradient rouge sur la courbe
 */
var gradient_red;
/**
 * Premiere distance pour le calcul de gradient
 */
var disone;
/**
 * Deuxieme distance pour le calcul de gradient
 */
var distwo;
/**
 * Troisieme distance pour le calcul de gradient
 */
var disthree;
/**
 * Quatrieme distance pour la calcul de gradient
 */
var disfour;
/**
 * Premier gradient calcule
 */
var gradeone;
/**
 * Deuxieme gradient calcule
 */
var gradetwo;
/**
 * Troisieme gradient calcule
 */
var gradethree;
/**
 * Quatrieme gradient calcule
 */
var gradefour;
/**
 * Boolean pour l'activation ou non des gradients
 */
var disabled;
/**
 * Valeur de l'angle moyen en radian
 */
var alpha_moy;
/**
 * Valeur de la pente moyenne en radian
 */
var pmoyen;
/**
 * Valeur de la pente fixe sur un trajet, utilise que dans resolution avec une pente fixe 0% par defaut
 */
var penteFixe =0;
/**
 * Boolean vrai si la resolution se fait avec une pente fixe
 */
var boolPenteFixe;
/**
 * Valeur de la pente fixe pour le calcul du cycle standard, utilise que dans resolution avec une pente fixe 0% par defaut
 */
var penteFixeStandard =0;
/**
 * Boolean vrai si la resolution se fait avec une pente fixe pour le cycle standard
 */
var boolPenteFixeStandard;

var Soc_low=40;


// --------------
// Variables
// des
// etapes
// du
// parcours
/**
 * Tableau contenant les informations des etapes du parcours
 */
var dataStep = new Array();
/**
 * Nombre d'etape du parcours
 */
var nbStep;
/**
 * Nombre de cycle different sur le parcours
 */
var nbCycle;

// --------------
// Grandeurs
// fixes
/**
 * Acceleration de la pesanteur m/s2
 */
var pes = 9.81;
/**
 * Densite de l'air kg/m3 a 20 degre
 */
var rho 	= 1.25;
/**
 * Densite de l'essence g/L a 20 degre
 */
var rhoessence = 750;
/**
 * Masse de Co2 produite par rapport à la masse d'essence utilisee
 */
var ratioCO2 = 3.088;
/**
 * Pouvoir calorifique de l'essence (Wh/g)
 */
var essencePCS = 13.139;
/**
 * ratio air/essence
 */
var AirFuelRatio = 14.7;


/**
 * Pas de temps du calcul pour la creation des cycles Pas de temps du calcul pour l'ajout de point intermediaire entre les cycles
 */
var inter	= 0.5;

// --------------
// Donnees
// du
// vehicule
/**
 * Masse du vehicule 1663 kg par defaut
 */
var masse;
/**
 * Masse supplementaire sur le vehicule 0 kg par defaut
 */
var masseSup;
/**
 * Coefficient de trainee du vehicule 0.63 par defaut
 */
var SCx;
/**
 * Rendement de la batterie 0.95 par defaut
 */
var rend_elec;

/**
 * Rayon des roues du vehicule 0.316 m par defaut
 */
var RRoue;
/**
 * Taux de charge de la batterie initial a 80% par defaut
 */
var socBatterie;
var socBatterieVE;
var socBatterieVP;
var socBatterieVPU;
/**
 * Capacite de la batterie 90 kWh par defaut
 */
var capBatterie;
/**
 * Niveau du réservoir d'hydrogène initial a 80% par defaut
 */
var niveauH2;
/**
 * Niveau minimum du réservoir initial a 5% par defaut
 */
var niveauMin;
/**
 * Capacite du réservoir d'hydrogène 5 kg par defaut
 */
var capH2;
/**
 * Coefficient de resistance au roulement 0.008 par defaut
 */
var res_roulement;
/**
 * Puissance nominale du moteur 30 kW par defaut
 */
var Pmotorrated;
/**
 * Puissance maximale du moteur
 */
var PmotorMax;
/**
 * Rapport de reduction de la transmission 8.2 par defaut
 */
var trRatio;
/**
 * Vitesse minimum a atteindre pour une possible charge de la batterie en freinage regeneratif 5 km/h par defaut
 */
var vmin;
/**
 * Vitesse a partir de laquelle le taux d'energie recuperee correspond a la totalite de l'energie de freinage 17 km/h par defaut
 */
var vmax;
/**
 * Consommation des auxiliaires 300 W par defaut
 */
var Paux;
/**
 * Coefficient de correction pour les pertes energetique par inertie elles correspondent a un pourcentage de la masse totale du vehicule en premiere approche (en general 5%)
 */
var coefInertie;
/**
 * Rendement de la transmission 0.95 par defaut
 */
var rend_gear;
/**
 * Facteur de normalisation fonction du moteur
 */
var normFactor;
/**
 * Seuil de deceleration maximale au dela de ce seuil l'energie recuperee est plafonnee a la valeur qu'elle aurait eu avec ce seuil
 */
var seuilDec;
/**
 * Taux de charge minimum dans la batterie sert de securite pour les calculs
 */
var socMin;
var socMinVE;
var socMinVP;
var socMinVPU;
/**
 * Nombre de rapports de la boite de vitesses 5 par defaut
 */
var nbRapportsVitesse;
/**
 * Tableau contenant l'ensemble des donnees brutes du trajet (comme dataObd mais juste sur un trajet)
 */
var dataObdLu = new Array();
/**
 * Tableau contenant l'ensemble des donnees du trajet post traitement (comme dataObd mais juste sur un trajet)
 */
var dataObdTrajet = new Array();
/**
 * Tableau contenant l'ensemble des donnees du trajet réelle post traitement
 */
var dataObdTrajetReelle = new Array();
/**
 * Tableau contenant l'ensemble des donnees Obd
 */
var dataObd = new Array();
/**
 * /** Tableaux contenant l'ensemble des donnees pour les calculs réelles
 */
var dataReellesVT = new Array();
var dataReellesVTBC = new Array();
var dataReellesVTC = new Array();
var dataReellesVTCbis = new Array();
var dataReellesVTBCO = new Array();
var dataReellesVTDonnees = new Array();
var dataReellesVTGraph = new Array();
/**
 * Consommations réelles
 */
var ConsommationMoyenneReelleVT = 0;
var ConsommationReelleVT = 0;
/**
 * Tableau contenant l'ensemble des donnees du trajet pour un véhicule electrique
 */
var dataConseilsVE = new Array();
/**
 * Nombre de parametres dans le fichier des conseils VE.
 */
var nbParamConseilsVE = 3;
/**
 * Nombre de parametres dans le fichier des conseils VE.
 */
var nbParamConseilsVETotal = 5;
/**
 * Nombre de lignes dans le fichier des conseilsVE
 */
var nbLignesParamConseilsVE;
/**
 * Booleen pour savoir si les parametres sont visibles ou non
 */
var paramConseilsVEVisible = false;
/**
 * Booleen pour savoir si le fichier des conseils pour VE est charge ou non
 */
var paramFileConseilsVELoaded = false;
/**
 * Nombre de parametres dans le fichier des donnees Obd.
 */
var nbParamObd = 6;
/**
 * Nombre de parametres dans le fichier des donnees réelles.
 */
var nbParamReelles = 4;
/**
 * Nombre de lignes dans le fichier de données réelles
 */
var nbLignesParamReelles = 0;
/**
 * Nombre de parametres dans le tableau des donnees Obd.
 */
var nbParamObdTotal = 15;
/**
 * Nombre de lignes dans le fichier des donnees Obd
 */
var nbLignesParamObd;
/**
 * Booleen pour savoir si les parametres sont visibles ou non
 */
var paramObdVisible = false;
/**
 * Booleen pour savoir si le fichier de parametres Cse est charge ou non
 */
var paramFileObdLoaded = false;
/**
 * Booleen pour savoir si le fichier de paramètres réelles est chargé ou non
 */
var paramFileReellesLoaded = false;
/**
 * Tableau contenant l'ensemble des rapports de reduction de la boite de vitesses.
 */
var dataVitesses = new Array();
/**
 * Tableau contenant les correspondances entre (vitesse, pente) et rapport de reduction.
 */
var MapVitesses = new Array();
/**
 * Tableau contenant les correspondances entre (vitesse, pente) et rapport de reduction.
 */
var MapVitessesStat = new Array();
/**
 * pas pour le calcul de la map des vitesses
 */
var pasVitesses = 5;
/**
 * tableau pour le calcul de la map vitesses
 */
var vecVitesses = new Array();
/**
 * longueur du tableau vecVitesses
 */
var longueurvecVitesses;
/**
 * minimum du tableau vecCmot
 */
var minvecVitesses;
/**
 * maximum du tableau vecNmot
 */
var maxvecVitesses;
/**
 * pas pour le calcul de la map des vitesses
 */
var pasPente = 1;
/**
 * tableau pour le calcul de la map vitesses
 */
var vecPente = new Array();
/**
 * longueur du tableau vecVitesses
 */
var longueurvecPente;
/**
 * minimum du tableau vecCmot
 */
var minvecPente;
/**
 * maximum du tableau vecNmot
 */
var maxvecPente;
/**
 * Tableau contenant les points permettant le trace de la courbe de couple
 */
var dataCouple = new Array();
/**
 * Nombre de parametres dans le fichier du couple.
 */
var nbParamCouple = 2;
/**
 * Nombre de lignes dans le fichier du couple
 */
var nbLignesParamCouple;
/**
 * Booleen pour savoir si les parametres sont visibles ou non
 */
var paramCoupleVisible = false;
/**
 * Booleen pour savoir si le fichier du couple est charge ou non
 */
var paramFileCoupleLoaded = false;
/**
 * Nom du fichier du couple
 */
var CoupleParamFileName = '';
/**
 * Coefficient pour le calcul de la map cse
 */
var coef;
/**
 * pas des vitesses de rotation pour le calcul de la map cse
 */
var pasNmot = 100;
/**
 * pas des couples pour le calcul de la map cse
 */
var pasCmot = 5;
/**
 * tableau des vitesses de rotation pour le calcul de la map cse
 */
var vecNmot = new Array();
/**
 * tableau des couples pour le calcul de la map cse
 */
var vecCmot = new Array();
/**
 * longueur du tableau vecNmot
 */
var longueurvecNmot;
/**
 * longueur du tableau vecCmot
 */
var longueurvecCmot;
/**
 * minimum du tableau vecNmot
 */
var minvecNmot;
/**
 * minimum du tableau vecCmot
 */
var minvecCmot;
/**
 * maximum du tableau vecNmot
 */
var maxvecNmot;
/**
 * maximum du tableau vecCmot
 */
var maxvecCmot;
/**
 * tableau pour le calcul de la map cse
 */
var iCseMin = new Array();
/**
 * tableau pour le calcul de la map cse
 */
var matNb = new Array();
/**
 * tableau pour le calcul de la map cse
 */
var Map_cse_init = new Array();
/**
 * tableau pour le calcul de la map cse
 */
var Map_cse = new Array();
/**
 * tableau pour le calcul de la map cse
 */
var Map_cse_t = new Array();
/**
 * tableau pour le calcul de la map cse
 */
var Map_cse_i = new Array();
/**
 * tableau pour le calcul de la map cse
 */
var Map_cse_ex = new Array();
/**
 * tableau pour le calcul de la map cse
 */
var Nmin = new Array();
/**
 * tableau pour le calcul de la map cse
 */
var Nmax = new Array();
/**
 * cse
 */
var taille_trou_max=0;
/**
 * cse
 */
var taille_trou=0;
/**
 * cse
 */
var taille_trou_max_ex=0;
/**
 * cse
 */
var taille_trou_ex=0;
/**
 * cse
 */
var penteCse;
/**
 * Tableau contenant l'ensemble des donnees parametres des vehicules electriques
 */
var dataVehiculesE = new Array();
/**
 * Nombre de parametres pour un vehicule electriques, nom y compris.
 */
var nbParamParVehiculeE = 19;
/**
 * Nombre de vehicules initial dans le fichier de parametres vehicule electriques
 */
var nbLignesParamVehE;
/**
 * Enumeration pour les paramètres du tableau dataVehiculesE.
 */
var paramTypeEnum = {
		  VEH_TYPE: 0,
		  VEH_MASS: 1,
		  WHEEL_RADIUS: 2,
		  DRAG_COEFF: 3,
		  ROLL_RES: 4,
		  TR_YIELD: 5,
		  TR_RATIO: 6,
		  ENG_TYPE: 7,
		  NOMINAL_ENG_POW: 8,
		  MAX_ENG_POW: 9,
		  MAX_ENG_TQ: 10,
		  BATT_TYPE: 11,
		  BATT_EFF: 12,
		  BATT_CAPA: 13,
		  REC_MIN_SPD: 14,
		  REC_OPT_SPD: 15,
		  REC_MAX_DEC : 16,
		  AUX_CONS :17,
		  COEFF_INERTIA:18,
		  VEH_ID: 19
		};
/**
 * Valeurs par defaut pour les parametres des vehicules electriques
 */
var defaultParamValue = ["", "1663", "0.316", "0.63", "0.008", "0.97", "8.2", "Synchrone", "60", "100", "200", "Lithium", "0.95", "40", "5", "17", "3", "300", "0.05"];
/**
 * rendement du moteur thermique (%)
 */
var rendementmoteur;
/**
 * rendement optimal du moteur thermique (%)
 */
var rendementmoteurOpti;
/**
 * rendement du moteur thermique (%)
 */
var rendementmoteurCycle;
/**
 * rendement optimal du moteur thermique (%)
 */
var rendementmoteurOptiCycle;
/**
 * Booleen pour savoir si les parametres sont visibles ou non
 */
var paramVisibleE = false;
/**
 * Booleen pour savoir si le fichier de parametres vehicules electriques est charge ou non
 */
var paramFileVehELoaded = false;

/**
 * Tableau contenant l'ensemble des donnees parametres des vehicules thermiques
 */
var dataVehiculesT = new Array();
/**
 * Tableau contenant l'ensemble des donnees parametres des vehicules pile a combustible
 */
var dataVehiculesP = new Array();
/**
 * Nombre de parametres pour un vehicule thermique, nom y compris.
 */
var nbParamParVehiculeT = 10;
/**
 * Nombre de vehicules initial dans le fichier de parametres vehicules thermiques
 */
var nbLignesParamVehT;
/**
 * Booleen pour savoir si les parametres sont visibles ou non
 */
var paramVisibleT = false;
/**
 * Booleen pour savoir si le fichier de parametres vehicules thermiques est charge ou non
 */
var paramFileVehTLoaded = false;

// Variables
// pour
// VPU

var P_max_fc_U;         // puissance
						// maxi
						// pac
						// instt
var rend_converter_D;	// Rendement
						// du
						// convertisseur
						// DC
var rend_converter_DC;
var valTemperatureExt;


// --------------
// Variable
// pour le
// bus
// hybride
/**
 * Variables pour le facteur d'équivalence
 */
var FEdonnee = new Array();	// Tableau
							// pour
							// les
							// valeurs
							// du
							// facteur
							// d'équivalen
							// --
							// pour
							// l'instant
							// fixe
FEdonnee[0] = new Array();
FEdonnee[1] = new Array();
FEdonnee[0][0] = 0.580;
FEdonnee[0][1] = 0.550;
FEdonnee[0][2] = 0.500;
FEdonnee[0][3] = 0.450;
FEdonnee[0][4] = 0.420;
FEdonnee[1][0] = 0;
FEdonnee[1][1] = 2;
FEdonnee[1][2] = 2.5;
FEdonnee[1][3] = 3;
FEdonnee[1][4] = 5;
/**
 * Varibles du moteur thermique
 */
var Pbusmax = 160000; 		// Puissance
							// max
							// (W)
var Cbusmax = 800;		// Couple
						// max
						// (N.m)
var Jmbus = 5; 			// Inertie
						// (kg.m²)
/**
 * Variables du moteur électrique
 */
var PBusBatmax = 125000; // Puissance
							// max
							// (W)
var PBusDRMmax = 140000; // Puissance
							// max
							// (W)
var CBusBatmax = 1200; 	// Couple
						// max
						// (N.m)
var CBusDRMmax = 800; 	// Couple
						// max
						// (N.m)
var Jinro = 5; 			// Inertie
						// rotor
						// (kg.m²)
var Ppertes = 1400;		// Pertes
						// dans
						// le
						// moteur
						// électrique
						// (W)
var Tdrm = 450;			// Couple
						// dans
						// le
						// rotor
						// qui
						// tourne
						// dans
						// le
						// vide
						// (N.m)
var Pdrm;				// Puissance
						// dans
						// le
						// rotor
						// qui
						// tourne
						// dans
						// le
						// vide
						// (W)
var Paux = 7000;		// Puisssance
						// électrique
						// des
						// auxiliaires
/**
 * Variables de la batterie
 */
var CapaBatBus1 = 170; 	// Capacité
						// de
						// la
						// batterie
						// (A.h)
						// variante
						// 1
var CapaBatBus2 = 340;	// Capacité
						// de
						// la
						// batterie
						// (A.h)
						// variante
						// 2
var VBatBus1 = 642; 	// Tension
						// batterie
						// à
						// Soc=0.3
var VBatBus2 = 658; 	// Tension
						// batterie
						// à
						// Soc=0.7
var RBusdis = 0.52;		// Résistance
						// de
						// déchargement
						// (Ohm)
var RBuscha = 0.48; 	// Résistance
						// de
						// chargement
						// (Ohm)
/**
 * Variables de résistance
 */
var CoefResAirBus = 3.6;// 0.032625;
						// //
						// Coefficient
						// de
						// résistance
						// à
						// l'air
						// :
						// (rho/2)*Af*cd
var CoefResRoulBus = 0.054; // 0.07848;
							// //
							// Coefficient
							// de
							// résistance
							// au
							// roulement
							// :
							// fr*g
/**
 * Variables des caractéristique du bus
 */
var MassePasBus = 1500; 	// Masse
							// des
							// passagers
							// (kg)--
							// pris
							// fixe
							// pour
							// l'instant
							// > 20
							// pasagers
							// de
							// 75kg
var MasseBus = 10000;		// Masse
							// du
							// bus
var Massetotbus = MassePasBus + MasseBus;	// 1945;
											// //
											// Masse
											// totale
											// pour
											// l'instant
var RwhBus = 0.471; // 0.316;
					// //
					// Rayon
					// de
					// la
					// roue
					// du
					// bus
					// (m)
/**
 * Variables de la chaine de transmission
 */
var RatioBusWH = 13.3; 	// Ratio
						// entre
						// le
						// final
						// drive
						// et
						// le
						// rayon
						// de
						// la
						// roue
						// :
						// If/Rwh
var RatioBus = RatioBusWH * RwhBus; // 8.2;
									// //
									// If :
									// 13.3
									// *
									// Rwh
var RendBus = 0.97;		// Rendement
						// du
						// final
						// drive
/**
 * Variables pour le modèle
 */
var TempsTurbo1 = 0.8;	// Temps
						// de
						// réponse
						// du
						// turbo
						// rapide
var TempsTurbo2 = 2.4;	// Temps
						// de
						// réponse
						// du
						// turbo
						// lent
var JtSoC = new Array();// Fonction
						// cost-to-go
var wpena = 4;			// Pénalité
						// si
						// le
						// moteur
						// doit
						// s'allumer
						// (g)
var dataConsoreverse; 	// Tableau
						// dataConso
						// inversé
var cptarret = 0;		// Compteur
						// pour
						// les
						// 2s
						// d'arrêt
						// du
						// moteur
var cptdem = 0;			// Compteur
						// pour
						// les
						// 2s
						// d'allumage
var ti;					// Temps
						// i de
						// la
						// tentative
var boolarret = true;	// Booléen
						// pour
						// savoir
						// si
						// le
						// test
						// est
						// réussi
						// ou
						// non
var courant;			// Variable
						// pour
						// le
						// courant
						// dans
						// la
						// batterie
var tension; 			// Variable
						// pour
						// la
						// tension
						// dans
						// la
						// batterie
var SSooCC;				// Valeur
						// de
						// l'état
						// de
						// charge
						// dans
						// le
						// test
var CSE_Bus;			// Consommation
						// spécifique
						// du
						// bus
var Consogs;			// Consommation
						// en
						// g/s
var PuiMot;				// Puissance
						// moteur
						// thermique
						// pour
						// le
						// couple
						// T
var PuiBat;				// Puissance
						// batterie
						// pour
						// le
						// couple
						// T
var SoC;				// État
						// de
						// charge
var DeltaSoC;			// Différence
						// d'état
						// de
						// charge
						// entre
						// deux
						// instants
var chargeMot;			// Charge
						// moteur
						// %
var indiceRegMot;		// Indice
						// régime
						// moteur
var indiceCouMot;		// Indice
						// couple
						// moteur
var testeur;			// Test
						// pour
						// minimiser
						// la
						// consommation
var Valeurtesteur;		// Valeur
						// minimale
						// du
						// test
var ValeurT;			// Valeur
						// optimale
						// de T
var ValeurPuiMot;		// Valeur
						// optimale
						// de
						// la
						// puissance
						// moteur
var ValeurPuiBat;		// Valeur
						// optimale
						// de
						// la
						// puissance
						// batterie
var ValeurSoC;			// Valeur
						// optimale
						// de
						// l'état
						// de
						// charge
var ValeurDeltaSoC;		// Valeur
						// optimale
						// de
						// la
						// différence
						// d'état
						// de
						// charge
var ValeurConsogs;		// Valeur
						// optimale
						// de
						// la
						// consommation
						// en
						// g/s
var wci;				// Couple
						// pendant
						// le
						// test
var TESTCSE = [];		// Test
						// pour
						// la
						// CSE
var nbPointHybride = 0;		// Nombre
							// de
							// point
							// pour
							// le
							// trajet
// --------------
// Variables
// de la
// conduite
/**
 * Tableau contenant les donnees du cycle unitaire Ville Lent (0-20km/h) le cycle unitaire correspond a un cycle de conduite, c'est le cycle importe
 */
var dataCycleVilleLent			= new Array();
/**
 * Tableau contenant les donnees du cycle unitaire Ville Rapide (20-50km/h) le cycle unitaire correspond a un cycle de conduite, c'est le cycle importe
 */
var dataCycleVilleRapide		= new Array();
/**
 * Tableau contenant les donnees du cycle unitaire Rural Lent (50-70km/h) le cycle unitaire correspond a un cycle de conduite, c'est le cycle importe
 */
var dataCycleRuralLent			= new Array();
/**
 * Tableau contenant les donnees du cycle unitaire Rural Rapide (70-90km/h) le cycle unitaire correspond a un cycle de conduite, c'est le cycle importe
 */
var dataCycleRuralRapide		= new Array();
/**
 * Tableau contenant les donnees du cycle unitaire Autoroute (>90km/h) le cycle unitaire correspond a un cycle de conduite, c'est le cycle importe
 */
var dataCycleAutoroute			= new Array(); // ADD
/**
 * Nombre de points du cycle unitaire Ville Lent (0-20km/h)
 */
var nbPointCycleVilleLent;
/**
 * Nombre de points du cycle unitaire Ville Rapide (20-50km/h)
 */
var nbPointCycleVilleRapide;
/**
 * Nombre de points du cycle unitaire Rural Lent (50-70km/h)
 */
var nbPointCycleRuralLent;
/**
 * Nombre de points du cycle unitaire Rural Rapide (70-90km/h)
 */
var nbPointCycleRuralRapide;
/**
 * Nombre de points du cycle unitaire Autoroute (>90km/h)
 */
var nbPointCycleAutoroute; // ADD
/**
 * Tableau Contenant les donnees du cycle repete lorsqu'on utilise pas la segmentation
 */
var dataCycleRep = new Array();
/**
 * Nombre de point du cycle repete
 */
var nbPointRep;
/**
 * Acceleration de confort pour l'utilisateur, elle permet de faire le lien entre deux portions de route ayant une vitesse differente elle est utilise dans la resolution par segmentation du trajet
 */
var accMax;
/**
 * Deceleration de confort pour l'utilisateur, elle permet de faire le lien entre deux portions de route ayant une vitesse differente elle est utilise dans la resolution par segmentation du trajet
 */
var decMax;

// --------------
// Variables
// de
// resolution
/**
 * Tableaux contenant l'ensemble des donnees calculées pour le bus hybride
 */
var dataBusHybride = new Array();
var dataBusHybridereverse = new Array();
/**
 * Tableau contenant l'ensemble des donnees calculées
 */
var dataConso = new Array();
/**
 * Tableau contenant l'ensemble des données réelles calculées
 */
var dataConsoReelleVT = new Array();
/**
 * Nombre de parametres dans le tableau dataConso
 */
var nbParamDataTable;
/**
 * Nombre de points du tableau contenant les données calculees
 */
var nbPointConso;
/**
 * Nombre de points du tableau contenant les données réelles
 */
var nbPointReelles;
/**
 * Tableau contenant l'ensemble des données calcules pour le cycle standard
 */
var dataCycle = new Array();
/**
 * Boolean vrai lorsque la resolution doit se refaire, ie l'utilisateur est tombe en panne et il doit refaire le trajet en passant par une borne
 */
var boolReDo = false;
/**
 * Boolean vrai si on a ajoute un waypoint par une borne
 */
var boolBorne = false;
/**
 * Booléen pour l'affichage des bouttons hybride
 */
var boolAffichageBouttons = true;
/**
 * valeur du taux de charge au moment de l'arrêt à une borne
 */
var tauxDeChargeBorne = 0;
/**
 * indice dans dataConso où se situe l'arret a une borne de recharge
 */
var tpsBorne;
/**
 * Type de resolution 1 : repetition du cycle de conduite 2 : segmentation (par cycle ou par vitesse constante selon parametrage de la conduite)
 */
var typeResolution = 0;
/**
 * Boolean vrai si le calcul de la route a ete lance
 */
var boolCalcRoute = false;
/**
 * Boolean vrai si on fait une resolution d'un trajet reel a partir de donnees obd
 */
var boolTrajet = false;
/**
 * compteur du temps ou le moteur est en phase motrice
 */
var temps_phase_motrice = 0;
/**
 * Boolean vrai si le calcul de la map cse a ete lance
 */
var boolOBD = false
/**
 * Boolean vrai si le changement de phase a eu lieu
 */
var boolChgtPhase = true;
/**
 * Boolean vrai si c'est le debut du calcul
 */
var boolInitCalcul = true;

/**
 * variable comptant le temps depuis le dernier changement de phase (s)
 */
var timerPhase;
/**
 * duree de la phase (en seconde) par default : 30s
 */
var dureePhase = 30;
/**
 * iterateur depuis le dernier changement de phase
 */
var compteurPhase;
/**
 * indice a partir duquel il faut reprendre les calculs
 */
var indiceReprisePhase = 2;
/**
 * Boolean vrai si le changement de phase a eu lieu
 */
var boolChgtPhaseTrajet = true;
/**
 * Boolean vrai si c'est le debut du calcul
 */
var boolInitCalculTrajet = true;
/**
 * variable comptant le temps depuis le dernier changement de phase (s)
 */
var timerPhaseTrajet;
/**
 * iterateur depuis le dernier changement de phase
 */
var compteurPhaseTrajet;
/**
 * indice a partir duquel il faut reprendre les calculs
 */
var indiceReprisePhaseTrajet = 2;
/**
 * Boolean vrai si il y a des conseils a donner
 */
var boolConseils = false;
// --------------
// Variables
// des
// bornes
/**
 * Tableau contenant la localisation des bornes de recharge sur le reseau francais
 */
var dataBorne = new Array();
/**
 * Tableau contenant l'ensemble des marqueurs des bornes de la map
 */
var markersBornes = [];
/**
 * Tableau contenant les bornes accessibles pour la recharge
 */
var markersRecharge = [];
/**
 * Tableau contenant les pannes
 */
var markersPanne = [];
/**
 * Latitude par laquelle le vehicule passe pour se recharger il y a une borne a cette position
 */
var latB;
/**
 * Longitude par laquelle le vehicule passe pour se recharger il y a une borne a cette position
 */
var longB;
/**
 * Tableau 1 des latitudes des bornes, toutes les latitudes des bornes sont enregistrées
 */
var latBtab = [];
/**
 * Tableau 1 des longitudes des bornes, toutes les longitudes des bornes sont enregistrées
 */
var longBtab = [];
/**
 * Tableau 2 des latitudes des bornes, les latitudes sont effacées lorsque l'on passe par une borne
 */
var latBtab2 = [];
/**
 * Tableau 2 des longitudes des bornes, les longitudes sont effacées lorsque l'on passe par une borne
 */
var longBtab2 = [];
/**
 * Indice du numéro de la borne de recharge
 */
var NumBorne = 0;
/**
 * Compteur de borne à proximité
 */
var cptborne = 0;
/**
 * Indice dans le dataConso correspondant a la panne puis au point ou la recherche de borne c'est arrete
 */
var indiceP=0;
/**
 * Tableau contenant les positions des bornes de recharge
 */
var dataBorne = new Array();
/**
 * Nombre de point du tableau dataBorne
 */
var nbPointBorne;
/**
 * Boolean vrai si l'utilisateur a choisi une borne a laquelle il souhaite se rendre
 */
var boolChoixBorne = false;
// --------------
// Variables
// du
// cycle
// standard
/**
 * Nombre de points du tableau contenant les donnees du cycle standard
 */
var nbPointCycle;
/**
 * Nom du fichier du cycle standard
 */
var fileName = '';
/**
 * Booleen pour savoir si le fichier de cycles est charge ou non
 */
var cyclesFileLoaded = false;
/**
 * Booleen pour savoir si le fichier de cycles contient plusieurs cycles (detection d'un $) ou non (pas de $)
 */
var cyclesFileSingle = false;
/**
 * Nom du fichier de cycles de conduite
 */
var cyclesFileName = '';
/**
 * Tableau contenant tous les tableaux data conso
 */
var gatheredDataConso = new Array();
/**
 * Puissance de recharge de la borne
 */
var puissanceBorne = 3.7;
// --------------
// Variables
// d'affichage
/**
 * Boolean pour l'affichage ou non des pentes maximales
 */
var hideminmax;
/**
 * Marge en pixel a gauche du graphique
 */
var chartleft;
/**
 * Marge en pixel a droite du graphique
 */
var chartright;
/**
 * Hauteur du graphique
 */
var chartheight;
/**
 * Largeur du graphique
 */
var chartwidth;
/**
 * Marge en pixel en haut du graphique
 */
var charttop;
/**
 * Boolean pour l'utilisation en plein ecrant
 */
var fullsize;
/**
 * Donnees pour le graphique d'elevation
 */
var data;
/**
 * Tableau contenant l'ensemble des donnees du trajet
 */
var dataxcos;
/**
 * Tableau contenant les donnees simplifiees du trajet utilise avant pour le graphique
 */
var dataxcos2;
/**
 * Numero de l'absisse utilise pour l'affichage
 */
var abs = 36;
/**
 * Numero de l'ordonnee, utilise pour l'affichage
 */
var ord = 1;
/**
 * Numero du cycle unitaire, utilise pour l'affichage
 */
var cycle = 0;
/**
 * Graphiques pour l'affichage il est instance en global pour permettre de l'utiliser avec les fonction mouseover et mouseout
 */
var myChartVE1;
var myChartVE2;
var myChartVT1;
var myChartVT2;
var myChartVT3;
var myChartVH1;
var myChartVH2;
var myChartVP1;
var myChartVP2;
var myChartVPU1;
var myChartVPU2;
var myChartVPU3;
/**
 * Variable pour afficher les graphiques VE
 */
var absVE1 = 36;
var absVE2 = 36;
var absVE3 = 36;
var ordVE1 = 1;
var ordVE2 = 1;
var ordVE3 = 1;
/**
 * Variable pour afficher les graphiques VT
 */
var absVT1 = 36;
var absVT2 = 36;
var absVT3 = 36;
var ordVT1 = 1;
var ordVT2 = 1;
var ordVT3 = 1;
/**
 * Variable pour afficher les graphiques VH
 */
var absVH1 = 3;
var absVH2 = 3;
var ordVH1 = 1;
var ordVH2 = 1;
/**
 * Variable pour afficher les graphiques VP
 */
var absVP1 = 7;
var absVP2 = 7;
var ordVP1 = 15;
var ordVP2 = 15;
/**
 * Variable pour afficher les graphiques VPU
 */
var absVPU1 = 7;
var absVPU2 = 7;
var absVPU3 = 7;
var ordVPU1 = 15;
var ordVPU3 = 15;
var ordVPU2 = 15;
/**
 * Graphique pour l'affichage il est instance en global pour permettre de l'effacer entre deux affichages
 */
var ChartCycleUnit;
/**
 * Boolean pour la reinitialisation des rapports
 */
var boolRapport = false;
/**
 * Boolean pour la reinitialisation des rapports d'etapes
 */
var boolRapportEtape = false;
/**
 * Boolean pour la reinitialisation des rapports de cycle
 */
var boolRapportCycle = false;
/**
 * Variable regroupant les bornes pour l'affichage
 */
var markerCluster;

var config;
/**
 * Affichage ou non du Gif sous la carte
 */
var boolGif = true;
/**
 * Variable definissant le type du vehicule (Thermique : VT / Electrique : VE / Hybride (électrique et thermique) : VH / Hybride PAC (Hydrogène et électrique) : VP et VPU (mécénat UTBM))
 */
var typeVehicule;

// --------------
// Autres
// variables
// globales
/**
 * Utilisation du systeme metrique
 */
var system_mi;
/**
 * Utilisation du systeme imperial
 */
var system_ft;
/**
 * Unite pour le systeme metrique
 */
var str_mi;
/**
 * Unite pour le systeme imperial
 */
var str_ft;
/**
 * Variable pour l'extraction des coordonnees sur le trajet
 */
var coordonnees;
/**
 * Longitude d'un point
 */
var longitude;
/**
 * Latitude d'un point
 */
var latitude;
/**
 * Position de l'utilisateur s'il autorise la geolocalisation
 */
var user_location;
/**
 * demande de geolocalisation a l'ouverture du demonstrateur
 */
var boolInitialize = true;
/**
 * Variable pour l'export de donnees unitilisee
 */
var csvElevationExport;
/**
 * Variable pour l'export de donnees unitilisee
 */
var csvDistanceExport;

// ----------------------------------------------------------------------------------
// TODO
// Fonctions
// ------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
// Initialisation
// de la
// page
// --------------------------------------------------------------------
/**
 * Initialisation de l'interface Google API au chargement de la page web
 */
function initialize() {

  var mapOptions = {
    zoom: 6,
    mapTypeId: 'roadmap',
    backgroundColor: '#eee'
    }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);	// Creation
																				// d'une
																				// nouvelle
																				// carte
  
  // Centre
	// la
	// carte
	// sur
	// la
	// position
	// de
	// l'utilisateur
	// s'il
	// autorise
	// la
	// geolocalisation
  geocoderService.geocode({'address' : "France"}, function(results, status){
    if (status == google.maps.GeocoderStatus.OK) {
      user_location = results[0].geometry.location;
      map.setCenter(user_location);
	}
  });

  directionsDisplay.setOptions({ 
		polylineOptions: { 
			strokeColor: "#0040ff",
			strokeOpacity:1.0,
			strokeWeight: 4,
		} 
  });

  directionsDisplay.setMap(map);															// Affectation
																							// des
																							// requetes
																							// de
																							// trafic
																							// a la
																							// carte

	
	// directionsDisplay.setPanel(document.getElementById('directionsPanel'));
	// //
	// Affichage
	// des
	// indications
	// directionnelles
  elevator = new google.maps.ElevationService();											// Initilisation
																							// des
																							// requetes
																							// d'elevation
  
  if(typeVehicule=="VE"){
	  chartCharge = new google.visualization.AreaChart(document.getElementById('tauxChargeChartVE'));
	  chartVE = new google.visualization.AreaChart(document.getElementById('elevation_chartVE'));	// Initialisation
																									// du
																									// graphique
																									// d'élévation
																									// pour
																									// le
																									// VE
	  myChartVE1 = new google.visualization.AreaChart(document.getElementById('chart_energieVE1'));	// Initialisation
																									// du
																									// graphique
																									// 1
																									// des
																									// variables
																									// pour
																									// le
																									// VE
	  myChartVE2 = new google.visualization.AreaChart(document.getElementById('chart_energieVE2'));	// Initialisation
																									// du
																									// graphique
																									// 2
																									// des
																									// variables
																									// pour
																									// le
																									// VE
	  myChartVE3 = new google.visualization.AreaChart(document.getElementById('chart_energieVE3'));
  }
  if(typeVehicule=="VT"){
	  chartVT = new google.visualization.AreaChart(document.getElementById('elevation_chartVT'));	// Initialisation
																									// du
																									// graphique
																									// d'élévation
																									// pour
																									// le
																									// VT
	  myChartVT1 = new google.visualization.AreaChart(document.getElementById('chart_energieVT1'));	// Initialisation
																									// du
																									// graphique
																									// 1
																									// des
																									// variables
																									// pour
																									// le
																									// VT
	  myChartVT2 = new google.visualization.AreaChart(document.getElementById('chart_energieVT2'));	// Initialisation
																									// du
																									// graphique
																									// 2
																									// des
																									// variables
																									// pour
																									// le
																									// VT
	  myChartVT3 = new google.visualization.AreaChart(document.getElementById('chart_energieVT3'));
  }
  if(typeVehicule=="VH"){
	  chartVH = new google.visualization.AreaChart(document.getElementById('elevation_chartVH'));	// Initialisation
																									// du
																									// graphique
																									// d'élévation
																									// pour
																									// le
																									// VH
	  myChartVH1 = new google.visualization.AreaChart(document.getElementById('chart_energieVH1'));	// Initialisation
																									// du
																									// graphique
																									// 1
																									// des
																									// variables
																									// pour
																									// le
																									// VH
	  myChartVH2 = new google.visualization.AreaChart(document.getElementById('chart_energieVH2'));	// Initialisation
																									// du
																									// graphique
																									// 2
																									// des
																									// variables
																									// pour
																									// le
																									// VH
  }
  if(typeVehicule=="VP"){
	  chartVP = new google.visualization.AreaChart(document.getElementById('elevation_chartVP'));	// Initialisation
																									// du
																									// graphique
																									// d'élévation
																									// pour
																									// le
																									// VP
	  myChartVP1 = new google.visualization.AreaChart(document.getElementById('chart_energieVP1'));	// Initialisation
																									// du
																									// graphique
																									// 1
																									// des
																									// variables
																									// pour
																									// le
																									// VP
	  myChartVP2 = new google.visualization.AreaChart(document.getElementById('chart_energieVP2'));	// Initialisation
																									// du
																									// graphique
																									// 2
																									// des
																									// variables
																									// pour
																									// le
																									// VP
  }
  
  if(typeVehicule=="VPU"){
	  chartCharge = new google.visualization.AreaChart(document.getElementById('tauxChargeChartVPU'));
	  chartVPU = new google.visualization.AreaChart(document.getElementById('elevation_chartVPU'));	// Initialisation
																									// du
																									// graphique
																									// d'élévation
																									// pour
																									// le
																									// VPU
	  myChartVPU1 = new google.visualization.AreaChart(document.getElementById('chart_energieVPU1'));	// Initialisation
																										// du
																										// graphique
																										// 1
																										// des
																										// variables
																										// pour
																										// le
																										// VPU
	  myChartVPU2 = new google.visualization.AreaChart(document.getElementById('chart_energieVPU2'));	// Initialisation
																										// du
																										// graphique
																										// 2
																										// des
																										// variables
																										// pour
																										// le
																										// VPU
	  myChartVPU3 = new google.visualization.AreaChart(document.getElementById('chart_energieVPU3'));	// Initialisation
																										// du
																										// graphique
																										// 2
																										// des
																										// variables
																										// pour
																										// le
																										// VPU
  }
  
  
  // Initialisation
	// des
	// variables
  inverse = false;
  gradient_orange = 0.06;
  gradient_red = 0.10;
  disone = 6000;
  distwo = 4000;
  disthree = 2000;
  disfour = 1000;
  gradeone = 0.07;
  gradetwo = 0.07;
  gradethree = 0.06;
  gradefour = 0.05;
  disabled = false;
  hideminmax = false;
  system_mi = 1;
  system_ft = 1;
  str_mi = "km";
  str_ft = "m";
  chartheight = '80%';
  chartwidth = '100%';
  chartleft = 55;
  chartright = 20;
  charttop = 20;
  fullsize = false;
  mode = "DRIVING";
  /*
	 * var contentString = '<div id="rightclick">'+ '<button id="rcbutton" name="" type="button" value="" onclick="setOrigin()">Set Origin</button><br><button id="rcbutton" name="" type="button" value="" onclick="setDestination()">Set Destination</button>' + '</div>'; infowindow = new google.maps.InfoWindow({content: contentString});
	 */
  document.getElementById("mode"+typeVehicule).options[0].selected = true;
  document.getElementById("system"+typeVehicule).options[0].selected = true;
  document.getElementById("orange").options[3].selected = true;
  document.getElementById("red").options[0].selected = true;
  document.getElementById("disone").options[1].selected = true;
  document.getElementById("distwo").options[2].selected = true;
  document.getElementById("disthree").options[1].selected = true;
  document.getElementById("disfour").options[1].selected = true;
  document.getElementById("gradeone").options[2].selected = true;
  document.getElementById("gradetwo").options[2].selected = true;
  document.getElementById("gradethree").options[1].selected = true;
  document.getElementById("gradefour").options[1].selected = true;
  document.getElementById("disone").disabled = false;
  document.getElementById("distwo").disabled = false;
  document.getElementById("disthree").disabled = false;
  document.getElementById("disfour").disabled = false;
  document.getElementById("gradeone").disabled = false;
  document.getElementById("gradetwo").disabled = false;  
  document.getElementById("gradethree").disabled = false;
  document.getElementById("gradefour").disabled = false;
  document.getElementById("disableClimbs").checked = false;
  document.getElementById("hideminmax").checked = false;  
  
  // Initialisation
	// des
	// Listener
  
  	/*
	 * google.maps.event.addListener(map, "rightclick", function(event) { infowindow.setPosition(event.latLng); rightclicklatlng = event.latLng; infowindow.open(map); });
	 */

  google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
    var route = directionsDisplay.getDirections().routes[0];
    start = route.legs[0].start_location;
    end = route.legs[0].end_location;
    saveWaypoints();
    if (mousemarker != null) {
      mousemarker.setMap(null);
      mousemarker = null;
    }
    drawPath(route.overview_path);
  });
  
  // Lien
	// entre
	// la
	// position
	// sur
	// les
	// graphiques
	// et
	// la
	// carte
	// pour
	// le
	// VE
  if(typeVehicule=="VE"){
	  // Chargement
		// de
		// la
		// liste
		// des
		// bornes
		// au
		// chargement
		// de
		// la
		// page
	  loadDataBorne();
	  google.visualization.events.addListener(chartVE, 'onmouseover', function(e) {
		    if (mousemarker == null) {
		      mousemarker = new google.maps.Marker({
		        position: elevations[e.row].location,
		        map: map,
		        animation: google.maps.Animation.DROP,
		        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
		      });
		    }
		    else {
		      mousemarker.setPosition(elevations[e.row].location);
		    }
		  });
		  google.visualization.events.addListener(myChartVE1, 'onmouseover', function(e) {
			  var Latlg = new google.maps.LatLng(dataConso[e.row][33],dataConso[e.row][34])
			  if (mousemarker == null) {
			      mousemarker = new google.maps.Marker({
			        position: Latlg,
			        map: map,
			        animation: google.maps.Animation.DROP,
			        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			      });
			    }
			    else {
			      mousemarker.setPosition(Latlg);
			    }
		  });
		  google.visualization.events.addListener(myChartVE2, 'onmouseover', function(e) {
			  var Latlg = new google.maps.LatLng(dataConso[e.row][33],dataConso[e.row][34])
			  if (mousemarker == null) {
			      mousemarker = new google.maps.Marker({
			        position: Latlg,
			        map: map,
			        animation: google.maps.Animation.DROP,
			        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			      });
			    }
			    else {
			      mousemarker.setPosition(Latlg);
			    }
		  });
  }
  
// Lien
// entre
// la
// position
// sur les
// graphiques
// et la
// carte
// pour le
// VT
  if(typeVehicule=="VT"){
	  google.visualization.events.addListener(chartVT, 'onmouseover', function(e) {
		    if (mousemarker == null) {
		      mousemarker = new google.maps.Marker({
		        position: elevations[e.row].location,
		        map: map,
		        animation: google.maps.Animation.DROP,
		        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
		      });
		    }
		    else {
		      mousemarker.setPosition(elevations[e.row].location);
		    }
		  });
		  google.visualization.events.addListener(myChartVT1, 'onmouseover', function(e) {
			  var Latlg = new google.maps.LatLng(dataConso[e.row][33],dataConso[e.row][34])
			  if (mousemarker == null) {
			      mousemarker = new google.maps.Marker({
			        position: Latlg,
			        map: map,
			        animation: google.maps.Animation.DROP,
			        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			      });
			    }
			    else {
			      mousemarker.setPosition(Latlg);
			    }
		  });
		  /*
			 * google.visualization.events.addListener(myChartVT2, 'onmouseover', function(e) { var Latlg = new google.maps.LatLng(dataReellesVT[e.row][33],dataReellesVT[e.row][34]) if (mousemarker == null) { mousemarker = new google.maps.Marker({ position: Latlg, map: map, animation: google.maps.Animation.DROP, icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png" }); } else {
			 * mousemarker.setPosition(Latlg); } });
			 */
  }
   
// Lien
// entre
// la
// position
// sur les
// graphiques
// et la
// carte
// pour le
// VH
  if(typeVehicule=="VH"){
	  google.visualization.events.addListener(chartVH, 'onmouseover', function(e) {
		    if (mousemarker == null) {
		      mousemarker = new google.maps.Marker({
		        position: elevations[e.row].location,
		        map: map,
		        animation: google.maps.Animation.DROP,
		        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
		      });
		    }
		    else {
		      mousemarker.setPosition(elevations[e.row].location);
		    }
		  });
		  google.visualization.events.addListener(myChartVH1, 'onmouseover', function(e) {
			  var Latlg = new google.maps.LatLng(dataConso[e.row][33],dataConso[e.row][34])
			  if (mousemarker == null) {
			      mousemarker = new google.maps.Marker({
			        position: Latlg,
			        map: map,
			        animation: google.maps.Animation.DROP,
			        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			      });
			    }
			    else {
			      mousemarker.setPosition(Latlg);
			    }
		  });
		  google.visualization.events.addListener(myChartVH2, 'onmouseover', function(e) {
			  var Latlg = new google.maps.LatLng(dataConso[e.row][33],dataConso[e.row][34])
			  if (mousemarker == null) {
			      mousemarker = new google.maps.Marker({
			        position: Latlg,
			        map: map,
			        animation: google.maps.Animation.DROP,
			        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			      });
			    }
			    else {
			      mousemarker.setPosition(Latlg);
			    }
		  });
  }
  
// Lien
// entre
// la
// position
// sur les
// graphiques
// et la
// carte
// pour le
// VPU
  if(typeVehicule=="VPU"){
	  google.visualization.events.addListener(chartVPU, 'onmouseover', function(e) {
		    if (mousemarker == null) {
		      mousemarker = new google.maps.Marker({
		        position: elevations[e.row].location,
		        map: map,
		        animation: google.maps.Animation.DROP,
		        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
		      });
		    }
		    else {
		      mousemarker.setPosition(elevations[e.row].location);
		    }
		  });
		  google.visualization.events.addListener(myChartVPU1, 'onmouseover', function(e) {
			  var Latlg = new google.maps.LatLng(dataConso[e.row][33],dataConso[e.row][34])
			  if (mousemarker == null) {
			      mousemarker = new google.maps.Marker({
			        position: Latlg,
			        map: map,
			        animation: google.maps.Animation.DROP,
			        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			      });
			    }
			    else {
			      mousemarker.setPosition(Latlg);
			    }
		  });
		  google.visualization.events.addListener(myChartVPU2, 'onmouseover', function(e) {
			  var Latlg = new google.maps.LatLng(dataConso[e.row][33],dataConso[e.row][34])
			  if (mousemarker == null) {
			      mousemarker = new google.maps.Marker({
			        position: Latlg,
			        map: map,
			        animation: google.maps.Animation.DROP,
			        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			      });
			    }
			    else {
			      mousemarker.setPosition(Latlg);
			    }
		  });
		 /* google.visualization.events.addListener(myChartVPU3, 'onmouseover', function(e) {
			  var Latlg = new google.maps.LatLng(dataConso[e.row][33],dataConso[e.row][34])
			  if (mousemarker == null) {
			      mousemarker = new google.maps.Marker({
			        position: Latlg,
			        map: map,
			        animation: google.maps.Animation.DROP,
			        icon: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			      });
			    }
			    else {
			      mousemarker.setPosition(Latlg);
			    }
		  });*/
  }
  
  
  initMarker();

  initAutocomplete();

  if(boolInitialize){
  // Geolocalisation
	// si
	// l'utilisateur
	// l'autorise
  if(get_url_param('slat') == '') { // Try
									// HTML5
									// geolocation
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        geocoderService.geocode({'location': pos}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
				user_location = pos;
				moveOnMapLttLgt(pos);
            } else {
              window.alert('No results found');
            }
		  }
          else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });
	  });
    }
  }
  boolInitialize = false;
  }
  parameterUpdate();
  // boolCalcRoute
	// =
	// false;

  // Chargement
	// de
	// la
	// liste
	// des
	// paramètres
	// des
	// véhicules
	// électriques
	// au
	// chargement
	// de
	// la
	// page
  // loadCarElecParam();
	// Inutile
	// car
	// on
	// affiche
	// maintenant
	// un
	// unique
	// message
	// sur
	// la
	// page
	// démarrage
	// :
	// "Veuillez
	// choisir
	// un
	// véhicule",
	// on
	// ne
	// démarre
	// plus
	// sur
	// la
	// page
	// du
	// VE
}

// ----------------------------------------------------------------------------------
// Initialisation
// des
// parametres
// ----------------------------------------------------------------
/**
 * Initialisation des parametres lecture des valeurs sur la page HTML ou utilisation des valeurs par defaut si les valeurs entrees ne sont pas conforme la seule verification concerne le fait que les entrees soient des nombres
 */
var E_batt_r_U_REF;
function initialisationParam(){
	
	var temp = document.getElementById('Masse'+typeVehicule).value;
	(temp!='' && isNumeric(temp)) ? masse = parseFloat(temp) : masse =1300;
	temp = document.getElementById('MasseSup'+typeVehicule).value;
	(temp!='' && isNumeric(temp)) ? masseSup = parseFloat(temp) : masseSup = 0;
	masse = masse + masseSup;
	temp = document.getElementById('SCx'+typeVehicule).value;
	(temp!='' && isNumeric(temp))? SCx = parseFloat(temp) : SCx = 0.6682;
	temp = document.getElementById('RayonRoue'+typeVehicule).value;
	(temp!='' && isNumeric(temp)) ?RRoue = parseFloat(temp) : RRoue = 0.316;
	temp = document.getElementById('res_roulement'+typeVehicule).value;
	(temp!='' && isNumeric(temp))? res_roulement = parseFloat(temp): res_roulement = 0.008;	
	temp = document.getElementById('rend_gear'+typeVehicule).value;
	(temp!='' && isNumeric(temp))? rend_gear = parseFloat(temp): rend_gear = 0.97;
	temp = document.getElementById('coefInertie'+typeVehicule).value;
	(temp!='' && isNumeric(temp))? coefInertie = parseFloat(temp): coefInertie = 0.05;
	if(typeVehicule=="VE"){		
		temp = document.getElementById('trRatio'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? trRatio = parseFloat(temp): trRatio = 8.2;
		temp = document.getElementById('PmotorMax'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? PmotorMax = parseFloat(temp): PmotorMax = 100;	
		temp = document.getElementById('Pmotorrated'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? Pmotorrated = parseFloat(temp)*1000: Pmotorrated = 80000;
		temp = document.getElementById('rend_elec'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? rend_elec = parseFloat(temp): rend_elec = 0.95;
		temp = document.getElementById('capBatterie'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ?capBatterie = parseFloat(temp): capBatterie = 90;
		temp = document.getElementById('Paux'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? Paux = parseFloat(temp): Paux = 300;
		temp = document.getElementById('seuilDec'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? seuilDec = parseFloat(temp): seuilDec = 0.3;
		temp = document.getElementById('vmin'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? vmin = parseFloat(temp): vmin = 5;
		temp = document.getElementById('vmax'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ?vmax = parseFloat(temp): vmax = 17;
		if(socBatterieVE == undefined){socBatterieVE = 80}
		if(socMinVE == undefined){socMinVE=5}
		
		if(memory==2 && tabMemory.length != 0 ){									//on récupère la dernière valeur du SOC calculé dans la portion précédante si la mémoire est activiée.
			socBatterieVE=tabMemory[32][tabMemory[32].length-1];
		}
		
		socBatterie=socBatterieVE;
		socMin=socMinVE;
		
		
	}
	else if(typeVehicule=="VT"){
		temp = document.getElementById('nbRapportsVitesse'+typeVehicule).value;
		(temp!='' && isNumeric(temp) && parseFloat(temp) == parseInt(temp))? nbRapportsVitesse = parseFloat(temp): nbRapportsVitesse = 5;
		nbRapportsVitesse = parseInt(nbRapportsVitesse);
	}
	else if(typeVehicule=="VH"){
		temp = document.getElementById('nbRapportsVitesse'+typeVehicule).value;
		(temp!='' && isNumeric(temp) && parseFloat(temp) == parseInt(temp))? nbRapportsVitesse = parseFloat(temp): nbRapportsVitesse = 5;
		nbRapportsVitesse = parseInt(nbRapportsVitesse);
		
		// Pris
		// de
		// la
		// partie
		// VE
		seuilDec = 0.1;

		vmin = 5;
		vmax = 17;
		rend_elec = 0.95;
	}
	else if(typeVehicule=="VP"){
		temp = document.getElementById('trRatio'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? trRatio = parseFloat(temp): trRatio = 8.2;
		temp = document.getElementById('PmotorMax'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? PmotorMax = parseFloat(temp): PmotorMax = 60;	
		temp = document.getElementById('Pmotorrated'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? Pmotorrated = parseFloat(temp)*1000: Pmotorrated = 50000;
		temp = document.getElementById('rend_elec'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? rend_elec = parseFloat(temp): rend_elec = 0.95;
		temp = document.getElementById('capBatterie'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ?capBatterie = parseFloat(temp): capBatterie = 1.3;
		temp = document.getElementById('Paux'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? Paux = parseFloat(temp): Paux = 300;
		temp = document.getElementById('seuilDec'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? seuilDec = parseFloat(temp): seuilDec = 0.3*pes;
		temp = document.getElementById('vmin'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? vmin = parseFloat(temp): vmin = 5;
		temp = document.getElementById('vmax'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ?vmax = parseFloat(temp): vmax = 17;
		if(socBatterieVP == undefined){socBatterieVP = 80}
		if(socMinVP == undefined){socMinVP=5}
		temp = document.getElementById('powerThreshold'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? powerThreshold = parseFloat(temp) : masseSup = 0;
		temp = document.getElementById('speedThreshold'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? speedThreshold = parseFloat(temp) : masseSup = 0;
		temp = document.getElementById('acceleratorThreshold'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? acceleratorThreshold = parseFloat(temp) : masseSup = 0;
		
		if(memory==2 && tabMemory.length != 0 ){									//on récupère la dernière valeur du SOC calculé dans la portion précédante si la mémoire est activiée.
			socBatterieVP=tabMemory[28][tabMemory[28].length-1];
		}	
		socBatterie=socBatterieVP;
		socMin=socMinVP;
		E_batt_r[0] = 45 * 0.01 * capBatterie * 1000; // initialisation
														// de
														// l'energie
														// contenue
														// dans
														// la
														// batterie
														// en
														// Wh
														// 45%
		E_batt_r[1] = E_batt_r[0];
		per_batt_r[0] = E_batt_r[0]/(capBatterie*10);   // initialisation
														// pourcentage
														// batterie
														// restant
														// pour
														// vehicule
														// pac
		per_batt_r[1] = per_batt_r[0];
	}
	else if(typeVehicule=="VPU"){
		
		temp = document.getElementById('trRatio'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? trRatio = parseFloat(temp): trRatio = 8.2;
		temp = document.getElementById('rend_elec'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? rend_elec = parseFloat(temp): rend_elec = 0.95;
		temp = document.getElementById('PmotorMax'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? PmotorMax = parseFloat(temp): PmotorMax = 120;	
		temp = document.getElementById('Pmotorrated'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? Pmotorrated = parseFloat(temp)*1000: Pmotorrated = 120000;
		temp = document.getElementById('capBatterie'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ?capBatterie = parseFloat(temp): capBatterie = -1;
		// Les
		// batteries
		// de
		// véhicules
		// hybrides
		// rechargeables
		// ont
		// une
		// capacité
		// bien
		// plus
		// élevées
		// que
		// les
		// batteries
		// de
		// véhicules
		// hybrides
		// non
		// rechargeables
		if(document.getElementById("loiCommande").value!="3" && capBatterie==-1 && temp==''){capBatterie = 1.3} 
		else if (document.getElementById("loiCommande").value=="3" && temp==''){capBatterie = 15}
		//La puissance auxiliaire ci-dessous n'a pas été prise en compte dans les calculs de prédiction de consommation : il faudra regarder comment l'intégrer
		temp = document.getElementById('Paux'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? Paux = parseFloat(temp): Paux = 800;
		temp = document.getElementById('seuilDec'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? seuilDec = parseFloat(temp): seuilDec = 0.3*pes;
		temp = document.getElementById('vmin'+typeVehicule).value;
		(temp!='' && isNumeric(temp))? vminVPU = parseFloat(temp): vminVPU = 5;
		temp = document.getElementById('vmax'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ?vmaxVPU = parseFloat(temp): vmaxVPU = 17;
		if(socBatterieVPU == undefined){socBatterieVPU = 45}
		if(socMinVPU == undefined){socMinVPU=5}
		if(niveauH2 == undefined){niveauH2 = 80}
		if(niveauMin == undefined){niveauMin=5}
		temp = document.getElementById('powerThreshold'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? powerThreshold = parseFloat(temp) : masseSup = 0;
		temp = document.getElementById('speedThreshold'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? speedThreshold = parseFloat(temp) : masseSup = 0;
		temp = document.getElementById('acceleratorThreshold'+typeVehicule).value;
		(temp!='' && isNumeric(temp)) ? acceleratorThreshold = parseFloat(temp) : masseSup = 0;
		temp = document.getElementById('valeurPuissMaxPACU').value;
		(temp!='' && isNumeric(temp)) ? P_max_fc_U = parseFloat(temp) : P_max_fc_U = 95000;
		temp = document.getElementById('valeurRendConvDCPACU').value; 
		(temp!='' && isNumeric(temp)) ? rend_converter_DC = parseFloat(temp) : rend_converter_DC = 0.85;
		temp = document.getElementById('capH2'+typeVehicule).value; 
		(temp!='' && isNumeric(temp)) ? capH2 = parseFloat(temp) : capH2 = 5;
		temp = document.getElementById('coefFrotVis').value; 
		(temp!='' && isNumeric(temp)) ? coefFrotVis = parseFloat(temp) : coefFrotVis = 0.0019;
		temp = document.getElementById('valTensionMot').value; 
		(temp!='' && isNumeric(temp)) ? valTensionMot = parseFloat(temp) : valTensionMot = 500;
		temp = document.getElementById('valResistPhaseMot').value; 
		(temp!='' && isNumeric(temp)) ? valResistPhaseMot = parseFloat(temp) : valResistPhaseMot = 0.085;
		temp = document.getElementById('ageBatt').value; 
		(temp!='' && isNumeric(temp)) ? ageBatt = parseFloat(temp) : ageBatt = 60;
		temp = document.getElementById('valCyclBatt').value; 
		(temp!='' && isNumeric(temp)) ? valCyclBatt = parseFloat(temp) : valCyclBatt = 10;
		temp = document.getElementById('tempExt').value; 
		(temp!='' && isNumeric(temp)) ? valTemperatureExt = parseFloat(temp) : valTemperatureExt = 20;
		temp = document.getElementById('valRapportTrans').value; 
		(temp!='' && isNumeric(temp)) ? valRapportTrans = parseFloat(temp) : valRapportTrans = 10;
		temp = document.getElementById('nbCellBatt').value; 
		(temp!='' && isNumeric(temp)) ? nbCellBatt = parseFloat(temp) : nbCellBatt = 34;
		temp = document.getElementById('valPrixH2').value; 
		(temp!='' && isNumeric(temp)) ? valPrixH2 = parseFloat(temp) : valPrixH2 = 10;
		temp = document.getElementById('valPrixElec').value; 
		(temp!='' && isNumeric(temp)) ? valPrixElec = parseFloat(temp) : valPrixElec = 0.25;
		temp = document.getElementById('puissanceCharge').value; 
		(temp!='' && isNumeric(temp)) ? puissanceCharge = parseFloat(temp) : puissanceCharge = 3680; // Puissance
																										// de
																										// charge
																										// sur
																										// prise
																										// domestique
																										// (cas
																										// le
																										// pire)
		
		
		if(memory==2 && tabMemory.length != 0 ){									//on récupère la dernière valeur du SOC calculé dans la portion précédante si la mémoire est activiée.
			socBatterieVPU=tabMemory[28][tabMemory[28].length-1];
		}	
		socBatterie=socBatterieVPU;
		socMin=socMinVPU;
		per_batt_r_Ah_U[0] = socBatterie; // State
											// of
											// CHARGE
		per_batt_r_Ah_U[1] = socBatterie;
		var OCV0 = nbCellBatt*7;
		var OCV1 = nbCellBatt*(0.000006*Math.pow(per_batt_r_Ah_U[0],3)-0.001*Math.pow(per_batt_r_Ah_U[0],2)+0.0581*per_batt_r_Ah_U[0]+7);
		if(document.getElementById("modeleBatterie").value=="2" || document.getElementById("modeleBatterie").value=="3"){ // Pour
																	// le
																	// modèle
																	// UTBM,
																	// on
																	// ne
																	// fait
																	// pas
																	// la
																	// différence
																	// entre
																	// SoE
																	// et
																	// SoC
			per_batt_r_U[0] = socBatterie;  
			per_batt_r_U[1] = socBatterie;
			
		}
		else{
			per_batt_r_U[0] = (1/OCV1)*socBatterie*((OCV0+OCV1)/2);   // //
																		// State
																		// of
																		// ENERGY
																		// -
																		// initialisation
																		// pourcentage
																		// batterie
																		// restant
																		// pour
																		// vehicule
																		// pac
			per_batt_r_U[1] = per_batt_r_U[0];
			U[0] = OCV0;
			U[1] = OCV1;
		}
		E_batt_r_U[0] = per_batt_r_U[0] * 0.01 * capBatterie * 1000; // initialisation
																		// de
																		// l'energie
																		// contenue
																		// dans
																		// la
																		// batterie
																		// en
																		// Wh
																		// avce
																		// SoC
																		// initial
		E_batt_r_U[1] = E_batt_r_U[0];
		
		if(memory==2 && tabMemory.length == 0 ){									//on récupère la dernière valeur du SOC calculé dans la portion précédante si la mémoire est activiée.
			window.E_batt_r_U_REF = E_batt_r_U[0];
		}	
		
		
		// alert(per_batt_r_Ah_U[0]+'
		// '+per_batt_r_Ah_U[1]+'
		// '+per_batt_r_U[0]+'
		// '+per_batt_r_U[1])
		
		if(memory==2 && tabMemory.length != 0 ){
			niveauH2=tabMemory[23][tabMemory[23].length-1];
		}	
		
		
		M_H2_r_U[0] = niveauH2 * 0.01 * capH2;
		M_H2_r_U[1] = M_H2_r_U[0];
		per_H2_r_U[0] = M_H2_r_U[0]/(capH2*0.01);
		per_H2_r_U[1] = per_H2_r_U[0];
		T[0]=valTemperatureExt;
		T[1]=valTemperatureExt;
	}
	// Initialisation
	// des
	// constantes
	// de
	// la
	// conduite
	// avec
	// les
	// valeurs
	// utilisateurs
	// ou
	// les
	// valeurs
	// par
	// defaut
	(document.getElementById('accMax'+typeVehicule).value != '')	? accMax = parseFloat(document.getElementById('accMax'+typeVehicule).value) : accMax = 1.2;
	(document.getElementById('decMax'+typeVehicule).value != '')	? decMax = parseFloat(document.getElementById('decMax'+typeVehicule).value) : decMax = 1.2;
}
/**
 * Initialisation des valeurs par recuperation des donnees rentrees par l'utilisateur dans le cadre d'une resolution par repetition du cycle
 */
function initialisationRepetition(){
	// Initialisation
	// des
	// constantes
	// du
	// vehicule
	// avec
	// les
	// valeurs
	// utilisateurs
	// ou
	// les
	// valeurs
	// par
	// defaut
	initialisationParam();
	
	if(typeVehicule=="VE" || typeVehicule == "VP" || typeVehicule =="VPU"){
		// Calcul
		// du
		// facteur
		// de
		// normalisation
		// en
		// fonction
		// de
		// la
		// puissance
		// moteur
		calcNormfactor(Pmotorrated);
	}
	
	// Creation
	// du
	// tableau
	// de
	// donnee
	dataConso=[];
	creaTableau(dataConso);	

	// Creation
	// du
	// tableau
	// du
	// cycle
	// (on
	// utilise
	// le
	// cycle
	// ville
	// lent
	// pour
	// reduire
	// le
	// nombre
	// de
	// variable)
	// puisque
	// ce
	// cycle
	// n'est
	// pas
	// utilise
	// dans
	// le
	// cas
	// d'une
	// repetition
	// d'un
	// cycle
	nbPointRep 	= creaCycle('#cycleRepetition'+typeVehicule,dataCycleRep,'vitesseVilleLent'+typeVehicule,1,0);

	// Calcul
	// de
	// la
	// distance
	// cumule
	// du
	// cycle
	calcDistanceCycle(dataCycleRep,nbPointRep)

	// Calcul
	// de
	// la
	// pente
	// moyenne
	// sur
	// le
	// trajet
	if(boolPenteFixe){
		alpha_moy = penteFixe;
	}else{
		/*
		 * var pente = dataxcos.getValue(1,5); var nb = 1; for (var i = 1; i < dataxcos.getNumberOfRows();i++){ pente += dataxcos.getValue(i,5); nb+=1; } alpha_moy = pente/nb;
		 */
		alpha_moy = 0;
	}
}
/**
 * Initialisation des valeurs par recuperation des donnees rentrees par l'utilisateur dans le cadre d'une resolution par segmenation du trajet
 */
function initialisationSegmentation(){
	
	// Initialisation
	// des
	// constantes
	// du
	// vehicule
	// avec
	// les
	// valeurs
	// utilisateurs
	// ou
	// les
	// valeurs
	// par
	// defaut
	initialisationParam();
	if(typeVehicule=="VE" || typeVehicule == "VP" || typeVehicule =="VPU"){
		// Calcul
		// du
		// facteur
		// de
		// normalisation
		// en
		// fonction
		// de
		// la
		// puissance
		// moteur
		calcNormfactor(Pmotorrated);
	}
	
	
	// Creation
	// du
	// tableau
	// de
	// donnee
	dataConso=[];
	creaTableau(dataConso);
	
	// Initialisation
	// de
	// la
	// discretisation
	var vitesseVilleLent;
	var temp = document.getElementById('vitesseVilleLent'+typeVehicule).value;
	(temp != '' && isNumeric(temp)) ? vitesseVilleLent = parseFloat(temp) : vitesseVilleLent = 10;
	var vitesseVilleRapide;
	temp = document.getElementById('vitesseVilleRapide'+typeVehicule).value;
	(temp != '' && isNumeric(temp)) ? vitesseVilleRapide = parseFloat(temp)	: vitesseVilleRapide = 40;
	var vitesseRuralLent;
	temp = document.getElementById('vitesseRuralLent'+typeVehicule).value;
	(temp != '' && isNumeric(temp)) 	? vitesseRuralLent 	 = parseFloat(temp) 	: vitesseRuralLent 	 = 60;
	var vitesseRuralRapide;
	temp = document.getElementById('vitesseRuralRapide'+typeVehicule).value;
	(temp != '' && isNumeric(temp)) ? vitesseRuralRapide = parseFloat(temp)	: vitesseRuralRapide = 80;
	var vitesseAutoroute;
	temp = document.getElementById('vitesseAutoroute'+typeVehicule).value;
	(temp != '' && isNumeric(temp)) 	? vitesseAutoroute 	 = parseFloat(temp) 	: vitesseAutoroute 	 = 120;
	
	// Creation
	// des
	// tableaux
	// de
	// chaque
	// cycle
	nbPointCycleVilleLent 	= creaCycle('#cycleVilleLent'+typeVehicule,dataCycleVilleLent,'vitesseVilleLent'+typeVehicule,300,vitesseVilleLent);
	nbPointCycleVilleRapide = creaCycle('#cycleVilleRapide'+typeVehicule,dataCycleVilleRapide,'vitesseVilleRapide'+typeVehicule,300,vitesseVilleRapide);
	nbPointCycleRuralLent	= creaCycle('#cycleRuralLent'+typeVehicule,dataCycleRuralLent,'vitesseRuralLent'+typeVehicule,300,vitesseRuralLent);
	nbPointCycleRuralRapide	= creaCycle('#cycleRuralRapide'+typeVehicule,dataCycleRuralRapide,'vitesseRuralRapide'+typeVehicule,300,vitesseRuralRapide);
	nbPointCycleAutoroute	= creaCycle('#cycleAutoroute'+typeVehicule,dataCycleAutoroute,'vitesseAutoroute'+typeVehicule,300,vitesseAutoroute);
		
	// Calcul
	// de
	// la
	// distance
	// cumule
	// des
	// cycles
	calcDistanceCycle(dataCycleVilleLent,nbPointCycleVilleLent)
	calcDistanceCycle(dataCycleVilleRapide,nbPointCycleVilleRapide)
	calcDistanceCycle(dataCycleRuralLent,nbPointCycleRuralLent)
	calcDistanceCycle(dataCycleRuralRapide,nbPointCycleRuralRapide)
	calcDistanceCycle(dataCycleAutoroute,nbPointCycleAutoroute)
	
	// Calcul
	// de
	// la
	// pente
	// moyenne
	// sur
	// le
	// trajet
	if(boolPenteFixe){
		alpha_moy = penteFixe;
	}else{
		/*
		 * var pente = dataxcos.getValue(1,5); var nb = 1; for (var i = 1; i < dataxcos.getNumberOfRows();i++){ pente += dataxcos.getValue(i,5); nb+=1; } alpha_moy = pente/nb;
		 */
		alpha_moy = 0;
	}
}
/**
 * Initialisation des variables en fonction du type de cycle dans lequel le point se trouve
 * 
 * @param iter :
 *            iterateur du tableau ou le point est a calculer
 * @param tab :
 *            tableau de donnee a lire
 * @returns nbPoint : nombre de point du cycle
 * @returns tabTampon : tableau du cycle correspondant au point
 * @returns distanceCycle : distance du cycle correspondant
 */
function initParcours(iter,tab){
	if(tab[iter][31] == 'VilleLent'){
		var nbPoint			= nbPointCycleVilleLent;
		var tabTampon 		= dataCycleVilleLent;
		var distanceCycle 	= tabTampon[nbPoint][3];
	}else if(tab[iter][31] == 'VilleRapide'){
		var nbPoint			= nbPointCycleVilleRapide;
		var tabTampon		= dataCycleVilleRapide;
		var distanceCycle	= tabTampon[nbPoint][3];
	}else if (tab[iter][31] == 'RuralLent'){
		var nbPoint			= nbPointCycleRuralLent;
		var tabTampon		= dataCycleRuralLent;
		var distanceCycle	= tabTampon[nbPoint][3];
	}else if(tab[iter][31] == 'RuralRapide'){
		var nbPoint			= nbPointCycleRuralRapide;
		var tabTampon		= dataCycleRuralRapide;
		var distanceCycle	= tabTampon[nbPoint][3];
	}else if (tab[iter][31] == 'Autoroute'){
		var nbPoint			= nbPointCycleAutoroute;
		var tabTampon		= dataCycleAutoroute;
		var distanceCycle	= tabTampon[nbPoint][3];
	}	
	return {
		nbPoint : nbPoint,
		tabTampon : tabTampon,
		distanceCycle : distanceCycle,
	}
}
/**
 * Initialisation de l'autocomplete des adresses Google API
 */
function initAutocomplete() {
	if (typeVehicule==undefined) typeVehicule = 'VE';
  // Create
	// the
	// autocomplete
	// object,
	// restricting
	// the
	// search
	// to
	// geographical
	// position
  autocomplete_departure = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocompleteDeparture'+typeVehicule)),
      {types: ['geocode']});
	  
  autocomplete_arrival = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocompleteArrival'+typeVehicule)),
      {types: ['geocode']});

  // When
	// the
	// user
	// selects
	// an
	// address
	// from
	// the
	// dropdown,
	// populate
	// the
	// address
	// fields
	// in
	// the
	// form.
  autocomplete_departure.addListener('place_changed', function(){
	  departure_place = autocomplete_departure.getPlace();
	  start = departure_place.geometry.location;
	  moveOnMap(departure_place);
  });
  autocomplete_arrival.addListener('place_changed', function(){
	  arrival_place = autocomplete_arrival.getPlace();
	  end = arrival_place.geometry.location;
	  moveOnMap(arrival_place);
  });
}
/**
 * Initialisation des markers Google API
 */
function initMarker() {
  marker = new google.maps.Marker({
  map: map,
  anchorPoint: new google.maps.Point(0, -29)
  });
}
/**
 * Initialisation des booleen au lancement du calcul pour les véhicules thermiques
 */
function initCalculThermique(){
	boolChgtPhase = true;
	boolInitCalcul = true;
	boolTrajet = false;	
	indiceReprisePhase = 2;
}
/**
 * Initialisation des booleen au lancement du calcul du trajet pour les véhicules thermiques
 */
function initCalculThermiqueTrajet(){
	boolChgtPhase = true;
	boolInitCalcul = true;
	boolTrajet = true;	
	indiceReprisePhase = 2;
	boolChgtPhaseTrajet = true;
	boolInitCalculTrajet = true;	
	indiceReprisePhaseTrajet = 2;
}
// ----------------------------------------------------------------------------------
// Lecture
// de
// fichier
// ---------------------------------------------------------------------------
/**
 * Lecture des donnees du cycle et affectation dans le taleau dataConso
 * 
 * @param nomCycle :
 *            nom du cycle a lire
 * @param tabCycle :
 *            tableau de donnee ou doit etre copie le cycle
 * @param readFromFile :
 *            true is read from a single txt file, false if read from gatherDataConso
 * @returns nbPoint : nombre de point du tableau de donnee du cycle
 */
var lastAltitude=0;
function lectureCycle(nomCycle,tabCycle,bool,readFromFile){
	var cycle;
	var nbPoint;
	
	
	/**
	if (readFromFile) {
		var fileInput = document.querySelector(nomCycle);
		    var reader = new FileReader();
		    reader.readAsText(fileInput.files[0]);
		    reader.addEventListener('load', function() {
		    	cycle=reader.result
		 });
		    
		alert("Le fichier " + fileInput.files[0].name +" est associe au parcours " + nomCycle)
		if(bool){
			var temp = fileInput.files[0].name.split(".")
			fileName = temp[0];
		}
		cycle=reader.result;
	}
	else
	{
		//var selected = document.getElementById("cycleType"+typeVehicule).selectedIndex;
		var selected = 1;
		alert("test Cycle 1");
		if (selected == 0)
			{
			alert("Veuillez selectionner un cycle dans le menu deroulant");
			return;
			}
		cycle = gatheredDataConso[selected - 1][1];	
	}
	
	**/
	
	if(enLigne && jsonResult != undefined){
		var JSONProfile = jsonResult;
	}
	else {
		var JSONProfile = JSON.parse(textFileProfile); 
	}
	
	
	//console.log(JSONProfile);
	
	//nbPoint 	= 1/3*cycle.match(/;/g).length;	// Compte le nombre de ';' dans le fichier txt et donc le nombre de points du cycle de conduite
	nbPoint=JSONProfile.vitesse.length;
	//console.log(nbPoint);
	
	creaTableauCycle(tabCycle,nbPoint);
	
	
	var valuestart =JSONProfile.heure[0];
	var timeStart = new Date("01/01/2007 " + valuestart).getTime();
	for(var i =0;i<nbPoint;i++){
		

		
			
		var valuestop = JSONProfile.heure[i];
		var timeEnd = new Date("01/01/2007 " + valuestop).getTime();

		var timeDiff = (timeEnd - timeStart)/1000; 
		tabCycle[i+1][0]	= timeDiff;	
		
											// Temps
																		// (s)
		tabCycle[i+1][1]	= parseFloat(JSONProfile.vitesse[i]);		// Vitesse
																		// (km/h)
		tabCycle[i+1][2]	= parseFloat(JSONProfile.vitesse[i])/3.6; 	 	// Vitesse
																		// (m/s)
		if(i==0){
			if(memory==2 && lastAltitude !=0){
				var vitesse=tabCycle[i+1][2];
				if(vitesse == 0){tabCycle[i+1][5]=0}  // on evite les pente à l'infini si v=0
				tabCycle[i+1][5]	= (parseFloat(JSONProfile.altitude[i+1])-lastAltitude)/(tabCycle[i+1][2]*(tabCycle[i+1][0]-tabCycle[i][0]));   //on récupère la dernière altitude récupérée
			} else{
				tabCycle[i+1][5] = 0;				//on fixe le premier point de la pente à 0 si c'est la première portion du trajet
			}
			
		}
		else {
			var vitesse=tabCycle[i+1][2];
			if(vitesse == 0){tabCycle[i+1][5]=0}  // on evite les pente à l'infini si v=0
			else{
				tabCycle[i+1][5]	= (parseFloat(JSONProfile.altitude[i])-parseFloat(JSONProfile.altitude[i-1]))/(tabCycle[i+1][2]*(tabCycle[i+1][0]-tabCycle[i][0]));
			}
				// pente : (différence altitude)/(vitesse*pas de temps)  
				// (%)
			
		}
		if(tabCycle[i+1][5]>0.4){			//sécurité si la pente dépasse 0.4 en fonction de la précision des points GPS
			tabCycle[i+1][5]=0.4;		//valeur 0.4 à changer
		}
		else if(tabCycle[i+1][5]<-0.4){			//sécurité si la pente dépasse 0.4 en fonction de la précision des points GPS
			tabCycle[i+1][5]=-0.4;		//valeur -0.4 à changer
		}
		
	}
	lastAltitude=parseFloat(JSONProfile.altitude[JSONProfile.altitude.length-1]);

	
	
	/**
	var iter 	= 1;	// iterateur pour le parcours des donnees lues
	var iterTab = 0;	// iterateur pour le parcours du tableau a remplir
	var ttampon;
	while(iterTab < nbPoint){
		// Lecture
		// du
		// temps
		var lu 	= lectureNb(iter,cycle)
		var t 	= lu.nb;
		iter 	= lu.iter1 + 2;
		if(t == ttampon){ // Securite
							// car
							// il
							// peut
							// y
							// avoir
							// des
							// fichiers
							// avec
							// plusieurs
							// point
							// au
							// meme
							// temps
			t +=0.002
		}
		ttampon=t;	
		// Lecture
		// de
		// la
		// vitesse
		var lu 	= lectureNb(iter,cycle)
		var v 	= lu.nb;
		iter 	= lu.iter1 + 2;
		// Lecture
		// de
		// la
		// pente
		var lu	= lectureNb(iter,cycle)
		var p	= lu.nb;
		iter	= lu.iter1+2;
		
		// Affectation
		// dans
		// le
		// tableau
		tabCycle[iterTab+1][0]	= t;		// Temps
											// (s)
		tabCycle[iterTab+1][1]	= v;		// Vitesse
											// (km/h)
		tabCycle[iterTab+1][2]	= v/3.6 	// Vitesse
											// (m/s)
		tabCycle[iterTab+1][5]	= p;		// pente
											// (%)
		iterTab += 1;
	}	
	
	**/
	return nbPoint;
}

/**
 * function lectureCycle(nomCycle,tabCycle,bool,readFromFile){ var cycle; var nbPoint;
 * 
 * if (readFromFile) { var fileInput = document.querySelector(nomCycle); var reader = new FileReader(); reader.readAsText(fileInput.files[0]); reader.addEventListener('load', function() { cycle=reader.result //alert(cycle); });
 * 
 * alert("Le fichier " + fileInput.files[0].name +" est associe au parcours " + nomCycle) if(bool){ var temp = fileInput.files[0].name.split(".") fileName = temp[0]; } cycle=reader.result; } else { var selected = document.getElementById("cycleType"+typeVehicule).selectedIndex; if (selected == 0) { alert("Veuillez selectionner un cycle dans le menu deroulant"); return; } cycle =
 * gatheredDataConso[selected - 1][1]; }
 * 
 * 
 * nbPoint = 1/3*cycle.match(/;/g).length; // Compte le nombre de ';' dans le fichier txt et donc le nombre de points du cycle de conduite
 * 
 * 
 * creaTableauCycle(tabCycle,nbPoint);
 * 
 * 
 * var iter = 1; // iterateur pour le parcours des donnees lues var iterTab = 0; // iterateur pour le parcours du tableau a remplir var ttampon; while(iterTab < nbPoint){ // Lecture du temps var lu = lectureNb(iter,cycle) var t = lu.nb; iter = lu.iter1 + 2; if(t == ttampon){ // Securite car il peut y avoir des fichiers avec plusieurs point au meme instant t +=0.002 } ttampon=t; //Lecture de la
 * vitesse var lu = lectureNb(iter,cycle) var v = lu.nb; iter = lu.iter1 + 2; // Lecture de la pente var lu = lectureNb(iter,cycle) var p = lu.nb; iter = lu.iter1+2; // Affectation dans le tableau tabCycle[iterTab+1][0] = t; // Temps (s) tabCycle[iterTab+1][1] = v; // Vitesse (km/h) tabCycle[iterTab+1][2] = v/3.6 // Vitesse (m/s) tabCycle[iterTab+1][5] = p; // pente (%) iterTab += 1; } return
 * nbPoint; }
 */







var JSONProfile;
var textFileProfile;
textFileProfile="";
var ready2=false;
function openFileProfile(event) {
	// Check for the various File API support
	if (window.File && window.FileReader) {
		// L'API FileReader est prise en charge
	} else {
		alert('Les FileReader APIs ne sont pas totalement prises en charge par ce navigateur');
		return;
	}
	

	
	
	if(enLigne == true){
		  downloadHttpFile();
		  var check = function() {
		    if (ready2 === true) {
		    	ready2 = false;
		    	
		        return;
		    }
		    setTimeout(check, 200);
		   }
		   check();
	}
	else {
		var input = event.target;
		// on met en place la fonction lectureGPS dans une sous fonction check afin de ne pas avoir de soucis 
		// avec le caractère asynchrone de reader.onlaod
	   var check = function() {
	    if (ready2 === true) {
	    	ready2 = false;
		    //var JSONProfile = JSON.parse(textFileProfile);    //On transforme le string en variable JSON 
	    	
	    	
	        return;
	    }
	    setTimeout(check, 200);
	   }
	   check();

	    var reader = new FileReader();
	    reader.onload = function(e){
	      textFileProfile = reader.result;			// on récupère tout le texte présent dans le fichier sous la forme d'un seul string
	      ready2 = true;
	      
	    };
	    reader.readAsText(input.files[0]);
  };
	}
	
	
//Test sur le lancement des requêtes avec le server

  
/*  function readTextFile(file)
  {
      var rawFile = new XMLHttpRequest();
      rawFile.open("GET", file, true);
      
      //rawFile.setRequestHeader("Access-Control-Allow-Origin", "*")
      rawFile.setRequestHeader("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS")
      //rawFile.setRequestHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
      rawFile.setRequestHeader('Access-Control-Allow-Headers', '*');
      rawFile.setRequestHeader('Content-type', 'application/json');
      rawFile.setRequestHeader('Access-Control-Allow-Origin', '*');
      rawFile.onreadystatechange = function ()
      {
          if(rawFile.readyState === 4)
          {
              if(rawFile.status === 200 || rawFile.status == 0)
              {
                  var allText = rawFile.responseText;
                  console.log(allText);
              }
          }
      }
      rawFile.send(null);
  }*/
  

  
  
  /*
   * function downloadHttpFile()
   * 
   * Cette fonction permet de dialoguer, de lire des fichiers avec un répétoire local dans le quel se situe
   * le fichiers jsonUpdated.json .
   * il faut auparavant lancer le répertoire local dans le script HttpServer.py puis en fonction de la situation, utiliser le script JsonUpdated.py ou un muxlink
   */
  var jsonResult;
  function downloadHttpFile() {
		$(document).ready(function () {
		    $.getJSON("http://127.0.0.1:8000/jsonUpdated.json", function (result) {			//on récupère le fichier en écoutant à l'adresse 127.0.0.1 et sur le port 8000
		        try
		        {
		        	jsonResult = result;   //On récupère l'information dans result
		            
		            console.log(result.heure);
		        	ready = true;
		            ready2 = true;
		            textfileProfile = jsonResult;		//on met l'information où elle a besoin d'être mise
		            JSONGPS = jsonResult;
		        }
		        catch (err) {
		            console.log(err.message);
		        }
		    });
		});
		}

  
  
/**
 * Lecture d'un fichier GPs fournis pas SATAN. Non optimiser et à modifier pour être plus générique
 * 
 * 
 * 
 * @param event
 * 		récupère l'instance de l'évènement en cour
 */
var arrayHeureGPS = new Array();
var tempArrayHeure = new Array();
var paramFileGPSLoaded = false;
var dataGPS= new Array();
var textFileGPS="";
var JSONGPS;
var boolGPS= false;
var ready =false;
function openFileGPS(event) {
	// Check for the various File API support
	boolGPS=true;
	if (window.File && window.FileReader) {
		// L'API FileReader est prise en charge
	} else {
		alert('Les FileReader APIs ne sont pas totalement prises en charge par ce navigateur');
		return;
	}
	
	if( enLigne == true){
		downloadHttpFile();
		var check = function() {
	    if (ready === true) {
	    	ready = false;
	    	
		    
	         
	  		dataGPS = lectureGPS(JSONGPS);
	  		paramFileGPSLoaded = true;
	         return;
	    }
	    setTimeout(check, 200);
		}
	    check();
	}
	else {
		var input = event.target;
		// on met en place la fonction lectureGPS dans une sous fonction check afin de ne pas avoir de soucis 
		// avec le caractère asynchrone de reader.onlaod
		   var check = function() {
		    if (ready === true) {
		    	ready = false;
		    	
		    	var JSONGPS = JSON.parse(textFileGPS);    //On transforme le string en variable JSON 
		    	
			    
		         
		  		dataGPS = lectureGPS(JSONGPS);
		  		paramFileGPSLoaded = true;
		         return;
		    }
		    setTimeout(check, 200);
		}
		    check();

		    var reader = new FileReader();
		    reader.onload = function(){
		      textFileGPS = reader.result;			// on récupère tout le texte présent dans le fichier sous la forme d'un seul string
		      ready = true;
		      
		    };
		    reader.readAsText(input.files[0]);
	  };
	}

	
	

  /**
   * Traitement du fichier GPS fournis pas SATAN. On récupère les éléments dont on a besoin (latitudes, longitudes)
   * Puis on initialise les requêtes HTTP. La variable nbPointsInter indique combien de points on désire sur le trajet. 
   * On peut la mêttre égale à la taille du tableau tempArrayLat pour avoir les vitesses limites sur tout le trajet.
   * 
   * On relève et met en place en même temps les waypoints que l'on désire afficher sur le trajet. Attention il y a une limite
   * de 23 waypoint sur un trajet. 
   * Il faudras dans le futur sortir 'addWaypoint' de la boucle for pour les afficher et calculer toute les vitesses
   * 
   * 
   * ----*****************************-**********************************************--.
   * 
   * @param JSONGPS
   * 		on récupère le ficher JSON contenant les coordonnées GPS
   */
  window.tabvlimAugmente= [];
  function lectureGPS(JSONGPS){
			
			var iterString = 0; // iterateur// sur le string ci dessus		
			var textString =""; // texte	// pour// les// options// a	// ajouter// dans// le// select
			waypoints = [];	
			//var vlim;
			// Check for the various File API support
			if (window.File && window.FileReader) {
				// L'API FileReader est prise en charge
			} else {
				alert('Les FileReader APIs ne sont pas totalement prises en charge par ce navigateur');
				return;
			}

			
		
			var tempArrayTest = new Array();		// on créer les tableau content les coords GPS
			var tempArrayLat = new Array();
			var tempArrayLng = new Array();
			
			for(var i = 0; i<JSONGPS.latitude.length-1;i++){
				tempArrayLat.push(JSONGPS.latitude[i]);
				tempArrayLng.push(JSONGPS.longitude[i]);
			}
			arrayHeureGPS = [];
			tempArrayHeure = [];
			
			for(var i = 0; i<JSONGPS.heure.length-1;i++){
				tempArrayHeure.push(JSONGPS.heure[i]);
			}
			
			//on initialise le départ et l'arrivée du trajet . Nécéssaire pour la requ^te Google
			departure = new google.maps.LatLng(tempArrayLat[0], tempArrayLng[0]);
			arrival = new google.maps.LatLng(tempArrayLat[tempArrayLat.length-1], tempArrayLng[tempArrayLng.length-1]);

			
			console.log("size tempArrayLat =" +tempArrayLng.length);
			var temp="";													// contient le string contenant la rqt en envoyer
			var tempList=[]; 												// contient la liste des rqt à envoyer
			temp= "http://overpass-api.de/api/interpreter?data=[out:json];"; //début obligatoire de la requête HTTP vers OverPass
			var nbPointInter=tempArrayLat.length;
			//var nbPointInter=227;
			 var pas = Math.round(tempArrayLat.length/nbPointInter);
	        var t1 = new Date().getTime();
	        var rqtLim=0;
			for(var k=0 ; k<=nbPointInter-1;k=k+13){
				var iter = k*pas;
				
				
				if(rqtLim == 40){				// la limite de point en une requête GPS etant 100 points, on découpe en requêtes de 100 points max  UPDATE : 100 points engendre des erreur de requêtes. decouper en 50 points max elimine les erreurs sans affecter le temps de calcul
                  tempList.push(temp);
                  temp= "http://overpass-api.de/api/interpreter?data=[out:json][timeout:50];";
                  rqtLim=0;
				}
				
			    //addWaypoint(tempArrayLat[iter],tempArrayLng[iter]);			//commande pour ajouter les waypoints, a mofifier plus tard

			    
			    
				arrayHeureGPS.push(tempArrayHeure[iter]);
			    temp = temp+ "way[maxspeed](around:55,"+tempArrayLat[iter]+","+tempArrayLng[iter]+");out+count;out+tags;"
			    rqtLim= rqtLim+1;
			   
	         }
	         tempList.push(temp);
	        
	         if(!enLigne){
	        	//rqtHTTPOSM(tempList);	 
	         }
	         //rqtHTTPOSM(tempList);										// a enlever.ajouter pour avoir les vitesses limites 
			
			
			var nbPointWaypoint = 10;
			var pas2 = Math.round(tempArrayLat.length/nbPointWaypoint);
			for(var k=1 ; k<nbPointWaypoint;k++){
				var iter = k*pas2
				addWaypoint(tempArrayLat[iter],tempArrayLng[iter]);			//commande pour ajouter les waypoints, a mofifier plus tard
			}	
			//console.log(departure,arrival);
			//console.log(waypoints);
						
			var wait = function() {											//On vérifie que toute les requ^tes sont bien terminées cela cause des soucis à cause du caractère asynchrone des rqt
																			// En effet le code n'est pas effectué séquentiellement mais en même temps
			    if (httpvlim === true) {
				    console.log("stop waiting");
				    httpvlim=false;
				    //console.log(tabvlim);
				    var t2 = new Date().getTime();
				    console.log("temps de traitement en m",(t2-t1)/60000);
				    
				    var nbErreur=0;
				    tabvlimAugmente= [];
				    
				    if(tabvlim.length == arrayHeureGPS.length){
				    	tabvlim[0]=50;
				    	for(var i=1;i<tabvlim.length;i++){				//On modifie les valeurs dans dataVlim car elle sont en string et il est plus facile de les manipluer en float
				    		tabvlim[i]=parseFloat(tabvlim[i]);
				        	if(tabvlim[i]==0){							// on retire les vitesses limites 0 car elles correspondent à un résulats nul de la requête overpass
				        		tabvlim[i]=tabvlim[i-1];				//on remplce ce zéro par la vlim précédente
				        		nbErreur+=1;
				        	}
				        }
				    	console.log("nombre de requêtes qui retourne 0 dans le trajet :", nbErreur);
				        for(var i=0;i<tabvlim.length-1;i++){								//ici, on filtre 'grossièrement' les valeurs pour éviter les pics ou erreurs trop excessvives. Par exemple les valeurs retournant 50 sur une portion d'autoroute.
				        	if(Math.abs(tabvlim[i-1]-tabvlim[i]) >29 ){					// il serait négatif d'enlever toutes les valeurs car il peut y avoir des changement brusques de vitesse légitime (ex: passage de 130 à 70 en sortie d'autoroute)
				        		tabvlim[i]=(tabvlim[i-1]+tabvlim[i+1])/2;				// il ne faut pas perdre à l'esprit qu'on point un point toutes les x (ici 13) secondes
				        	}   
				        }
				        
				    	
				    	var indexVlimReduit=0;
				    	
				    	var index = 1;
				    	
				        
				        for(var indexVlimAugmente = 0; indexVlimAugmente<=tempArrayLng.length-1;indexVlimAugmente++){								//on ajoute ici les valeurs aux indices correspondant.
																								// comme on a prit un point sur 13 pour les vitesse limites, on comble les trous par rapport aux vraies vitesses avec la vitesse limites précédantes. 
							if(index==14){
								
							indexVlimReduit++;
								index=0;
							}
							tabvlimAugmente.push(tabvlim[indexVlimReduit]);
							index++;
						}
				        
				        
				        console.log(tabvlim);
				        console.log(tabvlimAugmente);
				        
				    	alert("Requêtes Overpass terminées");
				    }else{
				    	alert("Une erreur est survenue dans les requêtes Overpass \n  Merci de recommencer");
				    }
				    
				    //console.log(tabvlim.length, arrayHeureGPS.length);
			         return;
			    }
			    setTimeout(wait, 3000);
			}
			wait();
			 
			
			
			
			

			calcRoute();
			waypoints=[];
			var t2 = new Date().getTime();
			
}
  


  /**
   * Initialise la lecture de données via un fichier JSON	  inutilisé/incomplètre actuellement			A MODIFIER
   * 
   * 
   */
var  textFile="";
var openFile = function(event) {
	// Check for the various File API support
	if (window.File && window.FileReader) {
		// L'API FileReader est prise en charge
	} else {
		alert('Les FileReader APIs ne sont pas totalement prises en charge par ce navigateur');
		return;
	}
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
      textFile = reader.result;
      //var node = document.getElementById('output');
      //node.innerText = text;
      //console.log(reader.result.substring(0, 200));
  		var dataObd = new Array();
  		dataObd[0] = new Array();
  		dataObd[0][1] = 1;
  		dataObd.splice(0,dataObd.length);
  		dataObd = lectureParam("#donneesObd"+typeVehicule, dataObd,nbParamObd,"Obd");
  		paramFileObdLoaded = true;
    };
    reader.readAsText(input.files[0]);
  };


/**
 * Lecture des donnees du fichier de parametres et affectation dans le tableau de donnees correspondant
 * 
 * @param nomFichier :
 *            nom du fichier de parametres a lire
 * @param tabData :
 *            tableau de donnee ou doivent être copie les parametres
 * @param nbParam :
 *            nombre de parametres dans le tableau
 * @param typeTableau :
 *            chaine de caractere indiquant comment le tableau doit être rempli
 * @returns nbLignesParamVehT : var globale, nombre de types de vehicules differents aka nombre de lignes du fichier
 */
/*function lectureParam(nomFichier,tabData,nbParam,typeTableau){
	//var file = this.event.srcElement.files[0];
	var params =""; // le fichier de parammètres sera rentre dans cette string params
	var iterString = 0; // iterateur// sur le string ci dessus		
	var textString =""; // texte	// pour// les// options// a	// ajouter// dans// le// select
			
	// Check for the various File API support
	if (window.File && window.FileReader) {
		// L'API FileReader est prise en charge
	} else {
		alert('Les FileReader APIs ne sont pas totalement prises en charge par ce navigateur');
		return;
	}
	*//**
	var fileInput = document.querySelector(nomFichier); // manipulation fichier
														
	var reader = new FileReader(); // lecture fichier
							

	// controle présence d'un ficher de paramètres
	if($(nomFichier).val() == ''){
		alert("Aucun fichier detecte");
		return;
	}
	var ParamFileName = fileInput.files[0].name; // nom rentre dans la variable locale								
	// lecture des données et stockage dans le string params
	reader.onload = function(event) {
		params = event.target.result;  
	};
	reader.readAsText(fileInput.files[0]);
	alert("Fichier de parametres utilises : " + ParamFileName);
	
	**//*
	
	
	//params = readFile(file);
	//console.log("data1 = " + data1);
	
	params=textFile;
	console.log("params" + params.substring(0, 200));
	
	
	// compte du nombre de lignes en fonction du nombre de paramètre par ligne
	 if(typeTableau=="Reelles"){ 
		 nbLignes = params.match(/\n/g).length;
		 //nbLignes = (params.match(/\n/g).length)/(2*nbParam);
	}
	else if(typeTableau!="Obd"){
		nbLignes = 1/nbParam*params.match(/;/g).length;
	}
	else{
		nbLignes = 1/(nbParams+4)*params.match(/\n/g).length;
	}
	console.log(nbLignes);
	switch(typeTableau){
	case "VehE" : nbLignesParamVehE = nbLignes; break;
	case "VehT" : nbLignesParamVehT = nbLignes; break;
	case "Obd"	: nbLignesParamObd = nbLignes; break;
	case "Couple"	: nbLignesParamCouple = nbLignes; break;
	case "ConseilsVE"	: nbLignesParamConseilsVE = nbLignes; break;
	case "Reelles"	: nbLignesParamReelles = nbLignes; break;
	}

	
																				*//**  Partie inférieur non focntionnel pour l"instant
																				 * 	l'implémentation des données n'est pas le plus urgent
																				 * 
																				 * suite des travaux sur cette fonction : 
																				 * 	Adapter ou refaire le remplissage de tabData pour chacun
																				 * des cas de tableau.
																				 * 
																				 * Trouver la différence entre le tableau réel et obd
																				 * voir si il y a une suite pour les données réelles.
																				 * 
																				 * 
																				 * La difficulté réside dans les formats des tableau qui sont 
																				 *//*
	
	
	
	
	// split du string en params en fonction des ';'
	var tempArray = new Array();
	params = params.split(';');

	// creation du tableau de données en fonction du nombre de lignes

	if(typeTableau!="Obd" && typeTableau!="Reelles"){	
		creaTableauParam(tabData, nbLignes,typeTableau);
		while(params[0]) {
			tempArray.push(params.splice(0,1));
		}
	}
	else if(typeTableau=="Obd"){
		creaTableauParam(tabData, nbLignes,typeTableau);
		while(params[0]) {
			tempArray.push(params.splice(0,1));
			tempArray.push(params.splice(0,1));
			params.splice(0,1);
			tempArray.push(params.splice(0,1));
			params.splice(0,1);
			tempArray.push(params.splice(0,1));
			params.splice(0,1);
			tempArray.push(params.splice(0,1));
			params.splice(0,1);
			tempArray.push(params.splice(0,1));
		}
	}
	else if(typeTableau=="Reelles"){ 
		var TempsParcours = 0;  // Compteur
								// de
								// temps
								// remis
								// à 0
		creaTableauParam(tabData, nbLignes,typeTableau);
		for(var ligne = 0; ligne<nbLignes;ligne++){
			if(ligne==0){
				TempsParcours = 0;
			}
			else{
				var T1 = params[2*nbParamReelles*(ligne-1)];   //params[ligne] ?
				var T2 = params[2*nbParamReelles*ligne];
				T1 = T1.split(':');
				T2 = T2.split(':');
				var date1 = T1[0]*3600+T1[1]*60+T1[2]*1;			//séparation au niveau de la date
				var date2 = T2[0]*3600+T2[1]*60+T2[2]*1;
				TempsParcours = TempsParcours + date2-date1;
			}
			tabData[ligne+1][0] = TempsParcours;
			
			for (var colonne = 1; colonne<=nbParamReelles; colonne ++){
				tabData[ligne+1][colonne] = params[(2*nbParamReelles)*ligne+(2*colonne-1)];
			}
		}
	}

	if(typeTableau == "VehE" || typeTableau =="VehT"){
		for(var i = 1; i < tabData.length; i++) {
			for(var j = 0; j < nbParam; j++) {
				tabData[i][j] = tempArray[(i - 1) * nbParam + j].toString().trim();       // trim enlève les blanks space au début et a la fin des strings
			}
		}
		// apres chargement du fichier, ajout des options correspondantes dans le menu deroulant
		for (var i = 1; i < tabData.length; i++) {
			var option = document.createElement("option");
			textString = tabData[i][0].toString().charAt(0).toUpperCase() + tabData[i][0].toString().substring(1).toLowerCase();
			option.text = textString;
			option.value = i;
			document.getElementById("carType"+typeVehicule).add(option);
		}
	}
	else{
		if(typeTableau!="Obd" && typeTableau!="Reelles"){
			if(typeTableau!="ConseilsVE"){
				for(var i = 1; i < tabData.length; i++) {
					for(var j = 0; j < nbParam; j++) {
						tabData[i][j] = parseFloat(tempArray[(i - 1) * nbParam + j]); 
					}
				}
				tabData = reversetab(tabData,tabData.length,nbParam);
			}
			else{
				for(var i = 1; i < tabData.length; i++) {
					for(var j = 0; j < nbParam; j++) {
						tabData[i][j] = parseFloat(tempArray[(i - 1) * nbParam + j]); 
					}
				}
				tabData = reversetab(tabData,tabData.length,nbParamConseilsVETotal);
			}
		}
		else{
			console.log(tempArray+"// " + nbParam);
			console.log(tabData);
			if(typeTableau=="Obd" && typeTableau!="Reelles"){
				for(var i = 1; i < tabData.length; i++) {
					tabData[i][0] = parseFloat(tempArray[(i - 1) * nbParam].toString().trim().substring(6)); 
					for(var j = 1; j < nbParam; j++) {
						tabData[i][j] = parseFloat(tempArray[(i - 1) * nbParam + j]); 
					}
				}
				tabData = reversetab(tabData,tabData.length,nbParamObdTotal);
			}
		}
		// stockage dans le tabData, tableau bidimensionnel 
		// En i = 0, on trouve le nom du type de données stokée
	}
		return tabData;
}*/
/**
 * Lecture d'un nombre du cycle de conduite a partir du caractere 'depart'
 * 
 * @param depart :
 *            numero caractere de depart de la lecture
 * @returns nb : nombre lu
 * @return iter1 : numero du caractere final
 */
function lectureNb(depart,file){
    var nombreEntier=file[depart-1];
    var nombreDec=0;
    var k = 1;
    var entier = true;
    var iter1 = depart;
    
    // lecture
	// separee
	// de
	// la
	// partie
	// entiere
	// et
	// de
	// la
	// partie
	// decimal
	// du
	// nombre
    while(file[iter1] != ";"){		// Arret
									// de
									// la
									// lecture
									// a la
									// lettre
									// ';'
    	var temp = file[iter1];
    	if(temp == "-"){
    		k=-1;
    	}
    	if(temp!="." && entier){
    		nombreEntier = nombreEntier + temp;
    	}
    	if(temp!="." && !entier){
    		nombreDec = nombreDec +temp;
    	}
    	if(temp == "."){
    		entier = false;
    	}
    	iter1+=1;
    }
    var l=nombreDec.length-2;
    nombreDec = parseInt(nombreDec);
    nombreEntier = parseInt(nombreEntier);
    var nombre = (nombreEntier + nombreDec*Math.pow(10,(-l)))*k;
    return{
    	nb: nombre,
    	iter1: iter1,
    }
}
/**
 * Lecture d'un nom du fichier a partir du caractere 'depart'
 * 
 * @param depart :
 *            numero caractere de depart de la lecture
 * @returns texte: texte lu
 * @return iter1 : numero du caractere final
 */
function lectureTexte(depart,file){
    var texte = '';
    var iter1 = depart;
    while(file[iter1] != ";"){		// Arret
									// de
									// la
									// lecture
									// a la
									// lettre
									// ';'
    	texte += file[iter1]
    	iter1+=1;
    }
    return{
    	texte: texte,
    	iter1: iter1,
    }
}

/**
 * Pour remplir gatheredDataConso avec les donnees du fichier de cycles
 * 
 * @param nomFichier
 * @returns tabData
 * @returns nbLignesCycle : nombre de cycles differents
 */

function lectureCycles(nomFichier) {
	var cycles =""; // le
					// fichier
					// de
					// cycles
					// sera
					// rentre
					// dans
					// cette
					// string
					// params
	var iterString = 0; // iterateur
						// sur
						// le
						// string
						// ci-dessus
    var textString =""; // texte
						// pour
						// les
						// options
						// a
						// ajouter
						// dans
						// le
						// select
    
 // Check
	// for
	// the
	// various
	// File
	// API
	// support.
    if (window.File && window.FileReader) {
      // L'API
		// FileReader
		// est
		// prise
		// en
		// charge.
    } else {
      alert('Les FileReader APIs ne sont pas totalement prises en charge par ce navigateur');
      return;
    }
    var fileInput = document.querySelector(nomFichier); // manipulation
														// fichier
	var reader = new FileReader(); // lecture
									// fichier

    // controle
	// presence
	// d'un
	// fichier
	// de
	// cycles
    if($(nomFichier).val() == ''){
    	alert("Aucun fichier detecte pour les cycles de conduite");
    	return;
    }
	cyclesFileName = fileInput.files[0].name; // nom
												// rentre
												// dans
												// la
												// variable
												// globale
												// cyclesFileName

	// lecture
	// des
	// donnees
	// et
	// stockage
	// dans
	// la
	// string
	// cycles
	reader.onload = function(event) {
		cycles = event.target.result;  
		};
	reader.readAsText(fileInput.files[0]);
    alert("Fichier de cycles utilises : " + cyclesFileName);
    
    if (null == cycles.match(/\$/g))
    	{
    	cyclesFileSingle = true;
    	document.getElementById("cycleType"+typeVehicule).style.visibility = 'hidden';
    	return 0;
    	}
    cyclesFileSingle = false;
    document.getElementById("cycleType"+typeVehicule).style.visibility = 'visible';
    
    // compte
	// du
	// nombre
	// de
	// cycles,
	// en
	// fonction
	// du
	// nombres
	// de
	// parametres
	// par
	// ligne
    nbLignesCycles 	= cycles.match(/\$/g).length;

    // creation
	// du
	// tableau
	// de
	// donnees
	// en
	// fonction
	// du
	// nombre
	// de
	// cycles
    creaTableauCycleRegroupes(gatheredDataConso, nbLignesCycles);

    // split
	// du
	// string
	// params
	// en
	// fonction
	// du
	// '$'.
    var tempArray = new Array();

    tempArray = cycles.split(/\$/);

	// le
	// fichier
	// de
	// cycles
	// doit
	// finir
	// par
	// un $
    for(var i = 0; i < tempArray.length - 1; i++) {
    	tempArray[i] = tempArray[i] +";";
    	gatheredDataConso[i][1] = tempArray[i];
    }

    // apres
	// chargement
	// du
	// fichier,
	// ajout
	// des
	// options
	// correspondantes
	// dans
	// le
	// menu
	// deroulant
    for (var j = 0; j < gatheredDataConso.length; j++) {
	    var option = document.createElement("option");
	    textString = gatheredDataConso[j][0].toString();
	    option.text = textString;
	    option.value = j;
	    document.getElementById("cycleType"+typeVehicule).add(option);
    }	
    return nbLignesCycles;
}
/**
 * function lectureCycles(nomFichier) { var cycles =""; //le fichier de cycles sera rentre dans cette string params var iterString = 0; //iterateur sur le string ci-dessus var textString =""; // texte pour les options a ajouter dans le select // Check for the various File API support. if (window.File && window.FileReader) { // L'API FileReader est prise en charge. } else { alert('Les FileReader
 * APIs ne sont pas totalement prises en charge par ce navigateur'); return; } var fileInput = document.querySelector(nomFichier); // manipulation fichier var reader = new FileReader(); // lecture fichier // controle presence d'un fichier de cycles if($(nomFichier).val() == ''){ alert("Aucun fichier detecte pour les cycles de conduite"); return; } cyclesFileName = fileInput.files[0].name; // nom
 * rentre dans la variable globale cyclesFileName // lecture des donnees et stockage dans la string cycles reader.onload = function(event) { cycles = event.target.result; }; reader.readAsText(fileInput.files[0]); alert("Fichier de cycles utilises : " + cyclesFileName);
 * 
 * if (null == cycles.match(/\$/g)) { cyclesFileSingle = true; document.getElementById("cycleType"+typeVehicule).style.visibility = 'hidden'; return 0; } cyclesFileSingle = false; document.getElementById("cycleType"+typeVehicule).style.visibility = 'visible'; // compte du nombre de cycles, en fonction du nombres de parametres par ligne nbLignesCycles = cycles.match(/\$/g).length;
 * 
 * //creation du tableau de donnees en fonction du nombre de cycles creaTableauCycleRegroupes(gatheredDataConso, nbLignesCycles);
 * 
 * //split du string params en fonction du '$'. var tempArray = new Array();
 * 
 * tempArray = cycles.split(/\$/); // le fichier de cycles doit finir par un $ for(var i = 0; i < tempArray.length - 1; i++) { tempArray[i] = tempArray[i] +";"; gatheredDataConso[i][1] = tempArray[i]; }
 * 
 * //apres chargement du fichier, ajout des options correspondantes dans le menu deroulant for (var j = 0; j < gatheredDataConso.length; j++) { var option = document.createElement("option"); textString = gatheredDataConso[j][0].toString(); option.text = textString; option.value = j; document.getElementById("cycleType"+typeVehicule).add(option); } return nbLignesCycles; }
 */

// ----------------------------------------------------------------------------------
// Creation
// des
// tableaux
// de
// donnees
// -------------------------------------------------------------
/**
 * Creation du tableau de donnees des etapes lors du trace du trajet GoogleMaps
 */
function creaTabStep(){
	dataStep[0] = new Array();
	dataStep[0][0] = ('Distances (m)');
	dataStep[0][1] = ('Duree (s)');
	dataStep[0][2] = ('Vitesse (m/s)');
	dataStep[0][3] = ('Vitesse (km/h)');
	dataStep[0][4] = ('Type de parcours');
	dataStep[0][5] = ('Distance cumulee (m)')
	dataStep[0][6] = ("Numero de l'etape")
}
/**
 * Creation de la premiere ligne d'un tableau, destine a la creation du tableau du cycle unitaire creation des lignes du tableau
 * 
 * @param tab :
 *            tableau a creer
 */
function creaTableauCycle(tab,nbPoint){

	for (var i 	= 0; i<nbPoint+1 ;i++){
		tab[i] 	= new Array();
	}	
	tab[0][0]	= ("Temps (s)");
	tab[0][1]	= ("Vitesse (km/h)");
	tab[0][2]	= ("Vitesse (m/s)");
	tab[0][3]	= ("Distance cumulee (m)");
	tab[0][4]	= ("Duree entre t et t+1 (s)");
	tab[0][5]	= ("Pente (%)");

}
/**
 * Creation de la premiere ligne d'un tableau contenant le nom et l'unite de chaque colonne la premiere ligne n'est utilisee que pour l'affichage des donnees
 * 
 * @param tab :
 *            tableau a creer
 */
function creaTableau(tab){
	
	if(typeVehicule == "VE" || typeVehicule == "VP" || typeVehicule == "VPU"){
		// Ajout
		tab[0] 		= new Array();
		tab[0][0]	= ("Temps (s)");
		tab[0][1]	= ("Vitesse (km/h)");
		tab[0][2]	= ("Vitesse (m/s)");
		tab[0][3]	= ("Distance cumulee (m)");
		tab[0][4]	= ("Pente");
		tab[0][5]	= ("Duree entre t et t+1 (s)");
		tab[0][6]	= ("Acceleration (m/s2)");
		tab[0][7]	= ("Phase");
		tab[0][8]	= ("Resistance au roulement en debut de phase(N)");
		tab[0][9]	= ("Resistance de l'air en debut de phase (N)");
		tab[0][10]	= ("Resistance de la pente en debut de phase (N)");
		tab[0][11]	= ("Force d'acceleration en debut de phase(N)");
		tab[0][12]	= ("Force Motrice en debut de phase(N)");
		tab[0][13]	= ("Puissance mecanique en debut de phase (W)");
		tab[0][14]	= ("Puissance electrique en debut de phase (W)");
		tab[0][15]	= ("Couple aux roues en debut de phase (N.m)");
		tab[0][16]	= ("Resistance au roulement en fin de phase(N)");
		tab[0][17]	= ("Resistance de l'air en fin de phase (N)");
		tab[0][18]	= ("Resistance de la pente en fin de phase (N)");
		tab[0][19]	= ("Force d'acceleration en fin de phase(N)");
		tab[0][20]	= ("Force Motrice en fin de phase(N)");
		tab[0][21]	= ("Puissance mecanique en fin de phase (W)");
		tab[0][22]	= ("Puissance electrique en fin de phase (W)");
		tab[0][23]	= ("Couple aux roues en fin de phase (N.m)");
		tab[0][24]	= ("Puissance mecanique maximale de la phase (kW)");
		tab[0][25]	= ("Puissance electrique maximale de la phase (kW)");
		tab[0][26]	= ("Couple aux roues maximal de la phase (N.m)");
		tab[0][27]	= ("Energie mecanique consommee (Wh)");
		tab[0][28]	= ("Energie electrique consommee (Wh)");
		tab[0][29]	= ("Energie totale consommee (Wh)");
		tab[0][30]	= ("Energie totale produite (Wh)");
		tab[0][31]	= ("Type de cycle");
		tab[0][32]	= ('Taux de charge de la batterie');
		tab[0][33]  = ("Latitude du point");
		tab[0][34]  = ("Longitude du point");
		tab[0][35]  = ("Rayon autonomie restante (km)");
		tab[0][36]  = ("Distance cumulee (km)");
		tab[0][37]  = ("Energie totale dissipee (Wh)");
		tab[0][38]  = ("Vitesse limite (km/h)");	
		nbParamDataTable = 39;
	}
	
	else if(typeVehicule=="VT" || typeVehicule=="VH"){
		tab[0] 		= new Array();
		tab[0][0]	= ("Temps (s)");
		tab[0][1]	= ("Vitesse (km/h)");
		tab[0][2]	= ("Vitesse (m/s)");
		tab[0][3]	= ("Distance cumulee (m)");
		tab[0][4]	= ("Pente");
		tab[0][5]	= ("Duree entre t et t+1 (s)");
		tab[0][6]	= ("Acceleration (m/s2)");
		tab[0][7]	= ("Phase");
		tab[0][8]	= ("Resistance au roulement en debut de phase(N)");
		tab[0][9]	= ("Resistance de l'air en debut de phase (N)");
		tab[0][10]	= ("Resistance de la pente en debut de phase (N)");
		tab[0][11]	= ("Force d'acceleration en debut de phase(N)");
		tab[0][12]	= ("Force Motrice en debut de phase(N)");
		tab[0][13]	= ("Puissance mecanique en debut de phase (W)");

		tab[0][15]	= ("Couple aux roues en debut de phase (N.m)");
		tab[0][16]	= ("Resistance au roulement en fin de phase(N)");
		tab[0][17]	= ("Resistance de l'air en fin de phase (N)");
		tab[0][18]	= ("Resistance de la pente en fin de phase (N)");
		tab[0][19]	= ("Force d'acceleration en fin de phase(N)");
		tab[0][20]	= ("Force Motrice en fin de phase(N)");
		tab[0][21]	= ("Puissance mecanique en fin de phase (W)");

		tab[0][23]	= ("Couple aux roues en fin de phase (N.m)");
		tab[0][24]	= ("Puissance mecanique maximale de la phase (kW)");

		tab[0][26]	= ("Couple aux roues maximal de la phase (N.m)");
		tab[0][27]	= ("Energie mecanique requise (Wh)");
		
		tab[0][29]	= ("Energie totale consommee (Wh)");
		tab[0][30]	= ("Energie totale produite (Wh)");
		tab[0][31]	= ("Type de cycle");

		tab[0][33]  = ("Latitude du point");
		tab[0][34]  = ("Longitude du point");

		tab[0][36]  = ("Distance cumulee (km)");
		tab[0][37]  = ("Energie totale dissipee (Wh)");
		
		// calcul
		// consommation
		// pour
		// l'utilisateur
		// :
		// prediction
		tab[0][38]	= ("Vitesse (km/h) ")
		tab[0][39]	= ("Pente (%)")
		tab[0][40]	= ("Rapport de vitesses enclenche")
		tab[0][41]	= ("Valeur du rapport de reduction")
		tab[0][42]	= ("Regime moteur (rpm)")
		tab[0][43]	= ("Couple moteur (N.m)")
		tab[0][44]	= ("Couple max (N.m)")
		tab[0][45]	= ("Charge moteur (%)")
		tab[0][46]	= ("Indice regime moteur")
		tab[0][47]	= ("Indice charge moteur")
		tab[0][48]	= ("Consommation specifique d'energie (g/kWh)")
		tab[0][49]	= ("Consommation de carburant (L)")
		tab[0][50]	= ("Consommation instantanee de carburant (litres/100 km)")
		tab[0][51]	= ("Emissions de Co2 (kg)")
		tab[0][52]	= ("Energie produite (Wh)")
		tab[0][53]	= ("Energie dissipee (Wh)")
		tab[0][54]	= ("Consommation totale de carburant (L)")
		tab[0][55]	= ("Consommation moyenne de carburant (l/100km)")
		tab[0][56]  = ("Emissions totales de Co2 (kg)")
		tab[0][57]  = ("Energie produite totale (Wh)")
		tab[0][58]  = ("Energie dissipee totale (Wh)")
		// Calcul
		// consommation
		// optimale
		// a
		// partir
		// de
		// la
		// prediction
		tab[0][59]  = ("Energie consommee (Wh)")
		tab[0][60]  = ("Vitesse (km/h)");
		tab[0][61]  = ("Couple a la roue (N.m)");
		tab[0][62]  = ("Vitesse de rotation de la roue (rpm)");
		tab[0][63]	= ("Regime moteur optimal(rpm)")
		tab[0][64]	= ("Couple moteur optimal(N.m)")
		tab[0][65]	= ("Charge moteur optimale(%)")
		tab[0][66]  = ("Consommation specifique d'energie optimale(g/kWh)");
		tab[0][67]  = ("Rapport de vitesses enclenche optimal");
		tab[0][68]  = ("Consommation de carburant optimale(L)");
		tab[0][69]  = ("Consommation instantanee de carburant optimale(L/100km)");
		tab[0][70]  = ("Rejets de Co2 optimaux(kg)");
		tab[0][71]	= ("Energie produite optimale(Wh)")
		tab[0][72]	= ("Energie dissipee optimale(Wh)")
		tab[0][73]	= ("Consommation totale de carburant optimale(L)")
		tab[0][74]	= ("Consommation moyenne de carburant optimale(l/100km)")
		tab[0][75]  = ("Emissions totales de Co2 optimaux(kg)")
		tab[0][76]  = ("Energie produite totale optimale(Wh)")
		tab[0][77]  = ("Energie dissipee totale optimale(Wh)")
		// calcul
		// consommation
		// réelle
		tab[0][78]	= ("Rapport de vitesses enclenche réel")
		tab[0][79]	= ("Regime moteur réel(rpm)")
		tab[0][80]	= ("Couple moteur réel(N.m)")
		tab[0][81]	= ("Charge moteur réelle(%)")
		tab[0][82]	= ("Consommation totale de carburant réelle(L)")
		tab[0][83]	= ("Consommation instantanee de carburant réelle(l/100km)")
		tab[0][84]  = ("Emissions totales de Co2 réelles(kg)")
		// Calcul
		// consommation
		// optimale
		// sur
		// les
		// donnees
		// obd
		tab[0][85]  = ("Vitesse réelle(km/h)");
		tab[0][86]  = ("Couple a la roue réel(N.m)");
		tab[0][87]  = ("Vitesse de rotation de la roue réelle(rpm)");
		tab[0][88]	= ("Regime moteur réel optimal (rpm)")
		tab[0][89]	= ("Couple moteur réel optimal(N.m)")
		tab[0][90]	= ("Charge moteur réelle optimale(%)")
		tab[0][91]  = ("Consommation specifique d'energie réelle optimale(g/kWh)");
		tab[0][92]  = ("Rapport de vitesses enclenche réel optimal");
		tab[0][93]  = ("Consommation de carburant réelle optimale(L)");
		tab[0][94]  = ("Consommation instantanee de carburant réelle optimale(L/100km)");
		tab[0][95]  = ("Rejets de Co2 réels optimaux(kg)");
		tab[0][96]	= ("Consommation totale de carburant réelle optimale(L)");
		tab[0][97]	= ("Consommation moyenne de carburant réelle optimale(l/100km)");
		tab[0][98]  = ("Emissions totales de Co2 réelles optimaux(kg)");
		tab[0][99]  = ("Duree phase (s)");
		nbParamDataTable = 100;
	}
}
/**
 * Creation de la premiere ligne d'un tableau, destine a la creation du tableau de donnees Creation des lignes du tableau
 * 
 * @param tab :
 *            tableau a creer
 * @param nbPoint :
 *            nombre de lignes dans le tableau
 * @param typeTableau :
 *            chaine de caractere indiquant comment le tableau doit être rempli
 */
function creaTableauParam(tab,nbPoint,typeTableau){
	for (var i 	= 0; i<nbPoint+1 ;i++){
		tab[i] 	= new Array();
	}	
	switch (typeTableau){
		case "VehE" : 
			// parametres
			// physiques
			// plus
			// le
			// nom
			// du
			// vehicule.
			// Si
			// nouveau
			// parametre
			// physique
			// rajouter
			// son
			// intitule
			// dans
			// tab[0][i]
			// ci-dessous.
			// Ce
			// champ
			// est
			// juste
			// present
			// pour
			// information.
			tab[0][paramTypeEnum.VEH_TYPE]	= ("Type de vehicule");
			tab[0][paramTypeEnum.VEH_MASS]	= ("Masse vehicule (kg)");
			tab[0][paramTypeEnum.WHEEL_RADIUS]	= ("Rayon de la roue (m)");
			tab[0][paramTypeEnum.DRAG_COEFF]	= ("Coefficient de trainee");
			tab[0][paramTypeEnum.ROLL_RES]	= ("Resistance au roulement");
			tab[0][paramTypeEnum.TR_YIELD]	= ("Rendement transmission");
			tab[0][paramTypeEnum.TR_RATIO]	= ("Rapport transmission");
			tab[0][paramTypeEnum.ENG_TYPE]	= ("Type Moteur Synchrone ou Induction");
			tab[0][paramTypeEnum.NOMINAL_ENG_POW]	= ("Puissance moteur nominale (KW)");
			tab[0][paramTypeEnum.MAX_ENG_POW]	= ("Puissance moteur maximale (KW)");
			tab[0][paramTypeEnum.MAX_ENG_TQ]	= ("Couple moteur maximal (NM)");
			tab[0][paramTypeEnum.BATT_TYPE]	= ("Type Batterie");
			tab[0][paramTypeEnum.BATT_EFF]	= ("Efficacite Batterie");
			tab[0][paramTypeEnum.BATT_CAPA]	= ("Capacite Batterie (kwh)");
			tab[0][paramTypeEnum.REC_MIN_SPD]	= ("Recuperation vitesse minimale (km/h)");
			tab[0][paramTypeEnum.REC_OPT_SPD]  = ("Recuperation vitesse maximale (km/h)");
			tab[0][paramTypeEnum.REC_MAX_DEC]  = ("Recuperation deceleration maximale (m/s)");
			tab[0][paramTypeEnum.AUX_CONS]  = ("Consommation des auxilaires (W)");
			tab[0][paramTypeEnum.COEFF_INERTIA]  = ("Pourcentage de la masse perdu dans l'inertie du moteur (%)");
			tab[0][paramTypeEnum.VEH_MASS_SUP]	= ("Masse ajoutee (kg)");
			break;
		
		case "VehP" : 
			// parametres
			// physiques
			// plus
			// le
			// nom
			// du
			// vehicule.
			// Si
			// nouveau
			// parametre
			// physique
			// rajouter
			// son
			// intitule
			// dans
			// tab[0][i]
			// ci-dessous.
			// Ce
			// champ
			// est
			// juste
			// present
			// pour
			// information.
			tab[0][paramTypeEnum.VEH_TYPE]	= ("Type de vehicule");
			tab[0][paramTypeEnum.VEH_MASS]	= ("Masse vehicule (kg)");
			tab[0][paramTypeEnum.WHEEL_RADIUS]	= ("Rayon de la roue (m)");
			tab[0][paramTypeEnum.DRAG_COEFF]	= ("Coefficient de trainee");
			tab[0][paramTypeEnum.ROLL_RES]	= ("Resistance au roulement");
			tab[0][paramTypeEnum.TR_YIELD]	= ("Rendement transmission");
			tab[0][paramTypeEnum.TR_RATIO]	= ("Rapport transmission");
			tab[0][paramTypeEnum.ENG_TYPE]	= ("Type Moteur Synchrone ou Induction");
			tab[0][paramTypeEnum.NOMINAL_ENG_POW]	= ("Puissance moteur nominale (KW)");
			tab[0][paramTypeEnum.MAX_ENG_POW]	= ("Puissance moteur maximale (KW)");
			tab[0][paramTypeEnum.MAX_ENG_TQ]	= ("Couple moteur maximal (NM)");
			tab[0][paramTypeEnum.BATT_TYPE]	= ("Type Batterie");
			tab[0][paramTypeEnum.BATT_EFF]	= ("Efficacite Batterie");
			tab[0][paramTypeEnum.BATT_CAPA]	= ("Capacite Batterie (kwh)");
			tab[0][paramTypeEnum.REC_MIN_SPD]	= ("Recuperation vitesse minimale (km/h)");
			tab[0][paramTypeEnum.REC_OPT_SPD]  = ("Recuperation vitesse maximale (km/h)");
			tab[0][paramTypeEnum.REC_MAX_DEC]  = ("Recuperation deceleration maximale (m/s)");
			tab[0][paramTypeEnum.AUX_CONS]  = ("Consommation des auxilaires (W)");
			tab[0][paramTypeEnum.COEFF_INERTIA]  = ("Pourcentage de la masse perdu dans l'inertie du moteur (%)");
			tab[0][paramTypeEnum.VEH_MASS_SUP]	= ("Masse ajoutee (kg)");
			break;
			
		case "VehT" :
			// parametres
			// physiques
			// plus
			// le
			// nom
			// du
			// vehicule.
			// Si
			// nouveau
			// parametre
			// physique
			// rajouter
			// son
			// intitule
			// dans
			// tab[0][i]
			// ci-dessous.
			// Ce
			// champ
			// est
			// juste
			// present
			// pour
			// information.
			tab[0][0]	= ("Type de vehicule");
			tab[0][1]	= ("Masse vehicule (kg)");
			tab[0][2]	= ("Rayon de la roue (m)");
			tab[0][3]	= ("Coefficient de trainee");
			tab[0][4]	= ("Resistance au roulement");
			tab[0][5]	= ("Rendement transmission");
			tab[0][6]	= ("Type Moteur Essence ou Diesel");
			tab[0][7]  	= ("Pourcentage de la masse perdu dans l'inertie du moteur (%)");
			tab[0][8]	= ("Nombre de rapports de la boite de vitesses");
			tab[0][9]	= ("Masse ajoutee (kg)");
			break;
		case "Obd" : 			
			tab[0][0]	= ("Temps (s) (date)");
			tab[0][1]	= ("Puissance developeee en %");
			tab[0][2]	= ("Regime moteur (tours/min");
			tab[0][3]	= ("Vitesse (km/h)");
			tab[0][4]	= ("Quantite d'air injectee dans le moteur (g/s)"); 
			tab[0][5]	= ("altitude");
			tab[0][6]	= ("Couple moteur (Nm)");
			tab[0][7]	= ("Pente");
			tab[0][8]	= ("Consommation (L)");
			tab[0][9]	= ("Consommation optimale (L)");
			tab[0][10]	= ("Distance parcourue (km)");
			tab[0][11]	= ("Temps cumulé (s)")
			tab[0][12]	= ("Rapport de vitesses enclenche")
			break;
		case "Couple":
			tab[0][0]	= ("Regime moteur (tours/min)");
			tab[0][1]	= ("Couple max (Nm)");
			break;
		case "ConseilsVE" : 
			tab[0][0]	= ("Temps (s)");
			tab[0][1]	= ("Vitesse (km/h)");
			tab[0][2]	= ("???");
			tab[0][3]	= ("Vitesse (m/s)");
			tab[0][4]	= ("Acceleration (m/s^2)");
		case "Reelles" : 			
			tab[0][0]	= ("Temps (s) (date)");
			tab[0][1]	= ("Regime moteur (tours/min");
			tab[0][2]	= ("Vitesse (km/h)");
			tab[0][3]	= ("Charge moteur en %");
			tab[0][4]	= ("Quantite d'air injectee dans le moteur (g/s)"); 
	}
}
	
/**
 * Creation de la premiere ligne d'un tableau, destine a la creation du tableau du cycle unitaire creation des lignes du tableau
 * 
 * @param tab :
 *            tableau a creer
 */
function creaTableauCycleRegroupes(tab,nbCycles){
	for (var i 	= 0; i<nbCycles ;i++){
		tab[i] 	= new Array();
	}	
	tab[0][0]	= ("HFET");
	tab[1][0]	= ("JP10");
	tab[2][0]	= ("JP15");
	tab[3][0]	= ("MOR1");
	tab[4][0]	= ("MOR2");
	tab[5][0]	= ("MOR3");
	tab[6][0]	= ("NDEC");
	tab[7][0]	= ("NDEC2");
	tab[8][0]	= ("NYC");
	tab[9][0]	= ("UDDS");
	tab[10][0]	= ("US06");
	
}

// ----------------------------------------------------------------------------------
// Creation
// des
// cycles
// de
// conduite
// --------------------------------------------------------------
/**
 * Creation du cycle de conduite a partir du fichier lu (prioritaire) ou en construisant un profil avec une vitesse constante la vitesse constante est celle en parametre ou la valeur par defaut
 * 
 * @param nomFile :
 *            nom du fichier du cycle en entree
 * @param tab :
 *            tableau dans lequel sont stocke les donnees
 * @param nomValeur :
 *            nom de l'input de la valeur de la vitesse moyenne
 * @param nbPoint :
 *            nombre de point du cycle a cree (uniquement valable si on ne lit pas un cycle)
 * @param Vdefaut :
 *            vitesse constante par defaut
 * @returns {nbPoint} : nombre de point du cycle
 */
function creaCycle(nomFile,tab,nomValeur,nbPoint,Vdefaut){
	var fileTampon = $(nomFile).val();
	if(dataConsoRecup.length != 0 && typeResolution !=2){
		fileTampon = "test";
	}
	var nbPointSortie;
	if(fileTampon.length != 0){
		nbPointSortie = lectureCycle(nomFile,tab,false,true);
	}else if(fileTampon.length == 0 && document.getElementById(nomValeur).value != ''){ 
		nbPointSortie = creaProfil(parseFloat(document.getElementById(nomValeur).value),tab,nbPoint);
	}else{
		nbPointSortie = creaProfil(Vdefaut,tab,nbPoint);
	}
	return nbPointSortie;
}
/**
 * Creation d'un profil de conduite a vitesse constante le cycle sera cree avec l'intervale de temps par defaut et un nombre de point choisi
 * 
 * @param Vmoy :
 *            vitesse moyenne du cycle de conduite
 * @param tab :
 *            tableau dans lequel le cycle est cree
 * @param nbPoint :
 *            nombre de point du cycle
 */
function creaProfil(Vmoy,tab,nbPoint){
	creaTableauCycle(tab,nbPoint);
	for(var i = 1; i < nbPoint+1;i++){
		tab[i][0] = (i-1)*inter;
		tab[i][1] = Vmoy;
		tab[i][2] = Vmoy/3.6;
	}
	return nbPoint;
}
/**
 * Calcul de la distance cumule du cycle et des differents delta T
 * 
 * @param tab:
 *            tableau du cycle a calculer et a remplir
 * @param: nbPoint : nombre de point du cycle
 */
function calcDistanceCycle(tab,nbPoint){
	// Calcul
	// de
	// delta
	// T
	for(var i = 1; i < nbPoint;i++){
		tab[i][4] = tab[i+1][0] - tab[i][0];
	}	
	tab[nbPoint][4]=0;		// Le
							// dernier
							// point
							// est
							// calcule
							// a
							// part
							// car
							// il
							// n'a
							// pas
							// de
							// 'i+1'
	// Calcul
	// de
	// la
	// distance
	// cumulee
	var dist = 0;
	tab[1][3] = dist;
	for(var i = 2; i < nbPoint;i++){
		dist = dist + 0.5*(tab[i][2]+tab[i+1][2])*tab[i][4]; // La
																// distance
																// est
																// l'intervale
																// de
																// temps
																// multiplie
																// par
																// la
																// moyenne
																// de
																// la
																// vitesse
																// avant
																// et
																// apres
		tab[i][3] = dist;
	}
	tab[nbPoint][3] = dist + tab[nbPoint][2]*tab[nbPoint-1][4]; // Le
																// dernier
																// point
																// est
																// calcule
																// a
																// part
																// car
																// il
																// n'a
																// pas
																// de
																// 'i+1'
}

// ----------------------------------------------------------------------------------
// Calcul
// de la
// route
// ---------------------------------------------------------------------------
/**
 * Possible callback d'une fonction a la fin du calcul de la route
 * 
 * @param callback :
 *            fonction a appeler a la fin de la requete sur le trajet
 */
function calcRoute(depart,arrivee,callback) {	// callback
												// finalement
												// non
												// utilise,
												// initialement
												// utilise
												// pour
												// rappeler
												// la
												// fonction
												// calcVStep()

	if (typeVehicule=="VPU" && parseFloat(document.getElementById("modePuissancePACU").value) == 0){
		  alert("Veuillez choisir un mode de puissance pour ce trajet");
		  return;
	  }
	polylines = []; // initialisation
					// de
					// la
					// polyline
	marker.setVisible(false);

	var date = new Date();	
	// On
	// calcule
	// le
	// trajet
	// pour
	// le
	// depart/arrivee
	// choisi
	// par
	// l'utilisateur
	// ou
	// pour
	// un
	// depart
	// et
	// une
	// arrivee
	// choisis
	//console.log(departure != undefined, arrival != "undefined", boolEcp);
	if (boolExp == false){
		if(boolGPS==false){
			if(departure_place.geometry.location== undefined ||arrival_place.geometry.location== undefined){
				departure = new google.maps.LatLng(47.638861, 6.862113);
				arrival = new google.maps.LatLng(47.746729, 7.338908);
				waypoints=[];
			}
			else{
				departure = departure_place.geometry.location;
				arrival = arrival_place.geometry.location;	
				waypoints=[];
			}
			
		}
	}
	
	
	
	
	else if(boolExp == true){
		departure = new google.maps.LatLng(47.63967400000001, 6.863848999999959);
		arrival = new google.maps.LatLng(48.57340529999999, 7.752111300000024);
	}
	boolGPS=false;
	
	//trajet Altran-Foulons w/ segmentation + waypoints
	/*departure = new google.maps.LatLng(47.653625, 6.839766);
	
	addWaypoint(47.688917, 6.957276);
	addWaypoint(47.714338, 7.097360);
	
	addWaypoint(47.842138, 7.232275);
	addWaypoint(47.954253, 7.338069);*/
	
	/*addWaypoint(47.737331, 7.278182);
	addWaypoint(47.739643, 7.339792);
	addWaypoint(47.866903, 7.451186);
	addWaypoint(47.950680, 7.378260);
	addWaypoint(48.078421, 7.353970);*/
	
	
	if((document.getElementById('avoidHighways').value)==''){avoidHighways=false}
	else if(parseFloat(document.getElementById('avoidHighways').value)=='1'){avoidHighways=true}
	else{avoidHighways=false}
	
	var request = {
		origin: departure,
		destination: arrival,
		travelMode: google.maps.TravelMode[mode],
		waypoints: waypoints,
		avoidHighways: avoidHighways,
	};

	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
			boolCalcRoute = true;
		}
		// callback();
	}
	);
}
/**
 * Ajoute un waypoint par lequel le passage est force sur le trajet
 * 
 * @param lat
 * @param long
 */
function addWaypoint(lat,long){
	var tampon = new google.maps.LatLng(lat,long);
	waypoints.push({
		location: tampon,
		stopover:false
	});
}
/**
 * Generation de la requete des donnees d'elevation On est limite a un echantillonnage a 512 intervale Le service est payant pour augementer cet echantillonnage
 */
function drawPath(path) {

	var pathRequest = {
		'path': path,
		'samples': 512
	}
  // Initialise
	// la
	// requete
	// de
	// calcul
	// du
	// chemin.
  elevator.getElevationAlongPath(pathRequest, plotElevation);
}
/**
 * Enregistrement des points de passage imposes
 */
function saveWaypoints() {
	directionsresult = directionsDisplay.getDirections();
	waypoints = [];
	var route = directionsresult.routes[0];	 // routes[0]
												// signifie
												// que
												// le
												// traitement
												// se
												// fait
												// sur
												// le
												// premier
												// trajet
												// propose
												// par
												// google
	var viawaypoints = route.legs[0].via_waypoints;	
	for( var i = 0; i < viawaypoints.length; i++) {
		waypoints.push({
			location: viawaypoints[i],
			
			stopover:false
		});
	}
	
}
/**
 * Genere le tableau de polylines pour permettre leurs affichages graphiques
 */
function generatePolyline(color,opacity,weight,nbEtape){

	var steps = directionsDisplay.getDirections().routes[0].legs[0].steps;
	polylines = [];
	var polylineOptions = {
			  strokeColor: color,
			  strokeOpacity: opacity,
			  strokeWeight: weight,
			  visible:false,
			};	
	for(var iterEtape = 1;iterEtape < nbEtape+1;iterEtape++){
		var stepPolyline = new google.maps.Polyline(polylineOptions);
		var step =iterEtape;
		for(var iterStep = 1; iterStep < nbStep+1;iterStep++){
			if(dataStep[iterStep][6] == step){
				var nextSegment = steps[iterStep-1].path;
			    for (k = 0; k < nextSegment.length; k++) {
			      stepPolyline.getPath().push(nextSegment[k]);
			    }
			}
		}
		stepPolyline.setMap(map);
		polylines.push(stepPolyline);
	}
	

}
// ----------------------------------------------------------------------------------
// Fonctions
// globales
// du
// calcul
// -----------------------------------------------------------------
/**
 * Fonction globale du calcul des donnees
 */
var testcalcul = true;
var dataConsoRecup=[];
var affichageComp=false;
function calcConsommation(){
	
	if(typeVehicule =="VE"){
		boolTrajet = false;
		for (var plot=0; plot<latBtab.length; plot++){ // valeurs
														// du
														// tableau
														// 1
														// mises
														// dans
														// le
														// tableau
														// 2
			latBtab2[plot] = latBtab[plot];
			longBtab2[plot] = longBtab[plot];
		}
		NumBorne = 0;		// Mise
							// à 0
							// du
							// compteur
							// des
							// numéros
							// de
							// borne
		cptborne = 0;	// Mise
						// à 0
						// du
						// compteur
						// du
						// nombre
						// de
						// borne
						// à
						// proximité
		calculConsommation();
	}
	else if(typeVehicule=="VT"){
		if(boolOBD){
			initCalculThermique();
			calculConsommation();
			// majDonneesSortie();
		}
		else{
			alert("Veuillez lancer le traitement des donnees OBD (onglet Map Cse)");
		}
	}
	else if(typeVehicule=="VH"){
		if(boolOBD){
			initCalculThermique();
			calculConsommation();
		}
		else{
			alert("Veuillez lancer le traitement des donnees OBD (onglet Map Cse)");
		}
	}
	else if(typeVehicule == "VP"){
		if(dataConsoPac[0].length > 2){                          // on
																	// verifie
																	// si
																	// des
																	// calculs
																	// ont
																	// été
																	// faits
																	// précédement
																	// pour
																	// savoir
																	// si
																	// il
																	// faut
																	// remettre
																	// les
																	// données
																	// de
																	// dataconsopac
																	// à 0
			for(var i = 0; i<dataConsoPac.length; i++){          // on
																	// remet
																	// le
																	// tableau
																	// à
																	// 0 si
																	// des
																	// calculs
																	// existent
																	// déjà
				dataConsoPac[i].length = 1;
			}
		}
		calculConsommation();
		// downloadCSV(dataConsoPac);
		
	}
	else if(typeVehicule == "VPU"){
		
		if(dataConsoPacUTBM!=undefined && dataConsoPacUTBM[0].length > 2){ // on
																			// verifie
																			// si
																			// des
																			// calculs
																			// ont
																			// été
																			// faits
																			// précédement
																			// pour
																			// savoir
																			// si
																			// il
																			// faut
																			// remettre
																			// les
																			// données
																			// de
																			// dataconsopac
																			// à 0
			for(var i = 0; i<dataConsoPacUTBM.length; i++){          // on
																		// remet
																		// le
																		// tableau
																		// à 0
																		// si
																		// des
																		// calculs
																		// existent
																		// déjà
				dataConsoPacUTBM[i].length = 1;
			}
		}
			
		if(dataConsoPacUTBM_CS!=undefined && dataConsoPacUTBM_CS[0].length > 2){ 
			for(var i = 0; i<dataConsoPacUTBM.length; i++){
				dataConsoPacUTBM[i].length = 1;
			}
		}
		if(P_batt_U!=undefined && P_batt_U.length > 2){ 
				P_batt_U.length = 2;
		}
		if(E_batt_i_U!=undefined && E_batt_i_U.length > 2){ 
				E_batt_i_U.length = 2;
		}
		if(E_batt_U!=undefined && E_batt_U.length > 2){ 
				E_batt_U[i].length = 2;
		}
		if(E_batt_r_U!=undefined && E_batt_r_U.length > 2){ 
			E_batt_r_U.length = 2;
		}
		if(P_fc_U!=undefined && P_fc_U.length > 2){ 
			P_fc_U.length = 2;
		}
		
		if(E_fc_i_U!=undefined && E_fc_i_U.length > 2){ 
			E_fc_i_U.length = 2;
		}
		if(E_fc_U!=undefined && E_fc_U.length > 2){ 
			E_fc_U.length = 2;
		}
		
		if(M_H2_U!=undefined && M_H2_U.length > 2){ 
			M_H2_U.length = 2;
		}
		if(act_batt_U!=undefined && act_batt_U.length > 2){ 
			act_batt_U.length = 2;
		}
		if(act_pac_U!=undefined && act_pac_U.length > 2){ 
			act_pac_U.length = 2;
		}
		if(per_batt_r_U!=undefined && per_batt_r_U.length > 2){ 
			per_batt_r_U.length = 2;
		}
		if(per_batt_r_Ah_U!=undefined && per_batt_r_Ah_U.length > 2){ 
			per_batt_r_Ah_U.length = 2;
		}
		if(E_r_U!=undefined && E_r_U.length > 2){ 
			E_r_U.length = 2;
		}
		if(M_H2_r_U!=undefined && M_H2_r_U.length > 2){ 
			M_H2_r_U.length = 2;
		}
		if(per_H2_r_U!=undefined && per_H2_r_U.length > 2){ 
			per_H2_r_U.length = 2;
		}
		if(M_H2_i_U!=undefined && M_H2_i_U.length > 2){ 
			M_H2_i_U.length = 2;
		}
		if(T!=undefined && T.length > 2){ 
			T.length = 2;
		}
		if(E_tot!=undefined && E_tot.length > 2){ 
			E_tot.length = 2;
		}
		if(PfreqVPU!=undefined && PfreqVPU.length > 2){ 
			PfreqVPU.length = 2;
		}
		if(pfcFreqU!=undefined && pfcFreqU.length > 2){ 
			pfcFreqU.length = 2;
		}
		if(I!=undefined && I.length > 2){ 
			I.length = 2;
		}
		if(U!=undefined && U.length > 2){ 
			U.length = 2;
		}
		if(prix_U!=undefined && prix_U.length > 2){ 
			prix_U.length = 2;
		}
		if(energieFournieMoteur!=undefined && energieFournieMoteur.length > 2){ 
			energieFournieMoteur.length = 2;
		}
		if(energieConsoGenerateur!=undefined && energieConsoGenerateur.length > 2){ 
			energieConsoGenerateur.length = 2;
		}
		if(energieFournieGenerateur!=undefined && energieFournieGenerateur.length > 2){ 
			energieFournieGenerateur.length = 2;
		}
		if(energieFournieBatterie!=undefined && energieFournieBatterie.length > 2){ 
			energieFournieBatterie.length = 2;
		}
		if(energieFournieBattParPac!=undefined && energieFournieBattParPac.length > 2){ 
			energieFournieBattParPac.length = 2;
		}
		if(energieChargeeBattParPac!=undefined && energieChargeeBattParPac.length > 2){ 
			energieChargeeBattParPac.length = 2;
		}
		if(energieChargeeBattParGen!=undefined && energieChargeeBattParGen.length > 2){ 
			energieChargeeBattParGen.length = 2;
		}
		calculConsommation();
		// downloadTXT(loiTXT,"etudeLoi.txt");
		// downloadCSV(dataConsoPac);
	}
	
	//partie calcul de comparaison avec un autre type de vehicule
	//On récupère dans dataConsoRecup les informations calculées auparavant dans dataConso
	if(calcComp){
		
		if(typeVehiculeInit == "VE" || typeVehiculeInit == "VT" ){
			window.dataConsoRecup = JSON.parse(JSON.stringify(dataConso));
		}
		else if(typeVehiculeInit == "VPU"){
			window.dataConsoRecup = JSON.parse(JSON.stringify(dataConsoPacUTBM));
		}
		
		
		
		
		//console.log(dataConsoRecup);
		
		
		calcComp=false;
		if (typeVehicule == typeVehiculeComp){
			alert("On ne peut pas faire de comparaison avec le même type de véhicule")
			return;
		}
		alert("Nouveau calcul"+ typeVehiculeComp);
		window.typeVehiculeInit = typeVehicule;
		typeVehicule=typeVehiculeComp;
		affichageComp=true;
		calcConsommation();
		
		affichage_RapportVehChartComparaison();
		affichageComp=false;
	}
	
}	

/*
 * Fonction associée au calcul d'un trajet en temps réel. Le but est de lire le fichier contenant les informations de trajet toutes les x secondes
 * il faut le temps qui le fichier soit changé en ammont et qu'il soit assez long pour pouvoir être affiché dans Maps.
 * la suite du calcul est analogue à une calcul classique.
 */
var enLigne=false;
var stopCalcEnLigne=false;
function calcConsommationEnLigne(){
	if(typeResolution==2){
		alert("Le calcul en ligne ne peut pas s'effectuer avec une segementation de trajet./nMerci de saisir un profile de vitesse.");
		return;
	}
	if(textFileProfile == undefined || textFileProfile.length ==0){
		alert("Veuillez entrer un profile de vitesse");
		return;
	}
	enLigne=true;
	//var file=document.getElementById("cycleRepetition"+typeVehicule);
	//openFileProfile(myEvent);
	
	
	var i=0;
    var lancementCalcConso = function() {							
    		
            
			if(stopCalcEnLigne==true){
				enLigne=false;
				console.log("Calcul stoppé");
				stopCalcEnLigne=false;
				return;
            }
			
			
			openFileGPS(event);
			openFileProfile(event);
			console.log("ok",i);
            i=i+1;
			calcConsommation();

		    setTimeout(lancementCalcConso, intervalDeTempsCalcul*1000);
		}
	lancementCalcConso();


}

/**
 * Fonction globale du calcul des donnees permet de basculer entre les differents types de resolution (pente fixe ou reele, segmentation ou repetition du cycle sur le trajet)
 */
function calculConsommation(){
	updateResolution(); // Choix
						// du
						// type
						// de
						// resolution
						// dans
						// l'interface
						// graphique
	boolReDo = false;
	var tempP = updatePente();
	var tempD = updateDegrade();
	var tempPac = document.getElementById("modelePACU").value;
	var tempM = document.getElementById("modeleBatterie").value;
	if(tempM==1){
		var tempTe = document.getElementById("temperatureBatterie").value;
	}
	var tempMot = document.getElementById("modeleMoteur").value;
	if (tempPac==2){
		var tempT=parseFloat(document.getElementById('tpsReponsePACU').value);
		var tempR = updateRend();
	}
	
	if(boolCalcRoute){
		if(tempP == 0){
			alert ("Veuillez choisir une modelisation de la pente");
			return;
		}
		if(typeResolution == 0){
			alert ("Veuillez choisir une methode de resolution");
			return;
		}
		
		if (typeVehicule == 'VPU' && tempPac==2 && tempT == 0) {
			alert("Veuillez choisir si le temps de réponse de la PAC doit ou non être pris en compte");
			return;
		}
		if (typeVehicule == 'VPU' && parseFloat(document.getElementById('modePuissancePACU').value) == 0) {
			alert("Veuillez choisir un mode de puissance PAC");
			return;
		}
		if (typeVehicule == 'VPU' && tempPac==2 && tempR == 0) {
			alert("Veuillez choisir un type de rendement PAC");
			return;
		}
		if (typeVehicule == 'VPU' && tempPac == 0) {
			alert("Veuillez choisir un modèle PAC");
			return;
		}
		if (typeVehicule == 'VPU' && tempM == 0) {
			alert("Veuillez choisir un modèle batterie");
			return;
		}
		if (typeVehicule == 'VPU' && tempM == 1 && tempTe == 0) {
			alert("Veuillez choisir un modèle température");
			return;
		}
		if (typeVehicule == 'VPU' && tempMot == 0) {
			alert("Veuillez choisir un modèle moteur");
			return;
		}
		else if (typeResolution == 1){ 	// Utilisation
										// d'un
										// profil
										// de
										// vitesse
			try{initialisationRepetition();
				var t0 = new Date().getTime();
				aucunmode = 0;
				resolutionRepetition(); 
				if (typeVehicule=="VH"){
					calculBusHybride();
				}
				if(!boolReDo){
					affichage();
					var t1 = new Date().getTime();
					
					// alert("nombre
					// d'iter
					// sans
					// mode:
					// " +
					// aucunmode);
					if(enLigne==false){
						alert("La resolution s'est deroulee normalement et a dure : " + (t1 - t0) + " milliseconds.");
					}
					
					
					if(typeVehicule=="VE" && enLigne==false){
						alert('Le trajet est faisable sans recharger la batterie')
					}
					else if (M_H2_r_U[M_H2_r_U.length-1]>=0 && enLigne==false && typeVehicule== "VPU"){
						alert("Il restera : " + (M_H2_r_U[M_H2_r_U.length-1]) + " kg d'hydrogène (" + (per_H2_r_U[per_H2_r_U.length-1]) + " %) dans le réservoir à la fin de ce trajet.");
					}
					else if (typeVehicule =='VPU'){
						if(enLigne==false){
							alert("Le trajet nécessite au moins une recharge d'hydrogène. Masse consommée totale pour le trajet : "+ (M_H2_U[M_H2_U.length-1])+" kg");	
						}
						
					}

				}else{
					affichage();
					document.getElementById("rechercheBorne"+typeVehicule).style.visibility = ''
					var t1 = new Date().getTime();
					alert("La resolution s'est deroulee normalement et a dure : " + (t1 - t0) + " milliseconds.");
					if(typeVehicule=="VE"){
						alert('Le trajet necessite au moins une recharge de la batterie')
					}
				}
			}catch (e){alert("Une erreur est survenue lors de la resolution \n" + e.name + ": " + e.message)}
		}else if(typeResolution == 2){		// Segmentation
											// du
											// trajet
											// en
											// fonction
											// des
											// etapes
											// GoogleMaps
			
			try{initialisationSegmentation();
				var t0 = new Date().getTime();
				aucunmode = 0;
				resolutionSegmentation();
				if (typeVehicule=="VH"){
					calculBusHybride();
				}
				if(!boolReDo){
					affichage();
					var t1 = new Date().getTime();
					alert("La resolution s'est deroulee normalement et a dure : " + (t1 - t0) + " milliseconds.");
					// alert("nombre
					// d'iter
					// sans
					// mode:
					// " +
					// aucunmode);
					if(typeVehicule=="VE"){
						alert('Le trajet est faisable sans recharger la batterie')
					}
					if(typeVehicule=="VPU"){
						if (M_H2_r_U[M_H2_r_U.length-1]>=0){
							alert("Il restera : " + (M_H2_r_U[M_H2_r_U.length-1]) + " kg d'hydrogène (" + (per_H2_r_U[per_H2_r_U.length-1]) + " %) dans le réservoir à la fin de ce trajet.");
						}
						else{
							alert("Le trajet nécessite au moins une recharge d'hydrogène. Masse consommée totale pour le trajet : "+ (M_H2_U[M_H2_U.length-1])+" kg");
						}
					}
				
				}else{
					affichage();
					var t1 = new Date().getTime();
					document.getElementById("rechercheBorne"+typeVehicule).style.visibility = ''
					alert("La resolution s'est deroulee normalement et a dure : " + (t1 - t0) + " milliseconds.");
					if(typeVehicule=="VE"){
						alert('Le trajet necessite au moins une recharge de la batterie')
					}
				}
			}catch(e){alert("Une erreur est survenue lors de la resolution \n" + e.name + ": " + e.message)}
		}
	}
	else
	{
		alert("Veuillez lancer une requête de trajet (onglet Route)");
	}

}
/**
 * Fonction permettant de recalculer la consommation
 */
function calcConsoBorne(){
	if(!boolChoixBorne){
		alert("Veuillez choisir une borne")
	}else{
		for (var plot=0; plot<latBtab.length-1; plot++){ // valeurs
															// du
															// tableau
															// 1
															// mises
															// dans
															// le
															// tableau
															// 2
															// sauf
															// celle
															// de
															// la
															// borne
															// sélectionné
															// pour
															// le
															// calcul
															// borne
			latBtab2[plot] = latBtab[plot];
			longBtab2[plot] = longBtab[plot];
		}
		NumBorne = 0;		// Mise
							// à 0
							// du
							// compteur
							// des
							// numéros
							// de
							// borne
		if(typeResolution == 0){
			alert ("Veuillez choisir une methode de resolution")
		} else if (typeResolution == 1){ 	// Utilisation
											// d'un
											// profil
											// de
											// vitesse
			try{
				boolReDo = false;
				resolutionRepetition();
				if (typeVehicule=="VH"){
					calculBusHybride();
				}				
				affichage();
				if(confirm("La resolution s'est deroulee normalement \n" + 'Le taux de charge final de la batterie est de ' + Math.round(dataConso[nbPointConso][32]*10)/10 + ' %'  + "\n Ajouter un point de passage vers cette borne ?")){	
					addWaypoint(latB,longB);
					document.getElementById("calculBorne"+typeVehicule).style.visibility = 'hidden';
					document.getElementById("rechercheBorne"+typeVehicule).style.visibility = 'hidden';
					for(var i =0;i<markersRecharge.length;i++){
						markersRecharge[i].setMap(null);
					}
					markersRecharge = [];
					calcRoute(departure_place.geometry.location,arrival_place.geometry.location);
					boolBorne = true;
					tauxDeChargeBorne = dataConso[nbPointConso][32];
				}
			}catch (e){alert("Une erreur est survenue lors de la resolution")}
		}else if(typeResolution == 2){		// Segmentation
											// du
											// trajet
											// en
											// fonction
											// des
											// etapes
											// GoogleMaps
			try{
				boolReDo = false;
				resolutionSegmentation();
				if (typeVehicule=="VH"){
					calculBusHybride();
				}
				affichage();
				if(confirm("La resolution s'est deroulee normalement \n" + 'Le taux de charge final de la batterie est de ' + Math.round(dataConso[nbPointConso][32]*10)/10 + ' %'  + "\n Ajouter un point de passage vers cette borne ?")){	
					addWaypoint(latB,longB);
					document.getElementById("calculBorne"+typeVehicule).style.visibility = 'hidden';
					document.getElementById("rechercheBorne"+typeVehicule).style.visibility = 'hidden';
					for(var i =0;i<markersRecharge.length;i++){
						markersRecharge[i].setMap(null);
					}
					markersRecharge = [];
					calcRoute(departure_place.geometry.location,arrival_place.geometry.location);
					boolBorne = true;
					tauxDeChargeBorne = dataConso[nbPointConso][32];
				}
			}catch(e){alert("Une erreur est survenue lors de la resolution")}
		}
	}

}	
/**
 * Fonction regroupant les etapes des calculs energetique dans le cas de l'utilisation de profil de vitesse (complet ou repete)
 */

function resolutionRepetition(){

	// Initialisation
	// des
	// variables
	var iterConso			= 1;						// iterateur
														// du
														// tableau
														// final
														// de
														// donnees
	var distTampon 			= 0;						// distance
														// tampon
														// pour
														// chaque
														// debut
														// de
														// cycle
	var tempsTampon			= 0;						// temps
														// tampon
														// pour
														// chaque
														// debut
														// de
														// cycle
	var cycleTampon			= dataStep[1][4]			// tampon
														// du
														// type
														// de
														// parcours
														// de
														// la
														// derniere
														// iteration

	dataConso[iterConso] 	= new Array();		
	// Ajout
	for (var k=0;k<nbParamDataTable; k++){					
		dataConso[iterConso][k] = 0;					// Toutes
														// les
														// variables
														// sont
														// initialisees
														// a
														// zero
														// car
														// il y
														// a
														// des
														// calculs
														// avec
														// iterConso-1
	}

	dataConso[iterConso][31]= dataStep[1][4]			// Type
														// de
														// parcours
	dataConso[iterConso][33] = dataxcos.getValue(0,0);  // Latitude
														// de
														// depart
	dataConso[iterConso][34] = dataxcos.getValue(0,1);  // Longitude
														// de
														// depart
	var nbPoint			= nbPointRep;					// Nombre
														// de
														// point
														// du
														// cycle
														// repete
	var tabTampon 		= dataCycleRep;					// Tableau
														// du
														// cycle
														// repete
	var distanceCycle 	= dataCycleRep[nbPoint][3];		// Distance
														// du
														// cycle
														// repete
	var iterCycle		= 1;							// Iterateur
														// du
														// cycle
	var sec 			= 0;							// Securite
														// :
														// limite
														// a
														// 30000
														// points
														// de
														// calcul
	if(typeVehicule=="VE"|| typeVehicule == "VP"){ // ||
													// typeVehicule
													// ==
													// "VPU"
		dataConso[iterConso][32] = socBatterie;				// Taux
															// de
															// charge
															// initial
															// de
															// la
															// batterie
		dataConso[iterConso][35] = (100-dataConso[iterConso][32])*dataConso[iterConso][3]; // Autonomie
																							// en
																							// km
																							// restante
		dataConso[iterConso][37] = 0;						// Energie
															// dissipee
		var energie = socBatterie/100*capBatterie*1000; 	// Variable
															// tampon
															// pour
															// le
															// calcul
															// du
															// taux
															// de
															// charge
															// de
															// la
															// batterie
	
	}
	
	// On
	// repete
	// le
	// cycle
	// de
	// conduite
	// tant
	// qu'on
	// a
	// pas
	// atteint
	// la
	// distance
	// de
	// parcours
	// et
	// que
	// le
	// cycle
	// de
	// conduite
	// ne
	// change
	// pas
	
	
	while(iterConso < nbPointRep && sec < 30000){				//distanceCycle//distanceTotaleRoute
		iterConso+=1;
		iterCycle+=1;
		dataConso[iterConso] = new Array();
		/*
		 * Si on depasse la longeur du cycle, on recommence a zero il faut alors ajouter la distance et le temps tampon aux futures valeurs puisque le temps et la distance du cycle recommencent a zero
		 * 
		 *Pour le passage de HEVEs en ligne on évite ce cas car il donne lieu à des erreur de continuité. 
		 */
		if(iterCycle > nbPoint){
			iterCycle 		= 1;												
			tempsTampon 	= dataConso[iterConso-1][0]+inter;
			distTampon 		= dataConso[iterConso-1][3];
		}		
		var valueTampon = calcdataConsoCycle(iterConso,distTampon,tempsTampon,energie,iterCycle,tabTampon);
		energie = valueTampon.energie; // Copie
										// des
										// donnees
		

		if(valueTampon.bool && typeVehicule =="VE"){
			boolReDo = true;
		// break;
		}
		sec +=1; // On
					// limite
					// le
					// calcul
					// a
					// 30000
					// points
					// par
					// cycle
	}
	// Mise
	// a
	// jour
	// des
	// variables
	// tampon
	distTampon 	= dataConso[iterConso][3];
	tempsTampon	= dataConso[iterConso][0];
	cycleTampon = dataConso[iterConso][31];

	// Retour
	// a
	// v=0
	// a la
	// fin
	// du
	// parcours
	// si
	// le
	// cycle
	// ne
	// revient
	// pas
	// a
	// zero
	
	if(memory==1){
		while(dataConso[iterConso][1] > 0){
		iterConso+=1;
		energie = calcdataConsoInter(iterConso,false,distTampon,energie).energie;
		}
	}else {
		window.lastVitesseMemory = dataConso[iterConso][1];
	}
	
	if(dataConso[iterConso][1]<0){ // remplacement
									// du
									// dernier
									// point
									// si
									// la
									// vitesse
									// est
									// negative
		dataConso[iterConso][1] = 0;
		dataConso[iterConso][2] = 0;
	}
	nbPointConso = iterConso;
	if(typeVehicule=="VE"){
		// Calcul
		// de
		// l'autonomie
		var consoMoy = (dataConso[nbPointConso][29]-dataConso[nbPointConso][30])/dataConso[nbPointConso][3]
		for(var iterRayon =1;iterRayon<nbPointConso+1;iterRayon++){
			dataConso[iterRayon][35] = (dataConso[iterRayon][32] - socMin)*capBatterie/consoMoy/100;
			dataConso[iterRayon][36] = dataConso[iterRayon][3]/1000;
		}
	}
	else if(typeVehicule=="VT"){
		for(var iterRayon =1;iterRayon<nbPointConso+1;iterRayon++){
			dataConso[iterRayon][36] = dataConso[iterRayon][3]/1000;
		}
		dataConso[nbPointConso][55] = dataConso[nbPointConso][54]/dataConso[nbPointConso][36]*100;	// conso
																									// moyenne
																									// en
																									// litres/100km
		rendementmoteur = Math.round(dataConso[nbPointConso][29]/dataConso[nbPointConso][57]*1000)/10;
		dataConso[nbPointConso][74] = dataConso[nbPointConso][73]/dataConso[nbPointConso][36]*100;   // conso
																										// opti
																										// moyenne
																										// en
																										// litres/100km
		rendementmoteurOpti = Math.round(dataConso[nbPointConso][29]/dataConso[nbPointConso][76]*1000)/10;
	}
	
	for(var i=1; i<nbPointConso+1;i++){
		dataConso[i][38]=tabvlimAugmente[i-1];
	}
}
/**
 * Fonction regroupant les etapes des calculs energetique dans le cas de la segmentation du trajet
 */
function resolutionSegmentation(){	
	
	// calcVStep();
	// //
	// Calcul
	// de
	// la
	// vitesse,
	// de
	// la
	// distance
	// et
	// du
	// type
	// de
	// cycle
	// de
	// chaque
	// etape
	// du
	// trajet
    // Initialisation
	// des
	// variables
	var iterConso			= 1;					// iterateur
													// du
													// tableau
													// final
													// de
													// donnees
	var distTampon 			= 0;					// distance
													// tampon
													// pour
													// chaque
													// debut
													// de
													// cycle
	var tempsTampon			= 0;					// temps
													// tampon
													// pour
													// chaque
													// debut
													// de
													// cycle
	var cycleTampon			= dataStep[1][4];		// tampon
													// du
													// type
													// de
													// parcours
													// de
													// la
													// derniere
													// iteration

	dataConso[iterConso] 	= new Array();	
// Ajout
	for (var k=0;k < nbParamDataTable; k++){					
		dataConso[iterConso][k] = 0;				// Toutes
													// les
													// variables
													// sont
													// initialisees
													// a
													// zero
													// car
													// il y
													// a
													// des
													// calculs
													// avec
													// iterConso-1
	}
	dataConso[iterConso][31]= dataStep[1][4]		// Type
													// de
													// parcours
	if(typeVehicule == "VE" || typeVehicule == "VP"|| typeVehicule == "VPU" ){	// ||
														// typeVehicule
														// ==
														// "VPU"
		dataConso[iterConso][32] = socBatterie;			// Taux
														// de
														// charge
														// initial
														// de
														// la
														// batterie
		dataConso[iterConso][35] = (100-dataConso[iterConso][32])*dataConso[iterConso][3];
	}
	dataConso[iterConso][33] = dataxcos.getValue(0,0);  // Latitude
														// de
														// depart
	dataConso[iterConso][34] = dataxcos.getValue(0,1);  // Longitude
														// de
														// depart
	dataConso[iterConso][37] = 0;
	if(typeVehicule=="VE" || typeVehicule=="VP" || typeVehicule == "VPU"){ // ||
													// typeVehicule
													// ==
													// "VPU"
		var energie = socBatterie/100*capBatterie*1000; // Variable
														// tampon
														// pour
														// le
														// calcul
														// du
														// taux
														// de
														// charge
														// de
														// la
														// batterie
	}
	else if(typeVehicule=="VT" || typeVehicule=="VH"){
		var energie = 0;
	}
	
	// On
	// execute
	// la
	// boucle
	// pour
	// chaque
	// etape
	// du
	// parcours
	// utilisant
	// le
	// profil
	// de
	// conduite
	// correspondant
	// a
	// l'etape
	boucleEtape:	for (var i = 0; i < nbCycle; i++){
		// initialisation
		// des
		// variables
		// en
		// fonction
		// du
		// point
		// de
		// depart
		// du
		// parcours
		var temp = initParcours(iterConso,dataConso)
		var nbPoint			= temp.nbPoint;			// Nombre
													// de
													// point
													// du
													// cycle
													// correspondant
													// a
													// cette
													// etape
		var tabTampon 		= temp.tabTampon;		// Tableau
													// du
													// cycle
													// correspondant
													// a
													// cette
													// etape
		var distanceCycle 	= temp.distanceCycle;	// Distance
													// du
													// cycle
													// correspondant
													// a
													// cette
													// etape
		var iterCycle		= 1;					// Iterateur
													// pour
													// le
													// cycle
		var sec = 0;			         	    	// Securite
													// pour
													// limiter
													// a
													// 30000
													// point
													// de
													// calcul
													// par
													// etape
		/*
		 * Ajout d'un point intermediaire pour faire le lien entre deux cycles successifs le lien se fait en ajoutant des points pour que le changement de vitesse se fasse a l'acceleration/decelereation choisi par l'utilisateur
		 */
		
		if(dataConso[iterConso][2] < tabTampon[1][2]){ 	// On
														// accelere
			
			var deltaV 	= tabTampon[1][2] - dataConso[iterConso][2];
			var deltaT 	= deltaV/accMax;
			var pas 	= Math.round(deltaT/inter);
			for(var j = 0; j<pas;j++){

				iterConso+=1;			
				var valueTampon = calcdataConsoInter(iterConso,true,distTampon,energie);
				energie = valueTampon.energie;
			}
	
			
			/*
			 * A cause du pas de calcul, il peut arriver qu'un point de trop soit calcule, il est supprime ici en faisant reculer l'iterateur de 1 si on a depasse la vitesse objectif
			 */
			
			if(dataConso[iterConso][2] > tabTampon[1][2]){ 
			
				iterConso -=1;
			}
			
		}else{ 	
			// On
			// decelere
			var deltaV	= dataConso[iterConso][2] - tabTampon[1][2];
			var deltaT	= deltaV/decMax;
			var pas 	= Math.round(deltaT/inter);
			for(var k = 0;k < pas;k++){
				iterConso+=1;
				var valueTampon = calcdataConsoInter(iterConso,false,distTampon,energie)
				energie = valueTampon.energie;
			}
			/*
			 * A cause du pas de calcul, il peut arriver qu'un point de trop soit calcule, il est supprime ici en faisant reculer l'iterateur de 1 si on depasse la vitesse objectif
			 */
			if(dataConso[iterConso][2] < tabTampon[1][2]){
				iterConso -=1;
			}
		}

		// Mise
		// a
		// jour
		// des
		// variables
		// tampon
		distTampon		= dataConso[iterConso][3];
		tempsTampon		= dataConso[iterConso][0];
		// On
		// repete
		// le
		// cycle
		// de
		// conduite
		// tant
		// qu'on
		// a
		// pas
		// atteint
		// la
		// distance
		// de
		// parcours
		// et
		// que
		// le
		// cycle
		// de
		// conduite
		// ne
		// change
		// pas
		while(dataConso[iterConso][3] < distanceTotaleRoute && sec < 30000 && cycleTampon == dataConso[iterConso][31]){
			iterConso+=1;
			iterCycle+=1;
			dataConso[iterConso] = new Array();
			/*
			 * Si on depasse la longeur du cycle, on recommence a zero il faut alors ajouter la distance et le temps tampon aux futurs valeurs puisque le temps du cycle recommence a zero
			 */
			if(iterCycle > nbPoint){
				iterCycle 		= 1;												
				tempsTampon 	= dataConso[iterConso-1][0]+inter;
				distTampon 		= dataConso[iterConso-1][3];
			}			
			// Copie
			// des
			// donnees
			var valueTampon = calcdataConsoCycle(iterConso,distTampon,tempsTampon,energie,iterCycle,tabTampon);
			energie = valueTampon.energie; 
			if(valueTampon.bool && typeVehicule =="VE"){
				boolReDo = true;
			}
			sec +=1;
		}
		// Mise
		// a
		// jour
		// des
		// variables
		// tampon
		distTampon 	= dataConso[iterConso][3]
		tempsTampon	= dataConso[iterConso][0]
		cycleTampon = dataConso[iterConso][31]
	}
	
	// Retour
	// a
	// v=0
	// a la
	// fin
	// du
	// parcours
	while(dataConso[iterConso][1] > 0){
		iterConso+=1;
		var valueTampon = calcdataConsoInter(iterConso,false,distTampon,energie);
		energie = valueTampon.energie;
		
	}

	if(dataConso[iterConso][1]<0){ // remplacement
									// du
									// dernier
									// point
									// si
									// la
									// vitesse
									// est
									// negative
		dataConso[iterConso][1] = 0;
		dataConso[iterConso][2] = 0;
	}
	
	nbPointConso = iterConso;
	if(typeVehicule=="VE"){
		// Calcul
		// de
		// l'autonomie
		var consoMoy = (dataConso[nbPointConso][29]-dataConso[nbPointConso][30])/dataConso[nbPointConso][3]
		for(var iterRayon =1;iterRayon<nbPointConso+1;iterRayon++){
			dataConso[iterRayon][35] = (dataConso[iterRayon][32] - socMin)*capBatterie/consoMoy/100;
			dataConso[iterRayon][36] = dataConso[iterRayon][3]/1000;
		}
	}
	else if(typeVehicule=="VT" || typeVehicule=="VH"){
		for(var iterRayon =1;iterRayon<nbPointConso+1;iterRayon++){
			dataConso[iterRayon][36] = dataConso[iterRayon][3]/1000;// distance
																	// totale
																	// parcourue
		}
		dataConso[nbPointConso][55] = dataConso[nbPointConso][54]/dataConso[nbPointConso][36]*100;	// conso
																									// moyenne
																									// en
																									// litres/100km
		rendementmoteur = Math.round(dataConso[nbPointConso][29]/dataConso[nbPointConso][57]*1000)/10;
		dataConso[nbPointConso][74] = dataConso[nbPointConso][73]/dataConso[nbPointConso][36]*100;	// conso
																									// moyenne
																									// opti
																									// en
																									// litres/100km
		rendementmoteurOpti = Math.round(dataConso[nbPointConso][29]/dataConso[nbPointConso][76]*1000)/10;

	}
}

/**
 * Fonction regroupant les etapes des calculs energetique dans lu calcul sur un trajet
 */

function resolutionTrajetVT(){	
	
	// Initialisation
	// des
	// variables
	var iterTrajet			= 1;						// iterateur
														// du
														// tableau
														// final
														// de
														// donnees
	var distTampon 			= 0;						// distance
														// tampon
														// pour
														// chaque
														// debut
														// de
														// cycle
	var tempsTampon			= 0;						// temps
														// tampon
														// pour
														// chaque
														// debut
														// de
														// cycle
	var cycleTampon			= dataStep[1][4]			// tampon
														// du
														// type
														// de
														// parcours
														// de
														// la
														// derniere
														// iteration
	
	
	dataConsoReelleVT[iterTrajet] 	= new Array();		
	// Ajout
	for (var k=0;k<nbParamDataTable; k++){					
		dataConsoReelleVT[1][k] = 0;					// Toutes
														// les
														// variables
														// sont
														// initialisees
														// a
														// zero
														// car
														// il y
														// a
														// des
														// calculs
														// avec
														// iterTrajet-1
	
	}
	dataConsoReelleVT[1][1] = dataObdTrajetReelle[3][1]; // vitesse
															// au
															// debut
															// de
															// l'enregistrement
															// des
															// donnees
	dataConsoReelleVT[1][2] = dataConsoReelleVT[1][1]/3.6;	// vitesse
															// m/s
	dataConsoReelleVT[1][3] = 0;	// distance
									// m
	dataConsoReelleVT[1][4] = dataObdTrajetReelle[7][1];	// pente
															// %
	nbPointTrajet = dataObdTrajetReelle[0].length-1;
	dataConsoReelleVT[1][31]= dataStep[1][4];			// Type
														// de
														// parcours
	dataConsoReelleVT[1][33] = dataxcos.getValue(0,0); // Latitude
														// de
														// depart
	dataConsoReelleVT[1][34] = dataxcos.getValue(0,1); // Longitude
														// de
														// depart
	dataConsoReelleVT[1][40] = dataObdTrajetReelle[12][1];	// rapport
															// de
															// reduction
															// au
															// debut
															// de
															// l'enregistrement
															// des
															// donnees
	dataConsoReelleVT[1][42] = dataObdTrajetReelle[2][1];	// regime
															// moteur
	dataConsoReelleVT[1][43] = dataObdTrajetReelle[6][1];	// Couple
															// moteur
	dataConsoReelleVT[1][45] = dataObdTrajetReelle[6][1]/dataObdTrajetReelle[1][1];	// Charge
																					// moteur
	dataConsoReelleVT[1][67] =	dataConsoReelleVT[1][40];	// vitesse
															// enclenchee
	dataConsoReelleVT[1][63] = dataConsoReelleVT[1][42];	// regime
															// moteur
	dataConsoReelleVT[1][64] = dataConsoReelleVT[1][43];	// couple
															// moteur
	dataConsoReelleVT[1][65] = dataConsoReelleVT[1][45];	// charge
															// moteur
	dataConsoReelleVT[1][78] = dataObdTrajetReelle[12][1];	// rapport
															// de
															// vitesse
															// enclenche
	if(dataConsoReelleVT[1][78] == -1){
		dataConsoReelleVT[1][78] = 1;
	}
	dataConsoReelleVT[1][79] = dataObdTrajetReelle[2][1];	// regime
															// moteur
															// (rpm)
	dataConsoReelleVT[1][80] = dataObdTrajetReelle[6][1];	// couple
															// moteur
															// (N.m)
	dataConsoReelleVT[1][81] = dataObdTrajetReelle[1][1];	// charge
															// moteur
															// (%)
	dataConsoReelleVT[1][82] = dataObdTrajetReelle[8][1];	// consommation
															// reelle
															// (litres)
	dataConsoReelleVT[1][83] = dataObdTrajetReelle[13][1];	// conso
															// instantanee
															// reelle
															// (litres/100km)
	dataConsoReelleVT[1][84] = dataObdTrajetReelle[14][1];	// rejets
															// de
															// co2
															// (kg)
	dataConsoReelleVT[1][86] = dataConsoReelleVT[1][80]*dataVitesses[dataConsoReelleVT[1][78]];
	dataConsoReelleVT[1][87] = dataConsoReelleVT[1][79]/dataVitesses[dataConsoReelleVT[1][78]];
	dataConsoReelleVT[1][88] = dataObdTrajetReelle[2][1];	// regime
															// moteur
															// (rpm)
	dataConsoReelleVT[1][89] = dataObdTrajetReelle[6][1];	// couple
															// moteur
															// (N.m)
	dataConsoReelleVT[1][90] = dataObdTrajetReelle[1][1];	// charge
															// moteur
															// (%)
	dataConsoReelleVT[1][92] = dataConsoReelleVT[1][78]; // rapport
															// de
															// vitesse
															// enclenche
	dataConsoReelleVT[1][93] = dataObdTrajetReelle[8][1];	// consommation
															// reelle
															// (litres)
	dataConsoReelleVT[1][94] = dataObdTrajetReelle[13][1];	// conso
															// instantanee
															// reelle
															// (litres/100km)
	dataConsoReelleVT[1][95] = dataObdTrajetReelle[14][1];	// rejets
															// de
															// co2
															// (kg)
	
	var nbPoint			= dataObdTrajetReelle[0].length-1;	// Nombre
															// de
															// point
															// du
															// cycle
															// repete
	var tabTampon 		= dataConsoReelleVT;					// Tableau
																// du
																// cycle
																// repete
	var distanceCycle 	= dataObdTrajetReelle[10][nbPoint]*1000;		// Distance
																		// du
																		// cycle
																		// repete
	var iterCycle		= 1;							// Iterateur
														// du
														// cycle
	var sec 			= 0;							// Securite
														// :
														// limite
														// a
														// 30000
														// points
														// de
														// calcul
	var energie = 0;
	
	// On
	// repete
	// le
	// cycle
	// de
	// conduite
	// tant
	// qu'on
	// a
	// pas
	// atteint
	// la
	// distance
	// de
	// parcours
	// et
	// que
	// le
	// cycle
	// de
	// conduite
	// ne
	// change
	// pas
	while(dataConsoReelleVT[iterTrajet][3] < distanceTotaleRoute && sec < 30000){
		iterTrajet+=1;
		iterCycle+=1;
		if(iterCycle > nbPoint){
			iterCycle 		= 1;												
			tempsTampon 	= dataConsoReelleVT[iterTrajet-1][0];
			distTampon 		= dataConsoReelleVT[iterTrajet-1][3];
		}	
		dataConsoReelleVT[iterTrajet] = new Array();
		dataConsoReelleVT[iterTrajet][0] = dataObdTrajetReelle[11][iterCycle]+tempsTampon;	// temps
																							// s
		dataConsoReelleVT[iterTrajet][1] = dataObdTrajetReelle[3][iterCycle];	// vitesse
																				// km/h
		dataConsoReelleVT[iterTrajet][2] = dataConsoReelleVT[iterTrajet][1]/3.6;	// vitesse
																					// m/s
		dataConsoReelleVT[iterTrajet][3] = dataObdTrajetReelle[10][iterCycle]*1000+distTampon;	// distance
																								// m
		dataConsoReelleVT[iterTrajet][4] = dataObdTrajetReelle[7][iterCycle];	// pente
																				// %
		dataConsoReelleVT[iterTrajet][78] = dataObdTrajetReelle[12][iterCycle];	// rapport
																				// de
																				// vitesse
																				// enclenche
		if(dataConsoReelleVT[iterTrajet][78] == -1){
			dataConsoReelleVT[iterTrajet][78] = dataConsoReelleVT[iterTrajet-1][78]
		}
		dataConsoReelleVT[iterTrajet][79] = dataObdTrajetReelle[2][iterCycle];	// regime
																				// moteur
																				// (rpm)
		dataConsoReelleVT[iterTrajet][80] = dataObdTrajetReelle[6][iterCycle];	// couple
																				// moteur
																				// (N.m)
		dataConsoReelleVT[iterTrajet][81] = dataObdTrajetReelle[1][iterCycle];	// charge
																				// moteur
																				// (%)
		dataConsoReelleVT[iterTrajet][82] = dataObdTrajetReelle[8][iterCycle];	// consommation
																				// reelle
																				// (litres)
		dataConsoReelleVT[iterTrajet][83] = dataObdTrajetReelle[13][iterCycle];	// conso
																				// instantanee
																				// reelle
																				// (litres/100km)
		dataConsoReelleVT[iterTrajet][84] = dataObdTrajetReelle[14][iterCycle];	// rejets
																				// de
																				// co2
																				// (kg)
		/*
		 * Si on depasse la longeur du cycle, on recommence a zero il faut alors ajouter la distance et le temps tampon aux futures valeurs puisque le temps et la distance du cycle recommencent a zero
		 */
			
		var valueTampon = calculdataConso(iterTrajet,energie,"cycle",dataConsoReelleVT);
		energie = valueTampon.energie; // Copie
										// des
										// donnees
		

		if(valueTampon.bool && typeVehicule =="VE"){
			boolReDo = true;
		// break;
		}
		sec +=1; // On
					// limite
					// le
					// calcul
					// a
					// 30000
					// points
					// par
					// cycle
		
	}
	for(var iterRayon =1;iterRayon<nbPointTrajet+1;iterRayon++){
		dataConsoReelleVT[iterRayon][36] = dataConsoReelleVT[iterRayon][3]/1000;	// distance
																					// totale
																					// parcourue
	}
	dataConsoReelleVT[nbPointTrajet][55] = dataConsoReelleVT[nbPointTrajet][54]/dataConsoReelleVT[nbPointTrajet][36]*100;	// conso
																															// moyenne
																															// en
																															// litres/100km
	rendementmoteur = Math.round(dataConsoReelleVT[nbPointTrajet][29]/dataConsoReelleVT[nbPointTrajet][57]*1000)/10;
	dataConsoReelleVT[nbPointTrajet][74] = dataConsoReelleVT[nbPointTrajet][73]/dataConsoReelleVT[nbPointTrajet][36]*100;	// conso
																															// moyenne
																															// opti
																															// en
																															// litres/100km
	rendementmoteurOpti = Math.round(dataConsoReelleVT[nbPointTrajet][29]/dataConsoReelleVT[nbPointTrajet][76]*1000)/10;

}
/**
 * Calcul des points intermediaires faisant le lien entre deux cycles consecutif
 * 
 * @param iter :
 *            iterateur du tableau a remplir
 * @param bool :
 *            vrai si on accelere, faux si on decelere
 * @param distTampon :
 *            distance deja parcouru
 * @param energie :
 *            etat de l'energie pour le calcul du taux de charge de la batterie
 */
function calcdataConsoInter(iter,bool,distTampon,energie){
	
	var bool2 = false;	// Boolean
						// vrai
						// si
						// on
						// doit
						// arreter
						// la
						// resolution
						// (panne
						// de
						// batterie)
	dataConso[iter]	 		= new Array();
	// Calcul
	// du
	// temps
	dataConso[iter][0]  	= dataConso[iter-1][0] + inter;	
	// Calcul
	// de
	// la
	// vitesse
	if(bool){ 	// l'utilisateur
				// accelere
		dataConso[iter][2]  = dataConso[iter-1][2] + accMax * inter;
	}else{ 		// l'utilisateur
				// decelere
		dataConso[iter][2]  = dataConso[iter-1][2] - decMax*inter;
	}
	dataConso[iter][1]  	= dataConso[iter][2] * 3.6;
	// Calcul
	// de
	// la
	// distance
	// cumulee
	dataConso[iter][3]  	= distTampon + dataConso[iter][2] * inter;
	// Calcul
	// de
	// la
	// pente
	dataConso[iter][4] 		= recherchePente(dataConso[iter][3]).pente;
	if(boolPenteFixe){
		dataConso[iter][4] = penteFixe;
	}
	
	var resultats = calculdataConso(iter,energie,"inter",dataConso)
	
	return {
		energie: resultats.energie,
		bool: resultats.bool,
	}
}
/**
 * Calcul des points du parcours
 * 
 * @param iter :
 *            iterateur du tableau a remplir
 * @param distTampon :
 *            distance tampon deja parcouru
 * @param tempsTampon :
 *            temps actuel
 * @param energie :
 *            energie pour le calcul du taux de charge de la batterie
 * @param iterCycle :
 *            iterateur pour le parcours du cycle
 * @param tabTampon :
 *            tableau tampon, cycle sur lequel on recupere les donnees de vitesse
 */
function calcdataConsoCycle(iter,distTampon,tempsTampon,energie,iterCycle,tabTampon){

	var bool = false;	// Boolean
						// vrai
						// si
						// on
						// doit
						// arreter
						// la
						// resolution
						// (panne
						// de
						// batterie)
	// Calcul
	// du
	// temps
	dataConso[iter][0] 		= tempsTampon + tabTampon[iterCycle][0];
	// Calcul
	// des
	// vitesses
	dataConso[iter][1] 		= tabTampon[iterCycle][1];	
	dataConso[iter][2] 		= tabTampon[iterCycle][2];
	
	// Calcul
	// de
	// la
	// distance
	// cumulee
	dataConso[iter][3] 		= distTampon + tabTampon[iterCycle][3];	
	// Calcul
	// de
	// la
	// pente
	if(typeResolution==1){
		dataConso[iter][4]=tabTampon[iterCycle][5];
	}
	else{
		dataConso[iter][4] 		= recherchePente(dataConso[iter][3]).pente;
	}
	
	if(boolPenteFixe){
		dataConso[iter][4] = penteFixe;
	}
	var resultats = calculdataConso(iter,energie,"cycle",dataConso)
	return {
		energie: resultats.energie,
		bool: resultats.bool,
	}
}

/**
 * Calcul des energies pour un trajet d'un cycle standard
 * 
 * @param iter :
 *            iterateur du tableau a remplir
 * @param energie :
 *            energie pour le calcul du taux de charge de la batterie
 * @param typeCalcul :
 *            type de calcul
 * @param tab :
 *            tableau a remplir
 */

/**
 * var mapEfficiency = new Array(); //Tableau pour cartographier le rendement moteur mapEfficiency[0]=new Array(); mapEfficiency[0][0]="C/w";
 */
var lastVitesseMemory=0;
function calculdataConso(iter,energie,typeCalcul,tab){
	
	// Calcul
	// de
	// delta
	// T
	tab[iter][5] 		= tab[iter][0] - tab[iter-1][0];
	// Calcul
	// de
	// l'acceleration
	if(memory==2 && iter==2 && lastVitesseMemory != 0){
		tab[iter-1][2]=lastVitesseMemory/3.6;
		tab[iter-1][1]=lastVitesseMemory;
	}

	var tempAcc 		= calcPhase(tab[iter-1][2],tab[iter][2],tab[iter-1][0],tab[iter][0]);
	tab[iter][6] 		= tempAcc.acc;
	tab[iter][7] 		= tempAcc.phase;
	
	
	
	 if(tab[iter][6]< -5){ tab[iter][6] = -5}				//filtrage accMax accMin
	 else if(tab[iter][6]>5){ tab[iter][6] = 5};
	// Calcul
	// des
	// forces
	// en
	// debut
	// de
	// phase

	var tempFDebut	= calcForce(tab[iter-1][2],tab[iter][6],tab[iter][4]);
	tab[iter][8]  	= tempFDebut.Fr;
	tab[iter][9]  	= tempFDebut.Fair;
	tab[iter][10] 	= tempFDebut.Fp;	
	tab[iter][11] 	= tempFDebut.Facc;
	tab[iter][12] 	= tempFDebut.Fm;
	// Calcul
	// des
	// puissances
	// en
	// debut
	// de
	// phase
	var PmecDebut	= calcPuissanceMeca(tab[iter-1][2],tab[iter][12]);
	tab[iter][13] 	= PmecDebut.Pmoteur;
	if(typeVehicule =="VE" || typeVehicule =="VP" ){
		tab[iter][14] 	= calcPuissanceElec(tab[iter][13]);
	}
	if(typeVehicule == "VPU"){
		tab[iter][14] = tab[iter][13];
	}
	tab[iter][15] 	= tab[iter][12]*RRoue;	
		
	// Calcul
	// des
	// forces
	// en
	// fin
	// de
	// phase

	var tempFFin	= calcForce(tab[iter][2],tab[iter][6],tab[iter][4]);
	tab[iter][16]  	= tempFFin.Fr;
	tab[iter][17]  	= tempFFin.Fair;
	tab[iter][18] 	= tempFFin.Fp;	
	tab[iter][19] 	= tempFFin.Facc;
	tab[iter][20] 	= tempFFin.Fm;
	// Calcul
	// des
	// puissances
	// en
	// fin
	// de
	// phase
	
	//console.log("vitesse", tab[iter][2],"acceleration",tab[iter][6],"puissance mot",tab[iter][20]);
	
	var PmecFin		= calcPuissanceMeca(tab[iter][2],tab[iter][20]);
	tab[iter][21] 	= PmecFin.Pmoteur;
	
	if(typeVehicule =="VE" || typeVehicule =="VP"){
		
		tab[iter][22] 	= calcPuissanceElec(tab[iter][21]);
	}
	if(typeVehicule == "VPU"){
		if(document.getElementById("modeleMoteur").value=="1"){ //Modèle Altran prenant en compte les pertes de puissances dans le moteurs : pertes fer, pertes thermiques, pertes mécaniques
			tab[iter][21] = tab[iter][2]*tab[iter][20]*rend_gear; //Puissance de sortie de moteur
			if (tab[iter][21]>0){
				var couple = tab[iter][20]*RRoue/valRapportTrans; //Couple de sortie
				var regime = (tab[iter][2]/RRoue)*valRapportTrans;//Régime de sortie
				var factPuiss = (-0.8404*Math.pow(tab[iter][21],2)/Math.pow((PmotorMax*1000),2))+(1.5268*Math.abs(tab[iter][21])/(PmotorMax*1000))+0.1497;
				var puissEntree = (-3*Math.pow(valTensionMot,2)*Math.pow(factPuiss,2)+valTensionMot*factPuiss*Math.sqrt(3)*Math.sqrt(3*Math.pow(valTensionMot,2)*Math.pow(factPuiss,2)-12*valResistPhaseMot*(0.05*tab[iter][21]+coefFrotVis*Math.pow(regime,2)+(tab[iter][21]))))/(-6*valResistPhaseMot);
				if ((puissEntree/(PmotorMax*1000))>=0.0592){
					nconv = 0.0898*(Math.abs(puissEntree)/(PmotorMax*1000))+0.8744;
				}
				else{
					nconv = 6.4804*(Math.abs(puissEntree)/(PmotorMax*1000))+0.1497;
				}
				tab[iter][22]=puissEntree/nconv;
				
			}
			else if (tab[iter][21]<0){
				var couple = tab[iter][20]*RRoue/valRapportTrans;
				var regime = (tab[iter][2]/RRoue)*valRapportTrans;
				var factPuiss = (-0.8404*Math.pow(tab[iter][21],2)/Math.pow((PmotorMax*1000),2))+(1.5268*Math.abs(tab[iter][21])/(PmotorMax*1000))+0.1497;
				var puissEntree = (-3*Math.pow(valTensionMot,2)*Math.pow(factPuiss,2)+valTensionMot*factPuiss*Math.sqrt(3)*Math.sqrt(3*Math.pow(valTensionMot,2)*Math.pow(factPuiss,2)-12*valResistPhaseMot*(0.05*tab[iter][21]+coefFrotVis*Math.pow(regime,2)-(tab[iter][21]))))/(6*valResistPhaseMot);
				if (Math.abs(puissEntree/(PmotorMax*1000))>=0.0592){
					nconv = 0.0898*(Math.abs(puissEntree)/(PmotorMax*1000))+0.8744;
				}
				else{
					nconv = 6.4804*(Math.abs(puissEntree)/(PmotorMax*1000))+0.1497;
				}
				tab[iter][22]=puissEntree*nconv;
				//alert("1 : "+nconv+" "+Math.abs(tab[iter][21])+" "+Math.abs(tab[iter][22]))
			}
			else {
				tab[iter][22]=0
			}
		}
		else if(document.getElementById("modeleMoteur").value=="2"){
			tab[iter][21] = tab[iter][2]*tab[iter][20]*rend_gear;
			if(tab[iter][21]>0){
				tab[iter][22]=tab[iter][21]/0.75;
			}
			else if(tab[iter][21]<0){
				tab[iter][22]=tab[iter][21]*0.5*0.75;
			}
			else{tab[iter][22]=0}
			
		}
		else if(document.getElementById("modeleMoteur").value=="3"){
			tab[iter][21] = tab[iter][2]*tab[iter][20]*rend_gear;
			var PmecFin		= calcPuissanceMeca(tab[iter][2],tab[iter][20]);
			tab[iter][22] 	= calcPuissanceElec(PmecFin.Pmoteur);
		}
		
	}
	
	tab[iter][23] 	= tab[iter][20]*RRoue;
	// Calcul
	// des
	// maximums
	// par
	// phase
	tab[iter][24] 	= maxAbsolu(tab[iter][13],tab[iter][21])/1000; 
	if(typeVehicule =="VE" || typeVehicule =="VP" || typeVehicule == "VPU"){
		tab[iter][25] 	= maxAbsolu(tab[iter][14],tab[iter][22])/1000; 
	}
	tab[iter][26] 	= maxAbsolu(tab[iter][15],tab[iter][23]);
	// Calcul
	// des
	// energies
	tab[iter][27]		= 0.5*(tab[iter][13]+tab[iter][21])*tab[iter][5]/3600;
	if(typeVehicule=="VE" || typeVehicule=="VP" || typeVehicule == "VPU"){
		tab[iter][28]		= 0.5*(tab[iter][14]+tab[iter][22])*tab[iter][5]/3600;
		// Calcul
		// de
		// l'energie
		// consommee
		// et
		// produite
		if(tab[iter][28] > 0){	// Energie
								// consommee
			tab[iter][29] = tab[iter-1][29] + tab[iter][28];
			tab[iter][30] = tab[iter-1][30];
		}else{							// Energie
										// produite
			tab[iter][29] = tab[iter-1][29];
			tab[iter][30] = tab[iter-1][30] - tab[iter][28];
		}
	}
	else if(typeVehicule=="VT" || typeVehicule=="VT"){
		tab[iter][28]	=0;
		if(tab[iter][27] > 0){
			tab[iter][29] = tab[iter-1][29] + tab[iter][27];
		}
		else{
			tab[iter][29] = tab[iter-1][29];
		}
		tab[iter][30] = 0;
	}
	// Calcul
	// du
	// type
	// de
	// parcours
	if(typeCalcul=="inter"){
		tab[iter][31] = tab[iter-1][31]
	}
	else{
		if(typeCalcul == "cycle"){
			tab[iter][31]		= rechercheStep(tab[iter][3]);
		}
		else if(typeCalcul == "cycleStandard"){
			dataCycle[iter][31]		= 'cycleStandard'+typeVehicule;
			
		}
	}

	// Calcul
	// du
	// taux
	// de
	// charge
	// de
	// la
	// batterie
	var energieT 				= energie - (tab[iter][29]-tab[iter-1][29]) + (tab[iter][30]-tab[iter-1][30]);
	if(typeCalcul!="cycleStandard"){
		// Recherche
		// de
		// la
		// latitude
		// longitude
		// du
		// point
		var temp = rechercheCoord(tab[iter][3])
		tab[iter][33] = temp.lat;
		tab[iter][34] = temp.long;
	}
	if(typeVehicule =="VE"){
		/*
		 * si la deceleration est superieur au seuil maximal permettant d'utiliser le freinage regeneratif (0.3g pour les systemes actuels) alors il faut plafonner la recuperation d'energie avec ce seuil de deceleration
		 */
		
		if(tab[iter][6] < -seuilDec){		
	
			var FfinTampon  = calcForce(tab[iter][2],-seuilDec,tab[iter][4]);	
			var PmecaT		= calcPuissanceMeca(tab[iter][2],FfinTampon.Fm);
			if (PmecaT.Pmoteur<0){
				var Pelec		= calcPuissanceElec(PmecaT.Pmoteur);
				PmecFin			= PmecaT;
				var eMecaT = Pelec/1000;
				var eElecT = Pelec*tab[iter][5]/3600;
				
				tab[iter][30]  = tab[iter-1][30] - eElecT;
				energieT = energie - eElecT;
			}
			else {
				tab[iter][30]  = tab[iter-1][30]
				energieT = energie
			}
		}
		tab[iter][32] 	= Math.round(10*energieT/capBatterie*100)/10/1000;
		
		
		if(tab[iter][32] < socMin){ // Si
									// la
									// batterie
									// descend
									// en
									// dessous
									// de
									// socMin%
									// il
									// faut
									// la
									// recharger
			var bool = true;
			// tab[iter][32]
			// =
			// socMin;
			
		}else if(tab[iter][32] > 100){ // La
										// batterie
										// ne
										// depasse
										// pas
										// 100%,
										// si
										// on
										// est
										// a
										// 100%
										// l'energie
										// n'est
										// pas
										// recuperee
			tab[iter][32] = 100;
			tab[iter][30] = tab[iter-1][30];
		}
		
		// Ajout
		// d'un
		// marqueur
		// si
		// c'est
		// le
		// premier
		// point
		// ou
		// l'autonomie
		// atteind
		// socMin%
		if(bool && !boolReDo && typeVehicule == "VE"){
			indiceP = iter;
			addMarker(tab[iter][33],tab[iter][34],'','',false,markersPanne); // Marqueur
																				// différent
																				// pour
																				// les
																				// points
																				// de
																				// pannes
		}
		
		// calcul
		// du
		// pas
		// de
		// latitude
		var paslat = Math.abs(tab[iter-1][33]-tab[iter][33]);
		// calcul
		// du
		// pas
		// de
		// longitude
		var paslong = Math.abs(tab[iter-1][34]-tab[iter][34]);
		

		// Recharge
		// de
		// la
		// batterie
		// si
		// on
		// passe
		// par
		// une
		// borne
		if(boolBorne && Math.abs(tab[iter][33]-latBtab2[0]) < paslat && Math.abs(tab[iter][34]-longBtab2[0]) < paslong){
			tab[iter][32] = 100;
			energieT = capBatterie*1000;
			tpsBorne = iter-1;
			
			// Suppression
			// des
			// coordonnées
			// de
			// la
			// borne
			// qui
			// vient
			// d'être
			// utilisée
			// pour
			// ne
			// pas
			// recharger
			// une
			// deuxième
			// fois
			// à
			// cette
			// borne
			latBtab2.shift();
			longBtab2.shift();
			
			// Compteur
			// pour
			// le
			// numéro
			// de
			// la
			// borne
			NumBorne += 1;
			alert("Le véhicule s'est rechargé à la borne numéro : " + NumBorne);
		}
		
		// Energie
		// perdue
		tab[iter][37] = tab[iter-1][37]+0.5*(calcPuissanceElec(PmecDebut.Pdissipee) + calcPuissanceElec(PmecFin.Pdissipee))*tab[iter][5]/3600;
	}
	else if(typeVehicule=="VT"){
		// L'energie
		// perdue
		// est
		// calculee
		// plus
		// tard
		// et
		// est
		// mise
		// dans
		// une
		// autre
		// colonne
		// du
		// tableau
		tab[iter][37] = 0;
		
		// calcul
		// :
		// prediction
		// suivant
		// le
		// profil
		// utilisateur
		// les
		// valeurs
		// suivantes
		// peuvent
		// toujours
		// être
		// calculées
		
		// Vitessse
		// phase
		tab[iter][38] = pasVitesses*Math.round(0.5*(tab[iter][1]+tab[iter-1][1])/pasVitesses);
		if(tab[iter][38]<=minvecVitesses){
			tab[iter][38] = pasVitesses;
		}
		if(tab[iter][38]>maxvecVitesses){
			tab[iter][38] = maxvecVitesses;
		}		
		// Pente
		// phase
		tab[iter][39] = pasPente*Math.round(0.5*(tab[iter][4]+tab[iter-1][4])*100/pasPente);
		// On
		// ramene
		// au
		// max
		// ou
		// au
		// min
		// pour
		// permettre
		// au
		// calcul
		// de
		// continuer
		if(tab[iter][39]<minvecPente){
			tab[iter][39] = minvecPente;
		}
		if(tab[iter][39]>maxvecPente){
			tab[iter][39] = maxvecPente;
		}	
		// rapport
		// enclenche
		// phase
		// (indice)
		tab[iter][40] = MapVitesses[(tab[iter][38]-minvecVitesses)/pasVitesses][(tab[iter][39]-minvecPente)/pasPente];
		// rapport
		// de
		// reduction
		// phase
		tab[iter][41] = dataVitesses[tab[iter][40]];
		// regime
		// moteur
		// phase
		tab[iter][42] = tab[iter-1][2]*60*tab[iter][41]/(RRoue*2*Math.PI); 
		// On
		// ramene
		// au
		// max
		// ou
		// au
		// min
		// pour
		// permettre
		// au
		// calcul
		// de
		// continuer
		if(tab[iter][42]<=minvecNmot){
			tab[iter][42] = minvecNmot+pasNmot;
		}
		if(tab[iter][42]>maxvecNmot){
			tab[iter][42] = maxvecNmot;
		}
		// couple
		// moteur
		// phase
		tab[iter][43] = tab[iter][15]/tab[iter][41]/rend_gear; 
		// couple
		// max
		// phase
		tab[iter][44] = coef[3]*Math.pow(tab[iter][42],3)+coef[2]*Math.pow(tab[iter][42],2)+coef[1]*tab[iter][42]+coef[0];
		// pourcentage
		// de
		// couple
		// phase
		if(tab[iter][43]>0){
			tab[iter][45] = tab[iter][43]/tab[iter][44]*100;
			if(tab[iter][45]<pasCmot){
				tab[iter][45] = pasCmot;
			}
			if(tab[iter][45]>100){
				tab[iter][45] = 100;
			}
		}
		else{
			tab[iter][45] = 0;
		}
		// On
		// ramene
		// au
		// max
		// ou
		// au
		// min
		// pour
		// permettre
		// au
		// calcul
		// de
		// continuer

		// calculs
		// possibles
		// seulement
		// quand
		// on
		// est
		// en
		// phase
		// motrice
		// :
		if(tab[iter][27]>0){
						
			// calcul
			// :
			// prediction
			// suivant
			// le
			// profil
			// utilisateur
												
			// indice
			// regime
			// moteur
			// phase
			tab[iter][46] = (pasNmot*(Math.round(tab[iter][42]/pasNmot))-minvecNmot)/pasNmot; 	
			// indice
			// pourcentage
			// de
			// couple
			// phase
			tab[iter][47] = (pasCmot*(Math.round(tab[iter][45]/pasCmot))-minvecCmot)/pasCmot;	
			// calcul
			// consommation
			// specifique
			// d'energie
			tab[iter][48] = Math.round(Map_cse_ex[tab[iter][46]][tab[iter][47]])
			// calcul
			// de
			// la
			// consommation
			// en
			// litre
			tab[iter][49] = tab[iter][48]*tab[iter][27]/1000/rhoessence;
			// calcul
			// de
			// la
			// consommation
			// en
			// litre/100
			// km
			tab[iter][50] = tab[iter][49]/(tab[iter][2]*tab[iter][5])*100000;
			// calcul
			// des
			// rejets
			// de
			// CO2
			// en
			// kg
			tab[iter][51] = tab[iter][49]*rhoessence*ratioCO2/1000;
			// calcul
			// de
			// l'energie
			// produite
			// par
			// l'essence
			tab[iter][52] = tab[iter][49]*rhoessence*essencePCS;
			// calcul
			// de
			// l'energie
			// dissipee
			// en
			// chaleur
			tab[iter][53] = tab[iter][52]-tab[iter][27];
			
			// fin
			// calcul
			// :
			// prediction
			// suivant
			// le
			// profil
			// utilisateur
			
			
			// calcul
			// consommation
			// optimale
			// avec
			// phases
			// de x
			// secondes
			
			// a
			// l'initialisation
			// du
			// calcul,
			// il
			// est
			// necessaire
			// de
			// definir
			// la
			// vitesse
			// enclenchee
			// (pour
			// le
			// calcul
			// de
			// conso
			// opti
			// qui
			// a
			// pour
			// parametre
			// la
			// vitesse
			// a la
			// phase
			// precedente)

			if(boolInitCalcul){
				tab[iter][67] = tab[iter][40];	
			}
			// si
			// l'energie
			// etait
			// nulle
			// a
			// l'indice
			// precedent,
			// ou
			// si
			// la
			// phase
			// precedente
			// est
			// finie,
			// on
			// debute
			// une
			// nouvelle
			// phase
			// on
			// initialise
			// les
			// compteurs,
			// la
			// puissance,
			// la
			// vitesse,
			// le
			// couple
			// a la
			// roue
			// et
			// la
			// vitesse
			// de
			// rotation
			// de
			// la
			// roue
			if(tab[iter-1][27]<=0 || boolChgtPhase){
				compteurPhase = 1;
				timerPhase = tab[iter][5];
				tab[iter][59] = tab[iter][27];
				tab[iter][60] = tab[iter][2];
				tab[iter][61] = 0.5*(tab[iter][15]+tab[iter][23]);
				tab[iter][62] = tab[iter][2]/RRoue*60/2/Math.PI;
				boolChgtPhase = false;
				indiceReprisePhase = iter;
			}
			else{
				
				// on
				// est
				// dans
				// la
				// phase,
				// on
				// incremente
				// les
				// comteurs,
				// on
				// moyenne
				// la
				// puissance,
				// la
				// vitesse,
				// le
				// couple
				// a la
				// roue
				// et
				// la
				// vitesse
				// de
				// rotation
				// de
				// la
				// roue
				compteurPhase ++;
				timerPhase += tab[iter][5];
				tab[iter][59] = (tab[iter][27]+(compteurPhase-1)*tab[iter-1][59])/compteurPhase;
				tab[iter][60] = (tab[iter][2]+(compteurPhase-1)*tab[iter-1][60])/compteurPhase;
				tab[iter][61] = (0.5*(tab[iter][15]+tab[iter][23])+(compteurPhase-1)*tab[iter-1][61])/compteurPhase;
				tab[iter][62] = (tab[iter][2]/RRoue*60/2/Math.PI+(compteurPhase-1)*tab[iter-1][62])/compteurPhase;			

				// on
				// va
				// changer
				// de
				// phase
				// car
				// la
				// duree
				// est
				// atteinte
				if(timerPhase>=dureePhase){
					// on
					// calcule
					// les
					// variables
					// pour
					// une
					// consommation
					// optimale
					var tempOpti = calcConsoOpti(tab[iter][62],tab[iter][61],tab[indiceReprisePhase-1][67]);						
					tab[iter][63] = tempOpti.RegimeMoteurOpti;
					tab[iter][64] = tempOpti.CoupleMoteurOpti;
					tab[iter][65] = tempOpti.ChargeMoteurOpti;
					tab[iter][66] = tempOpti.ConsoOpti;
					tab[iter][67] = tempOpti.RapportOpti;			
					// calcul
					// de
					// la
					// consommation
					// en
					// litre
					tab[iter][68] = tab[iter][66]*tab[iter][59]/1000/rhoessence;
					// calcul
					// de
					// la
					// consommation
					// en
					// litre/100
					// km
					tab[iter][69] = tab[iter][68]/(tab[iter][60]*tab[iter][5])*100000;
					// calcul
					// des
					// rejets
					// de
					// CO2
					// en
					// kg
					tab[iter][70] = tab[iter][68]*rhoessence*ratioCO2/1000;
					// calcul
					// de
					// l'energie
					// produite
					// par
					// l'essence
					tab[iter][71] = tab[iter][68]*rhoessence*essencePCS;
					// calcul
					// de
					// l'energie
					// dissipee
					// en
					// chaleur
					tab[iter][72] = tab[iter][71]-tab[iter][59];
					// toutes
					// les
					// variables
					// a
					// chaque
					// point
					// de
					// la
					// phase
					// prennent
					// la
					// valeur
					// optimale
					// calculee
					// sur
					// la
					// phase
					for(var z=indiceReprisePhase;z<iter;z++){
						tab[z][63] = tab[iter][63];
						tab[z][64] = tab[iter][64];
						tab[z][65] = tab[iter][65];
						tab[z][66] = tab[iter][66];
						tab[z][67] = tab[iter][67];
						tab[z][68] = tab[iter][68];
						tab[z][69] = tab[iter][69];
						tab[z][70] = tab[iter][70];
						tab[z][71] = tab[iter][71];
						tab[z][72] = tab[iter][72];
					}				
					boolChgtPhase = true;				
				}
				
			}

			// fin
			// calcul
			// conso
			// opti
		}
		else{
			// quand
			// la
			// force
			// motrice
			// est
			// negative,
			// la
			// consommation
			// et
			// les
			// variables
			// qui
			// en
			// decoulent
			// sont
			// nulles
			tab[iter][49] = 0;
			tab[iter][50] = 0;
			tab[iter][51] = 0;
			tab[iter][52] = 0;
			tab[iter][53] = 0;			
			// si
			// une
			// phase
			// etait
			// en
			// cours
			// de
			// calcul
			// on
			// arrete
			// la
			// phase
			// a
			// l'indice
			// precedent
			// et
			// on
			// effectue
			// le
			// calcul
			// de
			// consommation
			// optimale
			// pour
			// la
			// phase
			if(!boolChgtPhase){
				compteurPhase ++;
				timerPhase = dureePhase;
				var tempOpti = calcConsoOpti(tab[iter-1][62],tab[iter-1][61],tab[indiceReprisePhase-1][67]);		
				tab[iter-1][63] = tempOpti.RegimeMoteurOpti;
				tab[iter-1][64] = tempOpti.CoupleMoteurOpti;
				tab[iter-1][65] = tempOpti.ChargeMoteurOpti;
				tab[iter-1][66] = tempOpti.ConsoOpti;
				tab[iter-1][67] = tempOpti.RapportOpti;			
				// calcul
				// de
				// la
				// consommation
				// en
				// litre
				tab[iter-1][68] = tab[iter-1][66]*tab[iter-1][59]/1000/rhoessence;
				// calcul
				// de
				// la
				// consommation
				// en
				// litre/100
				// km
				tab[iter-1][69]	= tab[iter-1][68]/(tab[iter-1][60]*tab[iter-1][5])*100000;
				// calcul
				// des
				// rejets
				// de
				// CO2
				// en
				// kg
				tab[iter-1][70] = tab[iter-1][68]*rhoessence*ratioCO2/1000;
				// calcul
				// de
				// l'energie
				// produite
				// par
				// l'essence
				tab[iter-1][71] = tab[iter-1][68]*rhoessence*essencePCS;
				// calcul
				// de
				// l'energie
				// dissipee
				// en
				// chaleur
				tab[iter-1][72] = tab[iter-1][68]-tab[iter-1][59];	
				// toutes
				// les
				// variables
				// a
				// chaque
				// point
				// de
				// la
				// phase
				// prennent
				// la
				// valeur
				// optimale
				// (moyenne)
				// calculee
				// sur
				// la
				// phase
				for(var z=indiceReprisePhase;z<iter-1;z++){
					tab[z][63] = tab[iter-1][63];
					tab[z][64] = tab[iter-1][64];
					tab[z][65] = tab[iter-1][65];
					tab[z][66] = tab[iter-1][66];
					tab[z][67] = tab[iter-1][67];
					tab[z][68] = tab[iter-1][68];
					tab[z][69] = tab[iter-1][69];
					tab[z][70] = tab[iter-1][70];
					tab[z][71] = tab[iter-1][71];
					tab[z][72] = tab[iter-1][72];
				}		
				boolChgtPhase = true;
			}
			
			// quand
			// la
			// force
			// motrice
			// est
			// negative,
			// on
			// suppose
			// que
			// la
			// vitesse
			// enclenchee
			// reste
			// la
			// même
			// la
			// consommation
			// et
			// les
			// variables
			// qui
			// en
			// decoulent
			// sont
			// nulles
			tab[iter][67] = tab[iter-1][67];
			tab[iter][63] = tab[iter][42]/tab[iter][41]*dataVitesses[tab[iter][67]];
			tab[iter][64] = tab[iter][43]/tab[iter][41]*dataVitesses[tab[iter][67]];
			if(tab[iter][64]>0){
				tab[iter][65] = tab[iter][64]/(coef[3]*Math.pow(tab[iter][63],3)+coef[2]*Math.pow(tab[iter][63],2)+coef[1]*tab[iter][63]+coef[0])*100;
			}
			else{
				tab[iter][65] = 0;
			}
			// On
			// ramene
			// au
			// max
			// ou
			// au
			// min
			if(tab[iter][65]<pasCmot){
				tab[iter][65] = 0;
			}
			if(tab[iter][65]>100){
				tab[iter][65] = 100;
			}
			tab[iter][68] = 0;
			tab[iter][69] = 0;
			tab[iter][70] = 0;
			tab[iter][71] = 0;
			tab[iter][72] = 0;	
		}		
		
		// calculs
		// des
		// totaux
		// calcul
		// de
		// la
		// consommation
		// totale
		// en
		// litres
		tab[iter][54] = tab[iter-1][54]+tab[iter][49];
		// Emissions
		// totales
		// de
		// CO2
		tab[iter][56] = tab[iter-1][56]+tab[iter][51];
		// calcul
		// de
		// l'energie
		// totale
		// produite
		// par
		// l'essence
		tab[iter][57] = tab[iter-1][57]+tab[iter][52];
		// calcul
		// de
		// l'energie
		// totale
		// dissipee
		// en
		// chaleur
		tab[iter][58] = tab[iter-1][58]+tab[iter][53];
		
		// on
		// change
		// de
		// phase
		// : on
		// calcule
		// les
		// totaux
		// depuis
		// la
		// derniere
		// fois
		// qu'on
		// a
		// change
		// de
		// phase
		if(boolChgtPhase && !boolInitCalcul && timerPhase>=dureePhase){
			for(var z=indiceReprisePhase;z<=iter;z++){
			// calcul
			// de
			// la
			// consommation
			// totale
			// en
			// litres
			tab[z][73] = tab[z-1][73]+tab[z][68];
			// Emissions
			// totales
			// de
			// CO2
			tab[z][75] = tab[z-1][75]+tab[z][70];
			// calcul
			// de
			// l'energie
			// totale
			// produite
			// par
			// l'essence
			tab[z][76] = tab[z-1][76]+tab[z][71];
			// calcul
			// de
			// l'energie
			// totale
			// dissipee
			// en
			// chaleur
			tab[z][77] = tab[z-1][77]+tab[z][72];
			}
			
		}
		// sinon
		// on
		// fait
		// avancer
		// les
		// variables
		// pour
		// le
		// cas
		// de
		// l'energie
		// nulle
		else{
			// calcul
			// de
			// la
			// consommation
			// totale
			// en
			// litres
			tab[iter][73] = tab[iter-1][73];
			// Emissions
			// totales
			// de
			// CO2
			tab[iter][75] = tab[iter-1][75];
			// calcul
			// de
			// l'energie
			// totale
			// produite
			// par
			// l'essence
			tab[iter][76] = tab[iter-1][76];
			// calcul
			// de
			// l'energie
			// totale
			// dissipee
			// en
			// chaleur
			tab[iter][77] = tab[iter-1][77];
			
		}
		
		// au
		// debut
		// du
		// calcul,
		// il
		// faut
		// initialiser
		// les
		// valeurs
		// du
		// tableau
		// en
		// plus
		if(boolChgtPhase && boolInitCalcul && iter!=2){
			tab[indiceReprisePhase-1][73] = tab[indiceReprisePhase-1][68];
			tab[indiceReprisePhase-1][75] = tab[indiceReprisePhase-1][70];
			tab[indiceReprisePhase-1][76] = tab[indiceReprisePhase-1][71];
			tab[indiceReprisePhase-1][77] = tab[indiceReprisePhase-1][72];
			if(timerPhase>=dureePhase){			
				for(var z=indiceReprisePhase;z<=iter;z++){
				// calcul
				// de
				// la
				// consommation
				// totale
				// en
				// litres
				tab[z][73] = tab[z-1][73]+tab[z][68];
				// Emissions
				// totales
				// de
				// CO2
				tab[z][75] = tab[z-1][75]+tab[z][70];
				// calcul
				// de
				// l'energie
				// totale
				// produite
				// par
				// l'essence
				tab[z][76] = tab[z-1][76]+tab[z][71];
				// calcul
				// de
				// l'energie
				// totale
				// dissipee
				// en
				// chaleur
				tab[z][77] = tab[z-1][77]+tab[z][72];
				}
			}
			boolInitCalcul = false;			
		}
		
		// dans
		// le
		// cas
		// de
		// l'analyse
		// du
		// trajet,
		// calcul
		// de
		// la
		// conduite
		// optimale
		// pour
		// consommer
		// moins
		if(boolTrajet){
			
			// calculs
			// possibles
			// seulement
			// quand
			// on
			// est
			// en
			// phase
			// motrice
			// :
			if(tab[iter][27]>0){
			// if(tab[iter][1]-tab[iter-2][1]>=0){
				
				// calcul
				// consommation
				// optimale
				// avec
				// phases
				// de x
				// secondes
				
				// a
				// l'initialisation
				// du
				// calcul,
				// il
				// est
				// necessaire
				// de
				// definir
				// la
				// vitesse
				// enclenchee
				// (pour
				// le
				// calcul
				// de
				// conso
				// opti
				// qui
				// a
				// pour
				// parametre
				// la
				// vitesse
				// a la
				// phase
				// precedente)
				
				if(boolInitCalculTrajet){
					tab[iter][92] = tab[iter][78];	
				}
				// si
				// l'energie
				// etait
				// nulle
				// a
				// l'indice
				// precedent,
				// ou
				// si
				// la
				// phase
				// precedente
				// est
				// finie,
				// on
				// debute
				// une
				// nouvelle
				// phase
				// on
				// initialise
				// les
				// compteurs,
				// la
				// puissance,
				// la
				// vitesse,
				// le
				// couple
				// a la
				// roue
				// et
				// la
				// vitesse
				// de
				// rotation
				// de
				// la
				// roue
				// if(tab[iter-1][1]-tab[iter-2][1]>=0
				// ||
				// boolChgtPhaseTrajet){
				if(tab[iter-1][27]<=0 || boolChgtPhaseTrajet){
					compteurPhaseTrajet = 1;
					timerPhaseTrajet = tab[iter][5];
					tab[iter][85] = tab[iter][2];
					tab[iter][86] = tab[iter][80]*dataVitesses[tab[iter][78]];
					tab[iter][87] = tab[iter][79]/dataVitesses[tab[iter][78]];
					tab[iter][99] = tab[iter][5];
					boolChgtPhaseTrajet = false;
					indiceReprisePhaseTrajet = iter;
				}
				else{
					
					// on
					// est
					// dans
					// la
					// phase,
					// on
					// incremente
					// les
					// compteurs,
					// on
					// moyenne
					// la
					// puissance,
					// la
					// vitesse,
					// le
					// couple
					// a la
					// roue
					// et
					// la
					// vitesse
					// de
					// rotation
					// de
					// la
					// roue
					compteurPhaseTrajet ++;
					timerPhaseTrajet += tab[iter][5];
					tab[iter][99] = (tab[iter][5]+(compteurPhaseTrajet-1)*tab[iter-1][99])/compteurPhaseTrajet;
					tab[iter][85] = (tab[iter][2]+(compteurPhaseTrajet-1)*tab[iter-1][85])/compteurPhaseTrajet;
					tab[iter][86] = (tab[iter][80]*dataVitesses[tab[iter][78]]+(compteurPhaseTrajet-1)*tab[iter-1][86])/compteurPhaseTrajet;
					tab[iter][87] = (tab[iter][79]/dataVitesses[tab[iter][78]]+(compteurPhaseTrajet-1)*tab[iter-1][87])/compteurPhaseTrajet;	

					// on
					// va
					// changer
					// de
					// phase
					// car
					// la
					// duree
					// est
					// atteinte
					if(timerPhaseTrajet>=dureePhase){
						// on
						// calcule
						// les
						// variables
						// pour
						// une
						// consommation
						// optimale
						var tempOpti = calcConsoOpti(tab[iter][87],tab[iter][86],tab[indiceReprisePhaseTrajet-1][92],tab[iter][78]);						
						tab[iter][88] = tempOpti.RegimeMoteurOpti;
						tab[iter][89] = tempOpti.CoupleMoteurOpti;
						tab[iter][90] = tempOpti.ChargeMoteurOpti;
						tab[iter][91] = tempOpti.ConsoOpti;
						tab[iter][92] = tempOpti.RapportOpti;
						// calcul
						// de
						// la
						// consommation
						// en
						// litre
						tab[iter][93] = tab[iter][91]*tab[iter][86]*tab[iter][87]*2*Math.PI/60*tab[iter][99]/3600/1000/rhoessence;
						// calcul
						// de
						// la
						// consommation
						// en
						// litre/100
						// km
						tab[iter][94] = tab[iter][93]/(tab[iter][85]*tab[iter][99])*100000;
						// calcul
						// des
						// rejets
						// de
						// CO2
						// en
						// kg
						tab[iter][95] = tab[iter][93]*rhoessence*ratioCO2/1000;
						// toutes
						// les
						// variables
						// a
						// chaque
						// point
						// de
						// la
						// phase
						// prennent
						// la
						// valeur
						// optimale
						// calculee
						// sur
						// la
						// phase
						for(var z=indiceReprisePhaseTrajet;z<iter;z++){
							tab[z][88] = tab[iter][88];
							tab[z][89] = tab[iter][89];
							tab[z][90] = tab[iter][90];
							tab[z][91] = tab[iter][91];
							tab[z][92] = tab[iter][92];
							tab[z][93] = tab[iter][93];
							tab[z][94] = tab[iter][94];
							tab[z][95] = tab[iter][95];
						}				
						boolChgtPhaseTrajet = true;				
					}
					
				}
				
				// fin
				// calcul
				// conso
				// opti
			}
			else{

							
				// si
				// une
				// phase
				// etait
				// en
				// cours
				// de
				// calcul
				// on
				// arrete
				// la
				// phase
				// a
				// l'indice
				// precedent
				// et
				// on
				// effectue
				// le
				// calcul
				// de
				// consommation
				// optimale
				// pour
				// la
				// phase
				if(!boolChgtPhaseTrajet){
					compteurPhaseTrajet ++;
					timerPhaseTrajet = dureePhase;
					var tempOpti = calcConsoOpti(tab[iter-1][87],tab[iter-1][86],tab[indiceReprisePhaseTrajet-1][92],tab[iter][78]);		
					tab[iter-1][88] = tempOpti.RegimeMoteurOpti;
					tab[iter-1][89] = tempOpti.CoupleMoteurOpti;
					tab[iter-1][90] = tempOpti.ChargeMoteurOpti;
					tab[iter-1][91] = tempOpti.ConsoOpti;
					tab[iter-1][92] = tempOpti.RapportOpti;			
					// calcul
					// de
					// la
					// consommation
					// en
					// litre
					tab[iter-1][93] = tab[iter-1][91]*tab[iter-1][86]*tab[iter-1][87]*2*Math.PI/60*tab[iter-1][99]/3600/1000/rhoessence;
					// calcul
					// de
					// la
					// consommation
					// en
					// litre/100
					// km
					tab[iter-1][94]	= tab[iter-1][93]/(tab[iter-1][85]*tab[iter-1][99])*100000;
					// calcul
					// des
					// rejets
					// de
					// CO2
					// en
					// kg
					tab[iter-1][95] = tab[iter-1][93]*rhoessence*ratioCO2/1000;
					// toutes
					// les
					// variables
					// a
					// chaque
					// point
					// de
					// la
					// phase
					// prennent
					// la
					// valeur
					// optimale
					// (moyenne)
					// calculee
					// sur
					// la
					// phase
					for(var z=indiceReprisePhaseTrajet;z<iter-1;z++){
						tab[z][88] = tab[iter-1][88];
						tab[z][89] = tab[iter-1][89];
						tab[z][90] = tab[iter-1][90];
						tab[z][91] = tab[iter-1][91];
						tab[z][92] = tab[iter-1][92];
						tab[z][93] = tab[iter-1][93];
						tab[z][94] = tab[iter-1][94];
						tab[z][95] = tab[iter-1][95];
					}		
					boolChgtPhaseTrajet = true;
					
				}
				
				// quand
				// la
				// force
				// motrice
				// est
				// negative,
				// on
				// suppose
				// que
				// la
				// vitesse
				// enclenchee
				// reste
				// la
				// même
				// la
				// consommation
				// et
				// les
				// variables
				// qui
				// en
				// decoulent
				// sont
				// nulles
				tab[iter][92] = tab[iter-1][92];
				tab[iter][88] = tab[iter][79]/dataVitesses[tab[iter][78]]*dataVitesses[tab[iter][92]];
				tab[iter][89] = tab[iter][80]*dataVitesses[tab[iter][78]]/dataVitesses[tab[iter][92]];
				if(tab[iter][89]>0){
					tab[iter][90] = tab[iter][89]/(coef[3]*Math.pow(tab[iter][88],3)+coef[2]*Math.pow(tab[iter][88],2)+coef[1]*tab[iter][88]+coef[0])*100;
				}
				else{
					tab[iter][90] = 0;
				}
				// On
				// ramene
				// au
				// max
				// ou
				// au
				// min
				if(tab[iter][90]<pasCmot){
					tab[iter][90] = 0;
				}
				if(tab[iter][90]>100){
					tab[iter][90] = 100;
				}
				tab[iter][93] = 0;
				tab[iter][94] = 0;
				tab[iter][95] = 0;
			}		
			
			
				
			// on
			// change
			// de
			// phase
			// : on
			// calcule
			// les
			// totaux
			// depuis
			// la
			// derniere
			// fois
			// qu'on
			// a
			// change
			// de
			// phase
			if(boolChgtPhaseTrajet && !boolInitCalculTrajet && timerPhaseTrajet>=dureePhase){
				for(var z=indiceReprisePhaseTrajet;z<=iter;z++){
				// calcul
				// de
				// la
				// consommation
				// totale
				// en
				// litres
				tab[z][96] = tab[z-1][96]+tab[z][93];
				// Emissions
				// totales
				// de
				// CO2
				tab[z][98] = tab[z-1][98]+tab[z][95];
				}
				
			}
			// sinon
			// on
			// fait
			// avancer
			// les
			// variables
			// pour
			// le
			// cas
			// de
			// l'energie
			// nulle
			else{
				// calcul
				// de
				// la
				// consommation
				// totale
				// en
				// litres
				tab[iter][96] = tab[iter-1][96];
				// Emissions
				// totales
				// de
				// CO2
				tab[iter][98] = tab[iter-1][98];
			}
			
			// au
			// debut
			// du
			// calcul,
			// il
			// faut
			// initialiser
			// les
			// valeurs
			// du
			// tableau
			// en
			// plus
			if(boolChgtPhaseTrajet && boolInitCalculTrajet && iter!=2){
				tab[indiceReprisePhaseTrajet-1][96] = tab[indiceReprisePhaseTrajet-1][93];
				tab[indiceReprisePhaseTrajet-1][98] = tab[indiceReprisePhaseTrajet-1][95];
				if(timerPhaseTrajet>=dureePhase){			
					for(var z=indiceReprisePhaseTrajet;z<=iter;z++){
					// calcul
					// de
					// la
					// consommation
					// totale
					// en
					// litres
					tab[z][96] = tab[z-1][96]+tab[z][93];
					// Emissions
					// totales
					// de
					// CO2
					tab[z][98] = tab[z-1][98]+tab[z][95];
					}
				}
				boolInitCalculTrajet = false;			
			}
		}

	}

	
	if (typeVehicule =="VP"){
		var operating_mode = definition_mode_seuilsAcc(tab,iter);
		calculConsoElecPac(tab,iter);
	}
	
	if (typeVehicule =="VPU"){
		if((document.getElementById("loiCommande").value)=="1"){
			var operating_mode = definition_mode_seuilsAcc(tab,iter);
		}
		if((document.getElementById("loiCommande").value)=="2"){
			var operating_mode = definition_mode_seuilsVit(tab,iter);
			
		}
		if((document.getElementById("loiCommande").value)=="3"){
			var operating_mode = definition_mode_plugIn(tab,iter);
		}
		var CS = false;
		
		calculConsoElecPacUTBM(tab,iter);
		
	}
	
	return {
		energie: energieT,
		bool: bool,
	}
}
/**
 * détermination du mode de fonctionnement du véhicule à pile à combustible
 * 
 * @returns operating_mode
 */
var AP;                              // variable qui va definir la position accelerateur
var P_thre = 10000;                  // puissance seuil 10kW
var V_thre = 20;                     // vitesse seuil 20km/h
//var Soc_low = 20;                    // seuil bas etat de charge 40%
var Soc_up = 80;                     // seuil haut etat de charge 80%
var AP_thre = 10;                    // seuil d'acceleration lié à la pédale d'accelerateur, peut etre negatif lors d'un freinage
var operating_mode;                  // mode de fonctionnement défini dans cette fonction
var E_batt_r = new Array();          // tableau regroupant l'energie restante dans la batterie
var aucunmode = 0;
var operating_mode_tab = [];         // création d'un tableau qui va permettre d'enregistrer las modes de fonctionnement du véhicule

function definition_mode_seuilsAcc(tab,iter){

	if(typeVehicule == "VE" || typeVehicule == "VT" || typeVehicule == "VH"){
		alert("le mode de fonctionnement du véhicule n'est pas requis pour ce type de véhicule");
	}
	else if (typeVehicule == "VP"){
		AP= tab[iter][22]/ (PmotorMax*1000); // puissance
												// electrique
												// necessaire
												// /
												// puissance
												// moteur
												// max
												// en
												// watts
												// pour
												// estimer
												// la
												// position
												// pedale
												// accelerateur
		
		if(tab[iter][22] < P_thre && AP > 0 && tab[iter-1][1] < V_thre && per_batt_r[iter-1] > Soc_low){
			operating_mode = "batt";
			operating_mode_tab[iter] = "batt";
		}
		else if(tab[iter][22] > P_thre && AP > AP_thre && per_batt_r[iter-1] > Soc_low){
			operating_mode = "fc_batt";
			operating_mode_tab[iter] ="fc_batt";
		}
		else if(0 < AP < AP_thre && tab[iter][1] > V_thre && per_batt_r[iter-1] > Soc_low && tab[iter][22]>0){
			operating_mode = "fuel_cell";
			operating_mode_tab[iter] ="fuel_cell";
		}
		else if(AP < 0){
			operating_mode = "brake";
			operating_mode_tab[iter] ="brake";
		}
		else if(AP > 0 && tab[iter][1] > 0 && per_batt_r[iter-1] < Soc_low){
			operating_mode = "fuel_cell";
			operating_mode_tab[iter] ="fuel_cell";
		}
		else if(AP == 0 && tab[iter][1] == 0){
			operating_mode = "stop";
			operating_mode_tab[iter] ="stop";
		}
		else{
			// alert("AP: " + AP +"Puiss: " +tab[iter][22]+ " vit: " +tab[iter][1] + " soc: " + tab[iter][32] + "iter: " + iter);
			aucunmode+=1;
		}
	}
	else if (typeVehicule == "VPU"){
		
			AP= tab[iter][22]/ (PmotorMax*1000); // puissance
													// electrique
													// necessaire
													// /
													// puissance
													// moteur
													// max
													// en
													// watts
													// pour
													// estimer
													// la
													// position
													// pedale
													// accelerateur
			
			if(tab[iter][22] < P_thre && AP > 0 && tab[iter-1][1] < V_thre && per_batt_r_Ah_U[iter-1] > Soc_low){
				operating_mode = "batt";
				operating_mode_tab[iter] = "batt";
			}
			else if(tab[iter][22] > P_thre && AP > AP_thre && per_batt_r_Ah_U[iter-1] > Soc_low){
				operating_mode = "fc_batt";
				operating_mode_tab[iter] ="fc_batt";
			}
			else if(0 < AP < AP_thre && tab[iter][1] > V_thre && per_batt_r_Ah_U[iter-1] > Soc_low && tab[iter][22]>0){
				operating_mode = "fuel_cell";
				operating_mode_tab[iter] ="fuel_cell";
			}
			else if(AP < 0){
				operating_mode = "brake";
				operating_mode_tab[iter] ="brake";
			}
			else if(AP > 0 && tab[iter][1] > 0 && per_batt_r_Ah_U[iter-1] < Soc_low){
				operating_mode = "fuel_cell";
				operating_mode_tab[iter] ="fuel_cell";
			}
			else if(AP == 0 && tab[iter][1] == 0){
				operating_mode = "stop";
				operating_mode_tab[iter] ="stop";
			}
			else{
// alert("AP:
// " + AP
// +"
// Puiss:
// "
// +tab[iter][22]+
// " vit:
// "
// +tab[iter][1]
// + "
// soc:
// " +
// per_batt_r_Ah_U[iter-1]
// + "
// iter: "
// +
// iter);
				aucunmode+=1;
			}
		}
	return operating_mode;
}

function definition_mode_seuilsVit(tab,iter){
	//updateSOC();
	
	AP= tab[iter][22]/ (PmotorMax*1000); // puissance
											// electrique
											// necessaire
											// /
											// puissance
											// moteur
											// max
											// en
											// watts
											// pour
											// estimer
											// la
											// position
											// pedale
											// accelerateur
	
	if(tab[iter-1][1] < 85 && per_batt_r_Ah_U[iter-1] > Soc_low && AP>0){
		operating_mode = "batt";
		operating_mode_tab[iter] = "batt";
	}
	else if(tab[iter-1][1] >= 85 && per_batt_r_Ah_U[iter-1]>Soc_low && AP>0){
		operating_mode = "fc_batt";
		operating_mode_tab[iter] ="fc_batt";
	}
	else if(AP < 0){
		operating_mode = "brake";
		operating_mode_tab[iter] ="brake";
	}
	else if(tab[iter][1] > 0 && per_batt_r_Ah_U[iter-1] < Soc_low && AP>0){
		operating_mode = "fc_batt";
		operating_mode_tab[iter] ="fc_batt";
	}
	else if(tab[iter][1] == 0 && AP==0){
		operating_mode = "stop";
		operating_mode_tab[iter] ="stop";
	}
	else{
// alert("AP:
// " + AP
// +"
// Puiss:
// "
// +tab[iter][22]+
// " vit:
// " +
// tab[iter][1]
// + "
// soc: "
// +
// per_batt_r_Ah_U[iter-1]
// + "
// iter: "
// +
// iter);
		aucunmode+=1;
	}
	
	return operating_mode;
}

function definition_mode_plugIn(tab,iter){
	
	AP= tab[iter][22]/ (PmotorMax*1000); // puissance
											// electrique
											// necessaire
											// /
											// puissance
											// moteur
											// max
											// en
											// watts
											// pour
											// estimer
											// la
											// position
											// pedale
											// accelerateur
	var valOCVBatt0 = nbCellBatt*(0.000006*Math.pow(per_batt_r_Ah_U[iter-1],3)-0.001*Math.pow(per_batt_r_Ah_U[iter-1],2)+0.0581*per_batt_r_Ah_U[iter-1]+7)
	var valResistIntBatt0 = nbCellBatt*0.001*(-0.000005*Math.pow(per_batt_r_Ah_U[iter-1],3)+0.0009*Math.pow(per_batt_r_Ah_U[iter-1],2)-0.0533*per_batt_r_Ah_U[iter-1]+2.465)
	P_max_batt = Math.pow(valOCVBatt0,2)/(4*valResistIntBatt0)
	
	if(per_batt_r_Ah_U[iter-1] > 15 && AP>0 && tab[iter][22]<=P_max_batt){
		operating_mode = "batt";
		operating_mode_tab[iter] = "batt";
	}
	else if(per_batt_r_Ah_U[iter-1] > 15 && AP>0 && tab[iter][22]>P_max_batt){
		operating_mode = "batt_fc"
		operating_mode_tab[iter] = "batt_fc";
	}
	else if(per_batt_r_Ah_U[iter-1] <= 15 && AP>0){
		operating_mode = "fc_batt";
		operating_mode_tab[iter] ="fc_batt";
	}
	else if(AP < 0){
		operating_mode = "brake";
		operating_mode_tab[iter] ="brake";
	}
	else if(tab[iter][1] == 0){
		operating_mode = "stop";
		operating_mode_tab[iter] ="stop";
	}
	else{
// alert("AP:
// " + AP
// +"
// Puiss:
// "
// +tab[iter][22]+
// " vit:
// " +
// tab[iter][1]+
// " soc:
// " +
// tab[iter][32]
// + "
// iter: "
// +
// iter);
		aucunmode+=1;
	}
	
	return operating_mode;
}



/**
 * calcul de la consommation denergie provenant de la pac et de la batterie pour un vehicule hydrogène en plus de la consommation d'hydrogene
 * 
 * @returns la valeur d'energie consommée provenant de la pac et de la batterie
 */

var P_batt = new Array();     // tableau
								// des
								// puissance
								// instt
								// fournies
								// par
								// la
								// batterie
var E_batt_i = new Array();   // tableau
								// regroupant
								// l'historique
								// de
								// l'energie
								// batterie
								// fournie
								// entre
								// 2
								// increments
var E_batt = new Array();     // tableau
								// regrouppant
								// l'historique
								// de
								// l'energie
								// fournie
								// par
								// la
								// batterie
								// (total)
var P_fc = new Array();       // puissance
								// istt
								// fournie
								// par
								// la
								// pac
var E_fc_i = new Array();     // energie
								// instt
								// pac
var E_fc = new Array();       // tableau
								// regroupant
								// les
								// energies
								// de
								// la
								// pac
var M_H2 = new Array();       // masse
								// d'hydrogène
								// consommée
								// totale
var P_max_fc = 50000;         // puissance
								// maxi
								// pac
								// instt=
								// 50kW
var rend_fc = 0.6;            // rendement
								// pac
var d_en_H2 = 123*Math.pow(10, 6);        // densité
											// energétique
											// hydrogène
											// de
											// 123MJ/kg
											// à
											// 700bars
var act_batt = new Array();
var act_pac = new Array();
var per_batt_r = new Array();
var E_r = [];                 // tableau
								// des
								// energies
								// récupérées
P_batt[0] = 0;                // initialisation
								// puissance
								// instantannée
P_batt[1] = 0;
E_batt_i[0] = 0;              // initialisation
								// energie
								// entre
								// 2
								// increments
E_batt[0] = 0;                // initialisation
								// energie
								// fournie
								// par
								// la
								// batterie
E_r[0] = 0;
E_r[1] = 0;
E_batt[1] = 0;
P_fc[0] = 0;     		      // initialisation
								// puiss
								// instt
								// pac
P_fc[1] = 0;
E_fc_i[0] = 0;                // initialisation
								// energie
								// entre
								// 2
								// increments
								// fournie
								// par
								// la
								// pac
E_fc[0] = 0;                  // initialisation
								// energie
								// totale
								// fournie
								// par
								// la
								// pac
E_fc[1] = 0;
M_H2[0] = 0;                  // initialisation
								// masse
								// hydrogène
								// consommée
M_H2[1] = 0;
var dataConsoPac = new Array();                   // creation
													// d'un
													// tableau
													// pour
													// pouvoir
													// tracer
													// les
													// graphes
dataConsoPac[0] = new Array();
dataConsoPac[0][0] = "Puissance fournie par la batterie (W)";
dataConsoPac[1] = new Array();
dataConsoPac[1][0] = "Énergie batterie entre 2 incréments (Wh)";
dataConsoPac[2] = new Array();
dataConsoPac[2][0] = "Énergie fournie par la batterie (Wh)";
dataConsoPac[3] = new Array();
dataConsoPac[3][0] = "Puissance fournie par la pac (W)";
dataConsoPac[4] = new Array();
dataConsoPac[4][0] = "Énergie pac entre 2 increments (J)";
dataConsoPac[5] = new Array();
dataConsoPac[5][0] = "Énergie fournie par la pac (J)";
dataConsoPac[6] = new Array();
dataConsoPac[6][0] = "Consommation d'hydrogene (kg)";
dataConsoPac[7] = new Array();
dataConsoPac[7][0] = "Distance cumulée (km)";
dataConsoPac[8] = new Array();
dataConsoPac[8][0] = "Puissance moteur (W)";
dataConsoPac[9] = new Array();
dataConsoPac[9][0] = "Activation batterie";
dataConsoPac[10] = new Array();
dataConsoPac[10][0] = "Activation pile à combustible";
dataConsoPac[11] = new Array();
dataConsoPac[11][0] = "Energie restante dans la batterie (Wh)";
dataConsoPac[12] = new Array();
dataConsoPac[12][0] = "Temps (s)";
dataConsoPac[13] = new Array();
dataConsoPac[13][0] = "Couple (Nm)";
dataConsoPac[14] = new Array();
dataConsoPac[14][0] = "Taux de charge de la batterie (%)";
dataConsoPac[15]= [];
dataConsoPac[15][0] = "Vitesse (km/h)";
dataConsoPac[16] = [];
dataConsoPac[16][0] = "Somme des puissances";
dataConsoPac[17] = [];
dataConsoPac[17][0] = "Pente";
dataConsoPac[18] = [];
dataConsoPac[18][0] = "Accélération (m/s²)";
dataConsoPac[19] = [];
dataConsoPac[19][0] = "Force motrice (N)";
dataConsoPac[20] = [];
dataConsoPac[20][0] = "Couple aux roues (Nm)";
dataConsoPac[21] = [];
dataConsoPac[21][0] = "Énergie récupérée (Wh)"

function calculConsoElecPac(tab,iter){
	
	if(typeVehicule == "VE" || typeVehicule == "VT" || typeVehicule == "VH"){
		alert("Le véhicule selectionné ne fonctionne pas à l'hydrogène.")
	}
	else{
		if(operating_mode == "batt"){
			P_batt[iter] = tab[iter][22];                                 // W
																			// puissance
																			// instt
																			// fournie
																			// par
																			// la
																			// batterie
			E_batt_i[iter] = tab[iter][22]*tab[iter][5]/3600;             // Wh
																			// energie
																			// fournie
																			// par
																			// la
																			// batterie
																			// entre
																			// 2
																			// increments
																			// E=p*t
			E_batt[iter] = E_batt[iter-1] + E_batt_i[iter];               // Wh
																			// energie
																			// que
																			// doit
																			// fournir
																			// la
																			// batterie
																			// (totale)
																			// =
																			// energie
																			// a
																			// l'incrément
																			// precedent
																			// +
																			// p*t
			P_fc[iter] = 0;                                               // energie
																			// fournie
																			// par
																			// la
																			// pac
																			// nulle
																			// lors
																			// de
																			// ce
																			// mode
			E_fc_i[iter] = 0;                                             // energie
																			// fournie
																			// par
																			// la
																			// pac
																			// est
																			// nulle
																			// entre
																			// 2
																			// increments
			E_fc[iter] = E_fc[iter-1];                                    // energie
																			// que
																			// doit
																			// fournir
																			// la
																			// pac
																			// = 0
																			// car
																			// non
																			// utilisée
			M_H2[iter] = M_H2[iter-1];                                    // quantité
																			// d'H2
																			// consommée
																			// = 0
																			// car
																			// pac
																			// non
																			// utilisée
			act_pac[iter] = 0;
			act_batt[iter] = 1;
			E_r[iter] = E_r[iter-1];
		}
		else if(operating_mode == "fc_batt"){
			if (tab[iter][22] > P_max_fc){						     		// Si
																			// la
																			// puissance
																			// nécessaire
																			// pour
																			// l'acc
																			// est
																			// superieure
																			// à la
																			// puiss
																			// max
																			// pouvant
																			// etre
																			// fournie
																			// par
																			// la
																			// pac,
																			// il
																			// faut
																			// combiner
																			// la
																			// puissance
																			// max
																			// pac
																			// avec
																			// un
																			// supplément
																			// batterie
				P_batt[iter] = tab[iter][22] - P_max_fc;                    // W la
																			// puissance
																			// fournie
																			// par
																			// la
																			// batterie
																			// est
																			// la
																			// puissance
																			// nécessaire
																			// pour
																			// l'acceleration
																			// - la
																			// puissance
																			// max
																			// qui
																			// est
																			// fournie
																			// par
																			// la
																			// pac
				P_fc[iter] = P_max_fc;                                      // W la
																			// puissance
																			// fournie
																			// par
																			// la
																			// pac
																			// est
																			// la
																			// puissance
																			// maximale
				act_pac[iter] = 1;
				act_batt[iter] = 1;
			}
			else{
				P_batt[iter] = 0;
				P_fc[iter] = tab[iter][22];
				act_pac[iter] = 1;
				act_batt[iter] = 0;
			}
			E_batt_i[iter] = P_batt[iter]*tab[iter][5]/3600;              // Wh
			E_batt[iter] = E_batt[iter-1] + E_batt_i[iter];               // Wh
			E_fc_i[iter] = P_fc[iter]*tab[iter][5];                       // J
			E_fc[iter] = E_fc[iter-1] + E_fc_i[iter];                     // J
			M_H2[iter] = M_H2[iter-1] + E_fc_i[iter]/(rend_fc*d_en_H2);   // masse
																			// d'hydrogène
																			// consommée
																			// en
																			// kg
			E_r[iter] = E_r[iter-1];
		}
		else if(operating_mode == "fuel_cell"){
			P_batt[iter] = 0;
			E_batt_i[iter] = 0;
			E_batt[iter] = E_batt[iter-1];
			P_fc[iter] = tab[iter][22];
			E_fc_i[iter] = P_fc[iter]*tab[iter][5];
			E_fc[iter] = E_fc[iter-1] + E_fc_i[iter];
			M_H2[iter] = M_H2[iter-1] + E_fc_i[iter]/(rend_fc*d_en_H2);
			act_pac[iter] = 1;
			act_batt[iter] = 0;
			E_r[iter] = E_r[iter-1];
		}
		else if(operating_mode == "brake"){                       // attention
																	// à la
																	// recuperation
																	// d'energie
																	// jusqu'a
																	// 5km/h
																	// =
																	// 0%,
																	// 17km/h
																	// =
																	// 100%
																	// et
																	// 100%
																	// au
																	// dessus
																	// de
																	// 17
			P_batt[iter] = tab[iter][22];
			E_batt_i[iter] = P_batt[iter]*tab[iter][5]/3600;
			E_batt[iter] = E_batt[iter-1]; // +
											// E_batt_i[iter];
			P_fc[iter] = 0;
			E_fc_i[iter] = 0;
			E_fc[iter] = E_fc[iter-1];
			M_H2[iter] = M_H2[iter-1];
			act_pac[iter] = 0;
			act_batt[iter] = 1;
			E_r[iter] = E_r[iter-1] - E_batt_i[iter];
		}
		else if(operating_mode == "stop"){
			P_batt[iter] = 0;
			E_batt_i[iter] = 0;
			E_batt[iter] = E_batt[iter-1];
			P_fc[iter] = 0;                                               // energie
																			// fournie
																			// par
																			// la
																			// pac
																			// nulle
																			// lors
																			// de
																			// ce
																			// mode
			E_fc_i[iter] = 0;                                             // energie
																			// fournie
																			// par
																			// la
																			// pac
																			// est
																			// nulle
																			// entre
																			// 2
																			// increments
			E_fc[iter] = E_fc[iter-1];                                    // energie
																			// que
																			// doit
																			// fournir
																			// la
																			// pac
																			// = 0
																			// car
																			// non
																			// utilisée
			M_H2[iter] = M_H2[iter-1];                                    // quantité
																			// d'H2
																			// consommée
																			// = 0
																			// car
																			// pac
																			// non
																			// utilisée
			act_pac[iter] = 0;
			act_batt[iter] = 0;
			E_r[iter] = E_r[iter-1];
		}

		if(iter >=1){ // &&
						// E_batt_r[iter]
						// <
						// capBatterie*1000
						// &&
						// E_batt_i[iter]
						// >
						// 0){
			E_batt_r[iter] = E_batt_r[iter-1] - E_batt_i[iter];         // mise
																		// a
																		// jour
																		// de
																		// l'energie
																		// restante
																		// dans
																		// la
																		// batterie
			per_batt_r[iter] = E_batt_r[iter]/(capBatterie*10);         
		}
		else{
			E_batt_r[iter] = E_batt_r[iter-1];
			per_batt_r[iter] = E_batt_r[iter]/(capBatterie*10);
		}
		dataConsoPac[0].push(P_batt[iter]);
		dataConsoPac[1].push(E_batt_i[iter]);
		dataConsoPac[2].push(E_batt[iter]);
		dataConsoPac[3].push(P_fc[iter]);
		dataConsoPac[4].push(E_fc_i[iter]);
		dataConsoPac[5].push(E_fc[iter]);
		dataConsoPac[6].push(M_H2[iter]);
		dataConsoPac[7].push(tab[iter][3]/1000);
		dataConsoPac[8].push(tab[iter][22]);
		dataConsoPac[9].push(act_batt[iter]);
		dataConsoPac[10].push(act_pac[iter]);
		dataConsoPac[11].push(E_batt_r[iter]);
		dataConsoPac[12].push(tab[iter][0]);
		dataConsoPac[13].push(tab[iter][23]);
		dataConsoPac[14].push(per_batt_r[iter]);
		dataConsoPac[15].push(tab[iter][1]);
		dataConsoPac[16].push(P_fc[iter] + P_batt[iter]);
		dataConsoPac[17].push(tab[iter][4]);
		dataConsoPac[18].push(tab[iter][6]);
		dataConsoPac[19].push(tab[iter][20]);
		dataConsoPac[20].push(tab[iter][23]);
		dataConsoPac[21].push(E_r[iter]);
		
		
	}
}



/**
 * PAC UTBM calcul de la consommation denergie provenant de la pac et de la batterie pour un vehicule hydrogène en plus de la consommation d'hydrogene
 * 
 * @returns la valeur d'energie consommée provenant de la pac et de la batterie
 */



function updateDegrade(){
	var temp = parseFloat(document.getElementById("modePuissancePACU").value);
	if(temp == 1) { // Rendement
					// fixe
		document.getElementById("valeurDegradePACU").style.visibility = '';
	}else if(temp == 2){ //
		document.getElementById("valeurDegradePACU").style.visibility = 'hidden';
	}else if(temp == 3){ //
		document.getElementById("valeurDegradePACU").style.visibility = 'hidden';
	}
	return temp;
}

function valeurDegrade(){
	var val;
	((document.getElementById("valeurDegradePACU").value) != '')? 	val = parseFloat(document.getElementById("valeurDegradePACU").value) 			: val = 0.3
	return val;
}

function updateRend(){
	var temp = parseFloat(document.getElementById("rendPACU").value);
	if(temp == 1) { // Rendement
					// fixe
		document.getElementById("rendValeurPACU").style.visibility = '';
	}else if(temp == 2){ // rendement
							// selon
							// carte
							// constructeur
		document.getElementById("rendValeurPACU").style.visibility = 'hidden';
	}
	return temp;
}

function updateTemp(){
	var temp = parseFloat(document.getElementById("modeleBatterie").value);
	if(temp == 2 || temp == 3) { // Pas
									// de
									// prise
									// en
									// compte
									// de
									// la
									// température
		document.getElementById("titreTempBatt").style.visibility = 'hidden';
		document.getElementById("selectTempBatt").style.visibility = 'hidden';
	}else if(temp == 1){ // Prise
							// en
							// compte
							// de
							// la
							// température
		document.getElementById("titreTempBatt").style.visibility = '';
		document.getElementById("selectTempBatt").style.visibility = '';
	}
}

function updateModelePac(){
	var temp = parseFloat(document.getElementById("modelePACU").value);
	if(temp == 1) { // Modèle
					// pac
					// basique
		document.getElementById("titreTpsRepPACU").style.visibility = 'hidden';
		document.getElementById("titreRendPACU").style.visibility = 'hidden';
		document.getElementById("tpsReponsePACU").style.visibility = 'hidden';
		document.getElementById("rendPACU").style.visibility = 'hidden';
	}else if(temp == 2){ // Modèle
							// UTBM
		document.getElementById("titreTpsRepPACU").style.visibility = '';
		document.getElementById("titreRendPACU").style.visibility = '';
		document.getElementById("tpsReponsePACU").style.visibility = '';
		document.getElementById("rendPACU").style.visibility = '';
	}
}

function valeurRend(rP){
	var temp = parseFloat(document.getElementById("rendPACU").value);
	if(temp == 1) { // Rendement
					// fixe
		document.getElementById("rendValeurPACU").style.visibility = '';
		((document.getElementById("rendValeurPACU").value) != '')? 	rend_fc_U = parseFloat(document.getElementById("rendValeurPACU").value) 			: rend_fc_U = 0.5
	}else if(temp == 2){ // Rendement
							// selon
							// carte
							// constructeur
		document.getElementById("rendValeurPACU").style.visibility = 'hidden';
		rend_fc_U = rendConstructeurPAC(rP);
	}
	return rend_fc_U;
}

function rendConstructeurPAC(rP){	// Carte
									// de
									// rendement
									// constructeur
									// PAC

	var xA;
	var yA;
	var xB;
	var yB;
	var coeffDir;
	var C;
	if (rP=>0 && rP<0.04){
		xA = 0;
		yA = 0.1*0.85;
		xB = 0.04;
		yB = 0.33*0.85;
		coeffDir = (yB-yA)/(xB-xA);
		C = yA-xA*coeffDir;
		rend_fc_U = coeffDir*rP+C;
	}
	if (rP=>0.04 && rP<0.1){
		xA = 0.04;
		yA = 0.33*0.85;
		xB = 0.1;
		yB = 0.48*0.85;
		coeffDir = (yB-yA)/(xB-xA);
		C = yA-xA*coeffDir;
		rend_fc_U = coeffDir*rP+C;
	}	
	if (rP=>0.1 && rP<0.2){
		xA = 0.1;
		yA = 0.48*0.85;
		xB = 0.2;
		yB = 0.55*0.85;
		coeffDir = (yB-yA)/(xB-xA);
		C = yA-xA*coeffDir;
		rend_fc_U = coeffDir*rP+C;
	}	
	if (rP=>0.2 && rP<0.4){
		xA = 0.2;
		yA = 0.55*0.85;
		xB = 0.4;
		yB = 0.595*0.85;
		coeffDir = (yB-yA)/(xB-xA);
		C = yA-xA*coeffDir;
		rend_fc_U = coeffDir*rP+C;
	}	
	if (rP=>0.4 && rP<0.6){
		xA = 0.4;
		yA = 0.595*0.85;
		xB = 0.6;
		yB = 0.589*0.85;
		coeffDir = (yB-yA)/(xB-xA);
		C = yA-xA*coeffDir;
		rend_fc_U = coeffDir*rP+C;
	}	
	if (rP=>0.6 && rP<0.8){
		xA = 0.6;
		yA = 0.589*0.85;
		xB = 0.8;
		yB = 0.564*0.85;
		coeffDir = (yB-yA)/(xB-xA);
		C = yA-xA*coeffDir;
		rend_fc_U = coeffDir*rP+C;
	}	
	if (rP=>0.8 && rP<1){
		xA = 0.8;
		yA = 0.564*0.85;
		xB = 1;
		yB = 0.51*0.85;
		coeffDir = (yB-yA)/(xB-xA);
		C = yA-xA*coeffDir;
		rend_fc_U = coeffDir*rP+C;
	}
	return rend_fc_U;
}

var P_batt_U = new Array();     // tableau des puissance instt fournies par la batterie
var E_batt_i_U = new Array();   // tableau regroupant l'historique de l'energie batterie fournie entre 2 increments
var E_batt_U = new Array();     // tableau
								// regrouppant
								// l'historique
								// de
								// l'energie
			// fournie
			// par
			// la
			// batterie
			// (total)
var E_batt_r_U = new Array();
var P_fc_U = new Array();       // puissance
								// istt
								// fournie
								// par
								// la
								// pac
var E_fc_i_U = new Array();     // energie
								// instt
								// pac
var E_fc_U = new Array();       // tableau
								// regroupant
								// les
								// energies
								// de
								// la
								// pac
var M_H2_U = new Array();       // masse
								// d'hydrogène
								// consommée
								// totale

var rend_fc_U;

var d_en_H2_U = 120*Math.pow(10, 6)/3600;        // densité
													// energétique
// hydrogène
// de 120
// MJ/kg à
// 700
// bars
// (valeur
// de
// l'UTBM)
// On
// passe
// en
// Wh/kg
var act_batt_U = new Array();
var act_pac_U = new Array();
var per_batt_r_U = new Array();
var per_batt_r_Ah_U = new Array();
var E_r_U = [];                 // tableau
								// des
								// energies
								// récupérées
var M_H2_r_U = new Array();
var per_H2_r_U = new Array();
var M_H2_i_U = new Array();
var T = new Array();
var E_tot = new Array();
var PfreqVPU = new Array();
var pfcFreqU = new Array();
var I = new Array();
var U = new Array();
var prix_U = new Array();
var energieFournieMoteur = new Array();
var energieConsoGenerateur = new Array();
var energieFournieGenerateur = new Array();
var energieFournieBatterie = new Array();
var energieFournieBattParPac = new Array();
var energieChargeeBattParPac = new Array();
var energieChargeeBattParGen = new Array();
var tempsChargeBatterie = new Array();
var tempsChargeH2 = new Array();
var valOCVBatt;
var valResistIntBatt;
var P_max_batt;


var dataConsoPacUTBM = new Array();                   // creation
														// d'un
														// tableau
														// pour
														// pouvoir
														// tracer
														// les
														// graphes
var dataConsoPacUTBM_CS = new Array();                   // creation
															// d'un
// tableau
// pour
// pouvoir
// tracer
// les
// graphes

energieFournieMoteur[0]=0;
energieFournieMoteur[1]=0;
energieConsoGenerateur[0]=0;
energieConsoGenerateur[1]=0;
energieFournieGenerateur[0]=0;
energieFournieGenerateur[1]=0;
energieFournieBatterie[0]=0;
energieFournieBatterie[1]=0;
energieFournieBattParPac[0]=0;
energieFournieBattParPac[1]=0;
energieChargeeBattParPac[0]=0;
energieChargeeBattParPac[1]=0;
energieChargeeBattParGen[0]=0;
energieChargeeBattParGen[1]=0;
M_H2_i_U[0] = 0;
P_batt_U[0] = 0;                // initialisation
								// puissance
								// instantannée
P_batt_U[1] = 0;
E_batt_i_U[0] = 0;              // initialisation
								// energie
								// entre
								// 2
								// increments
E_batt_U[0] = 0;                // initialisation
								// energie
								// fournie
								// par
								// la
								// batterie
E_r_U[0] = 0;
E_r_U[1] = 0;
E_batt_U[1] = 0;
P_fc_U[0] = 0;     		      // initialisation
								// puiss
								// instt
								// pac
P_fc_U[1] = 0;
E_fc_i_U[0] = 0;                // initialisation
								// energie
								// entre
								// 2
								// increments
								// fournie
								// par
								// la
								// pac
E_fc_U[0] = 0;                  // initialisation
								// energie
								// totale
								// fournie
								// par
								// la
								// pac
E_fc_U[1] = 0;
M_H2_U[0] = 0;                  // initialisation
								// masse
								// hydrogène
								// consommée
M_H2_U[1] = 0;
E_tot[0]=0;
E_tot[1]=0;
PfreqVPU[0]=0;
PfreqVPU[1]=0;
var PfreqVPUMemory;
pfcFreqU[0]=0;
pfcFreqU[1]=0;
I[0]=0;
I[1]=0;

prix_U[0]=0;
prix_U[1]=0;
tempsChargeBatterie[0]=0;
tempsChargeBatterie[1]=0;
tempsChargeH2[0]=0;
tempsChargeH2[1]=0;

// var loiTXT = "Puissance batterie,Puissance PAC";

dataConsoPacUTBM[0] = new Array();
dataConsoPacUTBM[0][0] = "Puissance fournie par la batterie (W)";
dataConsoPacUTBM[1] = new Array();
dataConsoPacUTBM[1][0] = "Énergie batterie entre 2 incréments (Wh)";
dataConsoPacUTBM[2] = new Array();
dataConsoPacUTBM[2][0] = "Énergie fournie par la batterie (Wh)";
dataConsoPacUTBM[3] = new Array();
dataConsoPacUTBM[3][0] = "Puissance fournie par la pac (W)";
dataConsoPacUTBM[4] = new Array();
dataConsoPacUTBM[4][0] = "Énergie pac entre 2 increments (Wh)";
dataConsoPacUTBM[5] = new Array();
dataConsoPacUTBM[5][0] = "Énergie fournie par la pac (Wh)";
dataConsoPacUTBM[6] = new Array();
dataConsoPacUTBM[6][0] = "Consommation d'hydrogene (kg)";
dataConsoPacUTBM[7] = new Array();
dataConsoPacUTBM[7][0] = "Distance cumulée (km)";
dataConsoPacUTBM[8] = new Array();
dataConsoPacUTBM[8][0] = "Puissance moteur (W)";
dataConsoPacUTBM[9] = new Array();
dataConsoPacUTBM[9][0] = "Activation batterie";
dataConsoPacUTBM[10] = new Array();
dataConsoPacUTBM[10][0] = "Activation pile à combustible";
dataConsoPacUTBM[11] = new Array();
dataConsoPacUTBM[11][0] = "Energie restante dans la batterie (Wh)";
dataConsoPacUTBM[12] = new Array();
dataConsoPacUTBM[12][0] = "Temps (s)";
dataConsoPacUTBM[13] = new Array();
dataConsoPacUTBM[13][0] = "Couple (Nm)";
dataConsoPacUTBM[14] = new Array();
dataConsoPacUTBM[14][0] = "Taux d'énergie de la batterie (%)";
dataConsoPacUTBM[15]= [];
dataConsoPacUTBM[15][0] = "Vitesse (km/h)";
dataConsoPacUTBM[16] = [];
dataConsoPacUTBM[16][0] = "Différence entre la somme puissance sources et la puissance demandée au moteur";
dataConsoPacUTBM[17] = [];
dataConsoPacUTBM[17][0] = "Pente";
dataConsoPacUTBM[18] = [];
dataConsoPacUTBM[18][0] = "Accélération (m/s²)";
dataConsoPacUTBM[19] = [];
dataConsoPacUTBM[19][0] = "Force motrice (N)";
dataConsoPacUTBM[20] = [];
dataConsoPacUTBM[20][0] = "Couple aux roues (Nm)";
dataConsoPacUTBM[21] = [];
dataConsoPacUTBM[21][0] = "Énergie récupérée (Wh)",
dataConsoPacUTBM[22] = [];
dataConsoPacUTBM[22][0] = "Masse restante dans le réservoir d'hydrogène (kg)";
dataConsoPacUTBM[23] = [];
dataConsoPacUTBM[23][0] = "Niveau restant dans le réservoir d'hydrogène (%)";
dataConsoPacUTBM[24] = [];
dataConsoPacUTBM[24][0] = "Température batterie (°C)";
dataConsoPacUTBM[25] = [];
dataConsoPacUTBM[25][0] = "Energie totale consommée (Wh)"
dataConsoPacUTBM[26] = [];
dataConsoPacUTBM[26][0] = "Courant batterie"
dataConsoPacUTBM[27] = [];
dataConsoPacUTBM[27][0] = "Prix du trajet"
dataConsoPacUTBM[28] = [];
dataConsoPacUTBM[28][0] = "Taux de charge de la batterie (%)"
dataConsoPacUTBM[29] = [];
dataConsoPacUTBM[29][0] = "Tension batterie"
dataConsoPacUTBM[30] = [];
dataConsoPacUTBM[30][0] = "Energie fournie par le moteur"
dataConsoPacUTBM[31] = [];
dataConsoPacUTBM[31][0] = "Energie consommée par le générateur"
dataConsoPacUTBM[32] = [];
dataConsoPacUTBM[32][0] = "Energie fournie par le générateur"
dataConsoPacUTBM[33] = [];
dataConsoPacUTBM[33][0] = "Energie fournie par la batterie"
dataConsoPacUTBM[34] = [];
dataConsoPacUTBM[34][0] = "Energie rechargée à la batterie par la pile"
dataConsoPacUTBM[35] = [];
dataConsoPacUTBM[35][0] = "Energie fournie à la batterie par la pile"
dataConsoPacUTBM[36] = [];
dataConsoPacUTBM[36][0] = "Energie rechargée par la batterie par le freinage"
dataConsoPacUTBM[37] = [];
dataConsoPacUTBM[37][0] = "Numéro de loi de commande utilisée à chaque point";
dataConsoPacUTBM[37].push(10);
dataConsoPacUTBM[38]= [];
dataConsoPacUTBM[38][0] = "Vitesse limite";
dataConsoPacUTBM[39]= [];
dataConsoPacUTBM[39][0] = "Crisp";

dataConsoPacUTBM_CS[0] = new Array();
dataConsoPacUTBM_CS[0][0] = "Puissance fournie par la batterie (W)";
dataConsoPacUTBM_CS[1] = new Array();
dataConsoPacUTBM_CS[1][0] = "Énergie batterie entre 2 incréments (Wh)";
dataConsoPacUTBM_CS[2] = new Array();
dataConsoPacUTBM_CS[2][0] = "Énergie fournie par la batterie (Wh)";
dataConsoPacUTBM_CS[3] = new Array();
dataConsoPacUTBM_CS[3][0] = "Puissance fournie par la pac (W)";
dataConsoPacUTBM_CS[4] = new Array();
dataConsoPacUTBM_CS[4][0] = "Énergie pac entre 2 increments (Wh)";
dataConsoPacUTBM_CS[5] = new Array();
dataConsoPacUTBM_CS[5][0] = "Énergie fournie par la pac (Wh)";
dataConsoPacUTBM_CS[6] = new Array();
dataConsoPacUTBM_CS[6][0] = "Consommation d'hydrogene (kg)";
dataConsoPacUTBM_CS[7] = new Array();
dataConsoPacUTBM_CS[7][0] = "Distance cumulée (km)";
dataConsoPacUTBM_CS[8] = new Array();
dataConsoPacUTBM_CS[8][0] = "Puissance moteur (W)";
dataConsoPacUTBM_CS[9] = new Array();
dataConsoPacUTBM_CS[9][0] = "Activation batterie";
dataConsoPacUTBM_CS[10] = new Array();
dataConsoPacUTBM_CS[10][0] = "Activation pile à combustible";
dataConsoPacUTBM_CS[11] = new Array();
dataConsoPacUTBM_CS[11][0] = "Energie restante dans la batterie (Wh)";
dataConsoPacUTBM_CS[12] = new Array();
dataConsoPacUTBM_CS[12][0] = "Temps (s)";
dataConsoPacUTBM_CS[13] = new Array();
dataConsoPacUTBM_CS[13][0] = "Couple (Nm)";
dataConsoPacUTBM_CS[14] = new Array();
dataConsoPacUTBM_CS[14][0] = "Taux d'énergie de la batterie (%)";
dataConsoPacUTBM_CS[15]= [];
dataConsoPacUTBM_CS[15][0] = "Vitesse (km/h)";
dataConsoPacUTBM_CS[16] = [];
dataConsoPacUTBM_CS[16][0] = "Différence entre la somme puissance sources et la puissance demandée au moteur";
dataConsoPacUTBM_CS[17] = [];
dataConsoPacUTBM_CS[17][0] = "Pente";
dataConsoPacUTBM_CS[18] = [];
dataConsoPacUTBM_CS[18][0] = "Accélération (m/s²)";
dataConsoPacUTBM_CS[19] = [];
dataConsoPacUTBM_CS[19][0] = "Force motrice (N)";
dataConsoPacUTBM_CS[20] = [];
dataConsoPacUTBM_CS[20][0] = "Couple aux roues (Nm)";
dataConsoPacUTBM_CS[21] = [];
dataConsoPacUTBM_CS[21][0] = "Énergie récupérée (Wh)",
dataConsoPacUTBM_CS[22] = [];
dataConsoPacUTBM_CS[22][0] = "Masse restante dans le réservoir d'hydrogène (kg)";
dataConsoPacUTBM_CS[23] = [];
dataConsoPacUTBM_CS[23][0] = "Niveau restant dans le réservoir d'hydrogène (%)";
dataConsoPacUTBM_CS[24] = [];
dataConsoPacUTBM_CS[24][0] = "Température batterie (°C)";
dataConsoPacUTBM_CS[25] = [];
dataConsoPacUTBM_CS[25][0] = "Energie totale consommée (Wh)"
dataConsoPacUTBM_CS[26] = [];
dataConsoPacUTBM_CS[26][0] = "Courant batterie"
dataConsoPacUTBM_CS[27] = [];
dataConsoPacUTBM_CS[27][0] = "Prix du trajet"
dataConsoPacUTBM_CS[28] = [];
dataConsoPacUTBM_CS[28][0] = "Taux de charge de la batterie (%)"
dataConsoPacUTBM_CS[29] = [];
dataConsoPacUTBM_CS[29][0] = "Tension batterie"
dataConsoPacUTBM_CS[30] = [];
dataConsoPacUTBM_CS[30][0] = "Energie fournie par le moteur"
dataConsoPacUTBM_CS[31] = [];
dataConsoPacUTBM_CS[31][0] = "Energie consommée par le générateur"
dataConsoPacUTBM_CS[32] = [];
dataConsoPacUTBM_CS[32][0] = "Energie fournie par le générateur"
dataConsoPacUTBM_CS[33] = [];
dataConsoPacUTBM_CS[33][0] = "Energie fournie par la batterie"
dataConsoPacUTBM_CS[34] = [];
dataConsoPacUTBM_CS[34][0] = "Energie rechargée à la batterie par la pile"
dataConsoPacUTBM_CS[35] = [];
dataConsoPacUTBM_CS[35][0] = "Energie fournie à la batterie par la pile"
dataConsoPacUTBM_CS[36] = [];
dataConsoPacUTBM_CS[36][0] = "Energie rechargée par la batterie par le freinage"

var crisp = 0;
	

function calculConsoElecPacUTBM(tab,iter){
	tab[iter][32]=per_batt_r_Ah_U[iter-1];
	/**
	
	if(iter<= 300){
		//console.log(iter);
		console.log("Vitesse :"+ tab[iter][1]);
		console.log("pente  : " + tab[iter][4]);
		console.log("SOC : " + tab[iter][32]);
	}
	**/
	//console.log("pente  : " + tab[iter][4]*100);
	
	/** 
	 * Introduction de la loi Hybride qui determiner via statistique la meilleure loi à utiliser en fonction de la vitesse, la pente et le SOC pour minimiser l'énergie totale consommée
	 * la condition est introduite en début de fonction pour le faire faire appel à une récursion de calculConsoElecPacUTBM()
	 * 
	 * 
	 **/
	if((document.getElementById("loiCommande").value)=="10") {
		var loiTemp = 0; 
		loiTemp = defLoiHybride(tab[iter][1],tab[iter][4],tab[iter][32]);
		operating_mode = definition_mode_seuilsVit(tab,iter);
		//console.log(operating_mode+loiTemp);
		if(iter==2){dataConsoPacUTBM[37][1]=1};
		dataConsoPacUTBM[37][iter] = loiTemp;
	}
	
	
	
	//Pour les lois de commande, une des données principales utilisées est tab[iter][22] : c'est la valeur de la puissance demandée en entrée de moteur, ou bien la puissance fournie par le générateur en freinage
	
	//Cette fonction est divisée en 2 grandes parties : 
	// 1 : Les 8 lois de commande, qui calculent l'énergie fournie et consommée par la pile, ainsi que l'énergie fournie ou récupérée par la batterie
	//     A l'intérieur de ces lois on trouve donc les modèles de la PAC
	// 2 : Les modèles de la batterie, qui permettent de calculer l'état de charge de la batterie en fonction de l'énergie fournie ou récupérée
	
	//Le bloc comprenant les 3 premières lois de commande (qui fonctionnent par seuils) a été commenté de manière détaillée. Dans les autres lois de commande,
	//les commentaires portent uniquement sur les choses qui diffèrent des 3 premières lois. Par exemple, les modèles utilisés étant les mêmes, on ne les redécrit pas.
	
	if(typeVehicule == "VE" || typeVehicule == "VT" || typeVehicule == "VH"){
		alert("Le véhicule selectionné ne fonctionne pas à l'hydrogène.")
	}

	// Méthode des seuils sur l'accélération, sur la vitesse, et loi pour véhicule plug-in
	else if ((document.getElementById("loiCommande").value)=="1" || (document.getElementById("loiCommande").value)=="2" || (document.getElementById("loiCommande").value)=="3" || loiTemp == 2){
		
		if(operating_mode == "batt"){//La puissance est demandée uniquement à la batterie
			
			//Ci-dessous, le calcul de la puissance fournie par la PAC. La même démarche est utilisée dans toutes les lois.
			//Ici, la demande d'énergie à la PAC est de 0, puisqu'on demande otute l'énergie à la batterie
			if ((document.getElementById("modelePACU").value)=="1"){ //Modèle basique de la PAC
				P_fc_U[iter]=0;
			}
			else{ //Modèle UTBM de la PAC
				if ((document.getElementById("modePuissancePACU").value)=="2"){//Mode éteint
					P_fc_U[iter] = 0
				}
				else {//Mode normal ou mode dégradé
						//Si le mode dégradé a été choisi, le cycle de vitesse a déjà été recalculé en fonction
					if ((document.getElementById("tpsReponsePACU").value)=="1"){ //Temps de réponse pris en compte
						P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5]) //Fonction de transfert du premier ordre discrétisée afin de calculer la réponse de la PAC à la demande d'énergie
					}
					else { //Temps de réponse non pris en compte
						P_fc_U[iter] = 0;
					}
				}
			}
			
			//Ci-dessous, le calcul de la puissance et de l'énergie fournie / récupérée par la batterie. La démarche est la même pour toutes les lois.
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){ //Batterie fournit de l'énergie pour que la somme des deux puissnaces des sources soit égale à la puissnace demandée au moteur
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]; //Puissance batterie instantanée
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;//Energie batterie instantanée
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];//Energie fournie par la batterie cumulée
				E_r_U[iter] = E_r_U[iter-1];//Energie récupérée par la batterie (par freinage régénératif ou par la pile)
			}
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){//PAC plus de puissance que nécessaire (à cause du temps de réponse par exemple), la puissance supplémentaire sert à recharger la batterie
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
			
			//Calcul de l'énergie fournie par la PAC
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600; //Energie instantanée fournie
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter]; // Energie fournie par la pile cumulée

			//Calcul du rendement de la PAC afin de connaitre l'énergie consommée en hydrogène pour fournir l'énegrie demandée en sortie de pile
			if(document.getElementById("modelePACU").value=="2"){ //Modele UTBM : le rendement dépend de la puissance de sortie,suivant une courbe donnée
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}// Modèle basique : le rendement est de 0.6
			
			if(document.getElementById("modelePACU").value=="2"){ //Modèle UTBM : prise en compte d'un rendement convertisseur, valeur de densitée énergétique fournie par l'UTBM
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{ //Modèle basique : pas de rendement convertisseur
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter]; // Masse d'hydrogène consommée cumulée
			
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
		}
		else if(operating_mode == "fc_batt"){//Ici on fonctionne sur la PAC en complétant par la batterie si besoin
			
			if (tab[iter][22] > P_max_fc_U){ //La puissance demandée est supérieure à la puissance max de la PAC, on demande à la PAC de fournir sa puissance max, la batterie complètera.
				
				if(document.getElementById("modelePACU").value=="1"){
					P_fc_U[iter] = P_max_fc_U;
				}
				else{
					if ((document.getElementById("modePuissancePACU").value)=="2"){
						P_fc_U[iter] = 0
					}
					else {
						if ((document.getElementById("tpsReponsePACU").value)=="1"){
							P_fc_U[iter] = (P_max_fc_U + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
						}
						else { P_fc_U[iter] = P_max_fc_U  }
					}
				}
				
				act_pac_U[iter] = 1;
				act_batt_U[iter] = 1;
			}
			
			else{
				
				if(document.getElementById("modelePACU").value=="1"){
					P_fc_U[iter] = tab[iter][22];
				}
				else{
					if ((document.getElementById("modePuissancePACU").value)=="2"){
						P_fc_U[iter] = 0
					}
					else {
						if ((document.getElementById("tpsReponsePACU").value)=="1"){
							P_fc_U[iter] = (tab[iter][22] + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
						}
						else { P_fc_U[iter] = tab[iter][22]  }
					}
				}
				
				act_pac_U[iter] = 1;
				act_batt_U[iter] = 0;
			}
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;                       // Wh
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter]; // Wh
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter]; 			
		}
		
		else if(operating_mode == "batt_fc"){ //Ici on utilise la batterie au maximum et on complète avec la pile
			
			
			
			if(document.getElementById("modelePACU").value=="1"){
				P_fc_U[iter] = tab[iter][22]-P_max_batt;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = ((tab[iter][22]-P_max_batt) + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { P_fc_U[iter] = tab[iter][22]-P_max_batt  }
				}
			}
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1]+E_fc_i_U[iter];
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			
		}
		
		else if(operating_mode == "fuel_cell"){ // Ici on utilise seulement la pile
			
			if(document.getElementById("modelePACU").value=="1"){
				P_fc_U[iter] = tab[iter][22];
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (tab[iter][22] + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { P_fc_U[iter] = tab[iter][22]  }
				}
			}
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;

		}
		else if(operating_mode == "brake"){                       // Freinage : recuperation d'energie jusqu'a 5km/h = 0%, 17km/h = 100% et 100% au dessus de 17
			
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=0;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = 0;
					}
				}
			}		
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
			if (tab[iter][1]<=vminVPU){P_batt_U[iter] = 0 - P_fc_U[iter]}
			else if (vminVPU<tab[iter][1] && tab[iter][1]<=vmaxVPU){
				var xA = vminVPU;
				var yA = 0;
				var xB = vmaxVPU;
				var yB = 100;
				var coeffDir = (yB-yA)/(xB-xA);
				var C = yA-xA*coeffDir;
				P_batt_U[iter] = tab[iter][22]*(coeffDir*tab[iter][1]+C)/100-P_fc_U[iter];
			}
			else if (tab[iter][1]>vmaxVPU){P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]}
						
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
		else if(operating_mode == "stop"){ // Arret véhicule
			
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=0;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = 0;
					}
				}
			}		
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
		}

	}
	
	else if((document.getElementById("loiCommande").value)=="4"){ // Loi de commande de l'UTBM
		
		//Calcul puissance fournie par la pile
		
		//On impose un seuil max et un seuil min de fonctionnement à la PAC
		if (tab[iter][22]<0.1*P_max_fc_U){var PminVPU=0.1*P_max_fc_U}
		else if (tab[iter][22]>0.4*P_max_fc_U) {var PminVPU=0.4*P_max_fc_U} 
		else {var PminVPU=tab[iter][22]}
		
		// NB : SoC = State of Charge = état de charge de la batterie, capacité restante sur capacité totale. Noté per_batt_r_Ah_U dans le code (Ah pour Ampère*heure, comme on parle de capacité)
		// NB2 : SoE = State of Energy = état d'énergie, énergie restante sur l'énergie totale stockable dans la batterie. Noté per_batt_r_U dans le code
		
		//On multiplie par le rapport du SoC initial sur le SoC actuel
		//Cela permet de garder un SoC à peu près stable sur la durée du trajet
		var PRegulationSoCVPU = (PminVPU*(E_batt_r_U[0]/E_batt_r_U[iter-1]))
		
		
		//On met en place un test pour corriger les problèmes de continuité lié au calcul d'un trajet en plusieurs étapes
		//Si une partie d'un trajet à déja été réalisé, il ne faut pas faire repartir la PAC à zéro car cell-ci possède un temps de réponse non nul
		//Lors de la jonction des trajets, il faut récupérer la dernière valeur de la puissance de la PAC.
		//Sans cette veleurs, on créer une discontinuité de la puissance instanténée fournie par la PAC ce qui engendre une augmentation du coût  et une baisse du SOC   M.G.
		
		if(memory==2 && tabMemory.length !=0 && iter == 2){
			E_batt_r_U[0] =E_batt_r_U_REF;
			P_fc_U[iter-1]=tabMemory[3][tabMemory[3].length-1];
			PfreqVPU[iter-1]=PfreqVPUMemory;
		}
		
		
		
		//On met un filtre passe bas pour garder seulement les fréquences basses pour la pile, et on donne le reste à la batterie
		PfreqVPU[iter] = (PRegulationSoCVPU + 10*PfreqVPU[iter-1]/tab[iter][5])/(1+10/tab[iter][5]);
		window.PfreqVPUMemory=PfreqVPU[iter];															//On stock la dernière valeur calculée pour la continuité du calcul lors des calucls en ligne
		
		
		if(document.getElementById("modelePACU").value=="1"){
			P_fc_U[iter] = PfreqVPU[iter];
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (PfreqVPU[iter] + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { P_fc_U[iter] = PfreqVPU[iter]  }
			}
		}
		
		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		
		
		if(tab[iter][22]<0){ //freinage régénératif
			
			if (tab[iter][1]<=vminVPU){P_batt_U[iter] = 0 - P_fc_U[iter]}
			else if (vminVPU<tab[iter][1] && tab[iter][1]<=vmaxVPU){
				var xA = vminVPU;
				var yA = 0;
				var xB = vmaxVPU;
				var yB = 100;
				var coeffDir = (yB-yA)/(xB-xA);
				var C = yA-xA*coeffDir;
				P_batt_U[iter] = tab[iter][22]*(coeffDir*tab[iter][1]+C)/100-P_fc_U[iter];
			}
			else if (tab[iter][1]>vmaxVPU){P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]}
						
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]; 
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1]; 
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
		else if(tab[iter][22]==0){
			P_batt_U[iter] = -P_fc_U[iter];
			E_batt_i_U[iter] = 0;
			E_batt_U[iter] = E_batt_U[iter-1];		
			E_r_U[iter] = E_r_U[iter-1];
		}

	}
	
	else if((document.getElementById("loiCommande").value)=="5" || loiTemp == 5){ // Getsion de l'énergie par limitation de pente de puissance PAC
	
	if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]>40 && per_batt_r_Ah_U[iter-1]<80){ //Tant que l'état de charge de la batterie est entre ces deux seuils, on fonctionne comme suit :
		
		//Calcul de la puissance fournie par la PAC
		//On calcule la pente du signal de puissance demandé par le moteur, et si elle est supérieur à un seuil, alors on limite la puissance fournie par la pile à ce maximum
		//Le seuil a été fixé à 50Wh/s car c'ets la seule valeur truvée dans la littérature, mais on pourrait bien sûr en mettre un autre en argumentant le choix
		if((document.getElementById("modelePACU").value)=="1"){
			var pentePuissance = (tab[iter][22]-P_fc_U[iter-1])/(tab[iter][5]);
			
			if (pentePuissance > 50){
				P_fc_U[iter] = 50*tab[iter][5] + P_fc_U[iter-1];
			}
			else if (pentePuissance < -50){
				P_fc_U[iter] = -50*tab[iter][5] + P_fc_U[iter-1];
			}
			else {
				P_fc_U[iter] = tab[iter][22]
			}
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					var pentePuissance = (tab[iter][22]-P_fc_U[iter-1])/(tab[iter][5]);
	
					if (pentePuissance > 50){
						var Pfciter = 50*tab[iter][5] + P_fc_U[iter-1];
					}
					else if (pentePuissance < -50){
						var Pfciter = -50*tab[iter][5] + P_fc_U[iter-1];
					}
					else {
						var Pfciter = tab[iter][22]
					}
					P_fc_U[iter] = (Pfciter + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					var pentePuissance = (tab[iter][22]-P_fc_U[iter-1])/(tab[iter][5]);
	
					if (pentePuissance > 50){
						P_fc_U[iter] = 50*tab[iter][5] + P_fc_U[iter-1];
					}
					else if (pentePuissance < -50){
						P_fc_U[iter] = -50*tab[iter][5] + P_fc_U[iter-1];
					}
					else {
						P_fc_U[iter] = tab[iter][22]
					}
				}
			}
		}

		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		

		
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1]; 
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
	}
	
	else if(tab[iter][22]<0){  //freinage régénératif

		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter]=0;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					P_fc_U[iter] = 0;
				}
			}
		}
		
		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		
		if (tab[iter][1]<=vminVPU){P_batt_U[iter] = 0 - P_fc_U[iter]}
		else if (vminVPU<tab[iter][1] && tab[iter][1]<=vmaxVPU){
		var xA = vminVPU;
		var yA = 0;
		var xB = vmaxVPU;
		var yB = 100;
		var coeffDir = (yB-yA)/(xB-xA);
		var C = yA-xA*coeffDir;
		P_batt_U[iter] = tab[iter][22]*(coeffDir*tab[iter][1]+C)/100-P_fc_U[iter];
		}
		else if (tab[iter][1]>vmaxVPU){P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]}
		
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		E_batt_U[iter] = E_batt_U[iter-1];
		E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
	}
	
	else if(tab[iter][22]==0){//Arrêt

		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter]=0;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					P_fc_U[iter] = 0;
				}
			}
		}
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
	}
	
	else if (tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]<40){ //état de charge inférieur à 40% : on demande toute la puissance à la PC, le temps que la batterie remonte au dessus de 40%
			
			if(document.getElementById("modelePACU").value=="1"){
				P_fc_U[iter] = tab[iter][22];
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (tab[iter][22] + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { P_fc_U[iter] = tab[iter][22]  }
				}
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

			act_pac_U[iter] = 1;
			act_batt_U[iter] = 0;
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
	}
	
	else if (tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]>80){ //état de charge supérieur à 80 % : on demande toute la puissance à la batterie, jusuq'à ce qu'elle resdescende en-dessous de 80%
		
		//NB : on a choisit 40% et 80% comme seuil, dans d'autres lois ce sont 40 et 60. Ces choix ont été faits basés sur la littérature, ce sont en général les états de charge recherhés puisque les rendement sont meilleurs ici. 
		
		if((document.getElementById("modelePACU").value)=="1"){
			var pentePuissance = (0-P_fc_U[iter-1])/(tab[iter][5]);
			
			if (pentePuissance > 50){
				P_fc_U[iter] = 50*tab[iter][5] + P_fc_U[iter-1];
			}
			else if (pentePuissance < -50){
				P_fc_U[iter] = -50*tab[iter][5] + P_fc_U[iter-1];
			}
			else {
				P_fc_U[iter] = 0
			}
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					var pentePuissance = (0-P_fc_U[iter-1])/(tab[iter][5]);
	
					if (pentePuissance > 50){
						var Pfciter = 50*tab[iter][5] + P_fc_U[iter-1];
					}
					else if (pentePuissance < -50){
						var Pfciter = -50*tab[iter][5] + P_fc_U[iter-1];
					}
					else {
						var Pfciter = 0
					}
					P_fc_U[iter] = (Pfciter + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					var pentePuissance = (0-P_fc_U[iter-1])/(tab[iter][5]);
	
					if (pentePuissance > 50){
						P_fc_U[iter] = 50*tab[iter][5] + P_fc_U[iter-1];
					}
					else if (pentePuissance < -50){
						P_fc_U[iter] = -50*tab[iter][5] + P_fc_U[iter-1];
					}
					else {
						P_fc_U[iter] = 0
					}
				}
			}
		}

		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
	}
		
	}
	
	else if((document.getElementById("loiCommande").value)=="6" || loiTemp == 6){ // Partage de la puissance par bande

		if (tab[iter][22]>0 && 40<per_batt_r_Ah_U[iter-1] && per_batt_r_Ah_U[iter-1]<60){
			
			//On impose un seuil min (10% de la puissance max) et max (40% de la puissance max) de fonctionnement à la PAC
			
			if (tab[iter][22]<0.1*P_max_fc_U){var PfcU=0.1*P_max_fc_U}
			else if (tab[iter][22]>0.4*P_max_fc_U) {var PfcU=0.4*P_max_fc_U}
			else {var PfcU=tab[iter][22]}
			
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=PfcU;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
						P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (PfcU + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = PfcU;
					}
				}
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
			
			
			if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
		}
		
		else if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]>60){
			var PfcU=0.1*P_max_fc_U;
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=PfcU;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
						P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (PfcU + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = PfcU;
					}
				}
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
			
			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
		}
		
		else if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]<40){
			var PfcU=0.4*P_max_fc_U;
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=PfcU;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
						P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (PfcU + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = PfcU;
					}
				}
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
			
			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
		}
		
		else if(tab[iter][22]<0){ 
			
			var PfcU=0.1*P_max_fc_U;
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=PfcU;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
						P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (PfcU + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = PfcU;
					}
				}
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
			
			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			
			if (tab[iter][1]<=vminVPU){P_batt_U[iter] = 0 - P_fc_U[iter]}
			else if (vminVPU<tab[iter][1] && tab[iter][1]<=vmaxVPU){
				var xA = vminVPU;
				var yA = 0;
				var xB = vmaxVPU;
				var yB = 100;
				var coeffDir = (yB-yA)/(xB-xA);
				var C = yA-xA*coeffDir;
				P_batt_U[iter] = tab[iter][22]*(coeffDir*tab[iter][1]+C)/100-P_fc_U[iter];
			}
			else if (tab[iter][1]>vmaxVPU){P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]}
						
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			
			
		}
		
		
		
		else if(tab[iter][22]==0){
			
			var PfcU=0.1*P_max_fc_U;
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=PfcU;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
						P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (PfcU + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = PfcU;
					}
				}
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
			
			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
		}
		
	}
	
	else if((document.getElementById("loiCommande").value)=="7"){ // Optimisation de la consommation
		
		if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]>40 && per_batt_r_Ah_U[iter-1]<60){

			var consoU = new Array();
			var ii = 0;
			
			//ici on calcule pour différentes répartition de puissnace la consommation énergétique totale
			for (var Pb=0; Pb<=tab[iter][22]; Pb+=tab[iter][22]/1000){
				var Pfc = tab[iter][22] - Pb;
				consoU[ii] = Pfc/0.55 + Pb/0.95; //On prend en compte des rendements fixe pour le moment
				ii++;
			}
			//On trouve le minimum
			minConsoTot = Math.min(...consoU)

			var index = consoU.indexOf(minConsoTot);

			var PfcU = tab[iter][22]-index*tab[iter][22]/1000;

			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=PfcU;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (PfcU + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = PfcU;
					}
				}
			}

			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
	
			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
	
			act_pac_U[iter] = 1;
			act_batt_U[iter] = 1;
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
		}
		
		else if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]<=40){
			
			
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=tab[iter][22];
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (tab[iter][22] + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = tab[iter][22];
					}
				}
			}
			
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
		}
		
		else if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]>60){
			
			
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=0;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = 0;
					}
				}
			}
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
			
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

			
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
		}
		
		else if(tab[iter][22]<0){//freinage 
			
			if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=0;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = 0;
					}
				}
			}
						
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			
			if (tab[iter][1]<=vminVPU){P_batt_U[iter] = 0 - P_fc_U[iter]}
			else if (vminVPU<tab[iter][1] && tab[iter][1]<=vmaxVPU){
				var xA = vminVPU;
				var yA = 0;
				var xB = vmaxVPU;
				var yB = 100;
				var coeffDir = (yB-yA)/(xB-xA);
				var C = yA-xA*coeffDir;
				P_batt_U[iter] = tab[iter][22]*(coeffDir*tab[iter][1]+C)/100-P_fc_U[iter];
			}
			else if (tab[iter][1]>vmaxVPU){P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]}
					
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		

		
		 else if(tab[iter][22]==0){
			 
			 if ((document.getElementById("modelePACU").value)=="1"){
					P_fc_U[iter]=0;
			}
			else{
				if ((document.getElementById("modePuissancePACU").value)=="2"){
					P_fc_U[iter] = 0
				}
				else {
					if ((document.getElementById("tpsReponsePACU").value)=="1"){
						P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
					}
					else { 
						P_fc_U[iter] = 0;
					}
				}
			}
								
			if(document.getElementById("modelePACU").value=="2"){
				rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			}
			else{rend_fc_U = 0.6}
			E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
			E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
			if(document.getElementById("modelePACU").value=="2"){
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
			}
			else{
				M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
			}
			M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
			if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
				E_r_U[iter] = E_r_U[iter-1];
			}
			
			else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
				P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
				E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
				E_batt_U[iter] = E_batt_U[iter-1];
				E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			}
		 }
		
	}
	
else if((document.getElementById("loiCommande").value)=="8" || loiTemp == 8){ // Partage
																// fréquentiel
																// de
																// la
																// puissance
	
	
	if(tab[iter][22]>0 && 40<per_batt_r_Ah_U[iter-1] && per_batt_r_Ah_U[iter-1]<60){
		
		//Calcul de la puissance fournie par la PAC : utilisation d'un filtre passe bas afin que la pac fournisse les fréquences basses et que la batterie se chrage du reste
		pfcFreqU[iter] = (tab[iter][22] + 10*pfcFreqU[iter-1]/tab[iter][5])/(1+10/tab[iter][5]); 
		
		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter] = pfcFreqU[iter];
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (pfcFreqU[iter] + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { P_fc_U[iter] = pfcFreqU[iter]  }
			}
		}
		

		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
			
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
		
		
	}
	
	else if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]>60){
		
		pfcFreqU[iter] = (tab[iter][22] + 10*pfcFreqU[iter-1]/tab[iter][5])/(1+10/tab[iter][5]);
		
		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter] = 0;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { P_fc_U[iter] = 0 }
			}
		}
		

		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
	}
	
	else if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]<40){
		
		pfcFreqU[iter] = (tab[iter][22] + 10*pfcFreqU[iter-1]/tab[iter][5])/(1+10/tab[iter][5]);
		
		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter] = tab[iter][22];
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0;
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (tab[iter][22] + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { P_fc_U[iter] = tab[iter][22] }
			}
		}
		

		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
	}
	
	else if(tab[iter][22]<0){//freinage
		
		pfcFreqU[iter] = (pfcFreqU[iter-1]);
		
		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter] = 0;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { P_fc_U[iter] = 0  }
			}
		}
		
		

		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		
			if (tab[iter][1]<=vminVPU){P_batt_U[iter] = 0 - P_fc_U[iter]}
			else if (vminVPU<tab[iter][1] && tab[iter][1]<=vmaxVPU){
				var xA = vminVPU;
				var yA = 0;
				var xB = vmaxVPU;
				var yB = 100;
				var coeffDir = (yB-yA)/(xB-xA);
				var C = yA-xA*coeffDir;
				P_batt_U[iter] = tab[iter][22]*(coeffDir*tab[iter][1]+C)/100-P_fc_U[iter];
			}
			else if (tab[iter][1]>vmaxVPU){P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]}
						
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
			
	}
		
		
		
	else if(tab[iter][22]==0){
		
		pfcFreqU[iter] = (pfcFreqU[iter-1]);
		
		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter] = 0;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { P_fc_U[iter] = 0  }
			}
		}
		
		

		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
	}

	}
	
else if((document.getElementById("loiCommande").value)=="9"){ // Optimisation de la fonction coût
	
	//Même fonctionnement que l'optimisation de la consommation mais avce le coût
	
	if(tab[iter][22]>0 && 40<per_batt_r_Ah_U[iter-1] && per_batt_r_Ah_U[iter-1]<60){

		var prixU = new Array();
		var ii = 0;
		
		for (var Pb=0; Pb<=tab[iter][22]; Pb+=tab[iter][22]/1000){
			var Pfc = tab[iter][22] - Pb;
			if(document.getElementById("modelePACU").value=="1"){
				prixU[ii] = (Pfc/0.6)*(tab[iter][5]/3600)*valPrixH2/(d_en_H2/3600);
			}
			else {
				prixU[ii] = (Pfc/0.55)*(tab[iter][5]/3600)*valPrixH2/d_en_H2_U;
			}
			ii++;
		}
		minConsoTot = Math.min(...prixU)

		var index = prixU.indexOf(minConsoTot);

		var PfcU = tab[iter][22]-index*tab[iter][22]/1000;

		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter]=PfcU;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (PfcU + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					P_fc_U[iter] = PfcU;
				}
			}
		}
		
		
		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];

		act_pac_U[iter] = 1;
		act_batt_U[iter] = 1;
		
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
	}
	
	else if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]<=40){
		
		
		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter]=tab[iter][22];
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (tab[iter][22] + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					P_fc_U[iter] = tab[iter][22];
				}
			}
		}
		
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
		
	}
	
	else if(tab[iter][22]>0 && per_batt_r_Ah_U[iter-1]>60){
		
		
		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter]=0;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					P_fc_U[iter] = 0;
				}
			}
		}
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
		
		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

		
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
		
	}
	
	else if(tab[iter][22]<0){
		
		if ((document.getElementById("modelePACU").value)=="1"){
			P_fc_U[iter]=0;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					P_fc_U[iter] = 0;
				}
			}
		}
					
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
		
		if (tab[iter][1]<=vminVPU){P_batt_U[iter] = 0 - P_fc_U[iter]}
		else if (vminVPU<tab[iter][1] && tab[iter][1]<=vmaxVPU){
			var xA = vminVPU;
			var yA = 0;
			var xB = vmaxVPU;
			var yB = 100;
			var coeffDir = (yB-yA)/(xB-xA);
			var C = yA-xA*coeffDir;
			P_batt_U[iter] = tab[iter][22]*(coeffDir*tab[iter][1]+C)/100-P_fc_U[iter];
		}
		else if (tab[iter][1]>vmaxVPU){P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]}
				
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		E_batt_U[iter] = E_batt_U[iter-1];
		E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
	}
	
	
	 else if(tab[iter][22]==0){
		 
		 if ((document.getElementById("modelePACU").value)=="1"){
				P_fc_U[iter]=0;
		}
		else{
			if ((document.getElementById("modePuissancePACU").value)=="2"){
				P_fc_U[iter] = 0;
			}
			else {
				if ((document.getElementById("tpsReponsePACU").value)=="1"){
					P_fc_U[iter] = (0 + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
				}
				else { 
					P_fc_U[iter] = 0;
				}
			}
		}
							
		if(document.getElementById("modelePACU").value=="2"){
			rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
		}
		else{rend_fc_U = 0.6}
		E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
		E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];
		if(document.getElementById("modelePACU").value=="2"){
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
		}
		else{
			M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
		}
		M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
		if (tab[iter][22]>=0 && P_fc_U[iter]<tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
			E_r_U[iter] = E_r_U[iter-1];
		}
		
		else if (tab[iter][22]>=0 && P_fc_U[iter]>=tab[iter][22]){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter];
			E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
			E_batt_U[iter] = E_batt_U[iter-1];
			E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		}
	 }
	
}
else if((document.getElementById("loiCommande").value)=="11"){ 

	
	//P_max_fc_U;
	var Pmot_min = -90000;
	var Pmot_max=100000;
	
	var SOCHigh= 0.9;
	var SOCLow= 0.4;
	var SOCExpected=0.5*(SOCLow+SOCHigh);
	
	SOCExpected = 0.55;
	
	var SOC = tab[iter][32];
	var Pmot = tab[iter][22];
	
	var SOC_fuzz= SOC/100;
	var Pmot_fuzz = (Pmot/Pmot_max);
	/*
	if(Pmot_fuzz !=0 ){
		console.log("test");
	}
	*/
	
	var vectSOC = SOC_FLC(SOC_fuzz);
	//console.log('vectSOC :' + vectSOC);
	var vectPmot =Pmot_FLC(Pmot_fuzz);
	//console.log('vectPmot :' +vectPmot);
	
	
	var vectLoi =operationRègles(vectSOC, vectPmot);
	//console.log('vectLoi :' +vectLoi);
	crisp = deffuzzification(vectLoi);
	
	//console.log('crisp =' +crisp, 'SOC/Pmot ' + SOC_fuzz +'/' + Pmot_fuzz);
	
	var Puissance_PAC= crisp*P_max_fc_U;
	
	
	

	if(document.getElementById("modelePACU").value=="1"){
		P_fc_U[iter] = 0;
	}
	else{
		if ((document.getElementById("modePuissancePACU").value)=="2"){
			P_fc_U[iter] = 0
		}
		else {
			if ((document.getElementById("tpsReponsePACU").value)=="1"){
				P_fc_U[iter] = (Puissance_PAC  + (0.16*P_fc_U[iter-1])/tab[iter][5])/(1+0.16/tab[iter][5])
			}
			else { P_fc_U[iter] = Puissance_PAC;  }
		}
	}
	
	
	
	console.log('Temps : '+ iter + '/  Crisp : '+ crisp);
	var condition = 0.5*(SOCHigh-SOCLow);  //0.25
	
	if(tab[iter][0] <= 30){
		console.log("Démarage");
		P_fc_U[iter] = 10000;
		P_batt_U[iter] = tab[iter][22]; 
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
		E_r_U[iter] = E_r_U[iter-1];
	}
	
	

	else if(tab[iter][22]<0){ //freinage régénératif
		console.log("freinage");
		if (tab[iter][1]<=vminVPU){P_batt_U[iter] = 0 - P_fc_U[iter]}
		else if (vminVPU<tab[iter][1] && tab[iter][1]<=vmaxVPU){
			var xA = vminVPU;
			var yA = 0;
			var xB = vmaxVPU;
			var yB = 100;
			var coeffDir = (yB-yA)/(xB-xA);
			var C = yA-xA*coeffDir;
			P_batt_U[iter] = tab[iter][22]*(coeffDir*tab[iter][1]+C)/100-P_fc_U[iter];
			
		}
		else if (tab[iter][1]>vmaxVPU){
			P_batt_U[iter] = tab[iter][22]-P_fc_U[iter]
			//P_batt_U[iter] = P_batt_U[iter]*0.30;
		}
					
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		E_batt_U[iter] = E_batt_U[iter-1];
		
		E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		
	}
	
	
	
	else if(tab[iter][22] > 0 && tab[iter][22] > P_fc_U[iter]){   // Peut importe le SOC, Preq > P_fc_max , ==> Preq = P_fc_max +Pbatt
		console.log("Pfc+PB");
		P_batt_U[iter]= tab[iter][22] - P_fc_U[iter];
		
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
		E_r_U[iter] = E_r_U[iter-1];
		
		
		
	}
	
	
	else if(SOC_fuzz <= SOCExpected &&  tab[iter][22] > 0 && tab[iter][22] <= P_fc_U[iter]){    //Si SOC trop bas et Preq < P_fc max  ==> P_fc = Preq+Pbatt
		console.log("SOC bas / "+SOC );
		P_batt_U[iter] = ((SOC_fuzz-SOCExpected)/condition)*10000;
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		/*
		E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
		E_r_U[iter] = E_r_U[iter-1];
		*/
		E_batt_U[iter] = E_batt_U[iter-1]; 
		E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		
		P_fc_U[iter] = tab[iter][22] + P_batt_U[iter];
		
	}
	
	else if(SOC_fuzz > SOCExpected &&  tab[iter][22] > 0 && tab[iter][22] <= P_fc_U[iter]){   //Si SOC trop haut , Preq = PB+ P_fc
		console.log("SOC haut /" +SOC );
		P_batt_U[iter] = (SOC_fuzz-SOCExpected)/condition*10000;
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
		E_r_U[iter] = E_r_U[iter-1];
		
		P_fc_U[iter]= tab[iter][22]- P_batt_U[iter]
		
	}
	
	/*else if(tab[iter][22] > 0 && tab[iter][22] > P_fc_U[iter]){   // Peut importe le SOC, Preq > P_fc_max , ==> Preq = P_fc_max +Pbatt
		console.log("Pfc+PB");
		P_batt_U[iter]= tab[iter][22] - P_fc_U[iter];
		
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		E_batt_U[iter] = E_batt_U[iter-1] + E_batt_i_U[iter];
		E_r_U[iter] = E_r_U[iter-1];
		
		
		
	}*/
	
	
	
	
	
	
	
	
	
	
	else if(tab[iter][22]==0){
		console.log("pnulle");//Pmot =0
		P_batt_U[iter] = -P_fc_U[iter];
		E_batt_i_U[iter] = P_batt_U[iter]*tab[iter][5]/3600;
		E_batt_U[iter] = E_batt_U[iter-1]; 
		E_r_U[iter] = E_r_U[iter-1] - E_batt_i_U[iter];
		
	}
	
	
	
	E_fc_i_U[iter] = P_fc_U[iter]*tab[iter][5]/3600;
	E_fc_U[iter] = E_fc_U[iter-1] + E_fc_i_U[iter];

	
	if(document.getElementById("modelePACU").value=="2"){
		rend_fc_U = valeurRend(P_fc_U[iter]/P_max_fc_U);
	}
	else{rend_fc_U = 0.6}
	if(document.getElementById("modelePACU").value=="2"){
		M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2_U*rend_converter_DC);
	}
	else{
		M_H2_i_U[iter] = E_fc_i_U[iter]/(rend_fc_U*d_en_H2/3600);
	}
	M_H2_U[iter] = M_H2_U[iter-1] + M_H2_i_U[iter];
	
	
	
}
	
	
	
	if(document.getElementById("modeleBatterie").value=="1"){ // modèle Altran avec prise en compte des pertes joules dans la batterie
		
		if (document.getElementById("temperatureBatterie").value!="3"){ // Prise en compte de la température
			// Equation polynomiale tirée de graphiques de la littérature représentant l'évolution de la résistance interne fonction de la température et du SoC
			if(T[iter-1]<25){
				var valResistIntBatt = -0.007*T[iter-1]+0.225+nbCellBatt*0.001*(-0.000005*Math.pow(per_batt_r_Ah_U[iter-1],3)+0.0009*Math.pow(per_batt_r_Ah_U[iter-1],2)-0.0533*per_batt_r_Ah_U[iter-1]+2.465)-0.05
			}
			else {
				var valResistIntBatt = 0.05+nbCellBatt*0.001*(-0.000005*Math.pow(per_batt_r_Ah_U[iter-1],3)+0.0009*Math.pow(per_batt_r_Ah_U[iter-1],2)-0.0533*per_batt_r_Ah_U[iter-1]+2.465)-0.05
			}
		}
		else { // Non prise en compte de la température, équation polynomiale tirée d'un graphique de la littérature représentant l'évolution de la résistance interne fonction du SoC
			valResistIntBatt = nbCellBatt*0.001*(-0.000005*Math.pow(per_batt_r_Ah_U[iter-1],3)+0.0009*Math.pow(per_batt_r_Ah_U[iter-1],2)-0.0533*per_batt_r_Ah_U[iter-1]+2.465)
		}
		
		// Equation polynomiale tirée de graphiques de la littérature représentant l'évolution de la tension en circuit ouvert (OCV) fonction du SoC
		var valOCVBatt = nbCellBatt*(0.000006*Math.pow(per_batt_r_Ah_U[iter-1],3)-0.001*Math.pow(per_batt_r_Ah_U[iter-1],2)+0.0581*per_batt_r_Ah_U[iter-1]+7)
		
		// Calcul de l'intensité dans la batterie
		if(P_batt_U[iter]>=0){
			I[iter]=(valOCVBatt-Math.sqrt(Math.pow(valOCVBatt,2)-4*valResistIntBatt*P_batt_U[iter]))/(2*valResistIntBatt);
		}
		else if (P_batt_U[iter]<0){
			I[iter]=(-valOCVBatt+Math.sqrt(Math.pow(valOCVBatt,2)+4*valResistIntBatt*P_batt_U[iter]))/(2*valResistIntBatt);
		}

		// Calcul de la tension batterie
		U[iter] = valOCVBatt - valResistIntBatt*I[iter];	
		
		// Calcul de la température dans la batterie
		if(document.getElementById("temperatureBatterie").value=="1"){ // Ici on fait l'hypothèse que la batterie est à température ambiante (= température extérieure entrée par l'utilisateur, 20°C par défaut)
			T[iter]=valTemperatureExt;
		}
		else if (document.getElementById("temperatureBatterie").value=="2") { // Ici on prend en compte l'évolution de la température de la batterie avec un modèle thermique
			T[iter]=(valResistIntBatt*Math.pow(I[iter],2)*0.1+valTemperatureExt+15)/2;
		}
		// Calcul de l'énergie restante dans la batterie
		E_batt_r_U[iter] = E_batt_r_U[iter-1] - (tab[iter][5]/3600*(valOCVBatt*I[iter]));
		if(E_batt_r_U[iter]>capBatterie*1000) {E_batt_r_U[iter]=capBatterie*1000}
		
		// Calcul du SoC
		per_batt_r_Ah_U[iter] = per_batt_r_Ah_U[iter-1] - ((tab[iter][5]/3600)*(I[iter]))/(capBatterie*10/U[iter]);
		if(per_batt_r_Ah_U[iter]>100){per_batt_r_Ah_U[iter]=100}
			
		// Calcul du SoE
		per_batt_r_U[iter] = E_batt_r_U[iter]/(capBatterie*10);
		if(per_batt_r_U[iter]>100){per_batt_r_U[iter]=100}
		
		// Calcul de l'énegrie totale consommée (énergie consommée à la pile + énergie consommée à la batterie (charge et décharge !))
		if (document.getElementById("modelePACU").value=="1"){
			E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2/3600)+(tab[iter][5]/3600*(((valOCVBatt*I[iter]))));
		}
		else{
			E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2_U)+(tab[iter][5]/3600*(((valOCVBatt*I[iter]))));
		}

		// Prix du trajet
		if (document.getElementById("loiCommande").value=="3"){ //Calcul du prix pour la loi plug-in à revoir
			prix_U[iter]=prix_U[iter-1]+((M_H2_i_U[iter])*valPrixH2+(valPrixElec/1000)*(tab[iter][5]/3600*(valOCVBatt*I[iter])));
		}
		else{
			prix_U[iter]=prix_U[iter-1]+(M_H2_i_U[iter])*valPrixH2;
		}
		//if(P_batt_U[iter]>0) {alert("P electrochimique : "+valOCVBatt*I[iter]+" P batt : "+P_batt_U[iter]+" Pertes : "+valResistIntBatt*Math.pow(I[iter],2))}
	}
	
	else if (document.getElementById("modeleBatterie").value=="2"){ // Modèle développé par l'UTBM
		
		if(P_batt_U[iter]<0){
			E_batt_r_U[iter] = E_batt_r_U[iter-1] - (E_batt_i_U[iter]*0.9); // rendement de 0.9 en charge de la batterie
			if (document.getElementById("modelePACU").value=="1"){
				E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2/3600)+(E_batt_i_U[iter]*0.9);		
			}
			else{
				E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2_U)+(E_batt_i_U[iter]*0.9);		
			}
		}
		else{ 
			E_batt_r_U[iter] = E_batt_r_U[iter-1] - (E_batt_i_U[iter]/1); // rendement de 1 en décharge de la batterie
			if (document.getElementById("modelePACU").value=="1"){
				E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2/3600)+(E_batt_i_U[iter]/1);		
			}
			else{
				E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2_U)+(E_batt_i_U[iter]/1);		
			}
		}
		
		// Prix du trajet
		if (document.getElementById("loiCommande").value=="3"){
			prix_U[iter]=prix_U[iter-1]+((M_H2_i_U[iter])*valPrixH2+(valPrixElec/1000)*(tab[iter][5]/3600*((valOCVBatt*I[iter])+(valOCVBatt*I[iter-1]))/2));
		}
		else{
			prix_U[iter]=prix_U[iter-1]+(M_H2_i_U[iter])*valPrixH2;
		}		
		
		if(E_batt_r_U[iter]>capBatterie*1000) {E_batt_r_U[iter]=capBatterie*1000}
		
		// SoE
		per_batt_r_U[iter] = E_batt_r_U[iter]/(capBatterie*10);
		if(per_batt_r_U[iter]>100){per_batt_r_U[iter]=100}
		
		// SoC = SoE dans modèle UTBM
		per_batt_r_Ah_U[iter] = per_batt_r_U[iter]; 
		

	}
	else if (document.getElementById("modeleBatterie").value=="3"){ // Modèle basique
		
		if(P_batt_U[iter]<0){
			E_batt_r_U[iter] = E_batt_r_U[iter-1] - E_batt_i_U[iter]*rend_elec;
			// Energie totale consommée
			if (document.getElementById("modelePACU").value=="1"){
				E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2/3600)+(E_batt_i_U[iter]*rend_elec);		
			}
			else{
				E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2_U)+(E_batt_i_U[iter]*rend_elec);		
			}
		}
		else {
			E_batt_r_U[iter] = E_batt_r_U[iter-1] - E_batt_i_U[iter]/rend_elec; 
			// Energie totale consommée
			if (document.getElementById("modelePACU").value=="1"){
				E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2/3600)+(E_batt_i_U[iter]/rend_elec);		
			}
			else{
				E_tot[iter]=E_tot[iter-1]+(M_H2_i_U[iter]*d_en_H2_U)+(E_batt_i_U[iter]/rend_elec);		
			}
		}
		if(E_batt_r_U[iter]>capBatterie*1000) {E_batt_r_U[iter]=capBatterie*1000}
		per_batt_r_U[iter] = E_batt_r_U[iter]/(capBatterie*10);
		
		if(per_batt_r_U[iter]>100){per_batt_r_U[iter]=100}
		
		per_batt_r_Ah_U[iter] = per_batt_r_U[iter]; // le modèle basique ne fait pas la différence entre SoC et SoE
		
		
		
		// Prix du trajet
		if (document.getElementById("loiCommande").value=="3"){
			prix_U[iter]=prix_U[iter-1]+((M_H2_i_U[iter])*valPrixH2+(valPrixElec/1000)*(tab[iter][5]/3600*((valOCVBatt*I[iter])+(valOCVBatt*I[iter-1]))/2));
		}
		else{
			prix_U[iter]=prix_U[iter-1]+(M_H2_i_U[iter])*valPrixH2;
		}
	}
	
	M_H2_r_U[iter] = M_H2_r_U[iter-1] - M_H2_i_U[iter];
	per_H2_r_U[iter] = M_H2_r_U[iter]/(capH2*0.01);
	tab[iter][32] = per_batt_r_Ah_U[iter];
	
	// Ci-dessous calculs pour les énergies fournies/consommées par les différents organes et pour calculer leurs rendements
	
	if(tab[iter][22]>=0){// Pas de freinage régénératif
		energieFournieMoteur[iter] = energieFournieMoteur[iter-1]+(tab[iter][21]*tab[iter][5]/3600);
		energieConsoGenerateur[iter] = energieConsoGenerateur[iter-1];
		energieFournieGenerateur[iter] = energieFournieGenerateur[iter-1];
		if(P_fc_U[iter]>tab[iter][22]){ // PAC qui fournit de l'énergie à la batterie
			energieFournieBatterie[iter] = energieFournieBatterie[iter-1];
			energieFournieBattParPac[iter] = energieFournieBattParPac[iter-1]-E_batt_i_U[iter];
			energieChargeeBattParPac[iter] = energieChargeeBattParPac[iter-1]+(E_batt_r_U[iter]-E_batt_r_U[iter-1]);
			energieChargeeBattParGen[iter] = energieChargeeBattParGen[iter-1];
		}
		else if(P_fc_U[iter]==tab[iter][22]){// PAC ne fournit pas d'énergie à la batterie, la batterie ne fournit pas d'énergie
			energieFournieBatterie[iter] = energieFournieBatterie[iter-1];
			energieFournieBattParPac[iter] = energieFournieBattParPac[iter-1];
			energieChargeeBattParPac[iter] = energieChargeeBattParPac[iter-1];
			energieChargeeBattParGen[iter] = energieChargeeBattParGen[iter-1];
		}
		else if(P_fc_U[iter]<tab[iter][22]){//Batterie fournit l'énegrie supplémentaire nécessaire
			energieFournieBatterie[iter] = energieFournieBatterie[iter-1]+E_batt_i_U[iter];
			energieFournieBattParPac[iter] = energieFournieBattParPac[iter-1];
			energieChargeeBattParPac[iter] = energieChargeeBattParPac[iter-1];
			energieChargeeBattParGen[iter] = energieChargeeBattParGen[iter-1];
		}
	}
	else if(tab[iter][22]<0){// Freinage régénératif
		energieConsoGenerateur[iter] = energieConsoGenerateur[iter-1]-(tab[iter][21]*tab[iter][5]/3600);
		energieFournieMoteur[iter] = energieFournieMoteur[iter-1];
		if(P_fc_U[iter]>0){ // PAC qui fournit de l'énergie à la batterie
			var pourcentPfc = -P_fc_U[iter]/P_batt_U[iter];
			energieFournieBatterie[iter] = energieFournieBatterie[iter-1];
			energieFournieBattParPac[iter] = energieFournieBattParPac[iter-1]-E_batt_i_U[iter]*pourcentPfc;
			energieChargeeBattParPac[iter] = energieChargeeBattParPac[iter-1]+(E_batt_r_U[iter]-E_batt_r_U[iter-1])*pourcentPfc;
			energieChargeeBattParGen[iter] = energieChargeeBattParGen[iter-1]+(E_batt_r_U[iter]-E_batt_r_U[iter-1])*(1-pourcentPfc);
			energieFournieGenerateur[iter] = energieFournieGenerateur[iter-1]-E_batt_i_U[iter]*(1-pourcentPfc);
		}
		else if (P_fc_U[iter]==0){// PAC ne fournit pas d'énergie à la batterie
			energieFournieBatterie[iter] = energieFournieBatterie[iter-1];
			energieFournieBattParPac[iter] = energieFournieBattParPac[iter-1];
			energieChargeeBattParPac[iter] = energieChargeeBattParPac[iter-1];
			energieChargeeBattParGen[iter] = energieChargeeBattParGen[iter-1]+(E_batt_r_U[iter]-E_batt_r_U[iter-1]);
			energieFournieGenerateur[iter] = energieFournieGenerateur[iter-1]-E_batt_i_U[iter];
		}
	}

	
	if (CS==false){
		
		dataConsoPacUTBM[1].push(E_batt_i_U[iter]);
		dataConsoPacUTBM[0].push(P_batt_U[iter]);
		dataConsoPacUTBM[2].push(E_batt_U[iter]);
		dataConsoPacUTBM[3].push(P_fc_U[iter]);
		dataConsoPacUTBM[4].push(E_fc_i_U[iter]);
		dataConsoPacUTBM[5].push(E_fc_U[iter]);
		dataConsoPacUTBM[6].push(M_H2_U[iter]);
		dataConsoPacUTBM[7].push(tab[iter][3]/1000);
		dataConsoPacUTBM[8].push(tab[iter][22]);
		dataConsoPacUTBM[9].push(act_batt_U[iter]);
		dataConsoPacUTBM[10].push(act_pac_U[iter]);
		dataConsoPacUTBM[11].push(E_batt_r_U[iter]);
		dataConsoPacUTBM[12].push(tab[iter][0]);
		dataConsoPacUTBM[13].push(tab[iter][23]);
		dataConsoPacUTBM[14].push(per_batt_r_U[iter]);
		dataConsoPacUTBM[15].push(tab[iter][1]);
		dataConsoPacUTBM[16].push(tab[iter][22]-(P_fc_U[iter] + P_batt_U[iter]));
		dataConsoPacUTBM[17].push(tab[iter][4]);
		dataConsoPacUTBM[18].push(tab[iter][6]);
		dataConsoPacUTBM[19].push(tab[iter][20]);
		dataConsoPacUTBM[20].push(tab[iter][23]);
		dataConsoPacUTBM[21].push(E_r_U[iter]);
		dataConsoPacUTBM[22].push(M_H2_r_U[iter]);
		dataConsoPacUTBM[23].push(per_H2_r_U[iter]);
		if(document.getElementById("modeleBatterie").value=="1"){
			dataConsoPacUTBM[24].push(T[iter]);
			dataConsoPacUTBM[26].push(I[iter]);
			dataConsoPacUTBM[29].push(U[iter]);
		}
		else{
			dataConsoPacUTBM[24].push();
			dataConsoPacUTBM[26].push();
		}
		dataConsoPacUTBM[25].push(E_tot[iter]);
		dataConsoPacUTBM[27].push(prix_U[iter]);
		dataConsoPacUTBM[28].push(per_batt_r_Ah_U[iter]);
		dataConsoPacUTBM[30].push(energieFournieMoteur[iter]);
		dataConsoPacUTBM[31].push(energieConsoGenerateur[iter]);
		dataConsoPacUTBM[32].push(energieFournieGenerateur[iter]);
		dataConsoPacUTBM[33].push(energieFournieBatterie[iter]);
		dataConsoPacUTBM[34].push(energieChargeeBattParPac[iter]);
		dataConsoPacUTBM[35].push(energieFournieBattParPac[iter]);
		dataConsoPacUTBM[36].push(energieChargeeBattParGen[iter]);
		dataConsoPacUTBM[38].push(tabvlimAugmente[iter]);
		dataConsoPacUTBM[39].push(crisp);
		
		
	}
	
	if (CS==true) {	
		dataConsoPacUTBM_CS[1].push(E_batt_i_U[iter]);
		dataConsoPacUTBM_CS[0].push(P_batt_U[iter]);
		dataConsoPacUTBM_CS[2].push(E_batt_U[iter]);
		dataConsoPacUTBM_CS[3].push(P_fc_U[iter]);
		dataConsoPacUTBM_CS[4].push(E_fc_i_U[iter]);
		dataConsoPacUTBM_CS[5].push(E_fc_U[iter]);
		dataConsoPacUTBM_CS[6].push(M_H2_U[iter]);
		dataConsoPacUTBM_CS[7].push(tab[iter][3]/1000);
		dataConsoPacUTBM_CS[8].push(tab[iter][22]);
		dataConsoPacUTBM_CS[9].push(act_batt_U[iter]);
		dataConsoPacUTBM_CS[10].push(act_pac_U[iter]);
		dataConsoPacUTBM_CS[11].push(E_batt_r_U[iter]);
		dataConsoPacUTBM_CS[12].push(tab[iter][0]);
		dataConsoPacUTBM_CS[13].push(tab[iter][23]);
		dataConsoPacUTBM_CS[14].push(per_batt_r_U[iter]);
		dataConsoPacUTBM_CS[15].push(tab[iter][1]);
		dataConsoPacUTBM_CS[16].push(tab[iter][22]-(P_fc_U[iter] + P_batt_U[iter]));
		dataConsoPacUTBM_CS[17].push(tab[iter][4]);
		dataConsoPacUTBM_CS[18].push(tab[iter][6]);
		dataConsoPacUTBM_CS[19].push(tab[iter][20]);
		dataConsoPacUTBM_CS[20].push(tab[iter][23]);
		dataConsoPacUTBM_CS[21].push(E_r_U[iter]);
		dataConsoPacUTBM_CS[22].push(M_H2_r_U[iter]);
		dataConsoPacUTBM_CS[23].push(per_H2_r_U[iter]);
		if(document.getElementById("modeleBatterie")=="1"){
			dataConsoPacUTBM_CS[24].push(T[iter]);
			dataConsoPacUTBM_CS[26].push(I[iter]);
			dataConsoPacUTBM_CS[29].push(U[iter]);
		}
		else{
			dataConsoPacUTBM_CS[24].push();
			dataConsoPacUTBM_CS[26].push();
		}
		dataConsoPacUTBM_CS[25].push(E_tot[iter]);
		dataConsoPacUTBM_CS[27].push(prix_U[iter]);
		dataConsoPacUTBM_CS[28].push(per_batt_r_Ah_U[iter]);
		dataConsoPacUTBM_CS[30].push(energieFournieMoteur[iter]);
		dataConsoPacUTBM_CS[31].push(energieConsoGenerateur[iter]);
		dataConsoPacUTBM_CS[32].push(energieFournieGenerateur[iter]);
		dataConsoPacUTBM_CS[33].push(energieFournieBatterie[iter]);
		dataConsoPacUTBM_CS[34].push(energieChargeeBattParPac[iter]);
		dataConsoPacUTBM_CS[35].push(energieFournieBattParPac[iter]);
		dataConsoPacUTBM_CS[36].push(energieChargeeBattParGen[iter]);
	}
// if(P_batt_U[iter]<0){
// ppp=0;
// }
// else{
// ppp=P_batt_U[iter];
// }
// loiTXT += "\n"+ppp+","+P_fc_U[iter]

}

/**
 * Permet de determiner la loi à suivre en focntion des 3 paramètres de l'itération précedante : Vitesse, Penste et SOC
 * Décrit la loi statistique Hybrique 
 * 
 * @param vitesseHReelle
 *            vitesse réelle du vehicule à l'iteration 'iter' necessaire pour déterminer la loi à suivre
 * @param penteHReelle
 *            pente Reelle  à l'iteration 'iter'
 * @param batterieHReelle
 *            SOC réelle à l'itération 'iter'
 * @returns numeroLoi
 * 			retourne le numéro de la loi à suivre pour l'itération à venir (sorties possibles : 2,5,6,8 et 0 si pas de résulats) 
 */
function defLoiHybride(vitesseHReelle,penteHReelle, batterieHReelle){
	//var vitesseHReelle= 135;
	//var penteHReelle =0.1;
	//var batterieHReelle = 45.5;
	var vitesseHArr;													// Auncunes des valeurs initialisées ici ne va servir aux calculs de la consommation. Ces dernières servent
	var penteHArr;														// juste à déterminer la loi de commande à selectionner
	var batterieHArr;
	var numeroLoi= 0;
	
	
	if(vitesseHReelle <= 15) { vitesseHArr = 10}
	else if(15 < vitesseHReelle && vitesseHReelle <= 26) { vitesseHArr = 20}   //On compare les paramètres réels avec des intervarls pré-établis et on leur affecte des valeurs 
	else if(26 < vitesseHReelle && vitesseHReelle <= 36) { vitesseHArr = 30}   // "arrondies" afin de coller avec les valeurs utilisées lors de l'étude statistique de la loi hybride
	else if(36 < vitesseHReelle && vitesseHReelle <= 60) { vitesseHArr = 50}
	else if(60 < vitesseHReelle && vitesseHReelle <= 76) { vitesseHArr = 70}
	else if(76 < vitesseHReelle && vitesseHReelle <= 86) { vitesseHArr = 80}
	else if(86 < vitesseHReelle && vitesseHReelle <= 100) { vitesseHArr = 90}
	else if(100 < vitesseHReelle && vitesseHReelle < 120) { vitesseHArr = 110}
	else if(120 <= vitesseHReelle ) { vitesseHArr = 130}
	
	
	if (/**0 <= penteHReelle && **/ penteHReelle < 0.05){ penteHArr = 0}		// Pour la pente, on récupère une valeur de forme '0.04', ce qui correspond avec une pente
	else if (0.05 <= penteHReelle && penteHReelle < 0.1){ penteHArr = 5}		// de 4 %.  On affecte à cette valeur de pente 0%, 5%, ou 10% mais cette dernière n'est en 
	else if ( penteHReelle => 0.1){ penteHArr = 10}								// aucun cas utilisée pour des calculs. Elle va simplement servir à déterminer la loi de calcul
	
	
	if (/*0 <= batterieHReelle &&*/ batterieHReelle < 45){batterieHArr = 30}
	else if (45 <= batterieHReelle && batterieHReelle < 75){batterieHArr = 60}
	else if (75 <= batterieHReelle && batterieHReelle <= 100){batterieHArr = 100}
	//console.log(vitesseHReelle +"  "+ penteHReelle + "  " + batterieHReelle);
	//console.log(vitesseHArr +"  "+ penteHArr + "  " + batterieHArr);

	var table = [
		[10,0,30,2],[10,0,60,6],[10,0,100,8],[10,5,30,2],[10,5,60,2],[10,5,100,2],[10,10,30,2],[10,10,60,2],[10,10,100,2],				//Cette matrice comprend tous les résultats de l'étude statistique
		[20,0,30,2],[20,0,60,6],[20,0,100,8],[20,5,30,2],[20,5,60,2],[20,5,100,2],[20,10,30,2],[20,10,60,5],[10,10,100,2],				// pour déterminer la loi hybride. Chaque ligne correspond à un résultat
		[30,0,30,2],[30,0,60,2],[30,0,100,8],[30,5,30,2],[30,5,60,5],[30,5,100,2],[30,10,30,2],[30,10,60,5],[30,10,100,5],				// de l'étude. Format : [Vitesse, Pente, SOC, Numero Loi]
		[50,0,30,2],[50,0,60,2],[50,0,100,8],[50,5,30,2],[50,5,60,5],[50,5,100,2],[50,10,30,2],[50,10,60,5],[50,10,100,5],				// Exemple : Vitesse = 80, Pente = 10, SOC = 100 
		[70,0,30,2],[70,0,60,2],[70,0,100,2],[70,5,30,2],[70,5,60,5],[70,5,100,5],[70,10,30,6],[70,10,60,2],[70,10,100,5],				// D'aprés l'étude statistique on est ammené à utiliser la loi 6 : Partage / Bande
		[80,0,30,2],[80,0,60,5],[80,0,100,2],[80,5,30,2],[80,5,60,5],[80,5,100,5],[80,10,30,6],[80,10,60,6],[80,10,100,6],				// on rempli donc une ligne de la matrice comme suit : [80,10,100,6]
		[90,0,30,2],[90,0,60,5],[90,0,100,2],[90,5,30,2],[90,5,60,5],[90,5,100,5],[90,10,30,6],[90,10,60,6],[90,10,100,6],
		[110,0,30,2],[110,0,60,5],[110,0,100,5],[110,5,30,6],[110,5,60,6],[110,5,100,6],[110,10,30,6],[110,10,60,6],[110,10,100,6],
		[130,0,30,2],[130,0,60,5],[130,0,100,5],[130,5,30,6],[130,5,60,6],[130,5,100,6],[130,10,30,6],[130,10,60,6],[130,10,100,6],
		]
	
	
	
	for(var i = 0;i<table.length-1;i++){																								// On vérifie pour chaque ligne de la matrice si on a une correspondance 
		if( table[i][0] == vitesseHArr  && table[i][1] == penteHArr && table[i][2] == batterieHArr){									// Si oui, on retourne le dernier élément de cette liste qui correspond à la loi
			numeroLoi = table[i][3];
			
			//console.log('Numéro de la loi à utiliser  :' +numeroLoi);
			return numeroLoi
		}
	}
	alert("Pas de sortie trouvée par la loi hybride");
	return 0
}
	


/**
 * Plan d'expérience automatique
 */

var tabPlanExp = "#Loi de commande,Puissance Moteur,Puissance PAC,Capacite Batterie,,Modele PAC, Modele Batterie, Modele Moteur," +
		"Prix aux 100 km,Consommation totale energetique aux 100 km,Consommation d'hydrogene aux 100 km,Consommation electrique aux 100 km," +
		"Energie consommée par la batterie en decharge aux 100 km, Energie rechargée à la batterie par le générateur aux 100 km, " +
		"Energie totale fournie par la pile aux 100 km, Energie fournie par la pile au moteur aux 100 km, " +
		"Energie fournie par le moteur aux 100 km, Energie totale fournie au moteur aux 100 km, " +
		"Energie consommée parle générateur aux 100 km, Energie fournie par le générateur aux 100 km,Energie fournie par la batterie au moteur aux 100 km," +
		"Energie rechargée à la batterie par la pile aux 100 km, " +
		"Energie fournie par la pile à la batterie aux 100 km," +
		"Rendement moyen de la pile, Rendement moyen de la batterie en décharge, Rendement moyen de la batterie en charge par freinage, " +
		"Rendement moyen de la batterie en charge par la pile, Rendement moyen du moteur, Rendement moyen du générateur," +
		"Energie consommee par la pile aux 100 km, Energie totale recupérée à la batterie"
		//"Distance, Denivele, Cycle";
		// ,Distance trajet,Autoroute";
function calcPlanExp(){
	
	for (var i=0;i<8;i++){
		if (i==0){
			document.getElementById("loiCommande").value = 1;
		}
		else if (i==1){
			document.getElementById("loiCommande").value = 2;
		}
		else if (i==2){
			document.getElementById("loiCommande").value = 4;
		}
		else if (i==3){
			document.getElementById("loiCommande").value = 5;
		}
		else if (i==4){
			document.getElementById("loiCommande").value = 6;
		}
		else if (i==5){
			document.getElementById("loiCommande").value = 7;
		}
		else if (i==6){
			document.getElementById("loiCommande").value = 8;
		}
		else if (i==7){
			document.getElementById("loiCommande").value = 9;
		}
		for (var j=0;j<2;j++){
			if (j==0){
				document.getElementById('PmotorratedVPU').value = 100;
				document.getElementById('PmotorMaxVPU').value = 120;
			}
			else {
				document.getElementById('PmotorratedVPU').value = 160;
				document.getElementById('PmotorMaxVPU').value = 190;
			}
			
			for (var k=0;k<2;k++){
				if (k==0){
					document.getElementById('valeurPuissMaxPACU').value = 90000;
				}
				else {
					document.getElementById('valeurPuissMaxPACU').value = 140000;
				}
				for (var l=0;l<2;l++){
					if (l==0){
						document.getElementById('capBatterieVPU').value = 1.5;
					}
					else {
						document.getElementById('capBatterieVPU').value = 6;
					}
					
						
						for (var m=0;m<2;m++){
							if(m==0){
								document.getElementById("modelePACU").value = 1; 
							}
							else if(m==1){
								document.getElementById("modelePACU").value = 2;
								document.getElementById('tpsReponsePACU').value = 1;
								document.getElementById('rendPACU').value = 2;
							}
							for (var n=0;n<3;n++){
								if(n==0){
									document.getElementById('modeleBatterie').value = 1;
									document.getElementById("temperatureBatterie").value = 2;
								}
								else if (n==1){
									document.getElementById('modeleBatterie').value = 2;
								}
								else if (n==2){
									document.getElementById('modeleBatterie').value = 3;
								}
								for (var o=0;o<3;o++){
									if(o==0){
										document.getElementById('modeleMoteur').value = 1;
									}
									else if (o==1){
										document.getElementById('modeleMoteur').value = 2;
									}
									else if (o==2){
										document.getElementById('modeleMoteur').value = 3;
									}
												
									document.getElementById("modePuissancePACU").value = 3;
												
				// boolExp = true;
				//								
				// var depart = new google.maps.LatLng(47.63967400000001, 6.863848999999959);
				// var arrivee = new google.maps.LatLng(48.57340529999999, 7.752111300000024);
												
				// document.getElementById("autocompleteArrivalVPU").value = "Belfort, France";
				// document.getElementById("autocompleteDepartureVPU").value = "Strasbourg, France";
												
				// calcRoute(depart, arrivee);
				
												
												 
												document.getElementById('penteVPU').value = 1;
												// document.getElementById('penteValeurVPU').value = -1;
												document.getElementById('resolutionVPU').value = 2;
												
												
												calcConsommation();
												
												// calcCycleStandard();
												
												var consoElec100 = ((dataConsoPacUTBM[11][1]-dataConsoPacUTBM[11][dataConsoPacUTBM[11].length-1])/(dataStep[nbStep][5]/1000))*100;
												var consoElecCharge100 = (dataConsoPacUTBM[36][dataConsoPacUTBM[36].length-1]+dataConsoPacUTBM[34][dataConsoPacUTBM[34].length-1])/(dataStep[nbStep][5]/1000)*100;
												var consoElecDecharge100 = consoElec100 + consoElecCharge100;
	// var consoElec100 = ((dataConsoPacUTBM_CS[11][1]-dataConsoPacUTBM_CS[11][dataConsoPacUTBM_CS[11].length-1])/(dataCycle[nbPointCycle][3]/1000))*100;
	// var consoElecCharge100 = ((dataConsoPacUTBM_CS[21][dataConsoPacUTBM_CS[21].length-1])/(dataCycle[nbPointCycle][3]/1000))*100;
	// var consoElecDecharge100 = consoElec100 + consoElecCharge100;
	
												tabPlanExp += "\n"+document.getElementById("loiCommande").value+","+Pmotorrated+","+P_max_fc_U+","+capBatterie+","
																+document.getElementById("modelePACU").value+","+document.getElementById("modeleBatterie").value+","
																+document.getElementById("modeleMoteur").value+","
																// Prix aux 100 km
																+dataConsoPacUTBM[27][dataConsoPacUTBM[27].length-1]/(dataStep[nbStep][5]/1000)*100+","
																// Consommation énergétique totale aux 100 km
																+((dataConsoPacUTBM[25][dataConsoPacUTBM[25].length-1]/(dataStep[nbStep][5]/1000))*100)+","
																// Consommation en hydrogène aux 100 km
																+(dataConsoPacUTBM[6][dataConsoPacUTBM[6].length - 1])/(dataStep[nbStep][5]/1000)*100+","
																// Consommation électrique totale aux 100, consommation en décharge et énergie rechargée par le générateur à la batterie
																+consoElec100+","+consoElecDecharge100+","+(dataConsoPacUTBM[36][dataConsoPacUTBM[36].length-1]/(dataStep[nbStep][5]/1000)*100)+","
																// Energie totale fournie par la pile
																+dataConsoPacUTBM[5][dataConsoPacUTBM[5].length-1]/(dataStep[nbStep][5]/1000)*100+","
																// Energie fournie par la pile au moteur
																+(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length-1]-dataConsoPacUTBM[35][dataConsoPacUTBM[35].length-1])/(dataStep[nbStep][5]/1000)*100+","
																// Energie fournie par le moteur
																+dataConsoPacUTBM[30][dataConsoPacUTBM[30].length-1]/(dataStep[nbStep][5]/1000)*100+","
																// Energie consommée par le moteur
																+((dataConsoPacUTBM[5][dataConsoPacUTBM[5].length-1]-dataConsoPacUTBM[35][dataConsoPacUTBM[35].length-1])+dataConsoPacUTBM[33][dataConsoPacUTBM[33].length-1])/(dataStep[nbStep][5]/1000)*100+","
																// Energie consommée par le générateur = énergie fournie par le freinage
																+dataConsoPacUTBM[31][dataConsoPacUTBM[31].length-1]/(dataStep[nbStep][5]/1000)*100+","
																// Energie fournie par le générateur = énergie fournie à la batterie
																+dataConsoPacUTBM[32][dataConsoPacUTBM[32].length-1]/(dataStep[nbStep][5]/1000)*100+","
																// Energie fournie par la batterie au moteur
																+dataConsoPacUTBM[33][dataConsoPacUTBM[33].length-1]/(dataStep[nbStep][5]/1000)*100+","
																// Energie rechargée sur la batterie venant de la pile
																+dataConsoPacUTBM[34][dataConsoPacUTBM[34].length-1]/(dataStep[nbStep][5]/1000)*100+","
																// Energie fournie par la pile à la batterie
																+dataConsoPacUTBM[35][dataConsoPacUTBM[35].length-1]/(dataStep[nbStep][5]/1000)*100+","
																// Rendement moyen de la pile
																+dataConsoPacUTBM[5][dataConsoPacUTBM[5].length-1]/(dataConsoPacUTBM[25][dataConsoPacUTBM[25].length-1]-(dataConsoPacUTBM[11][1]-dataConsoPacUTBM[11][dataConsoPacUTBM[11].length-1]))+","
																// Rendement moyen de la batterie en décharge
																+(dataConsoPacUTBM[33][dataConsoPacUTBM[33].length-1]/(dataStep[nbStep][5]/1000)*100)/consoElecDecharge100+","
																// Rendement moyen de la batterie en charge par freinage
																+(dataConsoPacUTBM[36][dataConsoPacUTBM[36].length-1])/dataConsoPacUTBM[32][dataConsoPacUTBM[32].length-1]+","
																// Rendement moyen de la batterie en charge par la pile
																+dataConsoPacUTBM[34][dataConsoPacUTBM[34].length-1]/dataConsoPacUTBM[35][dataConsoPacUTBM[35].length-1]+","
																// Rendement moyen moteur
																+dataConsoPacUTBM[30][dataConsoPacUTBM[30].length-1]/(dataConsoPacUTBM[33][dataConsoPacUTBM[33].length-1]+(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length-1]-dataConsoPacUTBM[35][dataConsoPacUTBM[35].length-1]))+","
																// Rendement moyen générateur
																+(dataConsoPacUTBM[32][dataConsoPacUTBM[32].length-1])/dataConsoPacUTBM[31][dataConsoPacUTBM[31].length-1]+","
																// Energie consommée par la pile
																+((dataConsoPacUTBM[25][dataConsoPacUTBM[25].length-1]/(dataStep[nbStep][5]/1000)*100)-consoElec100)+","
																// +","+"3"+","+"4"+","+"1"
																//Energie récupérée totale à la batterie
																+dataConsoPacUTBM[21][dataConsoPacUTBM[21].length-1]/(dataStep[nbStep][5]/1000)*100
// dataConsoPacUTBM[30].push(energieFournieMoteur[iter]);
// dataConsoPacUTBM[31].push(energieConsoGenerateur[iter]);
// dataConsoPacUTBM[32].push(energieFournieGenerateur[iter]);
// dataConsoPacUTBM[33].push(energieFournieBatterie[iter]);
	
	// tabPlanExp +=
	// "\n"+Pmotorrated+","+P_max_fc_U+","+capBatterie+","+document.getElementById("loiCommande").value+","+document.getElementById("modelePACU").value+","+document.getElementById("modeleBatterie").value+","+document.getElementById("modeleMoteur").value+","+dataConsoPacUTBM_CS[27][dataConsoPacUTBM_CS[27].length-1]/(dataCycle[nbPointCycle][3]/1000)*100+","+((dataConsoPacUTBM_CS[25][dataConsoPacUTBM_CS[25].length-1]/(dataCycle[nbPointCycle][3]/1000))*100)+","+(dataConsoPacUTBM_CS[6][dataConsoPacUTBM_CS[6].length
	// - 1])/(dataCycle[nbPointCycle][3]/1000)*100+","+consoElec100+","+consoElecDecharge100+","+consoElecCharge100//+","+"3"+","+"2"
												
							}
						}
					}
				}
			}
		}
	}

	// document.body.innerHTML = tabPlanExp;
	
	downloadTXT(tabPlanExp,"tabPlanExp.txt");
}

/**
 * Pour telecharger des resultats
 * 
 * @param data
 *            donnees a sauvegarder
 * @param outName
 *            nom du fichier de sortie
 * @returns
 */
function downloadTXT(data, outName) {
         var a = window.document.createElement('a');
         a.setAttribute('href', 'data:text/csv; charset=utf-8,' + encodeURIComponent(data));
         a.setAttribute('download', outName);
         a.click(); 
}


/**
 * définition du réseau de neurones
 * 
 */

function logsig(t) {
    return 1/(1+Math.exp(-t));
}

function logsigprim(t){
	return Math.exp(-t)/Math.pow((1+Math.exp(-t)),2);
}

function tansig(t){
	return (2/(1+Math.exp(-2*t)))+1;
}

function tansigprim(t){
	return (4*Math.exp(-2*t))/Math.pow(1+Math.exp(-2*t),2);
}

function purelin(t){
	return t;
}

function purelinprim(t){
	return 1;
}

var w = [];       // on
					// crée
					// le
					// tableau
					// des
					// poids
var net = [];     // on
					// crée
					// le
					// tableau
					// des
					// poids
					// associés
					// aux
					// inputs
var out = [];     // création
					// du
					// tableau
					// des
					// sorties
var slope = [];   // tableau
					// des
					// pente
					// des
					// fonctions
var error;        // creation
					// de
					// la
					// variable
					// d'erreur
var delta = [];
var J = [];
out[1] = [];
net[1] = [];
out[2] = [];
net[2] = [];
out[3] = [];
net[3] = [];
slope[1] = [];
slope[2] = [];
slope[3] = [];
delta[1] = [];
delta[2] = [];
delta[3] = [];

function reseau_neur(neurc1,neurc2,ninputs){
	
	if(w[1] == undefined){                             // si
														// les
														// poids
														// ne
														// sont
														// pas
														// definis,
														// ils
														// ne
														// sont
														// pas
														// initialisés
														// donc
														// on
														// les
														// initialise
		
		w[1] = [];                                     // w[couche][entrée][neurone]
		w[2] = [];
		w[3] = [];
		
		// initialisation
		// des
		// poids
		// pour
		// la
		// 1è
		// couche
		
		for (var j = 0 ; j < ninputs+1; j++){          // j
														// est
														// l'input
														// et
														// +1
														// pour
														// les
														// biais
			w[1][j] = [];
			for(var k = 0 ; k < neurc1; k++){          // k
														// est
														// le
														// neurone
				w[1][j][k] = Math.random()*2-1;        // initialisation
														// du
														// poids
														// pourr
														// chaque
														// neurone
														// de
														// la
														// couche
														// 1
														// entre
														// -1
														// et 1
			}
		}
		
		// initialisation
		// des
		// poids
		// pour
		// la
		// 2è
		// couche
		
		for(var j = 0 ; j < neurc1+1 ; j++){
			w[2][j] = [];
			for(var k = 0 ; k < neurc2 ; k++){
				w[2][j][k] = Math.random()*2-1;
			}
		}
		
		// initialisation
		// des
		// poids
		// de
		// la
		// 3e
		// couche
		
		for(var j = 0; j < neurc2+1 ; j++){
			w[3][j] = [];
			w[3][j][1] = Math.random()*2-1;
		}
	}
	
	// calcul
	// du
	// net,
	// de
	// l'out
	// et
	// du
	// gradient
	// (slope)
	// de
	// chaque
	// neurone
	// pour
	// la
	// 1e
	// couche
	// (forward
	// computation)
	
	for(var k = 0 ; k < neurc1; k++){
		net[1][k] = 0;                                                      // initialisation
																			// de
																			// la
																			// somme
																			// des
																			// poids
																			// multipliés
																			// par
																			// les
																			// entrées
		for(var j = 0; j < inputs; j++){
			net[1][k] = net[1][k] + vitesse[j] * w[1][j][k];                // ajout
																			// de
																			// chaque
																			// terme
																			// de
																			// la
																			// somme
																			// (entrée
																			// du
																			// neurone)
		}
		
		net[1][k] = net[1][k] + 1 * w[1][inputs][k];                             // ajout
																					// du
																					// biais
		
		out[1][k] = logsig(net[1][k]);                                      // calcul
																			// de
																			// la
																			// sortie
																			// des
																			// neurones
																			// 1er
																			// couche
		
		slope[1][k] = logsigprim(net[1][k]);                                // calcul
																			// du
																			// gradient
																			// pour
																			// chaque
																			// neurone
	}
	
	// couche
	// 2
	
	for(var k = 0 ; k < neurc2 ; k++){
		net[2][k] = 0;
		for(var j = 0; j < neurc1; j++){
			net[2][k] = net[2][k] + out[2][j] * w[2][j][k];                // calcul
																			// entrée
																			// neurones
																			// sans
																			// biais
		}
		
		net[2][k] = net[2][k] + 1 * w[2][neurc1][k];                       // ajout
																			// du
																			// biais
		
		out[2][k] = tansig(net[2][k]);
		
		slope[2][k] = tansigprim(net[2][k]);
	}
	
	// couche
	// 3
	
	net[3][1] = 0;                                                          // ici
																			// on a
																			// un
																			// seul
																			// neurone
																			// donc
	for(var j = 0; j < neurc2 ; j++){
		net[3][1] = net[3][1] + out[2][j] * w[3][j][1];
	}
	
	net[3][1] = net[3][1] + 1 * w[3][neurc2][1];                            // ajout
																			// du
																			// biais
	
	out[3][1] = purelin(net[3][1]);                                         // output
																			// du
																			// reseau
																			// de
																			// neurone
	
	slope[3][1] = purelinprim(net[3][1]);                                   // pente
																			// au
																			// point
																			// etudie
	
	// calcul
	// de
	// l'erreur
	// et
	// des
	// delta
	// liés
	// à la
	// sortie
	// (backward
	// computation),
	// delta
	// est
	// la
	// propagation
	// en
	// arrière
	
	error = vitesse[inputs] - out[3][1];
	
	delta[3][1] = slope[3][1];  
	
	// calcul
	// delta
	// reliant
	// input
	// de
	// la
	// 3e
	// couche
	// à la
	// sortie
	// de
	// la
	// 2e
	// couche
	
	delta[2][1] = [];                                     // delta
															// couche
															// 2
	for(var j = 0; j < neurc2; j++){                      // delta
															// de
															// chaque
															// neurone,
															// couche
															// 2
		delta[2][j] = [];
		delta[2][j][1] = w[3][j][1] * delta[3][1];        // 1
															// seul
															// terme
															// car
															// 1
															// neurone
															// en
															// couche
															// 3
		delta[2][j][1] = delta[2][j][1] * slope[2][j];
	}
	
	// calcul
	// delta
	// reliant
	// input
	// couche
	// 2 et
	// output
	// couche
	// 1
	
	for(var j = 0; j < neurc1;j++){                        // pour
															// chaque
															// neurone
															// de
															// la
															// couche
															// 1
		delta[1][j] = 0;                                  
		for(var k = 0; k<neurc2;k++){                      // pour
															// chacun
															// des
															// poids
															// liés
															// au
															// neurone
															// de
															// la
															// couche
															// 1
															// (il
															// y en
															// a le
															// nombre
															// de
															// neurones
															// de
															// la
															// couche
															// 2
			delta[1][j] = delta[1][j] + delta[2][k][1] * w[2][j][k];
		}
		delta[1][j] = delta[1][j] * slope[2][j];
	}
	
	// calcul
	// des
	// éléments
	// de
	// la
	// matrice
	// Jacobienne
	// à
	// partir
	// des
	// delta
	
	for(var j= 0; j < neurc1; j++){
		J[j] = []
	}
	
}


function calcCycleStandard(){
	if(typeVehicule =="VE" || typeVehicule =="VP" || typeVehicule =="VPU"){
		boolTrajet = false;
		calcConsoCycleStandard();
	}
	else if(typeVehicule=="VT"){
		if(boolOBD){
			initCalculThermique();
			calcConsoCycleStandard();
		}
		else{
			alert("Veuillez lancer le traitement des donnees OBD (onglet OBD)");
		}
	}
	
}
/**
 * Calcul des energies pour un trajet d'un cycle standard
 */

function calcConsoCycleStandard(){
	
	var t0 = new Date().getTime();
	
	initialisationParam();
	calcNormfactor(Pmotorrated);

	if(typeVehicule=="VE" || typeVehicule =="VPU"){
		var energie = socBatterie/100*capBatterie*1000;
	}
	
	// Creation
	// du
	// tableau
	// de
	// donnee
	var tabCycle = new Array();
	creaTableau(dataCycle);
	nbPointCycle = lectureCycle('#cycleStandard'+typeVehicule,tabCycle,true,cyclesFileSingle);
	
	calcDistanceCycle(tabCycle,nbPointCycle)
	var pmoyen	= 0;
	
	if(boolPenteFixeStandard){
		((document.getElementById('PenteFixe'+typeVehicule).value) != '')? penteFixeStandard 		= parseFloat(document.getElementById('PenteFixe'+typeVehicule).value)/100 			: penteFixeStandard 	  = 0;
		pmoyen = penteFixeStandard;
	}else{
		for(var i=1;i<nbPointCycle+1;i++){
			pmoyen += tabCycle[i][5];
		}
		pmoyen 		 = pmoyen/nbPointCycle;
	}
	
	var ttemp 	 = 0.025;
	dataCycle[1] = new Array();
	var nbParamDataConso = 31;

	for (var k=0;k<nbParamDataConso; k++){					
		dataCycle[1][k] = 0;				// Toutes
											// les
											// variables
											// sont
											// initialisees
											// a
											// zero
	}

	dataCycle[1][31]= 'cycleStandard'+typeVehicule;		// Type
														// de
														// parcours
	if(typeVehicule=="VE" || typeVehicule =="VPU"){
			dataCycle[1][32] = socBatterie;			// taux
													// de
													// charge
													// initial
													// de
													// la
													// batterie
	}
	
	for(var iter = 2;iter<nbPointCycle;iter++){

		dataCycle[iter] 	 = new Array();
		// Calcul
		// du
		// temps
		dataCycle[iter][0] = tabCycle[iter][0]+ttemp; 
		// Calcul
		// des
		// vitesses
		dataCycle[iter][1] = tabCycle[iter][1];
		dataCycle[iter][2] = tabCycle[iter][2];
		// Calcul
		// de
		// la
		// distance
		// cumulee
		dataCycle[iter][3] = tabCycle[iter][3];	
		// Calcul
		// de
		// la
		// pente
		if(boolPenteFixeStandard){
			dataCycle[iter][4] = penteFixeStandard;
		}else{
			dataCycle[iter][4] = tabCycle[iter][5];
		}
		
		// Calcul
		// de
		// delta
		// T
		dataCycle[iter][5] = tabCycle[iter+1][0] - tabCycle[iter][0];

		// Calcul
		// de
		// l'acceleration
		
		var tempAcc 		 = calcPhase(tabCycle[iter][2],tabCycle[iter+1][2],tabCycle[iter][0],tabCycle[iter+1][0]);
		dataCycle[iter][6] = tempAcc.acc;
		dataCycle[iter][7] = tempAcc.phase;
		// Calcul
		// des
		// forces
		// en
		// debut
		// de
		// phase
		var tempFDebut			= calcForce(dataCycle[iter][2],dataCycle[iter][6],dataCycle[iter][4],pmoyen);
		dataCycle[iter][8]  	= tempFDebut.Fr;
		dataCycle[iter][9]  	= tempFDebut.Fair;
		dataCycle[iter][10] 	= tempFDebut.Fp;	
		dataCycle[iter][11] 	= tempFDebut.Facc;
		dataCycle[iter][12] 	= tempFDebut.Fm;
		// Calcul
		// des
		// puissances
		// en
		// debut
		// de
		// phase
		dataCycle[iter][13] 	= calcPuissanceMeca(dataCycle[iter][2],dataCycle[iter][12]).Pmoteur;
		if(typeVehicule=="VE" || typeVehicule =="VPU"){
		dataCycle[iter][14] 	= calcPuissanceElec(dataCycle[iter][13]);
		}
		dataCycle[iter][15] 	= dataCycle[iter][12]*RRoue;	
		// Calcul
		// des
		// forces
		// en
		// fin
		// de
		// phase
		var tempFFin			= calcForce(tabCycle[iter+1][2],dataCycle[iter][6],dataCycle[iter][4],pmoyen);
		
		dataCycle[iter][16]  	= tempFFin.Fr;
		dataCycle[iter][17]  	= tempFFin.Fair;
		dataCycle[iter][18] 	= tempFFin.Fp;	
		dataCycle[iter][19] 	= tempFFin.Facc;
		dataCycle[iter][20] 	= tempFFin.Fm;
		// Calcul
		// des
		// puissances
		// en
		// fin
		// de
		// phase
		dataCycle[iter][21] 	= calcPuissanceMeca(tabCycle[iter+1][2],dataCycle[iter][20]).Pmoteur;
		
		if(typeVehicule=="VE"){
			dataCycle[iter][22] 	= calcPuissanceElec(dataCycle[iter][21]);
		}
		if(typeVehicule =="VPU"){
			dataCycle[iter][22] 	= dataCycle[iter][21];
		}
		dataCycle[iter][23] 	= dataCycle[iter][20]*RRoue;
		// Calcul
		// des
		// maximums
		// par
		// phase
		dataCycle[iter][24] 	= maxAbsolu(dataCycle[iter][13],dataCycle[iter][21])/1000; 
		if(typeVehicule=="VE" || typeVehicule =="VPU"){
			dataCycle[iter][25] 	= maxAbsolu(dataCycle[iter][14],dataCycle[iter][22])/1000; 
		}
		dataCycle[iter][26] 	= maxAbsolu(dataCycle[iter][15],dataCycle[iter][23]); 
		dataCycle[iter][27]		= 0.5*(dataCycle[iter][13]+dataCycle[iter][21])*dataCycle[iter][5]/3600;
		if(typeVehicule=="VE" || typeVehicule =="VPU"){
			dataCycle[iter][28]		= 0.5*(dataCycle[iter][14]+dataCycle[iter][22])*dataCycle[iter][5]/3600;
			// Calcul
			// de
			// l'energie
			// consommee
			// et
			// produite
			if(dataCycle[iter][28] > 0){	// Energie
											// consommee
				dataCycle[iter][29] = dataCycle[iter-1][29] + dataCycle[iter][28];
				dataCycle[iter][30] = dataCycle[iter-1][30];
			}else{							// Energie
											// produite
				dataCycle[iter][29] = dataCycle[iter-1][29];
				dataCycle[iter][30] = dataCycle[iter-1][30] - dataCycle[iter][28];
			}
		}
		else{
			dataCycle[iter][28]	=0;
			if(dataCycle[iter][27] > 0){
				dataCycle[iter][29] = dataCycle[iter-1][29] + dataCycle[iter][27];
			}
			else{
				dataCycle[iter][29] = dataCycle[iter-1][29]
			}
			dataCycle[iter][30] = 0;
		}
		// Calcul
		// du
		// type
		// de
		// parcours
		dataCycle[iter][31]		= 'cycleStandard'+typeVehicule;
		
		if(typeVehicule=="VE" || typeVehicule =="VPU"){
			// Calcul
			// du
			// taux
			// de
			// charge
			// de
			// la
			// batterie
			energie 				= energie - (dataCycle[iter][29]-dataCycle[iter-1][29]) + (dataCycle[iter][30]-dataCycle[iter-1][30]);
			/*
			 * si la deceleration est superieur au seuil maximal permettant d'utiliser le freinage regeneratif (0.3g pour les systemes actuels) alors il faut plafonner la recuperation d'energie avec ce seuil de deceleration
			 */
			if(dataCycle[iter][6] < -3){
				var FfinTampon = calcForce(tabCycle[iter+1][2],-3,dataCycle[iter][4],pmoyen);
				var PmecaT		= calcPuissanceMeca(dataCycle[iter][2],FfinTampon.Fm).Pmoteur;
				var Pelec		= calcPuissanceElec(PmecaT);
				var eMecaT = PmecaT*dataCycle[iter][5]/3600;
				var eElecT = Pelec*dataCycle[iter][5]/3600;
				dataCycle[iter][30]  = dataCycle[iter-1][30] - eElecT;
				energie   = energie + (dataCycle[iter][30] - dataCycle[iter-1][30]);
			}

			dataCycle[iter][32] 	= Math.round(10*energie/capBatterie*100)/10/1000;
			if(dataCycle[iter][32] < 5){ // La
											// batterie
											// ne
											// descend
											// pas
											// en
											// dessous
											// de
											// 5%
				dataCycle[iter][32] = 5;
			}else if(dataCycle[iter][32]>100){
				dataCycle[iter][32] = 100;
				dataCycle[iter][30] = dataCycle[iter-1][30];
			}
		}
		
		else{
			dataCycle[iter][37] = 0;
			// test
			// vitesses
			// et
			// pentes
			// existent
			// dans
			// la
			// Map
			// Vitesses
			if(dataCycle[iter][27]	>0&&pasVitesses*Math.round(dataCycle[iter][1]/pasVitesses)<=maxvecVitesses
					&&pasPente*Math.round(dataCycle[iter][4]*100/pasPente)<=maxvecPente&&pasPente*Math.round(dataCycle[iter][4]*100/pasPente)>=minvecPente){
				
				// rapport
				// de
				// reduction
				// debut
				// de
				// phase
				dataCycle[iter][38] = dataVitesses[MapVitesses[(pasVitesses*Math.round(dataCycle[iter-1][1]/pasVitesses)-minvecVitesses)/pasVitesses][(pasPente*Math.round(dataCycle[iter-1][4]*100/pasPente)-minvecPente)/pasPente]];
				// rapport
				// de
				// reduction
				// fin
				// de
				// phase
				dataCycle[iter][39] = dataVitesses[MapVitesses[(pasVitesses*Math.round(dataCycle[iter][1]/pasVitesses)-minvecVitesses)/pasVitesses][(pasPente*Math.round(dataCycle[iter][4]*100/pasPente)-minvecPente)/pasPente]];
				
				// test
				// la
				// Map
				// Vitesses
				// n'a
				// pas
				// renvoye
				// 0
				// (=pas
				// de
				// donnees
				// pour
				// ces
				// valeurs)
				if(dataCycle[iter][38]!= "Valeurs des rapports de reduction"&&dataCycle[iter][39]!= "Valeurs des rapports de reduction"){
					
					// regime
					// moteur
					// debut
					// de
					// phase
					dataCycle[iter][40] = dataCycle[iter-1][2]*60*dataCycle[iter][38]/(RRoue*2*Math.PI); 
					// regime
					// moteur
					// fin
					// de
					// phase
					dataCycle[iter][41] = dataCycle[iter][2]*60*dataCycle[iter][39]/(RRoue*2*Math.PI); 
					// indice
					// de
					// la
					// moyenne
					// des
					// regimes
					// moteur
					// dans
					// la
					// Map
					// Cse
					dataCycle[iter][42] = (pasNmot*(Math.round(0.5*(dataCycle[iter][40]+dataCycle[iter][41])/pasNmot))-minvecNmot)/pasNmot; 
					// couple
					// moteur
					// exige
					// debut
					// de
					// phase
					dataCycle[iter][43] = dataCycle[iter][15]/dataCycle[iter][38]; 
					// couple
					// moteur
					// exige
					// fin
					// de
					// phase
					dataCycle[iter][44] = dataCycle[iter][23]/dataCycle[iter][39]; 	
					// couple
					// moteur
					// max
					// debut
					// de
					// phase
					dataCycle[iter][56] = coef[6]*Math.pow(dataCycle[iter][40],6)+coef[5]*Math.pow(dataCycle[iter][40],5)+coef[4]*Math.pow(dataCycle[iter][40],4)+coef[3]*Math.pow(dataCycle[iter][40],3)+coef[2]*Math.pow(dataCycle[iter][40],2)+coef[1]*dataCycle[iter][40]+coef[0];
					// couple
					// moteur
					// max
					// fin
					// de
					// phase
					dataCycle[iter][57] = coef[6]*Math.pow(dataCycle[iter][41],6)+coef[5]*Math.pow(dataCycle[iter][41],5)+coef[4]*Math.pow(dataCycle[iter][41],4)+coef[3]*Math.pow(dataCycle[iter][41],3)+coef[2]*Math.pow(dataCycle[iter][41],2)+coef[1]*dataCycle[iter][41]+coef[0];
					// pourcentage
					// de
					// couple
					// debut
					// de
					// phase
					dataCycle[iter][58] = Math.round(dataCycle[iter][43]/dataCycle[iter][56]*100/pasCmot)*pasCmot;
					if(dataCycle[iter][58]>100){
						dataCycle[iter][58]=100;
					}
					// pourcentage
					// de
					// couple
					// fin
					// de
					// phase
					dataCycle[iter][59] = Math.round(dataCycle[iter][44]/dataCycle[iter][57]*100/pasCmot)*pasCmot;
					if(dataCycle[iter][59]>100){
						dataCycle[iter][59] = 100;
					}
					// indice
					// de
					// la
					// moyenne
					// des
					// pourcentages
					// de
					// couples
					// dans
					// la
					// map
					// cse
					dataCycle[iter][60] = (pasCmot*(Math.round(0.5*(dataCycle[iter][58]+dataCycle[iter][59])/pasCmot))-minvecCmot)/pasCmot;
					// test
					// Couple
					// Moteur
					// et
					// Regime
					// moteur
					// existent
					// dans
					// la
					// Map
					// Cse
					if(	dataCycle[iter][42]*pasNmot>=0&&dataCycle[iter][42]*pasNmot<=maxvecNmot-minvecNmot&&dataCycle[iter][59]*pasCmot>=0
							&&dataCycle[iter][59]*pasCmot<=100){
						// calcul
						// de
						// la
						// consommation
						// en
						// litre
						dataCycle[iter][46] = Map_cse_ex[dataCycle[iter][42]][dataCycle[iter][60]]*dataCycle[iter][27]/1000/rhoessence;
						// calcul
						// de
						// la
						// consommation
						// en
						// litre/100
						// km
						dataCycle[iter][47]	= dataCycle[iter][46]/(dataCycle[iter][2]*dataCycle[iter][5])*100000;
						// calcul
						// des
						// rejets
						// de
						// CO2
						// en
						// kg
						dataCycle[iter][50] = dataCycle[iter][46]*rhoessence*ratioCO2/1000;
						// calcul
						// de
						// l'energie
						// produite
						// par
						// l'essence
						dataCycle[iter][52] = dataCycle[iter][46]*rhoessence*essencePCS;
						// calcul
						// de
						// l'energie
						// dissipee
						// en
						// chaleur
						dataCycle[iter][54] = dataCycle[iter][52]-dataCycle[iter][27];
					}
					else{
					
						dataCycle[iter][46] = 0;
						dataCycle[iter][50] = 0;
						dataCycle[iter][52] = 0;
						dataCycle[iter][54] = 0;
					}
				}
				else{
					
					dataCycle[iter][46] = 0;
					dataCycle[iter][50] = 0;
					dataCycle[iter][52] = 0;
					dataCycle[iter][54] = 0;
				}
			}
			else{

				dataCycle[iter][46] = 0;
				dataCycle[iter][50] = 0;
				dataCycle[iter][52] = 0;
				dataCycle[iter][54] = 0;
			}
			
			// calcul
			// de
			// la
			// consommation
			// totale
			// en
			// litres
			dataCycle[iter][48] = dataCycle[iter-1][48]+dataCycle[iter][46];
			dataCycle[iter][51] = dataCycle[iter-1][51]+dataCycle[iter][50];
			// calcul
			// de
			// l'energie
			// totale
			// produite
			// par
			// l'essence
			dataCycle[iter][53] = dataCycle[iter-1][53]+dataCycle[iter][52];
			// calcul
			// de
			// l'energie
			// totale
			// dissipee
			// en
			// chaleur
			dataCycle[iter][55] = dataCycle[iter-1][55]+dataCycle[iter][54];

		}
	}
	
	// Dernier
	// point
	var iter = nbPointCycle;
	dataCycle[iter] 	 = new Array();
	dataCycle[iter][0] = dataCycle[iter-1][0] + 0.25
	for (var i = 1;i<nbParamDataConso;i++){
		dataCycle[iter][i] = dataCycle[iter-1][i]
	}
	var t1 = new Date().getTime();
	for(var iterRayon =1;iterRayon<nbPointCycle+1;iterRayon++){
		dataCycle[iterRayon][36] = dataCycle[iterRayon][3]/1000;
	}
	
	if(typeVehicule =="VT"){
		dataCycle[nbPointCycle][49] = dataCycle[nbPointCycle][48]/dataCycle[nbPointCycle][36]*100;
		rendementmoteur = Math.round(dataCycle[nbPointCycle][29]/dataCycle[nbPointCycle][53]*1000)/10
	}
	
	var mode = parseFloat(document.getElementById("modePuissancePACU").value);
	var loi = parseFloat(document.getElementById("loiCommande").value);
	var modPac = parseFloat(document.getElementById("modelePACU").value);
	var modMot = parseFloat(document.getElementById("modeleBatterie").value);
	var modBat = parseFloat(document.getElementById("modeleMoteur").value);
	
	if (typeVehicule =="VPU" && mode!=0 && modMot!=0 && modBat!=0 && loi!=0 && modPac!=0){
		
		for (iter=2;iter<=nbPointCycle;iter++){

			if(loi=="1"){
				var operating_mode = definition_mode_seuilsAcc(dataCycle,iter);
			}
			if(loi=="2"){
				var operating_mode = definition_mode_seuilsVit(dataCycle,iter);
			}
			if(loi=="3"){
				var operating_mode = definition_mode_plugIn(dataCycle,iter);
			}
			CS = true;
			

			calculConsoElecPacUTBM(dataCycle,iter)			
			// var
			// operating_mode
			// =
			// definition_mode_CS(dataCycle,iter);
			// calculConsoElecPacUTBM_CS(dataCycle,iter);;

		}
	}
	
	else if (typeVehicule =="VPU") {alert("Veuillez choisir un mode de puissance (onglet Route), le type de rendement PAC, la loi de commande et la prise en compte ou non du temps de réponse PAC (onglet Calculs)");return;}
	
	
	affSyntheseCycle();
	alert("La resolution s'est deroulee normalement et a dure : " + (t1 - t0) + " milliseconds.");
	
	if(boolRapport){	// Affichage
						// de
						// la
						// synthese
						// du
						// cycle
						// si
						// un
						// parcours
						// a
						// ete
						// calcule
		affSynthese();
	}
		
}

/**
 * Affichage de la synthese du cycle standard
 */
function affSyntheseCycle(){
	// Suppression
	// de
	// l'ancien
	// rapport
/*
 * if(boolRapportCycle){ var nbligne = document.getElementById('body_syntheseCycle').rows.length for (var i = 1; i< nbligne+1; i++){ document.getElementById("body_syntheseCycle").deleteRow(-1); } document.getElementById("head_syntheseCycle").deleteRow(-1); }
 */

	var nouvelleLigne = document.getElementById("head_syntheseCycle"+typeVehicule).insertRow(-1);
	var colonne1 = nouvelleLigne.insertCell(0);
	if(typeVehicule=="VE"){
		colonne1.innerHTML += 'Synthese des energies pour le cycle ' + fileName;
	}
	else{
		colonne1.innerHTML += 'Synthese pour le cycle ' + fileName;
	}

	colonne1.setAttribute('style',"font-size: 16px; font-weight:bold;");
	if(typeVehicule=="VE" || typeVehicule == "VPU"){
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie totale consommee",Math.round(dataCycle[nbPointCycle][29]),"Wh",false,'','');
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie totale theorique generee",Math.round(dataCycle[nbPointCycle][30]),"Wh",false,'','');
		var temp = Math.round(dataCycle[nbPointCycle][30]/dataCycle[nbPointCycle][29]*100*100)/100;
		(temp>100)? temp=100:
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie regeneree",temp,"%",false,'','');
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie totale requise",Math.round(dataCycle[nbPointCycle][29]-dataCycle[nbPointCycle][30]),"Wh",false,'','');	
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Autonomie visee",100,"km",false,'','');
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie requise sans regeneratif",Math.round(10*((100*dataCycle[nbPointCycle][29]/dataCycle[nbPointCycle][3])))/10,"kWh",false,'','');	
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie requise avec regeneratif",Math.round(10*100*(dataCycle[nbPointCycle][29]-dataCycle[nbPointCycle][30])/dataCycle[nbPointCycle][3])/10,"kWh",false,'','');	
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Consommation normalisee",Math.round((dataCycle[nbPointCycle][29]-dataCycle[nbPointCycle][30])/dataCycle[nbPointCycle][3]*1000),"Wh/km",false,'','');		
	}
	else{
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Consommation d'essence",Math.round(dataCycle[nbPointCycle][48]*100)/100,"litres",false,'','');	
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Consommation d'essence moyenne",Math.round(dataCycle[nbPointCycle][49]*100)/100,"litres/100km",false,'','');
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Emissions de Co2",Math.round(dataCycle[nbPointCycle][51]*100)/100,"kg",false,'','');
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie consommee",Math.round(dataCycle[nbPointCycle][29]*100)/100,"Wh",false,'','');
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie dissipee",Math.round(dataCycle[nbPointCycle][55]*100)/100,"Wh",false,'','');
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Energie produite par le moteur",Math.round(dataCycle[nbPointCycle][53]*100)/100,"Wh",false,'','');
		affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Rendement moteur",rendementmoteur,"",false,'','');
	}

	affichageTabSynthese("body_syntheseCycle"+typeVehicule,"Distance parcourue",Math.round(dataCycle[nbPointCycle][3]/10)*10/1000,"km",false,'','');
	boolRapportCycle = true;
	document.getElementById('head_syntheseCycle'+typeVehicule).style.display = '';
	document.getElementById('syntheseCycle'+typeVehicule).style.display = '';
}

/**
 * Fonction globale du calcul des donnees pour un trajet a partir des donnees obd d'un trajet : pente reelle, profil de vitesse, couples ...
 */
function calcConsoTrajetVT(){
	boolPenteFixe = false;
	if(boolCalcRoute){
		try{
			initCalculThermiqueTrajet();
			boolTrajet = true;
			var t0 = new Date().getTime();
			initialisationParam();
			creaTableau(dataConsoReelleVT);		
			resolutionTrajetVT(); 
			nbPointConso = nbPointTrajet;
			AfficherConseilsVT();
			// majDonneesSortie();
			// affichage();
			var t1 = new Date().getTime();
			alert("La resolution s'est deroulee normalement et a dure : " + (t1 - t0) + " milliseconds.");
		}catch (e){alert("Une erreur est survenue lors de la resolution \n" + e.name + ": " + e.message)}
		
	}
	else{
		alert("Veuillez lancer une requête de trajet (onglet Route)");
	}
}

/**
 * Calcul des données réelles qui ne sont pas récupérées avec l'Obd
 */
function calcDonneesReelles(tab){
	
	// Variable
	var DT = 0; // Delta
				// temps
	var Longueur = 0; // Distance
						// parcourue
						// entre
						// deux
						// relevé
	var Longueurtot = 0; // Distance
							// parcourue
							// au
							// total
	var Pente = 0; // Pente
	var Egen = 0; // Energie
					// générée
	var Econ = 0; // Energie
					// consommée
	var RapportRed = 0;
	
	alert("Attention : \n Altitude imposée à 50 pour chaque acquisition");
	tab[5] = new Array();
	tab[5][0] = ('Altitude (=50)');
	tab[5][1] = 50;
	tab[6] = new Array();
	tab[6][0] = ('Vitesse (m/s)');
	tab[7] = new Array();
	tab[7][0] = ('Accélération (m/s²)');
	tab[8] = new Array();
	tab[8][0] = ('Puissance Mécanique (W)');
	tab[9] = new Array();
	tab[9][0] = ('Consommation instantannée (L)');
	tab[10] = new Array();
	tab[10][0] = ('Consommation totale (L)');
	tab[10][1] = 0;
	tab[11] = new Array();
	tab[11][0] = ('Vitesse enclenchée');
	tab[12] = new Array();
	tab[12][0] = ('Couple moteur (N.m)');
	tab[13] = new Array();
	tab[13][0] = ('Rejets de CO2 (kg)');
	
	// Calcul
	// des
	// coeficient
	// à
	// partir
	// des
	// couples
	// max
	coef = polynomialRegression(dataCouple[0],dataCouple[1],3);
		
	for (var mod = 2; mod < tab[0].length; mod++){
		// Calcul
		// des
		// vitesse
		// en
		// m/s
		tab[6][mod] = tab[2][mod]/3.6;
		
		// Calcul
		// des
		// accélération
		DT = tab[0][mod] - tab[0][mod-1];
		tab[7][mod] = (tab[6][mod]-tab[6][mod-1])/DT;
		
		// Calcul
		// de
		// la
		// pente
		tab[5][mod] = 50;
		Longueur = tab[6][mod] * DT;
		Longueurtot += Longueur;
		if(Longueur != 0){
			Pente = Math.atan((tab[5][mod]-tab[5][mod-1])/Longueur);
		}
		else{
			Pente = 0;
		}
		
		// Calcul
		// des
		// forces
		var Forcem = calcForce(tab[6][mod],tab[7][mod],Pente);
		Forcem= Forcem.Fm;
		
		// Calcul
		// des
		// puissances
		var Puimec = calcPuissanceMeca(tab[6][mod],Forcem);
		tab[8][mod] = Puimec.Pmoteur;
		
		
		// Calcul
		// des
		// consommations
		ConsoReelle = tab[4][mod]/(AirFuelRatio*rhoessence)*DT;
		tab[10][mod] = tab[10][mod-1] + ConsoReelle;
		tab[9][mod] = ConsoReelle;
		
		// Calcul
		// des
		// énergies
		if(mod!=2){
			Econ += (tab[8][mod]+tab[8][mod-1])*DT/(2*3600);
		}
		Egen += ConsoReelle * rhoessence * essencePCS;
		
		// Calcul
		// de
		// la
		// valeur
		// du
		// rapport
		// de
		// reduction
		// et
		// de
		// la
		// vitesse
		// enclenchée
		if(tab[6][mod] !=0){
			RapportRed = tab[1][mod]*2*Math.PI*RRoue/(tab[6][mod]*60);
			tab[11][mod] = findIndexReduction(dataVitesses,RapportRed);
		}
		else{
			tab[11][mod] = -1;
		}
		
		// Calcul
		// du
		// couple
		// moteur
		tab[12][mod] = tab[3][mod]*(coef[3]*Math.pow(tab[1][mod],3)+coef[2]*Math.pow(tab[1][mod],2)+coef[1]*tab[1][mod]+coef[0])/100;
		
		// Calcul
		// des
		// rejets
		// de
		// CO2
		if (tab[10][mod] !=0){
			tab[13][mod] = tab[10][mod] * rhoessence * ratioCO2 /1000;
		}
		else{
			tab[13][mod] = 0;
		}
	}
	rendMoteurReel = Econ/Egen;
	return rendMoteurReel;
}
/**
 * Calcul du mode de conduite à un instant t
 * 
 * @returns acc : acceleration
 * @returns modeConduite : nom du mode ('Accélération', 'Freinage', 'Vitesse constante', 'Arrêt')
 */
function calcMode(acc){
	var modeConduite;
	if(acc >= -0.4 && acc <= 0.4){
		modeConduite = "Vitesse constante";
	}
	else if(acc > 0.4){
		modeConduite = "Accélération";
	}
	else if(acc < -0.4){
		modeConduite = "Freinage";
	}
	else if (acc == 0 && V == 0){
		modeConduite = "Arrêt";
	}
	return modeConduite;
	
}
/**
 * Calcul de l'interpolation linéaire pour le facteur d'équivalence avec 5 points
 */
function InterpolationLambda(SoC){
	var facteurequivalence = 2.5;
	if(SoC == 0.175){
		facteurequivalence = 3;
	}
	else if(SoC == 0.500){
		facteurequivalence = 2.5;
	}
	else if(SoC == 0.875){
		facteurequivalence = 2;
	}
	else if(SoC > -0.1 && SoC < 0.175){
		facteurequivalence = (3-5)/(0.175+0.1)*(SoC+0.1)+5;
	}
	else if(SoC > 0.175 && SoC < 0.500){
		facteurequivalence = (2.5-3)/(0.500-0.175)*(SoC-0.175)+3;
	}
	else if(SoC > 0.500 && SoC < 0.875){
		facteurequivalence = (2-2.5)/(0.875-0.500)*(SoC-0.500)+2.5;
	}
	else if(SoC > 0.875 && SoC < 1.1){
		facteurequivalence = (0-2)/(1.1-0.875)*(SoC-0.875)+2;
	}
	return facteurequivalence;
}
/**
 * Calcul de l'interpolation linéaire pour la tension de la batterie du bus
 */
function InterpolationVoltage(SoC){
	var Voltage = 650;
	if(SoC > -0.1 && SoC < 1.1){
		Voltage = (VBatBus2-VBatBus1)/(1.1+0.1)*(SoC+0.1)+VBatBus1;
	}
	return Voltage;
}
/**
 * Calcul des forces appliquees au bus hybride
 * 
 * @param V :
 *            vitesse du bus
 * @param acc :
 *            acceleration du bus
 * @param pente :
 *            pente en %
 * @returns Fr : resistance au roulement
 * @returns Fair : resistance de l'air
 * @returns Fp : resistance de la pente
 * @returns Facc : force d'acceleration
 * @returns Fm : force motrice
 */	
 function calcForceBus(V,acc,pente){
	
	var angle	= Math.atan(pente);
	var Fr 		= CoefResRoulBus * Massetotbus * Math.cos(angle);	
	var Fair 	= CoefResAirBus * Math.pow(V,2);						
	var Fp		= pes * Massetotbus * Math.sin(angle);	
	var Facc	= acc * Massetotbus;									
	var Finert  = 0.05 * Massetotbus * acc;
	var Fm		= Fr + Fair + Fp + Facc + Finert;															
	
	return{
	Fr 		: Fr,
	Fair 	: Fair,
	Fp		: Fp,
	Facc	: Facc,
	Fm		: Fm,
	}
}
 /**
	 * Fonction si la batterie peut être utilisée
	 */
 function BatterieOn(t){
	dataBusHybride[29][t+5] = 1; // La
									// batterie
									// est
									// utilisée
	dataBusHybride[27][t+5] = InterpolationLambda(dataBusHybride[25][t+4]); // On
																			// calcule
																			// le
																			// facteur
																			// d'équivalence
																			// en
																			// fonction
																			// de
																			// l'état
																			// de
																			// charge
	
	if(PBusBatmax >= dataBusHybride[32][t+5]){ // Si
												// la
												// batterie
												// peut
												// fournir
												// toute
												// l'énergie
												// nécessaire
		// On
		// calcul
		// les
		// nouvelles
		// valeurs
		// de
		// Pbat
		// et
		// de
		// l'évolution
		// de
		// l'état
		// de
		// charge
		// SoC
		dataBusHybride[15][t+5] = dataBusHybride[32][t+5]; // Pbat
		courant = (tension - Math.sqrt(Math.pow(tension,2) - 4 * dataBusHybride[15][t+5] * RBusdis)) / (2 * RBusdis); // Courant
																														// dans
																														// la
																														// batterie
		dataBusHybride[25][t+5] = dataBusHybride[25][t+4] - dataBusHybride[5][t+5] * courant / (CapaBatBus1 * tension); // SoC
		dataBusHybride[26][t+5] = dataBusHybride[25][t+5] - dataBusHybride[25][t+4]; // Delta
																						// SoC
		
		if(dataBusHybride[28][t+4] == -2){ 	// Si
											// le
											// moteur
											// est
											// éteint
			dataBusHybride[28][t+5] = -2;	// Il
											// reste
											// éteint
			
			dataBusHybride[18][t+5] = 0;	// Pci
			dataBusHybride[16][t+5] = 0;	// wci
			dataBusHybride[17][t+5] = 0;	// Cci
		}
		else if(dataBusHybride[28][t+4] == -1){ 	// Si
													// le
													// moteur
													// est
													// entrain
													// de
													// s'arrêter
			if(cptarret == 4){						// Si
													// les
													// 2s
													// se
													// sont
													// écoulé
				dataBusHybride[28][t+5] = -2;		// Le
													// moteur
													// s'arrête
				cptarret = 0;						// On
													// remet
													// le
													// compteur
													// à 0
				
				dataBusHybride[18][t+5] = 0;	// Pci
				dataBusHybride[16][t+5] = 0;	// wci
				dataBusHybride[17][t+5] = 0;	// Cci
			}
			else{
				dataBusHybride[28][t+5] = -1;		// Sinon
													// il
													// est
													// continue
													// de
													// s'éteindre
				
				dataBusHybride[18][t+5] = 0;	// Pci
				dataBusHybride[16][t+5] = 0;	// wci
				dataBusHybride[17][t+5] = 0;	// Cci
			}
			cptarret += 1;							// Compteur
													// pour
													// les
													// 2s
													// d'arrêt
		}
		else if(dataBusHybride[28][t+4] == 2){	// Si
												// le
												// moteur
												// est
												// allumé
			if(t+4 >= ti && !boolarret){		// La
												// condition
												// reviens
												// à
												// true
												// si
												// on a
												// dépasser
												// le
												// temps
												// ti
												// de
												// la
												// tentative
												// qui
												// a
												// échouée
				boolarret = true;
			}
			var tentative = 5;
			SSooCC = dataBusHybride[25][t+4];
			while(tentative <= 40 && boolarret){	// <= à
													// 40
													// car
													// on
													// teste
													// sur
													// 17
													// secondes
													// à
													// partir
													// de
													// t+5
				ti = t + tentative;					// ti :
													// temps
													// actuel
													// de
													// la
													// tentative
				courant = (tension - Math.sqrt(Math.pow(tension,2) - 4 * dataBusHybride[32][ti] * RBusdis)) / (2 * RBusdis); // Courant
																																// dans
																																// la
																																// batterie
				SSooCC = SSooCC - dataBusHybride[5][t+5] * courant / (CapaBatBus1 * tension); // SoC
				if(PBusBatmax < dataBusHybride[32][ti]){	// Si
															// la
															// batterie
															// ne
															// peut
															// pas
															// fournir
															// la
															// puissance
															// nécessaire
					boolarret = false;				// Le
													// teste
													// à
													// échoué
				}
				if(SSooCC <= 0.2 && tentative != 40){	// Il
														// n'y
														// a
														// plus
														// de
														// batterie
														// avant
														// d'avoir
														// finit
														// le
														// test
					boolarret = false;				// Le
													// teste
													// à
													// échoué
				}
				tentative += 1;
			}
			if(boolarret){							// Si
													// le
													// teste
													// a
													// réussi
				dataBusHybride[28][t+5] = -1;		// On
													// va
													// éteindre
													// le
													// moteur
				cptarret = 1;
				
				dataBusHybride[18][t+5] = 0;	// Pci
	 			dataBusHybride[16][t+5] = 0;	// wci
	 			dataBusHybride[17][t+5] = 0;	// Cci
			}
			else{
				dataBusHybride[28][t+5] = 2;		// Sinon,
													// il
													// reste
													// allumé
				
	 			dataBusHybride[16][t+5] = dataBusHybride[13][t+5] * (2 * Math.PI) / 60;	// wci
	 			dataBusHybride[17][t+5] = 50; // Jmbus
												// *
												// (dataBusHybride[16][t+5]
												// -
												// dataBusHybride[16][t+4])
												// * (2
												// *
												// Math.PI)
												// /
												// (60
												// *
												// dataBusHybride[5][t+5]);
												// //
												// Cci
	 			dataBusHybride[18][t+5] = dataBusHybride[16][t+5] * dataBusHybride[17][t+5];	// Pci
			}
		}
	}
	else{	// La
			// batterie
			// ne
			// peut
			// pas
			// fournir
			// toute
			// la
			// puissance
			// nécessaire
		if(dataBusHybride[28][t+4] == 2){ // Si
											// le
											// moteur
											// est
											// allumé,
											// il
											// reste
											// allumé
			dataBusHybride[28][t+5] = 2;
		}
		else if(dataBusHybride[28][t+4] == -2 || dataBusHybride[28][t+4] == -1){	// Si
																					// le
																					// moteur
																					// est
																					// éteint,
																					// en
																					// train
																					// de
																					// s'allumer
																					// ou
																					// de
																					// s'éteindre,
																					// il
																					// faut
																					// l'allumer
			for(var gj = 1; gj <= 4; gj++){
				dataBusHybride[28][t+gj] = 1;	// On
												// l'allume
												// à
												// t+1
												// pour
												// qu'il
												// ait
												// 2
												// secondes
												// pour
												// s'allumer
			}
			dataBusHybride[28][t+5] = 2;		// Le
												// moteur
												// est
												// allumé
												// à
												// t+5
			dataBusHybride[30][t+5] = 4 / rhoessence;	// Pénalité
														// d'allumage
														// 4g
		}
		
		
		// En
		// utilisant
		// le
		// arg
		// min
		  
		// On
		// cherche
		// la
		// valeur
		// optimale
		// de
		// couple
		// moteur
		// pour
		// une
		// consommation
		// minimale
				
		for(var T = 0; T < dataBusHybride[12][t+5]; T++){		// On
																// teste
																// pour
																// des
																// valeur
																// de
																// Cci
																// de 0
																// à
																// Cout
			PuiMot = T * dataBusHybride[13][t+5] * (2 * Math.PI) / 60;	// On
																		// calcule
																		// la
																		// puissance
																		// moteur
																		// pour
																		// chaque
																		// T
			PuiBat = dataBusHybride[14][t+5] + Paux + Ppertes - PuiMot;	// On
																		// calcule
																		// la
																		// puissance
																		// batterie
			courant = (tension - Math.sqrt(Math.pow(tension,2) - 4 * PuiBat * RBusdis)) / (2 * RBusdis); // Courant
																											// dans
																											// la
																											// batterie
			SoC = dataBusHybride[25][t+4] - dataBusHybride[5][t+5] * courant / (CapaBatBus1 * tension); // SoC
			DeltaSoC = SoC - dataBusHybride[25][t+4]; // On
														// calcule
														// le
														// delta
														// SoC
			wci = dataBusHybride[13][t+5] * (2 * Math.PI) / 60;
			// Charge
			// moteur
			// (%)
			chargeMot = T / CBusDRMmax * 100;
				
			// On
			// ajuste
			// le
			// régime
			// moteur
			// au
			// min/max
			// pour
			// continuer
			// les
			// calculs
			if(wci <= minvecNmot){
				wci = minvecNmot+pasNmot;
			}
			if(wci > maxvecNmot){
				wci = maxvecNmot;
			}
			// Indice
			// regime
			// moteur
			// phase
			indiceRegMot = (pasNmot * (Math.round(wci / pasNmot)) - minvecNmot) / pasNmot; 	
			// Indice
			// pourcentage
			// de
			// couple
			// phase
			indiceCouMot = (pasCmot * (Math.round(chargeMot / pasCmot)) - minvecCmot) / pasCmot;	
			// Calcul
			// consommation
			// specifique
			// d'energie
			CSE_Bus = Math.round(Map_cse_ex[indiceRegMot][indiceCouMot]);
			TESTCSE[t+5] = CSE_Bus;
			// Pour
			// l'instant
			// CSE_Bus
			// est
			// fixe
			// à
			// 750
			// g/kW.h
	 		CSE_Bus = 750;
			// Calcul
			// de
			// la
			// consommation
			// en
			// g/s
			// à
			// partir
			// de
			// g/kW.h
			// * W
			Consogs = CSE_Bus * PuiMot * 3600 / 1000;
			testeur = dataBusHybride[5][t+5] * Consogs - dataBusHybride[27][t+5] * DeltaSoC; // testeur
																								// est
																								// la
																								// fonction
																								// que
																								// l'on
																								// cherche
																								// à
																								// minimiser
			if(T == 0){ // On
						// initiale
						// Valeurtesteur
				Valeurtesteur = 1000000000;
			}
			
			// Si
			// le
			// testeur
			// est
			// inférieur
			// ou
			// égale,
			// on
			// change
			// les
			// valuers
			// pour
			// celles
			// plus
			// optimales
			if(testeur < Valeurtesteur && PuiBat <= PBusBatmax || (testeur == Valeurtesteur && Consogs<ValeurConsogs)){
				Valeurtesteur = testeur;
				ValeurT = T;
				ValeurPuiMot = PuiMot;
				ValeurPuiBat = PuiBat;
				ValeurSoC = SoC;
				ValeurDeltaSoC = DeltaSoC;
				ValeurConsogs = Consogs;
			}
		}
		dataBusHybride[16][t+5] = dataBusHybride[13][t+5] * (2 * Math.PI) / 60;	// wci
		dataBusHybride[17][t+5] = ValeurT;
		dataBusHybride[18][t+5] = ValeurPuiMot;
		dataBusHybride[15][t+5] = ValeurPuiBat;
		dataBusHybride[25][t+5] = ValeurSoC;
		dataBusHybride[26][t+5] = ValeurDeltaSoC;
		
		
		// En
		// complétant
		// PBat
		/*
		 * dataBusHybride[15][t+5] = PBusBatmax; // Pbat dataBusHybride[18][t+5] = dataBusHybride[32][t+5] - dataBusHybride[15][t+5]; // Pmot dataBusHybride[16][t+5] = dataBusHybride[13][t+5] * (2 * Math.PI) / 60; // wci dataBusHybride[17][t+5] = dataBusHybride[18][t+5] / dataBusHybride[18][t+5]; // Cci courant = (tension - Math.sqrt(Math.pow(tension,2) - 4 * dataBusHybride[15][t+5] * RBusdis)) /
		 * (2 * RBusdis); // Courant dans la batterie dataBusHybride[25][t+5] = dataBusHybride[25][t+4] - dataBusHybride[5][t+5] * courant / (CapaBatBus1 * tension); // SoC dataBusHybride[26][t+5] = dataBusHybride[25][t+5] - dataBusHybride[25][t+4]; // Delta SoC
		 */
		
	}
 }
 /**
	 * Fonction si la batterie ne peut pas être utilisée
	 */
 function BatterieOff(t){
	// On
	// attribue
	// l'état
	// de
	// la
	// batterie
	// et
	// celui
	// du
	// moteur
	dataBusHybride[29][t+5] = 0;	// La
									// batterie
									// n'est
									// pas
									// utilisée
	if(dataBusHybride[28][t+4] == 2){ // Si
										// le
										// moteur
										// est
										// allumé,
										// il
										// reste
										// allumé
			dataBusHybride[28][t+5] = 2;
	}
	else if(dataBusHybride[28][t+4] == -2 || dataBusHybride[28][t+4] == -1){	// Si
																				// le
																				// moteur
																				// est
																				// éteint,
																				// il
																				// faut
																				// l'allumer
		for(var gj = 1; gj <= 4; gj++){
			dataBusHybride[28][t+gj] = 1;	// On
											// l'allume
											// à
											// t+1
											// pour
											// qu'il
											// ait
											// 2
											// secondes
											// pour
											// s'allumer
		}
		dataBusHybride[28][t+5] = 2;		// Le
											// moteur
											// est
											// allumé
											// à
											// t+5
		dataBusHybride[30][t+5] = 4 / rhoessence; // Pénalité
													// d'allumage
													// 4g
		if(dataBusHybride[28][t+4] == -1){ 	
			cptarret = 0;					// On
											// arrête
											// le
											// compteur
		}
	}
	
	dataBusHybride[27][t+5] = InterpolationLambda(dataBusHybride[25][t+4]); // On
																			// calcule
																			// le
																			// facteur
																			// d'équivalence
																			// en
																			// fonction
																			// de
																			// l'état
																			// de
																			// charge
		
	if(dataBusHybride[6][t+5] <= -seuilDec && dataBusHybride[32][t+5] <= 0){	// Décélération
																				// avec
																				// régénération
																				// de
																				// la
																				// batterie
 		dataBusHybride[15][t+5] = dataBusHybride[32][t+5];	// Pbat
															// =
															// Pnec
															// négatif
															// =
															// rechargement
 		courant = (tension - Math.sqrt(Math.pow(tension,2) - 4 * dataBusHybride[15][t+5] * RBuscha)) / (2 * RBuscha); // Courant
																														// dans
																														// la
																														// batterie
		dataBusHybride[25][t+5] = dataBusHybride[25][t+4] - dataBusHybride[5][t+5] * courant / (CapaBatBus1 * tension); // SoC
 		dataBusHybride[26][t+5] = dataBusHybride[25][t+5] - dataBusHybride[25][t+4]; // Delta
																						// SoC
 		 	 		
 		// Calcul
		// de
		// Pci,
		// Cci
		// et
		// wci
 		dataBusHybride[16][t+5] = dataBusHybride[13][t+5] * (2 * Math.PI) / 60; // wci
 		dataBusHybride[17][t+5] = 50; // Jmbus
										// *
										// (dataBusHybride[16][t+5]
										// -
										// dataBusHybride[16][t+4])
										// * (2
										// *
										// Math.PI)
										// /
										// (60
										// *
										// dataBusHybride[5][t+5]);
										// //
										// Cci
 		dataBusHybride[18][t+5] = dataBusHybride[16][t+5] * dataBusHybride[17][t+5]; // Pci
 	}
	else if(dataBusHybride[6][t+5] > -seuilDec && dataBusHybride[6][t+5] <= 0 && dataBusHybride[32][t+5] <= 0){	// Décélération
																												// sans
																												// régénération
																												// de
																												// la
																												// batterie
		dataBusHybride[15][t+5] = 0;	// Pbat
										// = 0
 		dataBusHybride[25][t+5] = dataBusHybride[25][t+4];  // SoC
 		dataBusHybride[26][t+5] = dataBusHybride[25][t+5] - dataBusHybride[25][t+4]; // Delta
																						// SoC
 		
 		// Calcul
		// de
		// Pci,
		// Cci
		// et
		// wci
 		dataBusHybride[16][t+5] = dataBusHybride[13][t+5] * (2 * Math.PI) / 60; // wci
 		dataBusHybride[17][t+5] = 50; // Jmbus
										// *
										// (dataBusHybride[16][t+5]
										// -
										// dataBusHybride[16][t+4])
										// * (2
										// *
										// Math.PI)
										// /
										// (60
										// *
										// dataBusHybride[5][t+5]);
										// //
										// Cci
 		dataBusHybride[18][t+5] = dataBusHybride[16][t+5] * dataBusHybride[17][t+5]; // Pci
	}
	else if(dataBusHybride[32][t+5] <= 0){	// Si
											// Pnec
											// est
											// négative
 		dataBusHybride[15][t+5] = 0;	// Pbat
										// = 0
 		dataBusHybride[25][t+5] = dataBusHybride[25][t+4]; // SoC
 		dataBusHybride[26][t+5] = dataBusHybride[25][t+5] - dataBusHybride[25][t+4]; // Delta
																						// SoC
 		 	 		
 		// Calcul
		// de
		// Pci,
		// Cci
		// et
		// wci
 		dataBusHybride[16][t+5] = dataBusHybride[13][t+5] * (2 * Math.PI) / 60; // wci
 		dataBusHybride[17][t+5] = 50; // Jmbus
										// *
										// (dataBusHybride[16][t+5]
										// -
										// dataBusHybride[16][t+4])
										// * (2
										// *
										// Math.PI)
										// /
										// (60
										// *
										// dataBusHybride[5][t+5]);
										// //
										// Cci
 		dataBusHybride[18][t+5] = dataBusHybride[16][t+5] * dataBusHybride[17][t+5]; // Pci
	}
	else{
 		dataBusHybride[15][t+5] = 0;	// Pbat
										// = 0
 		dataBusHybride[25][t+5] = dataBusHybride[25][t+4]; // SoC
 		dataBusHybride[26][t+5] = dataBusHybride[25][t+5] - dataBusHybride[25][t+4]; // Delta
																						// SoC
 		
 			
 		// Calcul
		// de
		// Pci,
		// Cci
		// et
		// wci
 		dataBusHybride[18][t+5] = dataBusHybride[14][t+5] + Paux + Ppertes; // Pci
 		dataBusHybride[16][t+5] = dataBusHybride[13][t+5] * (2 * Math.PI) / 60; // wci
 		if(dataBusHybride[16][t+5] == 0){
 			dataBusHybride[17][t+5] = 0;	// Cci
 		}
 		else{
 			dataBusHybride[17][t+5] = dataBusHybride[18][t+5] / dataBusHybride[16][t+5]; // Cci
 		}
	}
 }
 /**
	 * Fonction de calcul de la division du couple et de l'état du moteur
	 */
 function divisionCouple(t){ 
	tension = InterpolationVoltage(dataBusHybride[25][t+4]); // On
																// calcule
																// la
																// tension
																// de
																// la
																// batterie
																// en
																// fonction
																// de
																// l'état
																// de
																// charge
	dataBusHybride[30][t+5] = 0; // On
									// initialise
									// la
									// consommation
									// de
									// cet
									// itération
									// à 0
	
 	if(dataBusHybride[25][t+4] > 0.2){ // on
										// vérifie
										// que
										// la
										// batterie
										// peut
										// être
										// utilisée
 		BatterieOn(t);
 	}
 	else{// Si
			// la
			// batterie
			// ne
			// peut
			// pas
			// être
			// utilisée
 		BatterieOff(t); 		
 	}
 	
 	if(dataBusHybride[18][t+5] < 0){	// Si
										// la
										// puissance
										// moteur
										// est
										// négative,
										// on
										// la
										// met
										// à 0
 		dataBusHybride[18][t+5] = 0;
 	}
 	
 	// Calcul
	// des
	// énergies
 	if((dataBusHybride[15][t+5] > 0 || dataBusHybride[15][t+5] == 0)){
 		dataBusHybride[19][t+5] = dataBusHybride[15][t+5] * dataBusHybride[5][t+5] / 3600; 	// Ebat
																							// instantannée
 		dataBusHybride[33][t+5] = 0;
 	}
 	else{
 		dataBusHybride[19][t+5] = 0;	
 		dataBusHybride[33][t+5] = -1 * dataBusHybride[15][t+5] * dataBusHybride[5][t+5] / 3600;// Ebat
																								// instantannée
 	}
 	dataBusHybride[20][t+5] = dataBusHybride[20][t+4] + dataBusHybride[19][t+5];			// Ebat
																							// totale
 	dataBusHybride[34][t+5] = dataBusHybride[34][t+4] + dataBusHybride[33][t+5];
 	
 	if((dataBusHybride[18][t+5] + dataBusHybride[18][t+4]) > 0){
 		dataBusHybride[21][t+5] = 0.5 * (dataBusHybride[18][t+5] + dataBusHybride[18][t+4]) * dataBusHybride[5][t+5] / 3600;	// Emot
																																// instantannée
 	}
 	else{
 		dataBusHybride[21][t+5] = 0;														// Emot
																							// instantannée
 	}
 	dataBusHybride[22][t+5] = dataBusHybride[22][t+4] + dataBusHybride[21][t+5];			// Emot
																							// totale
 
 	dataBusHybride[23][t+5] = dataBusHybride[19][t+5] + dataBusHybride[21][t+5];			// Eme
																							// instantannée
 	dataBusHybride[24][t+5] = dataBusHybride[24][t+4] + dataBusHybride[23][t+5];			// Eme
																							// totale
 						
 	
 	// Calcul
	// de
	// la
	// consommation
 	
 	if(dataBusHybride[28][t+5] == 2){  // Le
										// moteur
										// fonctionne
 		// On
		// ajuste
		// le
		// régime
		// moteur
		// au
		// min/max
		// pour
		// continuer
		// les
		// calculs
 		if(dataBusHybride[16][t+5] <= minvecNmot){
 			dataBusHybride[16][t+5] = minvecNmot + pasNmot;
 		}
 		if(dataBusHybride[16][t+5] > maxvecNmot){
 			dataBusHybride[16][t+5] = maxvecNmot - pasNmot;
 		}
 	 	// Indice
		// regime
		// moteur
		// phase
 		indiceRegMot = (pasNmot * (Math.round(dataBusHybride[16][t+5] / pasNmot)) - minvecNmot) / pasNmot; 	
 		// Indice
		// pourcentage
		// de
		// couple
		// phase
 		if(dataBusHybride[17][t+5] < 0){
 			chargeMot = 0;
 		}
 		else{
 			chargeMot = dataBusHybride[17][t+5] / CBusDRMmax * 100;
 		}
 		indiceCouMot = (pasCmot * (Math.round(chargeMot / pasCmot)) - minvecCmot) / pasCmot;	
 		// Calcul
		// consommation
		// specifique
		// d'energie
 		CSE_Bus = Math.round(Map_cse_ex[indiceRegMot][indiceCouMot]);
 		// Pour
		// l'instant
		// CSE_Bus
		// est
		// fixe
		// à
		// 750
		// g/kW.h
 		CSE_Bus = 750;
 		// Consommation
		// instantannée
		// en L
 		dataBusHybride[30][t+5] = dataBusHybride[30][t+5] + dataBusHybride[21][t+5] * CSE_Bus / (1000 * rhoessence); // On
																														// ajoute
																														// à sa
																														// valeur
																														// précédente
 		// Consommation
		// totale
		// en L
		// s'il
		// y a
		// une
		// pénalité
		// d'allumage
 		dataBusHybride[31][t+5] = dataBusHybride[31][t+4] + dataBusHybride[30][t+5];
 		// Consommation
		// instantannée
		// en
		// L/100
		// km
 		dataBusHybride[30][t+5] = dataBusHybride[30][t+5] / (dataBusHybride[3][t+5] - dataBusHybride[3][t+4]) * 100000;
 	}
 	else{		// Le
				// moteur
				// ne
				// fonctione
				// pas
 		// Consommation
		// instantannée
		// en L
 		dataBusHybride[30][t+5] = dataBusHybride[30][t+5] + 0;  // On
																// ajoute
																// à
																// sa
																// valeur
																// précédente
																// s'il
																// y a
																// une
																// pénalité
																// d'allumage
 		// Consommation
		// totale
		// en L
 		dataBusHybride[31][t+5] = dataBusHybride[31][t+4];
 		// Consommation
		// instantannée
		// en
		// L/100
		// km
 		dataBusHybride[30][t+5] = dataBusHybride[30][t+5] / (dataBusHybride[3][t+5] - dataBusHybride[3][t+4]) * 100000;
 	}
 	
 	// Changement
	// des
	// unités
 	dataBusHybride[0][t+5] = dataBusHybride[0][t+5] / 3600; // Temps
															// de
															// heure
															// à
															// seconde
 	dataBusHybride[3][t+5] = dataBusHybride[3][t+5] / 1000; // Distance
															// de
															// km à
															// m
 }
 /**
	 * Fonction pour calculer la puissance nécessaire à fournir
	 */
 function calcPnec(t){
	var ForceBus = calcForceBus(dataBusHybride[2][t],dataBusHybride[6][t],dataBusHybride[4][t]); 
	dataBusHybride[8][t] = ForceBus.Fm ; // Calcul
											// de
											// la
											// force
											// dans
											// les
											// roues
											// Fr
	dataBusHybride[9][t] = dataBusHybride[8][t] * dataBusHybride[2][t]; // Calcul
																		// de
																		// Pr
	dataBusHybride[10][t] = dataBusHybride[2][t] / RwhBus; // Calcul
															// de
															// wr
	if(dataBusHybride[10][t] == 0){
		dataBusHybride[11][t] = 0;			// Calcul
											// de
											// Cr
	}
	else{
		dataBusHybride[11][t] = dataBusHybride[9][t] / dataBusHybride[10][t]; // Calcul
																				// de
																				// Cr
	}
	dataBusHybride[12][t] = dataBusHybride[11][t] / (RatioBus * RendBus);	// Calcul
																			// de
																			// Cout
	dataBusHybride[13][t] = dataBusHybride[10][t] * RatioBus * 60 / (2 * Math.PI);	// Calcul
																					// de
																					// wout
	dataBusHybride[14][t] = dataBusHybride[12][t] * dataBusHybride[13][t] * (2 * Math.PI) / 60; // Calcul
																								// de
																								// Pout
	var Pdrm = Tdrm * dataBusHybride[13][t] * (2 * Math.PI) / 60; // Calcul
																	// de
																	// la
																	// puissance
																	// perdue
																	// dans
																	// le
																	// roptor
																	// interne
	var P = Pdrm + Paux + Ppertes + dataBusHybride[14][t]; // Calcul
															// de
															// la
															// puissance
															// nécessaire
															// pour
															// avancer
	return P;
 }
/**
 * Fonction de calcul pour le bus hybride
 */
function calculBusHybride(){
	
	// Création
	// de
	// la
	// matrice
	// dataBusHybride
	dataConsoreverse = reversetab(dataConso,dataConso.length,dataConso[0].length);
	dataBusHybride[0] = dataConsoreverse[0];		// Temps
													// (s)
	dataBusHybride[0][0] = "Temps (h)";				// La
													// transition
													// entre
													// s et
													// heure
													// se
													// fait
													// à
													// chaque
													// itération
	dataBusHybride[1] = dataConsoreverse[1];		// Vitesse
													// (km/h)
	dataBusHybride[2] = dataConsoreverse[2];		// Vitesse
													// (m/s)
	dataBusHybride[3] = dataConsoreverse[3];		// Distance
													// cumulée
													// (m)
	dataBusHybride[3][0] = "Distance cumulée (km)";	// La
													// transition
													// entre
													// m et
													// km
													// se
													// fait
													// à
													// chaque
													// itération
	dataBusHybride[4] = dataConsoreverse[4];		// Pente
	dataBusHybride[5] = dataConsoreverse[5];		// DT
													// (0.5s)
	dataBusHybride[6] = dataConsoreverse[6];		// Accélération
													// (m/s²)
	dataBusHybride[7] = new Array();		
	dataBusHybride[7][0] = "Mode de conduite";
	dataBusHybride[8] = new Array();		
	dataBusHybride[8][0] = "Force motrice (N)";
	dataBusHybride[9] = new Array();
	dataBusHybride[9][0] = "Puissance requise dans les roues (W)";
	dataBusHybride[10] = new Array();	
	dataBusHybride[10][0] = "Vitesse des roues (rad/s)";
	dataBusHybride[11] = new Array();	
	dataBusHybride[11][0] = "Couple dans les roues (N.m)";
	dataBusHybride[12] = new Array();
	dataBusHybride[12][0] = "Couple en sortie du moteur électrique (N.m)";
	dataBusHybride[13] = new Array();	
	dataBusHybride[13][0] = "Vitesse en sortie du moteur électrique (tr/min)";
	dataBusHybride[14] = new Array();	
	dataBusHybride[14][0] = "Puissance en sortie du moteur électrique (W)";
	dataBusHybride[15] = new Array();	
	dataBusHybride[15][0] = "Puissance batterie (W)";
	dataBusHybride[16] = new Array();	
	dataBusHybride[16][0] = "Régime moteur thermique (tr/min)";
	dataBusHybride[17] = new Array();
	dataBusHybride[17][0] = "Couple moteur thermique (N.m)";
	dataBusHybride[18] = new Array();	
	dataBusHybride[18][0] = "Puissance moteur thermique (W)";
	dataBusHybride[19] = new Array();	
	dataBusHybride[19][0] = "Énergie fournie par la batterie instantannée (W.h)";
	dataBusHybride[19][1] = 0;
	dataBusHybride[20] = new Array();	
	dataBusHybride[20][0] = "Énergie fournie par la batterie totale (W.h)";
	dataBusHybride[20][1] = 0;
	dataBusHybride[21] = new Array();	
	dataBusHybride[21][0] = "Énergie fournie par le moteur thermique instantannée (W.h)";
	dataBusHybride[22] = new Array();	
	dataBusHybride[22][0] = "Énergie fournie par le moteur thermique totale (W.h)";
	dataBusHybride[22][1] = 0;
	dataBusHybride[23] = new Array();	
	dataBusHybride[23][0] = "Énergie fournie au moteur electrique instantannée (W.h)";
	dataBusHybride[24] = new Array();	
	dataBusHybride[24][0] = "Énergie fournie au moteur electrique totale (W.h)";
	dataBusHybride[24][1] = 0;
	dataBusHybride[25] = new Array();
	dataBusHybride[25][0] = "État de charge de la batterie (SoC)";
	dataBusHybride[25][1] = 1;
	dataBusHybride[26] = new Array();
	dataBusHybride[26][0] = "Delta état de charge de la batterie (Delta SoC)";
	dataBusHybride[27] = new Array();
	dataBusHybride[27][0] = "Valeur du facteur d'équivalence";
	dataBusHybride[28] = new Array();	
	dataBusHybride[28][0] = "État du moteur thermique";
	dataBusHybride[28][1] = -2;
	dataBusHybride[29] = new Array();	
	dataBusHybride[29][0] = "État de la batterie";
	dataBusHybride[29][1] = 1;
	dataBusHybride[30] = new Array();	
	dataBusHybride[30][0] = "Consommation instantannée (L/100km)";
	dataBusHybride[30][1] = 0;
 	dataBusHybride[31] = new Array();	
	dataBusHybride[31][0] = "Consommation totale (L)";
	dataBusHybride[31][1] = 0;
	dataBusHybride[32] = new Array();	
	dataBusHybride[32][0] = "Puissance nécessaire (W)";
	dataBusHybride[33] = new Array();	
	dataBusHybride[33][0] = "Énergie récupérée par la batterie instantannée (W.h)";
	dataBusHybride[34] = new Array();	
	dataBusHybride[34][0] = "Énergie récupérée par la batterie totale (W.h)";
	dataBusHybride[34][1] = 0;
	
	// Calcul
	// de
	// la
	// puissance
	// nécessaire
	// à
	// chaque
	// instant
	// du
	// trajet
	for(var t = 1; t <= dataBusHybride[0].length - 1; t++){
		dataBusHybride[7][t] = calcMode(dataBusHybride[6][t]); // Calcul
																// du
																// mode
																// de
																// conduite
		dataBusHybride[32][t] = calcPnec(t);
	}
	
	// Calcul
	// de
	// l'état
	// du
	// moteur
	// thermique
	// et
	// de
	// la
	// distribution
	// de
	// la
	// puisssance
	// pour
	// t<4
	for(var t = 1; t <= 4; t++){
		divisionCouple(t-4);
	}
	
	// Calcul
	// de
	// l'état
	// du
	// moteur
	// thermique
	// et
	// de
	// la
	// distribution
	// de
	// la
	// puisssance
	// à t
	// pour
	// t+5
	for(var t = 0; t <= dataBusHybride[0].length-6; t++){
		divisionCouple(t);
	}
	
	nbPointHybride = dataBusHybride[0].length - 1;
	// On
	// inverse
	// la
	// matrice
	// pour
	// pouvoir
	// faire
	// l'affichage
	dataBusHybridereverse = reversetab(dataBusHybride,dataBusHybride.length,nbPointHybride + 1);
}	
/**
 * Generation des tableaux de donnees d'elevation avec la requete Google trace du parcours sur la carte trace du graphique correspondant a l'elevation grace a l'API ColumnChart
 */
function plotElevation(results, status,t0) {
	// recuperation
	// des
	// donnees
	// des
	// etapes
	creaTabStep();
	if (status != google.maps.ElevationStatus.OK) {
		return;
	}
	elevations = results;
	elevation_status = status;

  // Extraction
	// des
	// echantillons
	// d'altitude
	// a
	// partir
	// des
	// donnees
	// recuperees
	// et
	// enregistrement
	// dans
	// un
	// tableau
	// Latitudes
	// /
	// Longitudes

  var elevationPath = [];

  for (var i = 0; i < results.length; i++) {
  	elevationPath.push(elevations[i].location);
  }
  var new_distance = directionsDisplay.getDirections().routes[0].legs[0].distance.value;
  var route_steps = directionsDisplay.getDirections().routes[0].legs[0].steps;
  var match = 0;
  var epsilon = 0.0001;

  var temp_step_factor = Math.floor(elevations.length / route_steps.length);
  var elevation_indice = 0;
  nbStep = route_steps.length;
  for (var w = 0; w < nbStep; w++) { // cette
										// partie
										// prends
										// a
										// intervalle
										// regulier
										// les
										// distances
										// et
										// les
										// durees
										// dans
										// un
										// tableau
	  if (typeVehicule=="VPU" && parseFloat(document.getElementById("modePuissancePACU").value) == 1){
		  var tampon_duree = route_steps[w].duration.value/valeurDegrade();
	  }
	  else {var tampon_duree = route_steps[w].duration.value;}
      var tampon_distance = route_steps[w].distance.value
              dataStep[w+1] = new Array();
              dataStep[w+1][0] = tampon_distance;
              dataStep[w+1][1] = tampon_duree;
    // if
	// (overview_duration[elevation_indice]==undefined)
	// {overview_duration[elevation_indice]=0}
    if (w % temp_step_factor == 0) {
              overview_duration[elevation_indice] += tampon_duree;
              overview_distance[elevation_indice] += tampon_distance;
              elevation_indice ++;
            }
    else  {
            overview_duration[elevation_indice] += tampon_duree; 
            overview_distance[elevation_indice] += tampon_distance;
            // elevation_indice
			// ++;
    }
    
  }
  // document.body.innerHTML=dataStep;
  overview_summed_durations[0]=overview_duration[0];
  for (var x = 1; x <= overview_duration.length; x++) {
    overview_summed_durations[x] = overview_duration[x] + overview_summed_durations[x-1];
  }
  // Extraction
	// des
	// donnees
	// utilisees
	// pour
	// construire
	// le
	// graphique.
  
  data = new google.visualization.DataTable();
  dataxcos = new google.visualization.DataTable();
  dataxcos2 = new google.visualization.DataTable();  
  var distance = 0;
  var distances = [];
  var altitudes = [];
  var differenceup = 0;
  var differencedown = 0;
  var cat_one = [];
  var cat_two = [];
  var cat_three = [];
  var cat_four = [];
  var max = 0;
  var min = 10000;
  var maxindex = 0;
  var minindex = 0;
  var temp_distance;
  var temp_elevation;
  var temp_slope;
  var temp_duration;
  var tempup = 0;
  var tempdown = 0;
  var gradient = 0;
  var count = 0;
  var gradientred = [];
  var gradientorange = [];
  var lastcolor;
  var gradients = [];
  var anno = '';
  var poly_distance = 0;
  // Creation
	// des
	// tableaux
	// de
	// donnees
  data.addColumn('string', '');
  data.addColumn('number', 'Elevation');
  data.addColumn({'type': 'string', 'role': 'tooltip'});
  data.addColumn({type:'string', role:'annotation'}); 
  data.addColumn('number', 'Gradient');
  data.addColumn({'type': 'string', 'role': 'tooltip'});
  data.addColumn({type:'string', role:'annotation'}); 
  data.addColumn('number', 'Gradient');
  data.addColumn({'type': 'string', 'role': 'tooltip'});
  data.addColumn({type:'string', role:'annotation'}); 
  
  dataxcos.addColumn('string', 'Latitude');
  dataxcos.addColumn('string', 'Longitude');
  dataxcos.addColumn('number', 'Altitude (m)');
  dataxcos.addColumn('number', 'Distance cumulee (m)');
  dataxcos.addColumn('number', 'Duree cumulee');
  dataxcos.addColumn('number', 'Pente (%)');
  
  dataxcos2.addColumn('string', 'Latitude');
  dataxcos2.addColumn('string', 'Longitude');
  dataxcos2.addColumn('number', 'Distance');
  dataxcos2.addColumn('number', 'Duree');

  for (var i = 0; i < results.length; i++) {
  	altitudes[i] = elevations[i].elevation;	
  	if (elevations[i].elevation < min){ min = elevations[i].elevation; minindex = i;}
  	if (elevations[i].elevation > max) {max = elevations[i].elevation; maxindex = i;}
  	if ( i < results.length - 1){
  		distances[i] = google.maps.geometry.spherical.computeDistanceBetween (elevations[i].location, elevations[i+1].location);
  		distance = distance + distances[i];
  		gradients[i] = (elevations[i+1].elevation - elevations[i].elevation) / distances[i];
  		if (distance > 100) {
  			gradient = (elevations[i+1].elevation - elevations[i - count].elevation) / distance;
  			if (gradient > gradient_red) {
  				for(var x = i-count; x <= i + 1; x++) {
  					gradientred[x] = true;
  					gradientorange[x] = false;
  				}
  			}
  			if (gradient > gradient_orange) {
  				for(var x = i-count; x <= i + 1; x++) {
  					gradientorange[x] = true;
  				}
  			}
  			distance = distance - distances[i-count];
  			count--;
  		}	
  		count++;
  		poly_distance = poly_distance + distances[i];
  	}
  } 

  var distance_ratio = new_distance / poly_distance;
  var distance_temp = 0;
  distance = 0;
  gradient = 0;
  lastcolor = "blue";
  var temptops = findtemptops(gradients);
  cat_one = findtops(temptops, gradients, distances, altitudes, disone, disone * 1.5, gradeone);
  cat_two = findtops(temptops, gradients, distances, altitudes, distwo, disone, gradetwo);
  cat_three = findtops(temptops, gradients, distances, altitudes, disthree, distwo, gradethree);
  cat_four = findtops(temptops, gradients, distances, altitudes, disfour, disthree, gradefour);
  clearcats(cat_one, disone, cat_two, distwo, cat_three, disthree, cat_four, disfour, distances);
  var distTampon = 0;
  for (var i = 0; i < results.length; i++) { 
  	temp_distance = distance/1000;	  
  	temp_elevation = elevations[i].elevation * system_ft;
  	temp_slope = 0;
     // --------------------------pour
		// remplissage
		// du
		// tableau
		// dataXcos
		// DEBUT
		// --------------------------------------------
    if (temp_distance != 0) {
      if (i == 0) {
        temp_slope = (elevations[i].elevation - elevations[i].elevation) / temp_distance;
      }
      else {
        temp_slope = (elevations[i].elevation - elevations[i-1].elevation) / ((temp_distance-distTampon)*1000);
      }
    }
    distTampon = temp_distance;

    // --------------------------pour
	// remplissage
	// du
	// tableau
	// dataXcos
	// FIN
	// --------------------------------------------
  	anno = '';	
  	if(i == minindex && !hideminmax) {
  		anno = 'MIN';
  	}
  	if(i == maxindex && !hideminmax) {
  		anno = 'MAX';	
  	}
  	if(cat_one[i]){
  		anno = '1';
  	}
  	else if(cat_two[i]){
  		anno = '2';
  	}
  	else if(cat_three[i]){
  		anno = '3';
  	}
  	else if(cat_four[i]){
  		anno = '4';
  	}

  	if (i < results.length -2 ){

  		if(gradientorange[i] && gradientorange[i+2]) {
  			gradientorange[i+1] = true;
  		}
  		if(!gradientorange[i] && !gradientorange[i+2]) {
  			gradientorange[i+1] = false;
  		}
  		if(gradientred[i] && gradientred[i+2]) {
  			gradientred[i+1] = true;
  		}

  	}
// Remplissage
// du
// tableau
// permettant
// la
// construction
// du
// graphique
// selon
// les
// deniveles
// de
// chaque
// portion.
// str_mi
// str_ft
	
  	if (gradientred[i]){
  		if (lastcolor == "orange"){
  			data.addRow(['' + Math.round(distance/1000), ,tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation),  anno, temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		else if (lastcolor == "blue"){
  			data.addRow(['' + Math.round(distance/1000),temp_elevation,tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation),  anno, , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno,temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		else {
  			data.addRow(['' + Math.round(distance/1000), ,tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation),  anno, , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation),  anno, temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		lastcolor = "red";
  	}
  	else if (gradientorange[i]){

  		if (lastcolor == "orange"){
  			data.addRow(['' + Math.round(distance/1000), , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		else if (lastcolor == "blue"){
  			data.addRow(['' + Math.round(distance/1000),temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, temp_elevation , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation),  anno, , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		else if (lastcolor = "red") {
  			data.addRow(['' + Math.round(distance/1000), , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation),  anno, temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		lastcolor = "orange";
  	}
  	else{
  		if (lastcolor == "orange"){
  			data.addRow(['' + Math.round(distance/1000), temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation),  anno, temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		else if (lastcolor == "blue"){
  			data.addRow(['' + Math.round(distance/1000),temp_elevation , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation),  anno, , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		else {
  			data.addRow(['' + Math.round(distance/1000), temp_elevation , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, , tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno, temp_elevation, tooltip('Distance(km)', temp_distance,'Elevation (m) ', temp_elevation), anno]);
  		}
  		lastcolor = "blue";
  	}

// Remplissage
// du
// tableau
// contenant
// les
// donnees
// d'altitude
// et de
// distance
// a
// destination
// d'Xcos
	// Extraction
	// des
	// coordonnees
	coordonnees = elevations[i].location;
	/*
	 * Les coordonnees sont au format (xx.xxxxxxxxxxx,yy.yyyyyyyyyyy) Pour extraire la latitude et la longitude, on enleve les parentheses et la virgule
	 */
	coordonnees = String(coordonnees);
	// Si
	// possible
	// :
	// Voir
	// ici
	// pour
	// determiner
	// la
	// duree
	// de
	// trajet
	// depuis
	// le
	// point
	// de
	// depart
	// selon
	// les
	// coordonnees
	// GPS.
	coordonnees = coordonnees.replace("(","");
	coordonnees = coordonnees.replace(")","");
	var latitudelongitude = coordonnees.split(", ");
	latitude = latitudelongitude[0];
	longitude = latitudelongitude[1];
	// alert("On
	// a"+dataStep[20][3]);
	dataxcos.addRow([latitude, longitude, temp_elevation, temp_distance, overview_summed_durations[i],temp_slope]);
// Calcul
// des
// distances
// et des
// altitudes
  	if ( i < results.length - 1){
  		distance_temp = distance_temp + distances[i];
  		distance = distance_temp * distance_ratio * system_mi;
  		if (elevations[i].elevation > elevations[i+1].elevation) {
  			differencedown = differencedown + elevations[i].elevation - elevations[i+1].elevation;
  		}
  		else{
  			differenceup = differenceup + elevations[i+1].elevation - elevations[i].elevation;
  		}
  	}
  }
  new_distance = (new_distance * system_mi /1000);
  differenceup = (differenceup * system_ft);
  differencedown = (differencedown * system_ft);
  max = (max * system_ft);
  min = (min * system_ft);
  document.getElementById("distance"+typeVehicule).innerHTML=Math.round(new_distance*10)/10 + " " + str_mi; // distance
																											// totale
																											// du
																											// trajet
  document.getElementById("differenceup"+typeVehicule).innerHTML=Math.round(differenceup*10)/10 + " " + str_ft;  // ascensions
																													// cumulees
  document.getElementById("differencedown"+typeVehicule).innerHTML= Math.round(differencedown*10)/10 + " " + str_ft; // descentes
																														// cumulees
  document.getElementById("max"+typeVehicule).innerHTML=Math.round(max*10)/10 + " " + str_ft; // altitude
																								// max
  document.getElementById("min"+typeVehicule).innerHTML=Math.round(min*10)/10 + " " + str_ft; // altitude
																								// min
  document.getElementById("tablesummary"+typeVehicule).style.visibility ="";
  document.getElementById("routeDistance"+typeVehicule).style.visibility ="";
  document.getElementById("routeCumulMontee"+typeVehicule).style.visibility ="";
  document.getElementById("routeCumulDescente"+typeVehicule).style.visibility ="";
  document.getElementById("routeAltMax"+typeVehicule).style.visibility ="";
  document.getElementById("routeAltMin"+typeVehicule).style.visibility ="";
  
  // Trace
	// du
	// graphique
	// pour
	// le
	// VE
  if(typeVehicule=="VE"){
	  document.getElementById('elevation_chartVE').style.display = 'block';
	  
	  chartVE.draw(data, {
		vAxis: {title: 'Distance (' + str_mi + ')', showTextEvery: 20, maxAlternation: 100, slantedText: 'false'},
		hAxis: {title: 'Elevation (' + str_ft + ')'},
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},
	  	bar: {groupWidth: '100%'},
	  	chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
	  	axisTitlesPosition: "in",
	  	isHtml: true,
	  	lineWidth: 3,
	  	areaOpacity: 0.4,
	  	colors: ['#295DBC', '#F29D00', '#ED0300'],
	  });
	  
	  document.getElementById('elevation_chartVE').style.width = '100%'
	  document.getElementById('elevation_chartVE').style.height = '100%'
	  document.getElementById('elevation_chartVE').style.display=''
  }
  
	  
  // Trace
	// du
	// graphique
	// pour
	// le
	// VT
  if(typeVehicule=="VT"){
	  document.getElementById('elevation_chartVT').style.display = 'block';
	  
	  chartVT.draw(data, {
		vAxis: {title: 'Distance (' + str_mi + ')', showTextEvery: 20, maxAlternation: 100, slantedText: 'false'},
	 	hAxis: {title: 'Elevation (' + str_ft + ')'},
	 	legend: {position:'none'},
	 	backgroundColor: '#FFFFF',
		animation: {duration: 2000, easing: 'linear'},
		bar: {groupWidth: '100%'},
		chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
		axisTitlesPosition: "in",
		isHtml: true,
		lineWidth: 3,
		areaOpacity: 0.4,
		colors: ['#295DBC', '#F29D00', '#ED0300'],
	  });
	  
	  document.getElementById('elevation_chartVT').style.width = '100%'
	  document.getElementById('elevation_chartVT').style.height = '100%'
	  document.getElementById('elevation_chartVT').style.display=''
  }
  
		
  // Trace
	// du
	// graphique
	// pour
	// le
	// VH
  if(typeVehicule=="VH"){
	  document.getElementById('elevation_chartVH').style.display = 'block';
	  
	  chartVH.draw(data, {
		vAxis: {title: 'Elevation (' + str_ft + ')'},vAxis: {title: 'Distance (' + str_mi + ')', showTextEvery: 20, maxAlternation: 100, slantedText: 'false'},
	 	hAxis: {title: 'Elevation (' + str_ft + ')'},
		legend: {position:'none'},
		backgroundColor: '#FFFFF',
		animation: {duration: 2000, easing: 'linear'},
		bar: {groupWidth: '100%'},
		chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
		axisTitlesPosition: "in",
		isHtml: true,
		lineWidth: 3,
		areaOpacity: 0.4,
		colors: ['#295DBC', '#F29D00', '#ED0300'],
	  });
	  
	  document.getElementById('elevation_chartVH').style.width = '100%'
	  document.getElementById('elevation_chartVH').style.height = '100%'
	  document.getElementById('elevation_chartVH').style.display=''
  }
  
// Trace
// du
// graphique
// pour le
// VP
  if(typeVehicule=="VP"){
	  document.getElementById('elevation_chartVP').style.display = 'block';
	  
	  chartVP.draw(data, {
		vAxis: {title: 'Distance (' + str_mi + ')', showTextEvery: 20, maxAlternation: 100, slantedText: 'false'},
		hAxis: {title: 'Elevation (' + str_ft + ')'},
		legend: {position:'none'},
		backgroundColor: '#FFFFF',
		animation: {duration: 2000, easing: 'linear'},
		bar: {groupWidth: '100%'},
		chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
		axisTitlesPosition: "in",
		isHtml: true,
		lineWidth: 3,
		areaOpacity: 0.4,
		colors: ['#295DBC', '#F29D00', '#ED0300'],
	  });
	  
	  document.getElementById('elevation_chartVP').style.width = '100%'
	  document.getElementById('elevation_chartVP').style.height = '100%'
	  document.getElementById('elevation_chartVP').style.display=''
  }
  
// Trace
// du
// graphique
// pour le
// VPU
  if(typeVehicule=="VPU"){
	  document.getElementById('elevation_chartVPU').style.display = 'block';
	  
	  chartVPU.draw(data, {
		vAxis: {title: 'Distance (' + str_mi + ')', showTextEvery: 20, maxAlternation: 100, slantedText: 'false'},
		hAxis: {title: 'Elevation (' + str_ft + ')'},
		legend: {position:'none'},
		backgroundColor: '#FFFFF',
		animation: {duration: 2000, easing: 'linear'},
		bar: {groupWidth: '100%'},
		chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
		axisTitlesPosition: "in",
		isHtml: true,
		lineWidth: 3,
		areaOpacity: 0.4,
		colors: ['#295DBC', '#F29D00', '#ED0300'],
	  });
	  
	  document.getElementById('elevation_chartVPU').style.width = '100%'
	  document.getElementById('elevation_chartVPU').style.height = '100%'
	  document.getElementById('elevation_chartVPU').style.display=''
  }

	// Calcul
	// des
	// temps
	// de
	// trajet
	// selon
	// les
	// troncons
	var temp1;
	var temp2;
	var temp3;
	var temp4;
			
	var steps = directionsDisplay.getDirections().routes[0].legs[0].steps;
		for(var i = 0; i < steps.length; i++) {
			coordonnees = steps[i].start_location;
			coordonnees = String(coordonnees);
			coordonnees = coordonnees.replace("(","");
			coordonnees = coordonnees.replace(")","");
				
			var latitudelongitude = coordonnees.split(", ");
			
			temp1 = latitudelongitude[0];
			temp2 = latitudelongitude[1];
			
			temp3 = steps[i].distance.value;
			temp4 = steps[i].duration.value;
			
			dataxcos2.addRow([temp1, temp2, temp3, temp4]);
		}
	distanceTotaleRoute = distance;  
	// Affichage
	// des
	// donnees
	// contenues
	// dans
	// les
	// datatables
	// pour
	// xcos.
    var tablexcos = new google.visualization.Table(document.getElementById('table_elevation_div'+typeVehicule));
    var tempsDraw = new google.visualization.DataTable();
    tempsDraw.addColumn('number', 'Elevation (m)');
    tempsDraw.addColumn('number', 'Distance cumule (km)');
    tempsDraw.addColumn('number', 'Pente (%)');
    for (var iterateur = 0; iterateur < dataxcos.getNumberOfRows();iterateur++){
    	tempsDraw.addRow([dataxcos.getValue(iterateur,2),dataxcos.getValue(iterateur,3),dataxcos.getValue(iterateur,5)]);
    }
    var tableTemps = new google.visualization.Table(document.getElementById('table_elevation_div'+typeVehicule));
// tableTemps.draw(tempsDraw,
// {showRowNumber:
// false,
// width:
// '100%',
// height:
// '100%'});
// tablexcos.draw(dataxcos,
// {showRowNumber:
// false,
// width:
// '100%',
// height:
// '100%'});
		   
		var tablexcos2 = new google.visualization.Table(document.getElementById('table_etapes_div'+typeVehicule));
// tablexcos2.draw(dataxcos2,
// {showRowNumber:
// true,
// width:
// '100%',
// height:
// '100%'});
		calcVStep();
		
		
		
		// alert(nbStep);
		
}

function defLimitation(V){
	
	var lim;
	
		if (V<=40) {lim=30.0}
		if (V>40 && V<=60) {lim=50.0}
		if (V>60 && V<=80) {lim=70.0}
		if (V>80 && V<=100) {lim=90.0}
		if (V>100 && V<=120) {lim=110.0}
		if (V>120) {lim=130.0}
	
	return lim;
	
}


// ----------------------------------------------------------------------------------
// Calcul
// energetique
// --------------------------------------------------------------------------
/**
 * Calcul de l'acceleration d'un point entre T1 et T2
 * 
 * @param V1 :
 *            vitesse au debut de la phase
 * @param V2 :
 *            vitesse a la fin de la phase
 * @param T1 :
 *            temps au debut de la phase
 * @param T2 :
 *            temps a la fin de la phase
 * @returns duree : delta T entre les points
 * @returns acc : acceleration entre T1 et T2
 * @returns phase : nom de la phase ('Acceleration', 'Deceleration', 'Vitesse constante')
 */
function calcPhase(V1,V2,T1,T2){
	var duree	= T2-T1;
	var acc 	= (V2-V1)/duree;
	var phase;
	if(V1 == 0){
		acc =0.5
	}
	if(V2 == 0){
		acc =-0.5
	}
	if(acc == 0){
		phase = "Vitesse constante"
	}
	else if(acc > 0){
		phase = "Acceleration"
	}
	else if(acc < 0){
		phase = "Deceleration"
	}
	
	return{
		duree: 	duree,
		acc: 	acc,
		phase: 	phase
	}
}
/**
 * Calcul des forces appliquees au vehicule
 * 
 * @param V :
 *            vitesse du vehicule
 * @param acc :
 *            acceleration du vehicule
 * @param pente :
 *            pente en %
 * @returns Fr : resistance au roulement
 * @returns Fair : resistance de l'air
 * @returns Fp : resistance de la pente
 * @returns Facc : force d'acceleration
 * @returns Fm : force motrice
 */
function calcForce(V,acc,pente){
	
	var angle	= Math.atan(pente);
	var Fr 		= res_roulement * pes * masse * Math.cos(angle);	
	var Fair 	= 0.5*rho*SCx*Math.pow(V,2);						
	var Fp		= pes*masse*Math.sin(angle);	
	var Facc	= acc*masse;									
	var Finert  = coefInertie*masse*acc;// Force
										// du a
										// l'inertie
										// des
										// parties
										// du
										// moteur,
										// cette
										// variable
										// n'a
										// pas
										// de
										// colonne
										// dans
										// dataConso,
										// = Ci
										// * m
										// *
										// acc
										// ou
										// Ci
										// est
										// 5%
										// de
										// la
										// masse
										// du
										// vehicule
	var Fm		= Fr + Fair + Fp + Facc + Finert;															
	
	return{
	Fr 		: Fr,
	Fair 	: Fair,
	Fp		: Fp,
	Facc	: Facc,
	Fm		: Fm,
	}
}	


/**
 * Determination du rapport de vitesse pour optimiser la consommation pour le vehicule pendant une phase,
 * 
 * @param RotationRouePhase :
 *            Vitesse de rotation de la roue (trs/min)
 * @param CoupleRouePhase :
 *            Couple a la roue (N.m)
 * @param RapportOptiPhasePrecedente :
 *            Vitesse optimale enclenchee pendant la phase precedente
 * @param RapportPhase :
 *            Vitesse enclenchee pendant la phase (optionnel)
 * @returns RegimeMoteurOpti : Regime Moteur Optimal (trs/min),
 * @returns CoupleMoteurOpti : Couple Moteur Optimal (N.m),
 * @returns ChargeMoteurOpti : Charge Moteur Optimale (%),
 * @returns ConsoOpti : Consommation Optimale (g/kWh)
 * @returns RapportOpti : Rapport de vitesse optimal
 */
function calcConsoOpti(RotationRouePhase,CoupleRouePhase,RapportOptiPhasePrecedente,RapportPhase){

	var tabRegimesMoteur = new Array();
	var tabCouplesMoteur = new Array();
	var tabCouplesMax = new Array();
	var tabChargesMoteur = new Array();	
	var indiceRegimeMoteur = new Array();
	var indiceChargeMoteur = new Array();
	var tabConso =  new Array();
	var iOpti;
	var RegimeMoteurOpti = 0;
	var CoupleMoteurOpti = 0;
	var ChargeMoteurOpti = 0;
	var ConsoOpti = 0;
	var boolOpti = true;
	
	if(RapportOptiPhasePrecedente == -1){
		RapportOptiPhasePrecedente = RapportPhase;
	}
	
	// On
	// calcule
	// le
	// couple
	// et
	// le
	// regime
	// moteur
	// en
	// 1ere
	// vitesse
	var CoupleMoteurPhase = CoupleRouePhase / dataVitesses[1] /rend_gear;
	var RegimeMoteurPhase = RotationRouePhase*dataVitesses[1];

	// On
	// calcule
	// seulement
	// pour
	// les
	// rapports
	// voisins
	// de
	// la
	// phase
	// precedente
	for(i=1;i<=nbRapportsVitesse;i++){
		// if(i<=nbRapportsVitesse){
		if(Math.abs(i-RapportOptiPhasePrecedente)<=1){
			// On
			// calcule
			// le
			// regime
			// moteur,
			// le
			// couple
			// moteur,
			// le
			// couple
			// max
			// et
			// la
			// charge
			// moteur
			// pour
			// les
			// rapports
			// possibles
			tabRegimesMoteur[i] = RegimeMoteurPhase/dataVitesses[1]*dataVitesses[i];
			tabCouplesMoteur[i] = CoupleMoteurPhase*dataVitesses[1]/dataVitesses[i];
			tabCouplesMax[i] = coef[3]*Math.pow(tabRegimesMoteur[i],3)+coef[2]*Math.pow(tabRegimesMoteur[i],2)+coef[1]*tabRegimesMoteur[i]+coef[0];
			tabChargesMoteur[i] = tabCouplesMoteur[i]/tabCouplesMax[i]*100;
			indiceRegimeMoteur[i] = (pasNmot*(Math.round(tabRegimesMoteur[i]/pasNmot))-minvecNmot)/pasNmot;
			indiceChargeMoteur[i] = (pasCmot*(Math.round(tabChargesMoteur[i]/pasCmot))-minvecCmot)/pasCmot;
			if(indiceChargeMoteur[i]<=0){
				indiceChargeMoteur[i] = 1;
			}
			if(indiceChargeMoteur[i]>=longueurvecCmot){
				indiceChargeMoteur[i] = longueurvecCmot-1;
			}
			// on
			// calcule
			// la
			// consommation
			// quand
			// ça
			// rentre
			// dans
			// la
			// map
			// cse
			if(indiceRegimeMoteur[i]<longueurvecNmot && indiceRegimeMoteur[i]>0 && tabChargesMoteur[i]<100){
					tabConso[i] = Math.round(Map_cse_ex[indiceRegimeMoteur[i]][indiceChargeMoteur[i]]);
					boolOpti = false;
			}
			// sinon
			// on
			// met
			// une
			// valeur
			// impossible
			// a
			// atteindre
			else{
				tabConso[i] = 100000;
			}
		}
		// sinon
		// on
		// met
		// une
		// valeur
		// impossible
		// a
		// atteindre
		else{
			tabConso[i] = 100000;
		}
	}
	// si
	// aucune
	// valeur
	// de
	// consommation
	// n'a
	// ete
	// calculee
	// car
	// ca
	// sortait
	// de
	// la
	// map
	// cse,
	// on
	// va
	// en
	// creer
	// une
	if(boolOpti){
		var boolInf = true;
		var boolSup = true;
		for(i=RapportOptiPhasePrecedente-1;i<=RapportOptiPhasePrecedente+1;i++){
			if(i>0 && i <= nbRapportsVitesse){
				if(indiceRegimeMoteur[i]>0){
					boolInf = false;
				}
				if(indiceRegimeMoteur[i]<longueurvecNmot){
					boolSup = false;
				}
			}
		}
		
		// si
		// les
		// valeurs
		// etaient
		// en
		// dessous
		// du
		// minimum,
		// alors
		// on
		// les
		// mets
		// au
		// minimum
		// et
		// on
		// calcule
		// on
		// est
		// en
		// 1ere
		if(boolInf){
			tabRegimesMoteur[1] = minvecNmot+pasNmot;
			tabCouplesMoteur[1] = CoupleMoteurPhase*dataVitesses[1]/dataVitesses[1];
			tabCouplesMax[1] = coef[3]*Math.pow(tabRegimesMoteur[1],3)+coef[2]*Math.pow(tabRegimesMoteur[1],2)+coef[1]*tabRegimesMoteur[1]+coef[0];
			tabChargesMoteur[1] = tabCouplesMoteur[1]/tabCouplesMax[1]*100;
			if(tabChargesMoteur[1]<0){
				tabChargesMoteur[1]=0;
			}
			indiceRegimeMoteur[1] = (pasNmot*(Math.round(tabRegimesMoteur[1]/pasNmot))-minvecNmot)/pasNmot;
			indiceChargeMoteur[1] = (pasCmot*(Math.round(tabChargesMoteur[1]/pasCmot))-minvecCmot)/pasCmot;
			if(indiceChargeMoteur[1]<=0){
				indiceChargeMoteur[1] = 1;
			}
			if(indiceChargeMoteur[1]>=longueurvecCmot){
				indiceChargeMoteur[1] = longueurvecCmot-1;
			}
			tabConso[1] = Math.round(Map_cse_ex[indiceRegimeMoteur[1]][indiceChargeMoteur[1]]);
		}
		
		// si
		// les
		// valeurs
		// etaient
		// au
		// dessus
		// du
		// maximmum,
		// alors
		// on
		// les
		// mets
		// au
		// maximum
		// et
		// on
		// calcule,
		// on
		// utilise
		// le
		// plus
		// faible
		// rapport
		// de
		// reduction
		if(boolSup){
			tabRegimesMoteur[nbRapportsVitesse] = maxvecNmot;
			tabCouplesMoteur[nbRapportsVitesse] = CoupleMoteurPhase*dataVitesses[1]/dataVitesses[nbRapportsVitesse];
			tabCouplesMax[nbRapportsVitesse] = coef[3]*Math.pow(tabRegimesMoteur[nbRapportsVitesse],3)+coef[2]*Math.pow(tabRegimesMoteur[nbRapportsVitesse],2)+coef[1]*tabRegimesMoteur[nbRapportsVitesse]+coef[0];
			tabChargesMoteur[nbRapportsVitesse] = tabCouplesMoteur[nbRapportsVitesse]/tabCouplesMax[nbRapportsVitesse]*100;	
			if(tabChargesMoteur[nbRapportsVitesse]<0){
				tabChargesMoteur[nbRapportsVitesse]=0;
			}
			indiceRegimeMoteur[nbRapportsVitesse] = (pasNmot*(Math.round(tabRegimesMoteur[nbRapportsVitesse]/pasNmot))-minvecNmot)/pasNmot;
			indiceChargeMoteur[nbRapportsVitesse] = (pasCmot*(Math.round(tabChargesMoteur[nbRapportsVitesse]/pasCmot))-minvecCmot)/pasCmot;
			if(indiceChargeMoteur[nbRapportsVitesse]<=0){
				indiceChargeMoteur[nbRapportsVitesse] = 1;
			}
			if(indiceChargeMoteur[nbRapportsVitesse]>=longueurvecCmot){
				indiceChargeMoteur[nbRapportsVitesse] = longueurvecCmot-1;
			}
			tabConso[nbRapportsVitesse] = Math.round(Map_cse_ex[indiceRegimeMoteur[nbRapportsVitesse]][indiceChargeMoteur[nbRapportsVitesse]]);
		}
		if(!boolInf && !boolSup){
			for(i=1;i<=nbRapportsVitesse;i++){
					// On
					// calcule
					// le
					// regime
					// moteur,
					// le
					// couple
					// moteur,
					// le
					// couple
					// max
					// et
					// la
					// charge
					// moteur
					// pour
					// les
					// rapports
					// possibles
					tabRegimesMoteur[i] = RegimeMoteurPhase/dataVitesses[1]*dataVitesses[i];
					tabCouplesMoteur[i] = CoupleMoteurPhase*dataVitesses[1]/dataVitesses[i];
					tabCouplesMax[i] = coef[3]*Math.pow(tabRegimesMoteur[i],3)+coef[2]*Math.pow(tabRegimesMoteur[i],2)+coef[1]*tabRegimesMoteur[i]+coef[0];
					tabChargesMoteur[i] = tabCouplesMoteur[i]/tabCouplesMax[i]*100;
					indiceRegimeMoteur[i] = (pasNmot*(Math.round(tabRegimesMoteur[i]/pasNmot))-minvecNmot)/pasNmot;
					indiceChargeMoteur[i] = (pasCmot*(Math.round(tabChargesMoteur[i]/pasCmot))-minvecCmot)/pasCmot;
					if(indiceChargeMoteur[i]<=0){
						indiceChargeMoteur[i] = 1;
					}
					if(indiceChargeMoteur[i]>=longueurvecCmot){
						indiceChargeMoteur[i] = longueurvecCmot-1;
					}
					// on
					// calcule
					// la
					// consommation
					// quand
					// ça
					// rentre
					// dans
					// la
					// map
					// cse
					if(indiceRegimeMoteur[i]<longueurvecNmot && indiceRegimeMoteur[i]>0 && tabChargesMoteur[i]<100){
							tabConso[i] = Math.round(Map_cse_ex[indiceRegimeMoteur[i]][indiceChargeMoteur[i]]);
					}
					// sinon
					// on
					// met
					// une
					// valeur
					// impossible
					// a
					// atteindre
					else{
						tabConso[i] = 100000;
					}
			}
		}
	}
	
	// le
	// rapport
	// optimal
	// est
	// celui
	// pour
	// lequel
	// la
	// consommation
	// est
	// la
	// plus
	// faible
	iOpti = findIndex(tabConso,rechercheMin(tabConso),1);
	RegimeMoteurOpti = tabRegimesMoteur[iOpti];
	CoupleMoteurOpti = tabCouplesMoteur[iOpti];
	ChargeMoteurOpti = tabChargesMoteur[iOpti];
	ConsoOpti = tabConso[iOpti];
	
	return{
		RapportOpti : iOpti,
		RegimeMoteurOpti : RegimeMoteurOpti,
		CoupleMoteurOpti : CoupleMoteurOpti,
		ChargeMoteurOpti : ChargeMoteurOpti ,
		ConsoOpti : ConsoOpti	
	}
}	


/**
 * Calcul de la puissance mecanique
 * 
 * @param V :
 *            vitesse du vehicule
 * @param Fm :
 *            force motrice mecanique
 * @returns puissance mecanique
 */
function calcPuissanceMeca(V,Fm){
	var e = V*Fm; // Puissance
					// mecanique
					// pour
					// la
					// force
					// et
					// la
					// vitesse
	var Pdissipee =0;
	// Calcul
	// de
	// la
	// puissance
	// a
	// fournir
	// par
	// le
	// moteur
	// (prise
	// en
	// compte
	// des
	// rendements)
	var Pmoteur;
	if(typeVehicule=="VE" || typeVehicule =="VP" || typeVehicule =="VPU")
		{
			if(Fm > 0){
				Pmoteur = e / (calcEfficacite(Pmotorrated,e)*rend_gear*normFactor);
			}else{
				Pmoteur = e*calcEfficacite(Pmotorrated,e)*rend_gear*regenFactor(V*3.6)*normFactor;
				Pdissipee = e*calcEfficacite(Pmotorrated,e)*rend_gear*(1-regenFactor(V*3.6))*normFactor;
			}
		}

	else if(typeVehicule=="VT"){
		if(Fm>0){
			Pmoteur=e/rend_gear;
		}
		else{
			Pmoteur = 0;
		}
	}
	return {
		Pmoteur : Pmoteur,
		Pdissipee: Pdissipee,
	}
}
/**
 * Calcul de la puissance electrique
 * 
 * @param Pm :
 *            puissance motrice
 * @returns puissance electrique
 */
function calcPuissanceElec(Pm){
	var pelec;
	var Ptemp = Pm;
	if(Ptemp>0){ 	// Decharge
					// de
					// la
					// batterie
		pelec = Ptemp/Math.sqrt(rend_elec);
	}else{			// Charge
					// de
					// la
					// batterie
		pelec = Ptemp*Math.sqrt(rend_elec);
	}
	return pelec;
}
/**
 * Calcul du facteur de regeneration fonction de la vitesse du vehicule
 * 
 * @param V
 */
function regenFactor(V){
	var facteur = 1;
	if(V<vmin){				// Regeneration
							// nulle
							// en
							// dessous
							// de
							// vmin
		facteur = 0;
	}else if(V<vmax){		// Regeneration
							// lineaire
							// en
							// fonction
							// de
							// la
							// vitesse
		facteur = V/(vmax-vmin)/100;
	}else{					// Regeneration
							// a
							// 100%
							// au
							// dessus
							// de
							// vmax
		facteur = 1;
	}
	return facteur;
}
/**
 * Calcul du maximum de la valeur absolue de deux nombres
 * 
 * @param N1 :
 *            nombre 1
 * @param N2 :
 *            nombre 2
 * @returns N1 si |N1|>|N2| et N2 sinon
 */
function maxAbsolu(N1,N2){
	if(Math.abs(N1) > Math.abs(N2)){
		return N1;
	}else{
		return N2;
	}
}
/**
 * Calcul de l'efficacite de la batterie en fonction du taux de charge ??? Pas plutot l'efficacité du moteur électrique en fonction de la puissance nominale ???
 * 
 * @param Pnom :
 *            Puissance nominale du moteur
 * @param Pout :
 *            Puissance exige dans les calculs energetique
 * @returns eff : efficacite de la batterie
 */
function calcEfficacite(Pnom,Pout){
	var frac = Math.abs(Pout)/Pnom;
	var eff  = 0;
	var c1; var c2; var c3; var d1; var d2; var e1; var e2;
	if(Pout > 0){ // Consommation
					// energie
		c1 = 0.924300;
		c2 = 0.000127;
		c3 = 0.012730;
		d1 = 0.08;
		d2 = 0.86;
		e1 = -0.0736;
		e2 = 0.9752;
	}else{ // Production
			// energie
		c1 = 0.925473;
		c2 = 0.000148;
		c3 = 0.014948;
		d1 = 0.075312;
		d2 = 0.8586;
		e1 = -0.062602;
		e2 = 0.971034;
	}
	if(frac < 0.25){
		eff = (c1*frac+c2)/(frac + c3);
	}else if(frac < 0.75){
		eff = d1*frac + d2;
	}else{
		eff = e1*frac + e2;
	}
	return eff;
}
/**
 * Calcul du facteur de normalisation en fonction de la puissance moteur
 * 
 * @param Pmotorrated
 */
function calcNormfactor(Pmotorrated){
	var temp = 0.144*(1-Math.exp(-Pmotorrated/9.71))+0.852 // Les
															// coefficients
															// proviennent
															// de
															// l'interpolation
															// de
															// la
															// courbe
															// de
															// normalisation
	if(temp>1 || Pmotorrated > 200){
		normFactor = 1;
	}else{
		normFactor = temp;
	}
}

// ----------------------------------------------------------------------------------
// Autres
// fonctions
// de
// calcul
// -------------------------------------------------------------------
/**
 * Pour verifier une string ne contient que des nombres et . (ie nombre int/float ou non)
 * 
 * @returns true si valide, false si des lettres ou caracteres autres que . sont presents
 */
function isNumeric(string) {
	  return string.match(/^-?\d*(\.\d+)?$/) != null;
}
/**
 * Renvoie la distance en km entre deux points
 * 
 * @param lat1 :
 *            latitude du premier point
 * @param long1 :
 *            longitude du premier point
 * @param lat2 :
 *            latitude du deuxieme point
 * @param long2 :
 *            longitude du deuxieme point
 * @returns distance en km
 */
function calDistLatLg(lat1,long1,lat2,long2){
	var e = Math.PI * lat1  /180;
	var f = Math.PI * long1 /180;
	var g = Math.PI * lat2  /180;
	var h = Math.PI * long2 /180;
	var R = 6371;
	var i =Math.sin(e)*Math.sin(g) + Math.cos(e)*Math.cos(g)*Math.cos(f-h);
	var dist = R * Math.acos(i)
	return dist;
}
/**
 * Recuperation de l'url d'un parametre
 * 
 * @param name :
 *            url a chercher
 * @returns url du parametre
 */
function get_url_param( name ){
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( window.location.href );

	if ( results == null )
		return "";
	else
		return results[1];
}
/**
 * Arrondi d'un nombre
 * 
 * @param number :
 *            nombre a arrondir
 * @param pas :
 *            pas de l'arrondi
 * @returns arrondi
 */
function rounder(number,pas){
	return Math.round(number * pas) / pas;
}
/**
 * Trouve le minimum dans un vecteur
 * 
 * @param tab :
 *            tableau source
 * @returns minimum
 */
function rechercheMin(tab){
	var min= tab[1];
	for(i=2;i<tab.length;i++){
		if(min>tab[i]){
			min = tab [i];
		}
	}
	return min;
}
/**
 * Trouve le maximum dans un vecteur
 * 
 * @param tab :
 *            tableau source
 * @returns maximum
 */
function rechercheMax(tab){
	var max=tab[1];
	for(var i=2;i<tab.length;i++){
		if(max<tab[i]){
			max = tab [i];
		}
	}
	return max;
}
/**
 * inverse les lignes et les colonnes d'une matrice
 * 
 * @param tab :
 *            tableau source
 * @returns matrice inversee
 */
function reversetab(tab,x,y){
	var temp = new Array();
	for(var i=0;i<y;i++){
		temp [i] = new Array();
		for(var j=0;j<x;j++){
			temp[i][j]=tab[j][i];
		}
	}
	return temp;
}
/**
 * Regression polynomiale
 * 
 * @param x :
 *            Premiere serie de valeurs
 * @param y :
 *            Deuxieme serie de valeurs
 * @param order :
 *            ordre
 * @returns coefficients de la courbe
 */
function polynomialRegression(x,y,order){
	var xMatrix = [];
	var xTemp = [];
	var yMatrix = [];

	for(i=1;i<y.length;i++){
		yMatrix.push(y[i]);
	}
	var yMatrixT = numeric.transpose(yMatrix);
	for (j=1;j<x.length;j++)
	{
	    xTemp = [];
	    for(i=0;i<=order;i++)
	    {
	        xTemp.push(1*Math.pow(x[j],i));
	    }
	    xMatrix.push(xTemp);
	}
	
	var xMatrixT = numeric.transpose(xMatrix);
	var dot1 = numeric.dot(xMatrixT,xMatrix);
	var dotInv = numeric.inv(dot1);
	var dot2 = numeric.dot(xMatrixT,yMatrix);
	var solution = numeric.dot(dotInv,dot2);
	// Coefficients
	// a +
	// bx^1
	// +
	// cx^2...
	return solution;
	
}

/**
 * Renvoie l'indice du premier element du tableau qui est egal a l'element d'entree.
 * 
 * @param tab :
 *            tableau contenant toutes les valeurs possibles
 * @param i :
 *            indice dans le tableau d'entree
 * @param element :
 *            element recherche dans le tableau
 * @param pas :
 *            pas qui separe les valeurs dans le tableau
 * @returns indice de l'element dans le tableau, -1 si element non present
 */
function findIndex(tab,element,pas) {
	var i=-1;
	while (i<tab.length){
		i++;	
		if ( tab[i] == pas*Math.round(element/pas)){
			return i;
		}
	}
	return -1;
	}
/**
 * Renvoie l'indice du premier element du tableau qui est proche de la valeur de l'element en entree.
 * 
 * @param tab :
 *            tableau contenant toutes les valeurs possibles
 * @param i :
 *            indice dans le tableau d'entree
 * @param element :
 *            element recherche dans le tableau
 * @param pas :
 *            pas qui separe les valeurs dans le tableau
 * @returns indice de l'element dans le tableau, -1 si element non present
 */
function findIndexReduction(tab,element) {
	var i=0;
	while (i<tab.length){
		i++;	
		if (element<tab[i]*1.05 && element > tab[i]*0.95){
			return i;
		}
	}
	return -1;
	}
/**
 * Renvoie la copie d'un tableau.
 * 
 * @param tab :
 *            tableau à copie
 * @param copietab :
 *            copie du tableau
 * @returns
 */
function copieTableau(tab,copietab){
	for(i=0;i<longueurvecNmot;i++)
		{
			copietab[i] = new Array();
			for(j=0;j<longueurvecCmot;j++){
				copietab[i][j]=tab[i][j];
			}
		}
	return;
}
/**
 * Compare deux tableaux
 * 
 * @param tabA :
 *            1er tableau
 * @param tabB :
 *            2e tableau
 * @returns booleen vrai si les deux tableaux sont egaux, faux sinon
 */
function tabIsEqual(tabA,tabB){
	for(i=0;i<longueurvecNmot;i++){
		for(j=0;j<longueurvecCmot;j++){
			if(tabA[i][j]!=tabB[i][j]){
				return false;
			}
		}
	}
	return true;
}
/**
 * Recherche si la valeur existe dejà dans le tableau.
 * 
 * @param tab :
 *            tableau de donnees
 * @param val :
 *            valeur à tester
 * @param j :
 *            nombre de lignes du tableau
 * @returns booleen vrai si la valeur existe, faux sinon
 */
function valExiste(tab,val,j){
	i=1;
	while(i<=j){
		if(val>tab[i][0]*1.04||val<tab[i][0]*0.96){
			i++;
		}
		else{
			return true;
		}	
	}
	return false;
}
/**
 * Renvoie l'indice du premier element du tableau qui satisfait une condition donnee par une fonction.
 * 
 * @param tab :
 *            tableau contenant toutes les valeurs possibles
 * @param val :
 *            element recherche dans le tableau
 * @returns indice de l'element dans le tableau
 */
function RechercheIndexLigne(tab,val,j){
		var i=1;
		while (i<=j){
			if ( val<tab[i][0]*1.04&&val>tab[i][0]*0.96){
				return i;
			}
			i++;
		}
		return 0;
}

/**
 * Trouve le maximum dans un tableau
 * 
 * @param tab :
 *            tableau source
 * @returns maximum
 */
function rechercheMaxLigne(tab,j){
	var indice = 1;
	var max=tab[1][1];
	for(var i=2;i<=j;i++){
		if(max<tab[i][1]){
			max = tab [i][1];
			indice = i;
			
		}
	}
	return indice;
}

/**
 * Tri les valeurs du tableau par ordre decroissant
 * 
 * @param tab :
 *            tableau source
 */
function tri_bulles(tab)
{
    var tab_en_ordre = false;
    var taille = tab.length;
    var temp;
    while(!tab_en_ordre)
    {
        tab_en_ordre = true;
        for(i=1 ; i < taille ; i++)
        {
            if(tab[i] < tab[i+1])
            {
            	temp = tab[i];
            	tab[i]=tab[i+1];
            	tab[i+1]=temp;
                tab_en_ordre = false;
            }
        }
        taille--;
    }
}
// ----------------------------------------------------------------------------------
// Traitement
// des
// donnees
// OBD
// -------------------------------------------------------------------------
/**
 * Fonction globale du traitement des donnees OBD VT
 */
/*function traitementOBDVT(){

	initialisationParam();	
	calcMapVitesses();
	affichage_VitessesChartVT();
	calcCse();
	AffMapCse();
    boolOBD = true;
    return;
}*/
/**
 * Fonction globale du traitement des donnees OBD VH
 */
function traitementOBDVH(){

	initialisationParam();	
	calcMapVitesses();
	affichage_VitessesChartVH();
	calcCse();
	AffMapCse();

    boolOBD = true;
    return;
}
// ----------------------------------------------------------------------------------
// Calcul
// de la
// map
// Vitesses
// -------------------------------------------------------------------------
/**
 * Remplissage de la mapVitesses donnant le rapport de reduction en fonction de la pente et de la vitesse
 */
function calcMapVitesses(){
	
	recuperationRapportsVitesses();
	
	// calcul
	// de
	// la
	// pente
	var diffaltitude = 0;
	var difftemps = 0;
	var angle = 0;
	var distancehypothenuse = 0;
	dataObd[7][1]=0;
	for(var i=2;i<dataObd[0].length;i++)	
	{
		if(dataObd[3][i]>0){
			diffaltitude = dataObd[5][i]-dataObd[5][i-1];
			difftemps = dataObd[0][i]-dataObd[0][i-1];
			if(difftemps<0){
				distancehypothenuse = (dataObd[3][i]/3.6)*(dataObd[0][i]-dataObd[0][i-1]);
			}
			else{
				distancehypothenuse = (dataObd[3][i]/3.6)*(dataObd[0][i]+60-dataObd[0][i-1]);
			}
			angle = Math.asin(diffaltitude/distancehypothenuse);
			dataObd[7][i]=Math.round(100*diffaltitude/(distancehypothenuse*Math.cos(angle)));	
		}
		else{
			dataObd[7][i]=dataObd[7][i-1];
		}
	}
	
	// choix
	// des
	// dimensions
	// de
	// la
	// MapVitesses
	minvecVitesses = 0;
	var maxdonneesvecVitesses = pasVitesses*Math.round(rechercheMax(dataObd[3])/pasVitesses);
	if(maxdonneesvecVitesses<130){
		maxvecVitesses = 130;
	}
	else{
		maxvecVitesses= maxdonneesvecVitesses
	}
	longueurvecVitesses = (maxvecVitesses-minvecVitesses)/pasVitesses+1;
	for(i=0;i<longueurvecVitesses;i++){
		vecVitesses[i] = minvecVitesses+i*pasVitesses;	
	}
	
	
	var mindonneesvecPente = pasPente*Math.round(rechercheMin(dataObd[7])/pasPente);
	if (mindonneesvecPente>-15){
		minvecPente = -15;
	}
	else{
		minvecPente = mindonneesvecPente;
	}
	var maxdonneesvecPente = pasPente*Math.round(rechercheMax(dataObd[7])/pasPente);
	if (maxdonneesvecPente<15){
		maxvecPente = 15;
	}
	else{
		maxvecPente = maxdonneesvecPente;
	}
	longueurvecPente = (maxvecPente-minvecPente)/pasPente+1;

	for(i=0;i<longueurvecPente;i++){
		vecPente[i] = minvecPente + i*pasPente;
	}

	for(i=0;i<longueurvecVitesses;i++){
		MapVitessesStat[i] = new Array();
		for(j=0;j<longueurvecPente;j++){
			MapVitessesStat[i][j] = new Array();
			for(k=0;k<dataVitesses.length;k++){
				MapVitessesStat[i][j][k] = 0;
			}
		}
	}
	// Remplissage
	// de
	// la
	// mapStatVitesses
	var a;
	var b;
	var c;
	var rapportred;
	for (i=1;i<dataObd[3].length;i++){
		if(dataObd[3][i]>0){
			a = findIndex(vecVitesses,dataObd[3][i],pasVitesses);
			b = findIndex(vecPente,dataObd[7][i],1);
			rapportred = dataObd[2][i]*2*Math.PI*RRoue*3.6/(dataObd[3][i]*60);
			c = findIndexReduction(dataVitesses,rapportred);
			if(c!=-1){
				MapVitessesStat[a][b][c] ++;
			}			
		}		
	}

	// Remplissage
	// de
	// la
	// mapVitesses
	// : la
	// valeur
	// de
	// rapport
	// appelee
	// le
	// plus
	// est
	// retenue
	for(i=0;i<longueurvecVitesses;i++){
		MapVitesses[i] = new Array();	
		for(j=0;j<longueurvecPente;j++){
			MapVitesses[i][j] = findIndex(MapVitessesStat[i][j],rechercheMax(MapVitessesStat[i][j]),1);
		}
	}
	
	// on
	// remplit
	// les
	// trous
	// de
	// la
	// map
	// Vitesses
	var k;
	var l;
	var minVitesses;
	var maxVitesses;
	var minPente;
	var maxPente;	
	var rayon;
	var bool;
	var tabStat = new Array();	
	for(i=0;i<longueurvecVitesses;i++){
		for(j=0;j<longueurvecPente;j++){
			if(MapVitesses[i][j] == 0){
				rayon = 0;
				bool = true;
				for(k=0;k<nbRapportsVitesse+1;k++){
					tabStat[k]=0;
				}
				
				while(bool&&((i+rayon)<longueurvecVitesses||(j+rayon)<longueurvecPente)){
					rayon++;
					minVitesses = i;
					if(i+rayon<longueurvecVitesses){
						maxVitesses=i+rayon;
					}
					else{
						maxVitesses=longueurvecVitesses-1;
					}
					
					minPente = j;
					if(j+rayon<longueurvecPente){
						maxPente=j+rayon;
					}
					else{
						maxPente=longueurvecPente-1;
					}
					for(k=minVitesses;k<=maxVitesses;k++){
						for(l=minPente;l<=maxPente;l++){
							if((k+l-i-j)==rayon){
								if(MapVitesses[k][l]!=0){
									tabStat[MapVitesses[k][l]]+=1;
									bool=false;
								}
							}
						}
					}	
				}
				rayon = 0;
				while(bool&&((i+rayon)<longueurvecVitesses||(j-rayon)>0)){
					rayon++;
					minVitesses = i;
					if(i+rayon<longueurvecVitesses){
						maxVitesses=i+rayon;
					}
					else{
						maxVitesses=longueurvecVitesses-1;
					}
					if(j-rayon<0){
						minPente=0;
					}
					else{
						minPente=j-rayon;
					}
					maxPente = j;
					for(k=minVitesses;k<=maxVitesses;k++){
						for(l=maxPente;l>=minPente;l--){
							if((k-l-i+j)==rayon){
								if(MapVitesses[k][l]!=0){
									tabStat[MapVitesses[k][l]]+=1;
									bool=false;
								}
							}
						}
					}
				}	
				rayon = 0;
				while(bool&&((i-rayon)>1||(j-rayon)>0)){
					rayon++;
					if(i-rayon>1){
						minVitesses=i-rayon;
					}
					else{
						minVitesses=1;
					}
					maxVitesses = i;
					if(j-rayon<0){
						minPente=0;
					}
					else{
						minPente=j-rayon;
					}
					maxPente = j;
					for(k=maxVitesses;k>=minVitesses;k--){
						for(l=maxPente;l>=minPente;l--){
							if((-k-l+i+j)==rayon){
								if(MapVitesses[k][l]!=0){
									tabStat[MapVitesses[k][l]]+=1;
									bool=false;
								}
							}
						}
					}
				}
				MapVitesses[i][j]=findIndex(tabStat,rechercheMax(tabStat),1);
			} 
		}
	}
}

/**
 * Calcul des rapports de reduction à partir des donnees OBD Resultats dans le tableau dataVitesses
 */
function recuperationRapportsVitesses(){
	
	var mapStatRapports = new Array();
	var a;
	var j=0;
	var val;
	var i; 
	// Remplissage
	// de
	// la
	// mapStatRapports
	for(i = 1;i<dataObd[0].length;i++){
		if(dataObd[3][i]>10){	
			// calcul
			// du
			// rapport
			// de
			// reduction
			// à
			// chaque
			// point
			val = dataObd[2][i]/(dataObd[3][i]/3.6)*RRoue/60*2*Math.PI
			if(valExiste(mapStatRapports,val,j)){
				a = RechercheIndexLigne(mapStatRapports,val,j)				
				mapStatRapports[a][1] += 1;	
				mapStatRapports[a][0] = ((mapStatRapports[a][1]-1)*mapStatRapports[a][0]+val)/mapStatRapports[a][1];				
			}
			else{
				j++;
				mapStatRapports[j] = new Array();
				mapStatRapports[j][0] = val;
				mapStatRapports[j][1] = 1;
			}
		}
	}
	
	
	// Remplissage
	// du
	// tableau
	// des
	// rapports
	// de
	// reduction
	// :
	// les
	// x
	// valeurs
	// de
	// rapport
	// appelees
	// le
	// plus
	// sont
	// retenues
	dataVitesses[0] = "Valeurs des rapports de reduction";
	for(i=1;i<nbRapportsVitesse+1;i++){
		a = rechercheMaxLigne(mapStatRapports,j)
		dataVitesses[i] = Math.round(mapStatRapports[a][0]*10)/10;
		mapStatRapports[a][1] = 0;
	}
	tri_bulles(dataVitesses);
}

/*
 * function calcCourbeCouple(){ var dataObd = new Array(); var nbParamObd = 28; creaTableauParam(dataObd,dataObd[0].length,"Obd"); dataObd = reversetab(dataObd,dataObd[0].length,nbParamObd); for(i=0;i<nbParamObd+2;i++) {
 * 
 * for(j=1;j<dataObd[0].length;j++){ dataObd[i][j]=dataObd[i][j]; } } for (var i=nbParamObd+2;i<nbParamObd; i++){ dataObd[i][1] = 0; // Toutes les variables sont initialisees a zero car il y a des calculs avec i-1 } for(i=11;i<dataObd[0].length;i=i+10){ if(dataObd[3][i]>10 && dataObd[1][i]>0){ dataObd[10][i] = dataObd[0][i]-dataObd[0][i-10]; dataObd[9][i] =
 * dataObd[10][i]/3.6*0.5*(dataObd[3][i]+dataObd[3][i-10]); dataObd[11][i] = (dataObd[3][i]-dataObd[3][i-10])/3.6/dataObd[10][i]; // Calcul des forces en debut de phase var tempFDebut = calcForceObd(dataObd[3][i-10]/3.6,dataObd[11][i],dataObd[7][i]); dataObd[12][i] = tempFDebut.Fr; dataObd[13][i] = tempFDebut.Fair; dataObd[14][i] = tempFDebut.Fp; dataObd[15][i] = tempFDebut.Facc; dataObd[16][i] =
 * tempFDebut.Fm; dataObd[17][i] = dataObd[16][i]*RRoue; dataObd[18][i] = dataObd[2][i-10]*2*Math.PI*RRoue*3.6/(dataObd[3][i-10]*60); dataObd[19][i] = dataObd[17][i]/dataObd[18][i]/rend_gear; //alert("Debut : Fm : "+dataObd[16][i]+" Couple roue : "+dataObd[17][i]+" red : "+dataObd[18][i]+" couplemoteur : "+ dataObd[19][i]) // Calcul des forces en fin de phase var tempFFin =
 * calcForceObd(dataObd[3][i]/3.6,dataObd[11][i],dataObd[7][i]); dataObd[20][i] = tempFDebut.Fr; dataObd[21][i] = tempFDebut.Fair; dataObd[22][i] = tempFDebut.Fp; dataObd[23][i] = tempFDebut.Facc; dataObd[24][i] = tempFDebut.Fm; dataObd[25][i] = dataObd[16][i]*RRoue; dataObd[26][i] = dataObd[2][i]*2*Math.PI*RRoue*3.6/(dataObd[3][i]*60); dataObd[27][i] = dataObd[25][i]/dataObd[26][i]/rend_gear;
 * //alert("Fin : Fm : "+dataObd[24][i]+" Couple roue : "+dataObd[25][i]+" red : "+dataObd[26][i]+" couplemoteur : "+ dataObd[27][i])
 * 
 * 
 * dataObd[6][i] = 0.5*(dataObd[19][i]+dataObd[27][i]); if(dataObd[6][i]>0 && valExiste(dataVitesses,dataObd[18][i],nbRapportsVitesse+1) && valExiste(dataVitesses,dataObd[26][i],nbRapportsVitesse+1) && dataObd[11][i]>0){ dataObd[8][i] = dataObd[6][i]/dataObd[1][i]*100; //alert(i+" "+dataObd[6][i]+" "+dataObd[8][i]+" "+dataObd[1][i]+" "+dataObd[2][i]+" "+dataObd[3][i]+" "+dataObd[10][i]+"
 * "+dataObd[11][i]+ // " "+dataObd[17][i]) } } } alert(dataObd[8]) alert(dataObd[2]) affichage_CoupleChart(dataObd); var coef2=new Array(); coef2=polynomialRegression(dataObd[2],dataObd[8],2); alert(coef2);
 * 
 * alert("ah") }
 */
// ----------------------------------------------------------------------------------
// Calcul
// de la
// map cse
// -------------------------------------------------------------------------


/**
 * Calcul de la MapCse à partir des donnees obd interpolation et extrapolation lineaire
 */
function calcCse(){
	
	// calcul
	// du
	// Couple
	// moteur
	coef = polynomialRegression(dataCouple[0],dataCouple[1],3);
	
	for(var i=1;i<dataObd[0].length;i++){
			dataObd[6][i] = dataObd[1][i]*(coef[3]*Math.pow(dataObd[2][i],3)+coef[2]*Math.pow(dataObd[2][i],2)+coef[1]*dataObd[2][i]+coef[0])/100;
	}
	// choix
	// des
	// dimensions
	// de
	// la
	// map
	// cse
	minvecNmot = pasNmot*Math.round(rechercheMin(dataObd[2])/pasNmot);
	maxvecNmot = pasNmot*Math.round(rechercheMax(dataObd[2])/pasNmot);;
	longueurvecNmot = (maxvecNmot-minvecNmot)/pasNmot+1;
	for(i=0;i<longueurvecNmot;i++){
		vecNmot[i] = minvecNmot+i*pasNmot;	
	}
	minvecCmot = 0;
	maxvecCmot=pasCmot*Math.round(100/pasCmot);
	longueurvecCmot = (maxvecCmot-minvecCmot)/pasCmot+1;
	for(i=0;i<longueurvecCmot;i++){
		vecCmot[i] = minvecCmot+i*pasCmot;	
	}

	for(i=0;i<longueurvecNmot;i++){
		matNb[i] = new Array();
		Map_cse_init[i] = new Array();
		Map_cse[i]= new Array();	
		for(j=0;j<longueurvecCmot;j++){
			matNb[i][j] = 0;
			Map_cse_init[i][j] = 0;
			Map_cse[i][j] = 0;
		}
	}
	
	// Partie
	// cartographie
	// Remplissage
	// de
	// la
	// map
	// cse
	// (moyenne)

	var a;
	var b;
	for (i=1;i<dataObd[1].length;i++){
		if (dataObd[2][i]>=0 && dataObd[1][i]>0 && dataObd[6][i]>0){
			a = findIndex(vecNmot,dataObd[2][i],pasNmot);
			b = findIndex(vecCmot,dataObd[1][i],pasCmot);
			matNb[a][b] = matNb[a][b]+1;
			Map_cse_init[a][b]=((dataObd[4][i]*3600/AirFuelRatio)/(dataObd[6][i]*dataObd[2][i]*2*Math.PI/60/1000) + Map_cse_init[a][b]*(matNb[a][b]-1))/matNb[a][b];
			// pour
			// moteur
			// essence
			// uniquement
			// (on
			// ne
			// peut
			// pas
			// calculer
			// la
			// consommation
			// de
			// gasoil
			// en
			// fonction
			// de
			// la
			// quantite
			// d'air
			// injectee
			// car
			// un
			// moteur
			// diesel
			// fonctionne
			// en
			// excedant
			// d'air.
			// De
			// ce
			// fait,
			// le
			// ratio
			// air/gasoil
			// varie
			// en
			// fonction
			// des
			// modeles
			// de
			// vehicule
			// et
			// n'est
			// pas
			// forcement
			// constant
			// selon
			// les
			// regimes
			// et
			// la
			// charge
			// du
			// moteur)
		}
	}

	// Remplissage
	// des
	// trous
	// par
	// interpolation
	// /
	// extrapolation

	// Partie
	// interpolation

	// Determination
	// de
	// la
	// taille
	// maximale
	// des
	// trous
	// de
	// la
	// matrice
	// de
	// la
	// matrice
	// (dans
	// les
	// deux
	// directions
	// et
	// dans
	// l'intervalle
	// de
	// definition)
	var k;
	var l;
	for (i=1;i<vecNmot.length;i++){
	    for (j=1;j<vecCmot.length;j++){
	        if (Map_cse_init[i][j]==0){
	            k=j;
	            l=j;
	            while (Map_cse_init[i][l]==0 && l<vecCmot.length-1){
	                l++;
	            }
	            if(Map_cse_init[i][l]==0)
	            {
	            	l++;
	            }
	            while (Map_cse_init[i][k]==0 && k>1){
	                k--;
	            }
	            if (l-k>=taille_trou_max && k>1 && l<vecCmot.length){
	            	taille_trou_max = l-k-1;
	            }
	        }
	    }
	}

	for (i=1;i<vecNmot.length;i++){
	    for (j=1;j<vecCmot.length;j++){
	        if (Map_cse_init[i][j]==0){
	            k=i;
	            l=i;
	            while (Map_cse_init[l][j]==0 && l<vecNmot.lenght-1){
	                l++;
	            }
	            if(Map_cse_init[l][j]==0)
	            {
	            	l++;
	            }
	            while (Map_cse_init[k][j]==0 && k>1){
	                k--;
	            }
	            if (l-k>=taille_trou_max && k>1 && l<vecNmot.length){
	            	taille_trou_max = l-k-1;
	            }
	        }
	    }
	}	

	// Remplissage
	// des
	// trous
	// de
	// la
	// map
	// cse
	// par
	// interpolations
	// lineaires

	// On
	// remplit
	// en
	// priorite
	// les
	// trous
	// de
	// petite
	// dimension,
	// suivant
	// i et
	// suivant
	// j
	copieTableau(Map_cse_init,Map_cse);
	taille_trou=1;
	while (taille_trou_max > 0){
	    copieTableau(Map_cse,Map_cse_t);
	    var indice=1;
	    while (indice > 0){
	        if (indice==1){
	            for (i=1;i<longueurvecNmot;i++){
	                for (j=1;j<longueurvecCmot;j++){
	                    if (Map_cse[i][j]==0 ){
	                        k=i;
	                        l=i;
	                        while (k>1 && Map_cse[k][j]==0){
	                            k=k-1;
	                        }
	                        while ( l<longueurvecNmot-1 && Map_cse[l][j]==0){	              
	                            l=l+1;
	                        }
	                        if(Map_cse[l][j]==0)
	        	            {
	        	            	l++;
	        	            }
	                        if(l<longueurvecNmot){
	                        	penteCse = (Map_cse[l][j]-Map_cse[k][j])/(l-k);
	                        	if (l-k-1 == taille_trou ){
	                        		for (a=i;a<l;a++){
	                        			if (a>1 && Map_cse[k][j]>0 && Map_cse[l][j]>0){
	                        				Map_cse[a][j]=Map_cse[a-1][j]+penteCse;
	                        			}
	                        		}
	                        	}
	                        }
	                        
	                    }
	                }
	            }
	            indice=2;
	        }
	        if (indice==2){
	            copieTableau(Map_cse,Map_cse_i);
	            for (i=1;i<vecNmot.length;i++){
	                for (j=1;j<vecCmot.length;j++){
	                    if (Map_cse[i][j]==0){
	                        k=j;
	                        l=j;
	                        while (Map_cse[i][k]==0 && k>1){
	                            k=k-1;
	                        }
	                        while (Map_cse[i][l]==0 && l<vecCmot.length-1){
	                            l=l+1;
	                        }
	                        if(Map_cse[i][l]==0)
	        	            {
	        	            	l++;
	        	            }
	                        if(l<longueurvecCmot){
	                        	penteCse = (Map_cse[i][l]-Map_cse[i][k])/(l-k);
	                        	if (l-k-1 == taille_trou){
	                        		for (a=j;a<l;a++){
	                        			if (a>1 && Map_cse[i][k]>0 && Map_cse[i][l]>0){
	                        				Map_cse[i][a]=Map_cse[i][a-1]+penteCse;
	                        			}
	                        		}
	                        	}
	                        }
	                        
	                    }
	                }
	            }
	            if (tabIsEqual(Map_cse,Map_cse_i)){
	                indice=0;
	                if (taille_trou==taille_trou_max){
	                    taille_trou_max=taille_trou_max-1;
	                }
	                if (taille_trou >= 1 && tabIsEqual(Map_cse,Map_cse_t) && taille_trou < taille_trou_max){
	                    taille_trou=taille_trou+1;
	                }
	                else{
	                    taille_trou=1;
	                }
	            }
	            else{
	                indice=1;
	            }
	        }
	        
	    }	
	}

	// Partie
	// extrapolation

	// Determination
	// de
	// la
	// taille
	// maximale
	// des
	// trous
	// de
	// la
	// matrice
	// de
	// la
	// matrice
	// (dans
	// les
	// deux
	// directions
	// et
	// hors
	// de
	// l'intervalle
	// de
	// definition)

    for (i=1;i<longueurvecNmot;i++){
        for (j=1;j<longueurvecCmot;j++){
	        if (Map_cse_init[i][j]==0){
	            k=j;
	            l=j;
	            while (Map_cse_init[i][l]==0 && l<longueurvecCmot-1){
	                l=l+1;
	            }
	            if(Map_cse_init[i][l]==0){
	            	l++;
	            }
	            while (Map_cse_init[i][k]==0 && k>1){
	                k=k-1;
	            }
	            if (l-k-1>=taille_trou_max_ex){
	                taille_trou_max_ex = l-k-1;
	            }
	        }
        }
    }
    for (i=1;i<longueurvecNmot;i++){
        for (j=1;j<longueurvecCmot;j++){
        	if (Map_cse_init[i][j]==0){
	            k=i;
	            l=i;
	            while (Map_cse_init[l][j]==0 && l<longueurvecNmot-1){
	                l=l+1;
	            }
	            if(Map_cse_init[l][j]==0){
	            	l++;
	            }
	            while (Map_cse_init[k][j]==0 && k>1){
	                k=k-1;
	            }
	            if (l-k-1>=taille_trou_max_ex){
	                taille_trou_max_ex = l-k-1;
	            }
        	}
        }
    }

    // Remplissage
	// des
	// trous
	// par
	// extrapolation
	// lineaire

    copieTableau(Map_cse,Map_cse_ex);
    taille_trou_ex=1;
    while (taille_trou_max_ex > 0){
    	Map_cse_t=Map_cse_ex ;
    	indice=1;
    	while (indice > 0){
    		if (indice==1 ){
    			for (i=1;i<longueurvecNmot;i++){
    				for (j=1;j<longueurvecCmot;j++){
    					if (Map_cse_ex[i][j]==0){
    						k=i;
    						l=i;
    						while (Map_cse_ex[k][j]==0 && k>0){
    							k=k-1;
    						}
    						while (Map_cse_ex[l][j]==0 && l<longueurvecNmot-1){
    							l=l+1;
    						}
    						if(Map_cse_ex[l][j]==0){
    			            	l++;
    			            }
    						if (k==0 && l < longueurvecNmot-1){
    							penteCse = (Map_cse_ex[l][j]-Map_cse_ex[l+1][j])
    							if (l-k-1 == taille_trou_ex){
    								b=taille_trou_ex;
    								for (a=i;a<l;a++){
    									if(Map_cse_ex[l][j]+penteCse*b>0){
    										Map_cse_ex[a][j]=Map_cse_ex[l][j]+penteCse*b;
    									}
    									b=b-1;
    								}
    							}
    						}
    						if (k > 1 && l == longueurvecNmot){
    							penteCse = (Map_cse_ex[k][j]-Map_cse_ex[k-1][j])
    							if (l-k-1 == taille_trou_ex ){
    								for (a=i;a<l;a++){
    									if(Map_cse_ex[a-1][j]+penteCse>0){
    										Map_cse_ex[a][j]=Map_cse_ex[a-1][j]+penteCse;
    									}
    								}
    							}
    						}
    					}
    				}
        	   }
    			indice=2;
    		}
    		if (indice==2){
    			copieTableau(Map_cse_ex,Map_cse_i);
    			for (i=1;i<longueurvecNmot;i++){
    				for (j=1;j<longueurvecCmot;j++){
    					if (Map_cse_ex[i][j]==0){
                        	k=j;
                        	l=j;
                        	while (Map_cse_ex[i][k]==0 && k>0){
                            	k=k-1;
                        	}
                        	while (Map_cse_ex[i][l]==0 && l<longueurvecCmot-1){
                            	l=l+1;
                        	}
                        	if(Map_cse_ex[i][l]==0){
            	            	l++;
            	            }
                        	if (k==0 && l <longueurvecCmot-1){
                            	penteCse = (Map_cse_ex[i][l]-Map_cse_ex[i][l+1]);
                            	if (l-k-1 == taille_trou_ex){
                                	b=taille_trou_ex;
                                	for (a=j;a<l;a++){
                                		if(Map_cse_ex[i][l]+penteCse*b>0){
                                			Map_cse_ex[i][a]=Map_cse_ex[i][l]+penteCse*b;
                                		}                                    	
                                    	b=b-1;
                                	}
                            	}
                        	}
                        	if (k > 1 && l == longueurvecCmot){
                            	penteCse = (Map_cse_ex[i][k]-Map_cse_ex[i][k-1]);
                            	if (l-k-1 == taille_trou_ex){
                            		for (a=j;a<l;a++){
                            			if(Map_cse_ex[i][a-1]+penteCse>0){
                            				Map_cse_ex[i][a]=Map_cse_ex[i][a-1]+penteCse;
                            			}
                            		}
                            	}
                        	}
    					}
    				}
    			}
            	if (tabIsEqual(Map_cse_ex,Map_cse_i)){
                	indice=0;
                	if (taille_trou_ex==taille_trou_max_ex){
                    	taille_trou_max_ex=taille_trou_max_ex-1;
                	}
                	if (taille_trou_ex >= 1 && tabIsEqual(Map_cse_ex,Map_cse_t) && taille_trou_ex < taille_trou_max_ex){
                    	taille_trou_ex=taille_trou_ex+1;
                	}
                	else{
                    	taille_trou_ex=1;
                	}
            	}
            	else{
                	indice=1;
            	}
            	
    		}
    	}
	}
}
// ----------------------------------------------------------------------------------
// Conseils
// sur la
// conduite
// -------------------------------------------------------
function AnalyseTrajetVE(){
	
	var acceleration_faible=0;
	var acceleration_excessive=0;
	var freinage_faible=0;
	var freinage_excessif=0;
	var vitesse_constante=0
	var temps_total=0
	var distance_acceleration_faible=0;
	var distance_acceleration_excessive=0;
	var distance_freinage_faible=0;
	var distance_freinage_excessif=0;
	var distance_vitesse_constante=0;
	var distance_totale = 0;
	
	dataConseilsVE[3][1] = dataConseilsVE[1][1] /3.6
	for (var i = 2;i<dataConseilsVE[0].length;i++){
		dataConseilsVE[3][i] = dataConseilsVE[1][i] /3.6;
		deltaT = dataConseilsVE[0][i]-dataConseilsVE[0][i-1];
		distance = dataConseilsVE[3][i]*deltaT;
		dataConseilsVE[4][i] = (dataConseilsVE[3][i]-dataConseilsVE[3][i-1])/deltaT;
		temps_total+=deltaT;
		distance_totale+=distance;
		if (dataConseilsVE[4][i]>1){
			acceleration_excessive+=deltaT;
			distance_acceleration_excessive+=distance;
		}
		else{
			if (dataConseilsVE[4][i]>0.3){
				acceleration_faible+=deltaT;
				distance_acceleration_faible+=distance;
			}
			else{
				if (dataConseilsVE[4][i]<-1){
					freinage_excessif+=deltaT;
					distance_freinage_excessif+=distance;
				}
				else{
					if (dataConseilsVE[4][i]<-0.3){
						freinage_faible+=deltaT;
						distance_freinage_faible+=distance;
					}
					else{
						vitesse_constante+=deltaT;
						distance_vitesse_constante+=distance;
					}
				}	
			}	
		}	
	}
	var pourcentage_acceleration_faible=Math.round(acceleration_faible/temps_total*100)/100;
	var pourcentage_acceleration_excessive=Math.round(acceleration_excessive/temps_total*100)/100;
	var pourcentage_freinage_faible=Math.round(freinage_faible/temps_total*100)/100;
	var pourcentage_freinage_excessif=Math.round(freinage_excessif/temps_total*100)/100;
	var pourcentage_vitesse_constante=Math.round(vitesse_constante/temps_total*100)/100;
	var pourcentage_distance_acceleration_faible=Math.round(distance_acceleration_faible/distance_totale*100)/100;
	var pourcentage_distance_acceleration_excessive=Math.round(distance_acceleration_excessive/distance_totale*100)/100;
	var pourcentage_distance_freinage_faible=Math.round(distance_freinage_faible/distance_totale*100)/100;
	var pourcentage_distance_freinage_excessif=Math.round(distance_freinage_excessif/distance_totale*100)/100;
	var pourcentage_distance_vitesse_constante=Math.round(distance_vitesse_constante/distance_totale*100)/100;
	affichage_ConseilsVE(pourcentage_acceleration_faible,pourcentage_acceleration_excessive,pourcentage_freinage_faible,pourcentage_freinage_excessif,
			pourcentage_vitesse_constante,pourcentage_distance_acceleration_faible,pourcentage_distance_acceleration_excessive,
			pourcentage_distance_freinage_faible,pourcentage_distance_freinage_excessif,pourcentage_distance_vitesse_constante);
}

function AnalyseTrajetVTBC(){
	
	var acceleration_faible=0;
	var acceleration_excessive=0;
	var freinage_faible=0;
	var freinage_excessif=0;
	var vitesse_constante=0
	var temps_total=0
	var distance_acceleration_faible=0;
	var distance_acceleration_excessive=0;
	var distance_freinage_faible=0;
	var distance_freinage_excessif=0;
	var distance_vitesse_constante=0;
	var distance_totale = 0;
	
	dataReellesVTBC[3][1] = dataReellesVTBC[1][1] /3.6;
	for (var i = 2;i<dataReellesVTBC[0].length;i++){
		dataReellesVTBC[3][i] = dataReellesVTBC[1][i] /3.6;
		deltaT = dataReellesVTBC[0][i]-dataReellesVTBC[0][i-1];
		distance = dataReellesVTBC[3][i]*deltaT;
		dataReellesVTBC[4][i] = (dataReellesVTBC[3][i]-dataReellesVTBC[3][i-1])/deltaT;
		temps_total+=deltaT;
		distance_totale+=distance;
		if (dataReellesVTBC[4][i]>1){
			acceleration_excessive+=deltaT;
			distance_acceleration_excessive+=distance;
		}
		else{
			if (dataReellesVTBC[4][i]>0.3){
				acceleration_faible+=deltaT;
				distance_acceleration_faible+=distance;
			}
			else{
				if (dataReellesVTBC[4][i]<-1){
					freinage_excessif+=deltaT;
					distance_freinage_excessif+=distance;
				}
				else{
					if (dataReellesVTBC[4][i]<-0.3){
						freinage_faible+=deltaT;
						distance_freinage_faible+=distance;
					}
					else{
						vitesse_constante+=deltaT;
						distance_vitesse_constante+=distance;
					}
				}	
			}	
		}	
	}
	var pourcentage_acceleration_faible=Math.round(acceleration_faible/temps_total*100)/100;
	var pourcentage_acceleration_excessive=Math.round(acceleration_excessive/temps_total*100)/100;
	var pourcentage_freinage_faible=Math.round(freinage_faible/temps_total*100)/100;
	var pourcentage_freinage_excessif=Math.round(freinage_excessif/temps_total*100)/100;
	var pourcentage_vitesse_constante=Math.round(vitesse_constante/temps_total*100)/100;
	var pourcentage_distance_acceleration_faible=Math.round(distance_acceleration_faible/distance_totale*100)/100;
	var pourcentage_distance_acceleration_excessive=Math.round(distance_acceleration_excessive/distance_totale*100)/100;
	var pourcentage_distance_freinage_faible=Math.round(distance_freinage_faible/distance_totale*100)/100;
	var pourcentage_distance_freinage_excessif=Math.round(distance_freinage_excessif/distance_totale*100)/100;
	var pourcentage_distance_vitesse_constante=Math.round(distance_vitesse_constante/distance_totale*100)/100;
	affichage_ConseilsVT(pourcentage_acceleration_faible,pourcentage_acceleration_excessive,pourcentage_freinage_faible,pourcentage_freinage_excessif,
			pourcentage_vitesse_constante,pourcentage_distance_acceleration_faible,pourcentage_distance_acceleration_excessive,
			pourcentage_distance_freinage_faible,pourcentage_distance_freinage_excessif,pourcentage_distance_vitesse_constante);
}



/*Function analyseProfilJSON
 * 
 * Cette fonction est dérivée de la fucntion analyseTrajetVTBc ou VTC mais qui ne sont pas utilisé à l'heure acteulle pour cause de paramètre d'entré inconnus. 
 * Cette fonction permet pour n'importe quel trajet, qu'il soit obenus avec Segmentation ou profile de vitesse de determiner
 * une série de statistiques concernant la route. Elle prends les infos de dataConso.
 */
function analyseProfileJSON(){
	var tab = dataConso;					//recuperation de données
	
	var acceleration_faible=0;				//df des variables utilisées et calculées
	var acceleration_excessive=0;
	var freinage_faible=0;
	var freinage_excessif=0;
	var vitesse_constante=0
	var temps_total=0
	var distance_acceleration_faible=0;
	var distance_acceleration_excessive=0;
	var distance_freinage_faible=0;
	var distance_freinage_excessif=0;
	var distance_vitesse_constante=0;
	var distance_totale = 0;
	var vitesse_totale=0;
	var vitesse_moy=0;
	var pente_max= 0;
	var pente_min=0;
	var pente_totale=0;
	var pente_moy=0;
	var vitesse_max=0;
	var vitesseLim_totale=0;
	var vitesseLim_moy=0;
	var acceleration_totale=0;
	var acceleration_moy=0;
	var distance_Ville=0;
	var distance_Rurale=0;
	var distance_Autoroute=0;
	var vitesse_Ville=0;
	var vitesse_Rurale=0;
	var vitesse_Autoroute=0;
	var nbPointVitesse_Ville=0;
	var nbPointVitesse_Rurale=0;
	var nbPointVitesse_Autoroute=0;
	var vitesse_moy_Ville=0;
	var vitesse_moy_Rurale=0;
	var vitesse_moy_Autoroute=0;
	
	var reverseTab = reversetab(tab,tab.length,tab[0].length)
	
	vitesse_max=Math.max(...reverseTab[1].slice(2));
	pente_max= Math.max(...reverseTab[4].slice(1));
	pente_min= Math.min(...reverseTab[4].slice(1));
	
	
	
	for (var i = 2;i<tab.length;i++){				//partie pour le premier tableau : étude de l'accélération + calcul des vitesse moyenne et max
		
		deltaT = tab[i][0]-tab[i-1][0];
		distance = tab[i][2]*deltaT;
		
		temps_total+=deltaT;
		distance_totale+=distance;
		pente_totale+=tab[i][4];
		vitesse_totale+=tab[i][1];
		acceleration_totale+=tab[i][6];
		if( i != tab.length-1){
			vitesseLim_totale+=tab[i][38];
		}
		
		
		if (tab[i][6]>1.5){
			acceleration_excessive+=deltaT;
			distance_acceleration_excessive+=distance;
		}
		else{
			if (tab[i][6]>0.2){
				acceleration_faible+=deltaT;
				distance_acceleration_faible+=distance;
			}
			else{
				if (tab[i][6]<-1.5){
					freinage_excessif+=deltaT;
					distance_freinage_excessif+=distance;
				}
				else{
					if (tab[i][6]<-0.2){
						freinage_faible+=deltaT;
						distance_freinage_faible+=distance;
					}
					else{
						vitesse_constante+=deltaT;
						distance_vitesse_constante+=distance;
					}
				}	
			}	
		}	
	
		if (tab[i][1]<= 57 ){										//partie pour les second graph : étude des types de route
			distance_Ville+=distance;
			vitesse_Ville+=tab[i][1];
			nbPointVitesse_Ville+=1;
		}
		else{
			if (tab[i][1]> 57 & tab[i][1] <= 97){					//On peut +7km/h au dessus les 'limites' pour éviter un dépassement occasionnel sans changement de type de route
				distance_Rurale+=distance;
				vitesse_Rurale+=tab[i][1];
				nbPointVitesse_Rurale+=1;
			}
			else{
				if (tab[i][1] > 97 ){
					distance_Autoroute+=distance;
					vitesse_Autoroute+=tab[i][1];
					nbPointVitesse_Autoroute+=1;
				}
				
			}	
		}	
	}
	//réalisation des calculs
	vitesse_moy=Math.round(vitesse_totale/tab.length*10)/10;
	pente_moy= pente_totale/tab.length;
	vitesseLim_moy=vitesseLim_totale/tab.length;
	acceleration_moy=acceleration_totale/tab.length;
	
	vitesse_moy_Ville=Math.round(vitesse_Ville/nbPointVitesse_Ville*10)/10;
	vitesse_moy_Rurale=Math.round(vitesse_Rurale/nbPointVitesse_Rurale*10)/10;
	vitesse_moy_Autoroute=Math.round(vitesse_Autoroute/nbPointVitesse_Autoroute*10)/10;
	
	var pourcentage_acceleration_faible=Math.round(acceleration_faible/temps_total*100)/100;
	var pourcentage_acceleration_excessive=Math.round(acceleration_excessive/temps_total*100)/100;
	var pourcentage_freinage_faible=Math.round(freinage_faible/temps_total*100)/100;
	var pourcentage_freinage_excessif=Math.round(freinage_excessif/temps_total*100)/100;
	var pourcentage_vitesse_constante=Math.round(vitesse_constante/temps_total*100)/100;
	var pourcentage_distance_acceleration_faible=Math.round(distance_acceleration_faible/distance_totale*100)/100;
	var pourcentage_distance_acceleration_excessive=Math.round(distance_acceleration_excessive/distance_totale*100)/100;
	var pourcentage_distance_freinage_faible=Math.round(distance_freinage_faible/distance_totale*100)/100;
	var pourcentage_distance_freinage_excessif=Math.round(distance_freinage_excessif/distance_totale*100)/100;
	var pourcentage_distance_vitesse_constante=Math.round(distance_vitesse_constante/distance_totale*100)/100;
	var pourcentage_distcance_Ville = Math.round(distance_Ville/distance_totale*100)/100;
	var pourcentage_distcance_Rurale = Math.round(distance_Rurale/distance_totale*100)/100;
	var pourcentage_distcance_Autoroute = Math.round(distance_Autoroute/distance_totale*100)/100;
	
	affichage_Analyse(pourcentage_acceleration_faible,pourcentage_acceleration_excessive,pourcentage_freinage_faible,
			pourcentage_freinage_excessif,pourcentage_vitesse_constante,pourcentage_distance_acceleration_faible,
			pourcentage_distance_acceleration_excessive,pourcentage_distance_freinage_faible,pourcentage_distance_freinage_excessif,
			pourcentage_distance_vitesse_constante,vitesse_max,pente_max,vitesse_moy,pente_moy,pente_min,vitesseLim_moy,acceleration_moy,
			pourcentage_distcance_Ville,pourcentage_distcance_Rurale,pourcentage_distcance_Autoroute,
			vitesse_moy_Ville,vitesse_moy_Rurale,vitesse_moy_Autoroute);
	
}

function affichage_Analyse(pourcentage_acceleration_faible,pourcentage_acceleration_excessive,pourcentage_freinage_faible,
		pourcentage_freinage_excessif,pourcentage_vitesse_constante,pourcentage_distance_acceleration_faible,
		pourcentage_distance_acceleration_excessive,pourcentage_distance_freinage_faible,pourcentage_distance_freinage_excessif,
		pourcentage_distance_vitesse_constante,vitesse_max,pente_max,vitesse_moy,pente_moy,pente_min,vitesseLim_moy,acceleration_moy,
		pourcentage_distcance_Ville,pourcentage_distcance_Rurale,pourcentage_distcance_Autoroute,
		vitesse_moy_Ville,vitesse_moy_Rurale,vitesse_moy_Autoroute){
	
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string','')
	dataChart.addColumn('number','pourcentage du temps')
	dataChart.addColumn('number','pourcentage de la distance')
	dataChart.addRows([
		['freinage excessif', pourcentage_freinage_excessif, pourcentage_distance_freinage_excessif],
		['freinage modéré', pourcentage_freinage_faible, pourcentage_distance_freinage_faible],
		['vitesse quasi-constante', pourcentage_vitesse_constante, pourcentage_distance_vitesse_constante],
		['accélération modérée', pourcentage_acceleration_faible, pourcentage_distance_acceleration_faible],
		['accélération excessive',pourcentage_acceleration_excessive, pourcentage_distance_acceleration_excessive]
	])
	var options = {
			width: '95%', 
			height: '100%',
			vAxis:{format: 'percent'},
			legend:{position : 'top'}
	        };
	var ChartAnalyse = new google.visualization.ColumnChart(document.getElementById('GraphAnalyse'+typeVehicule));
	ChartAnalyse.draw(dataChart,options);
	
	var ChartAnalyse;
	ChartAnalyse = new google.visualization.DataTable();
	ChartAnalyse.addColumn('string',"Statistiques");
	ChartAnalyse.addColumn('number',"");
	ChartAnalyse.addRow(["Vitesse moyenne (km/h)",vitesse_moy])
	ChartAnalyse.addRow(["Vitesse Max (km/h)",vitesse_max])
	if(vitesseLim_moy != undefined){
		ChartAnalyse.addRow(["Vitesse Limite moyenne (km/h)",vitesseLim_moy]);
	}
	ChartAnalyse.addRow(["Acceleration moyenne (m.s-2)" ,acceleration_moy ]);
	
	ChartAnalyse.addRow(["Pente moyenne (%)" ,pente_moy ]);
	ChartAnalyse.addRow(["Pente min (%)", pente_min]);
	ChartAnalyse.addRow(["Pente max(%)",pente_max]);
	
	
	
	var TabAnalyse = new google.visualization.Table(document.getElementById('TabAnalyse'+typeVehicule));
	TabAnalyse.draw(ChartAnalyse,{
		width: '70%', 
		height: '100%'
	}); 
	
	var dataChart2;
	dataChart2 = new google.visualization.DataTable();
	dataChart2.addColumn('string','')
	dataChart2.addColumn('number','pourcentage de la distance sur les types de route')
	dataChart2.addRows([
		['Ville \n(Vitesse moye ='+parseFloat(vitesse_moy_Ville)+')', pourcentage_distcance_Ville],
		['Rurale \n(Vitesse moy ='+parseFloat(vitesse_moy_Rurale)+')', pourcentage_distcance_Rurale],
		['Autoroute \n(Vitesse moy ='+parseFloat(vitesse_moy_Autoroute)+')', pourcentage_distcance_Autoroute]
		
	])
	var options = {
			width: '65%', 
			height: '100%',
			vAxis:{format: 'percent'},
			legend:{position : 'top'}
	        };
	var ChartAnalyse2 = new google.visualization.ColumnChart(document.getElementById('GraphAnalyse2'+typeVehicule));
	ChartAnalyse2.draw(dataChart2,options);
	
	
}
/**
 * Verification pour le lancement du calcul
 */
function ResumeTrajet(){
	if(boolOBD){
		initialisationParam();
		AnalyseTrajet();
	}
	else{
		alert("Veuillez lancer le traitement des donnees OBD (onglet OBD)");
	}
}

/**
 * Verification pour le lancement du calcul Conseils VT
 */
function ResumeTrajetVTC(){
	if(boolOBD){
		initialisationParam();
		AnalyseTrajetVTC();
	}
	else{
		alert("Veuillez lancer le traitement des donnees OBD (onglet OBD)");
	}
}

/**
 * Rediscrétisation du trajet pour un intervalle de temps plus grand Calcul de la consommation sur le trajet enregistre à partir des donnees obd Affichage du bilan du trajet
 */
function AnalyseTrajet(){
	
	var deltaT;	
	var dureetampon = 1;
	var tempsfindephase;
	tempsfindephase = dureetampon;
	var tempstampon = 0;
	var compteurtampon = 0;
	var chargemoy=0;
	var regimemoy=0;
	var vitessemoy=0;
	var quantiteairmoy=0;
	var altitudemoy=0;
	var j = 1;
	var RapportLu = new Array();
	var VitesseLue = new Array();
	
	creaTableauParam(dataObdTrajet,nbParamObdTotal,"Obd");
	dataObdTrajet = reversetab(dataObdTrajet,dataObdTrajet.length,nbParamObdTotal);
	
	RapportLu[1] = dataObdLu[2][1]*2*Math.PI*RRoue*3.6/(dataObdLu[3][1]*60)
	VitesseLue[1] = findIndexReduction(dataVitesses,RapportLu[1]);	
	// on
	// discrétise
	// selon
	// un
	// pas
	// de
	// temps
	// plus
	// grand
	// (d'1
	// seconde)
	// car
	// les
	// données
	// étaient
	// trop
	// rapprochees
	// et
	// la
	// précision
	// du
	// capteur
	// de
	// vitesse
	// étant
	// trop
	// faible
	// (1km/h),
	// cela
	// amenait
	// de
	// nombreuses
	// erreurs
	// dans
	// les
	// calculs
	// qui
	// suivent
	// les
	// donnees
	// sont
	// mises
	// dans
	// le
	// tableau
	// dataObdTrajet
	// a
	// partir
	// du
	// tableau
	// dataObdLu
	for(var i = 2;i<dataObdLu[0].length;i++){
		
		deltaT = dataObdLu[0][i]-dataObdLu[0][i-1];
		// comme
		// on a
		// acces
		// qu'aux
		// secondes,
		// quand
		// on
		// change
		// de
		// minute,
		// si
		// on
		// passe
		// de
		// 59.90
		// a
		// 0.1
		// par
		// exemple
		// alors
		// on a
		// deltaT
		// =
		// 0.2
		if(deltaT<0){
			deltaT += 60;
		}
		// on
		// s'arrête
		// si
		// l'acquisition
		// n'est
		// pas
		// continue
		if(deltaT>3){
			alert("l'acquisition des donnees n'a pas été continue, le calcul ne peut pas continuer." +
					"\nVeuillez utiliser une fichier résultant d'une seule prise de données");
			return ;
		}
		tempstampon += deltaT;	
		RapportLu[i] = dataObdLu[2][i]*2*Math.PI*RRoue*3.6/(dataObdLu[3][i]*60)
		VitesseLue[i] = findIndexReduction(dataVitesses,RapportLu[i]);	
		// on
		// teste
		// les
		// rapports
		// de
		// vitesse
		// enclenchés
		// car
		// ils
		// amenent
		// des
		// erreurs
		// sur
		// les
		// moyennes
		// lorsqu'ils
		// changent
		// notament
		// sur
		// les
		// couples
		// et
		// les
		// regimes
		// moteurs
		if(VitesseLue[i]==VitesseLue[i-1]&&VitesseLue[i-1]!=-1){			
			compteurtampon ++ ;
			// moyenne
			// sur
			// la
			// phase
			// des
			// variables
			chargemoy = (dataObdLu[1][i]+chargemoy*(compteurtampon-1))/compteurtampon;
			regimemoy = (dataObdLu[2][i]+regimemoy*(compteurtampon-1))/compteurtampon;
			vitessemoy = (dataObdLu[3][i]+vitessemoy*(compteurtampon-1))/compteurtampon;
			quantiteairmoy = (dataObdLu[4][i]+quantiteairmoy*(compteurtampon-1))/compteurtampon;
			altitudemoy = (dataObdLu[5][i]+altitudemoy*(compteurtampon-1))/compteurtampon;	
			if(tempstampon>tempsfindephase){	// remplissage
												// du
												// tableau
												// avec
												// les
												// valeurs
												// moyennes,
												// on
												// commence
												// une
												// nouvelle
												// phase
				dataObdTrajet[0][j]=tempstampon;
				dataObdTrajet[1][j]=chargemoy;
				dataObdTrajet[2][j]=regimemoy;
				dataObdTrajet[3][j]=vitessemoy;
				dataObdTrajet[4][j]=quantiteairmoy;
				dataObdTrajet[5][j]=altitudemoy;
				chargemoy = dataObdLu[1][i];
				regimemoy = dataObdLu[2][i];
				vitessemoy = dataObdLu[3][i];
				quantiteairmoy = dataObdLu[4][i];
				altitudemoy = dataObdLu[5][i];
				compteurtampon=1;
				j++;
				tempsfindephase=tempstampon+dureetampon;
			}	
		}
		else{
			// si
			// changement
			// de
			// rapport
			// de
			// vitesses,
			// on
			// arrete
			// la
			// phase
			// a
			// l'indice
			// precedent,
			// on
			// remplit
			// le
			// tableau,
			// on
			// commence
			// une
			// nouvelle
			// phase
			if(i!=2 && tempstampon-deltaT!=dataObdTrajet[0][j-1]){
				dataObdTrajet[0][j]=tempstampon-deltaT;
				dataObdTrajet[1][j]=chargemoy;
				dataObdTrajet[2][j]=regimemoy;
				dataObdTrajet[3][j]=vitessemoy;
				dataObdTrajet[4][j]=quantiteairmoy;
				dataObdTrajet[5][j]=altitudemoy;
				j++;
			}
			chargemoy = dataObdLu[1][i];
			regimemoy = dataObdLu[2][i];
			vitessemoy = dataObdLu[3][i];
			quantiteairmoy = dataObdLu[4][i];
			altitudemoy = dataObdLu[5][i];
			compteurtampon=1;	
			tempsfindephase=tempstampon+dureetampon;		
		}
	}

	
	
	var diffaltitude = 0;
	var difftemps = 0;
	var angle = 0;
	var distancehypothenuse = 0;
	var Puissance;
	var Consommation;
	var RegimeMoteurPhase = new Array();
	var CoupleMoteurPhase = new Array();
	var VitessePhase = new Array();
	var RapportRed = new Array();

	
	// initialisation
	// des
	// variables
	dataObdTrajet[6][1] = dataObdTrajet[1][1]*(coef[3]*Math.pow(dataObdTrajet[2][1],3)+coef[2]*Math.pow(dataObdTrajet[2][1],2)+coef[1]*dataObdTrajet[2][1]+coef[0])/100;
	dataObdTrajet[7][1] = 0;
	dataObdTrajet[8][1] = 0;
	dataObdTrajet[10][1] = 0;
	dataObdTrajet[11][1] = 0;
	RapportRed[1] = dataObdTrajet[2][1]*2*Math.PI*RRoue*3.6/(dataObdTrajet[3][1]*60);	
	dataObdTrajet[12][1] = findIndexReduction(dataVitesses,RapportRed[1]);	
	dataObdTrajet[14][1] = 0;
	
	
	
	for(var i=2;i<dataObdTrajet[2].length;i++){
		
		
		deltaT = dataObdTrajet[0][i]-dataObdTrajet[0][i-1];
		// calcul
		// du
		// couple
		dataObdTrajet[6][i] = dataObdTrajet[1][i]*(coef[3]*Math.pow(dataObdTrajet[2][i],3)+coef[2]*Math.pow(dataObdTrajet[2][i],2)+coef[1]*dataObdTrajet[2][i]+coef[0])/100;
		// calcul
		// de
		// la
		// pente
		if(dataObdTrajet[3][i]>0){
			diffaltitude = dataObdTrajet[5][i]-dataObdTrajet[5][i-1];
			distancehypothenuse = (dataObdTrajet[3][i]/3.6)*deltaT;
			angle = Math.asin(diffaltitude/distancehypothenuse);
			dataObdTrajet[7][i]=Math.round(100*diffaltitude/(distancehypothenuse*Math.cos(angle)));	
		}
		else{
			dataObdTrajet[7][i]=dataObdTrajet[7][i-1];
		}	
		

		
		// calculs
		// des
		// valeurs
		// sur
		// une
		// phase
		RegimeMoteurPhase[i] = 0.5*(dataObdTrajet[2][i]+dataObdTrajet[2][i-1]);
		VitessePhase[i] = 0.5*(dataObdTrajet[3][i]+dataObdTrajet[3][i-1]);
		CoupleMoteurPhase[i] = 0.5*(dataObdTrajet[6][i]+dataObdTrajet[6][i-1]);
		
		// calcul
		// de
		// la
		// distance
		// cumulee
		dataObdTrajet[10][i] = dataObdTrajet[10][i-1] + VitessePhase[i]/3.6*(deltaT)/1000;
		
		// calcul
		// du
		// temps
		// cumule
		dataObdTrajet[11][i] = dataObdTrajet[11][i-1] + deltaT;
		
		// calcul
		// de
		// la
		// valeur
		// du
		// rapport
		// de
		// reduction
		RapportRed[i] = RegimeMoteurPhase[i]*2*Math.PI*RRoue*3.6/(VitessePhase[i]*60);	
		
		// indice
		// de
		// cette
		// valeur
		dataObdTrajet[12][i] = findIndexReduction(dataVitesses,RapportRed[i]);	
		
		
		/*
		 * on teste si on accelere ou si on decelere et si la valeur de rapport de reduction existe si on accelere, on calcule la consommation en fonction du débit d'air, sinon on dit que le moteur ne consomme pas. Ceci est faux : on ne consomme que si le moteur fournit un couple positif, donc si la force motrice est positive, sinon c'est du frein moteur, le couple et la force motrice sont alors
		 * négatifs. il est pour l'instant impossible d'avoir accès à ces données
		 */
		if(VitessePhase[i]-VitessePhase[i-1]>=0 && dataObdTrajet[12][i]!=-1 && VitessePhase[i]>0){
			// temps_phase_motrice++;
			Consommation = dataObdTrajet[4][i]/AirFuelRatio/rhoessence*deltaT;
		}
		else{	
			// si
			// on
			// accelere
			// alors
			// on
			// est
			// en
			// phase
			// motrice
			// et
			// on
			// consomme
			if(VitessePhase[i]-VitessePhase[i-1]>=0 && VitessePhase[i]>0){	
				// temps_phase_motrice++;
				Consommation = dataObdTrajet[4][i]/AirFuelRatio/rhoessence*deltaT;
			}
			else{	// on
					// n'est
					// pas
					// en
					// phase
					// motrice,
					// on
					// ne
					// consomme
					// pas
				Consommation = 0;
			}
			
		}
		dataObdTrajet[8][i] = Consommation + dataObdTrajet[8][i-1];	// consommation
																	// totale
																	// en
																	// litres
		if(VitessePhase[i]){
			dataObdTrajet[13][i] = Consommation/(dataObdTrajet[10][i]-dataObdTrajet[10][i-1])*100	// consommation
																									// instantanée
																									// litres/100km
		}
		else{
			dataObdTrajet[13][i] = 0;
		}
		dataObdTrajet[14][i] = dataObdTrajet[8][i]*rhoessence*ratioCO2/1000;
	}
	
	var nbPointConseils = i-1;
	var ConsommationMoyenne = dataObdTrajet[8][nbPointConseils]/dataObdTrajet[10][nbPointConseils]*100;	// consommation
																										// moyenne
																										// en
																										// litres/100km
	
	// Affichage
	// des
	// conseils
	// sur
	// la
	// conduite
	var RapportsTrajet= new google.visualization.Table(document.getElementById('ChartRapportConseilsVT'));
    dataChart = new google.visualization.DataTable();
    dataChart.addColumn('string', "Resume du trajet");

	dataChart.addRow(["La distance parcourue lors du trajet est de "+Math.round(dataObdTrajet[10][nbPointConseils]*100)/100+" km."]);
	dataChart.addRow([" Vous avez consomme "+Math.round(dataObdTrajet[8][nbPointConseils]*100)/100+" litres de carburant lors de votre trajet " +
			" ("+Math.round(ConsommationMoyenne*100)/100+" litres/100km)."]);	
	dataChart.addRow(["Pour obtenir des conseils pour consommer moins, entrer votre trajet dans l'onglet Route," +
			" puis lancez le calcul en cliquant sur le bouton ci-dessous"]);
	RapportsTrajet.draw(dataChart,{
		width: '100%', 
		height: '100%'	  		
	});
	
	document.getElementById("buttoncalcultrajetVT").style.display = '';
	
	return;
	
}

/**
 * Rediscrétisation du trajet pour un intervalle de temps plus grand Calcul de la consommation sur le trajet enregistre à partir des donnees obd Affichage du bilan du trajet
 */
function AnalyseTrajetVTC(){
	
	var deltaT;	
	var dureetampon = 1;
	var tempsfindephase;
	tempsfindephase = dureetampon;
	var tempstampon = 0;
	var compteurtampon = 0;
	var chargemoy=0;
	var regimemoy=0;
	var vitessemoy=0;
	var quantiteairmoy=0;
	var altitudemoy=0;
	var j = 1;
	var RapportLu = new Array();
	var VitesseLue = new Array();
	
	creaTableauParam(dataObdTrajetReelle,nbParamObdTotal,"Obd");
	dataObdTrajetReelle = reversetab(dataObdTrajetReelle,dataObdTrajetReelle.length,nbParamObdTotal);
	
	if(dataReellesVTC[3][1]==0){
		VitesseLue[1]=-1;
		RapportLu[1] = 0;
	}
	else{
		RapportLu[1] = dataReellesVTC[2][1]*2*Math.PI*RRoue*3.6/(dataReellesVTC[3][1]*60);
		VitesseLue[1] = findIndexReduction(dataVitesses,RapportLu[1]);	
	}
	// on
	// discrétise
	// selon
	// un
	// pas
	// de
	// temps
	// plus
	// grand
	// (d'1
	// seconde)
	// car
	// les
	// données
	// étaient
	// trop
	// rapprochees
	// et
	// la
	// précision
	// du
	// capteur
	// de
	// vitesse
	// étant
	// trop
	// faible
	// (1km/h),
	// cela
	// amenait
	// de
	// nombreuses
	// erreurs
	// dans
	// les
	// calculs
	// qui
	// suivent
	// les
	// donnees
	// sont
	// mises
	// dans
	// le
	// tableau
	// dataObdTrajetReelle
	// a
	// partir
	// du
	// tableau
	// dataReellesVTC
	for(var i = 2;i<dataReellesVTC[0].length;i++){
		
		deltaT = dataReellesVTC[0][i]-dataReellesVTC[0][i-1];
		// comme
		// on a
		// acces
		// qu'aux
		// secondes,
		// quand
		// on
		// change
		// de
		// minute,
		// si
		// on
		// passe
		// de
		// 59.90
		// a
		// 0.1
		// par
		// exemple
		// alors
		// on a
		// deltaT
		// =
		// 0.2
		if(deltaT<0){
			deltaT += 60;
		}
		// on
		// s'arrête
		// si
		// l'acquisition
		// n'est
		// pas
		// continue
		if(deltaT>5){
			alert(i);
			alert("l'acquisition des donnees n'a pas été continue, le calcul ne peut pas continuer." +
					"\nVeuillez utiliser une fichier résultant d'une seule prise de données");
			return ;
		}
		tempstampon += deltaT;	
		
		if(dataReellesVTC[3][1]==0){
			VitesseLue[i]=-1;
			RapportLu[i] = 0;
		}
		else{
			RapportLu[i] = dataReellesVTC[2][i]*2*Math.PI*RRoue*3.6/(dataReellesVTC[3][i]*60);
			VitesseLue[i] = findIndexReduction(dataVitesses,RapportLu[i]);
		}
		// on
		// teste
		// les
		// rapports
		// de
		// vitesse
		// enclenchés
		// car
		// ils
		// amenent
		// des
		// erreurs
		// sur
		// les
		// moyennes
		// lorsqu'ils
		// changent
		// notament
		// sur
		// les
		// couples
		// et
		// les
		// regimes
		// moteurs
		if(VitesseLue[i]==VitesseLue[i-1]&&VitesseLue[i-1]!=-1){			
			compteurtampon ++ ;
			// moyenne
			// sur
			// la
			// phase
			// des
			// variables
			chargemoy = (dataReellesVTC[1][i]+chargemoy*(compteurtampon-1))/compteurtampon;
			regimemoy = (dataReellesVTC[2][i]+regimemoy*(compteurtampon-1))/compteurtampon;
			vitessemoy = (dataReellesVTC[3][i]+vitessemoy*(compteurtampon-1))/compteurtampon;
			quantiteairmoy = (dataReellesVTC[4][i]+quantiteairmoy*(compteurtampon-1))/compteurtampon;
			altitudemoy = (dataReellesVTC[5][i]+altitudemoy*(compteurtampon-1))/compteurtampon;	
			if(tempstampon>tempsfindephase){	// remplissage
												// du
												// tableau
												// avec
												// les
												// valeurs
												// moyennes,
												// on
												// commence
												// une
												// nouvelle
												// phase
				dataObdTrajetReelle[0][j]=tempstampon;
				dataObdTrajetReelle[1][j]=chargemoy;
				dataObdTrajetReelle[2][j]=regimemoy;
				dataObdTrajetReelle[3][j]=vitessemoy;
				dataObdTrajetReelle[4][j]=quantiteairmoy;
				dataObdTrajetReelle[5][j]=altitudemoy;
				chargemoy = dataReellesVTC[1][i];
				regimemoy = dataReellesVTC[2][i];
				vitessemoy = dataReellesVTC[3][i];
				quantiteairmoy = dataReellesVTC[4][i];
				altitudemoy = dataReellesVTC[5][i];
				compteurtampon=1;
				j++;
				tempsfindephase=tempstampon+dureetampon;
			}	
		}
		else{
			// si
			// changement
			// de
			// rapport
			// de
			// vitesses,
			// on
			// arrete
			// la
			// phase
			// a
			// l'indice
			// precedent,
			// on
			// remplit
			// le
			// tableau,
			// on
			// commence
			// une
			// nouvelle
			// phase
			if(i!=2 && tempstampon-deltaT!=dataObdTrajetReelle[0][j-1]){
				dataObdTrajetReelle[0][j]=tempstampon-deltaT;
				dataObdTrajetReelle[1][j]=chargemoy;
				dataObdTrajetReelle[2][j]=regimemoy;
				dataObdTrajetReelle[3][j]=vitessemoy;
				dataObdTrajetReelle[4][j]=quantiteairmoy;
				dataObdTrajetReelle[5][j]=altitudemoy;
				j++;
			}
			chargemoy = dataReellesVTC[1][i];
			regimemoy = dataReellesVTC[2][i];
			vitessemoy = dataReellesVTC[3][i];
			quantiteairmoy = dataReellesVTC[4][i];
			altitudemoy = dataReellesVTC[5][i];
			compteurtampon=1;	
			tempsfindephase=tempstampon+dureetampon;		
		}
	}

	
	
	var diffaltitude = 0;
	var difftemps = 0;
	var angle = 0;
	var distancehypothenuse = 0;
	var Puissance;
	var Consommation;
	var RegimeMoteurPhase = new Array();
	var CoupleMoteurPhase = new Array();
	var VitessePhase = new Array();
	var RapportRed = new Array();

	
	// initialisation
	// des
	// variables
	dataObdTrajetReelle[6][1] = dataObdTrajetReelle[1][1]*(coef[3]*Math.pow(dataObdTrajetReelle[2][1],3)+coef[2]*Math.pow(dataObdTrajetReelle[2][1],2)+coef[1]*dataObdTrajetReelle[2][1]+coef[0])/100;
	dataObdTrajetReelle[7][1] = 0;
	dataObdTrajetReelle[8][1] = 0;
	dataObdTrajetReelle[10][1] = 0;
	dataObdTrajetReelle[11][1] = 0;
	RapportRed[1] = dataObdTrajetReelle[2][1]*2*Math.PI*RRoue*3.6/(dataObdTrajetReelle[3][1]*60);	
	dataObdTrajetReelle[12][1] = findIndexReduction(dataVitesses,RapportRed[1]);	
	dataObdTrajetReelle[14][1] = 0;
	
	
	
	for(var i=2;i<dataObdTrajetReelle[2].length;i++){
		
		
		deltaT = dataObdTrajetReelle[0][i]-dataObdTrajetReelle[0][i-1];
		// calcul
		// du
		// couple
		dataObdTrajetReelle[6][i] = dataObdTrajetReelle[1][i]*(coef[3]*Math.pow(dataObdTrajetReelle[2][i],3)+coef[2]*Math.pow(dataObdTrajetReelle[2][i],2)+coef[1]*dataObdTrajetReelle[2][i]+coef[0])/100;
		// calcul
		// de
		// la
		// pente
		if(dataObdTrajetReelle[3][i]>0){
			diffaltitude = dataObdTrajetReelle[5][i]-dataObdTrajetReelle[5][i-1];
			distancehypothenuse = (dataObdTrajetReelle[3][i]/3.6)*deltaT;
			angle = Math.asin(diffaltitude/distancehypothenuse);
			dataObdTrajetReelle[7][i]=Math.round(100*diffaltitude/(distancehypothenuse*Math.cos(angle)));	
		}
		else{
			dataObdTrajetReelle[7][i]=dataObdTrajetReelle[7][i-1];
		}	
		

		
		// calculs
		// des
		// valeurs
		// sur
		// une
		// phase
		RegimeMoteurPhase[i] = (dataObdTrajetReelle[2][i]*1+dataObdTrajetReelle[2][i-1]*1)/2;
		VitessePhase[i] = (dataObdTrajetReelle[3][i]*1+dataObdTrajetReelle[3][i-1]*1)/2;
		CoupleMoteurPhase[i] = (dataObdTrajetReelle[6][i]*1+dataObdTrajetReelle[6][i-1]*1)/2;
		
		// calcul
		// de
		// la
		// distance
		// cumulee
		dataObdTrajetReelle[10][i] = dataObdTrajetReelle[10][i-1] + VitessePhase[i]/3.6*(deltaT)/1000;
		
		// calcul
		// du
		// temps
		// cumule
		dataObdTrajetReelle[11][i] = dataObdTrajetReelle[11][i-1] + deltaT;
		
		// calcul
		// de
		// la
		// valeur
		// du
		// rapport
		// de
		// reduction
		RapportRed[i] = RegimeMoteurPhase[i]*2*Math.PI*RRoue*3.6/(VitessePhase[i]*60);	
		
		// indice
		// de
		// cette
		// valeur
		dataObdTrajetReelle[12][i] = findIndexReduction(dataVitesses,RapportRed[i]);	
		
		
		/*
		 * on teste si on accelere ou si on decelere et si la valeur de rapport de reduction existe si on accelere, on calcule la consommation en fonction du débit d'air, sinon on dit que le moteur ne consomme pas. Ceci est faux : on ne consomme que si le moteur fournit un couple positif, donc si la force motrice est positive, sinon c'est du frein moteur, le couple et la force motrice sont alors
		 * négatifs. il est pour l'instant impossible d'avoir accès à ces données
		 */
		if(VitessePhase[i]-VitessePhase[i-1]>=0 && dataObdTrajetReelle[12][i]!=-1 && VitessePhase[i]>0){
			// temps_phase_motrice++;
			Consommation = dataObdTrajetReelle[4][i]/(AirFuelRatio*rhoessence)*deltaT;
		}
		else{	
			// si
			// on
			// accelere
			// alors
			// on
			// est
			// en
			// phase
			// motrice
			// et
			// on
			// consomme
			if(VitessePhase[i]-VitessePhase[i-1]>=0 && VitessePhase[i]>0){	
				// temps_phase_motrice++;
				Consommation = dataObdTrajetReelle[4][i]/(AirFuelRatio*rhoessence)*deltaT;
			}
			else{	// on
					// n'est
					// pas
					// en
					// phase
					// motrice,
					// on
					// ne
					// consomme
					// pas
				Consommation = 0;
			}
			
		}
		dataObdTrajetReelle[8][i] = Consommation + dataObdTrajetReelle[8][i-1];	// consommation
																				// totale
																				// en
																				// litres
		if(VitessePhase[i]){
			dataObdTrajetReelle[13][i] = Consommation/(dataObdTrajetReelle[10][i]-dataObdTrajetReelle[10][i-1])*100	// consommation
																													// instantanée
																													// litres/100km
		}
		else{
			dataObdTrajetReelle[13][i] = 0;
		}
		dataObdTrajetReelle[14][i] = dataObdTrajetReelle[8][i]*rhoessence*ratioCO2/1000;
	}
	
	var nbPointConseils = i-1;
	var ConsommationMoyenne = dataObdTrajetReelle[8][nbPointConseils]/dataObdTrajetReelle[10][nbPointConseils]*100;	// consommation
																													// moyenne
																													// en
																													// litres/100km
	
	// Affichage
	// des
	// conseils
	// sur
	// la
	// conduite
	var RapportsTrajet= new google.visualization.Table(document.getElementById('ChartRapportConseilsVT'));
    dataChart = new google.visualization.DataTable();
    dataChart.addColumn('string', "Resume du trajet");

	dataChart.addRow(["La distance parcourue lors du trajet est de "+Math.round(dataObdTrajetReelle[10][nbPointConseils]*100)/100+" km."]);
	dataChart.addRow([" Vous avez consomme "+Math.round(dataObdTrajetReelle[8][nbPointConseils]*100)/100+" litres de carburant lors de votre trajet " +
			" ("+Math.round(ConsommationMoyenne*100)/100+" litres/100km)."]);	
	dataChart.addRow(["Pour obtenir des conseils pour consommer moins, entrer votre trajet dans l'onglet Route," +
			" puis lancez le calcul en cliquant sur le bouton ci-dessous"]);
	RapportsTrajet.draw(dataChart,{
		width: '100%', 
		height: '100%'	  		
	});
	
	document.getElementById("buttoncalcultrajetVT").style.display = '';
	
	ConsommationReelleVT = dataObdTrajetReelle[8][nbPointConseils];
	ConsommationMoyenneReelleVT = ConsommationMoyenne;
	return;
	
}

/**
 * Affiche les conseils pour consommer moins après avoir comparé la conduite réelle et la conduite optimale
 */
function AfficherConseilsVT(){
	var ConsommationMoyenneOpti = dataConsoReelleVT[nbPointTrajet][96]/dataObdTrajetReelle[10][nbPointTrajet]*100;	// consommation
																													// opti
																													// moyenne
																													// en
																													// litres/100km
	var sous_regime = 0 ;
	var sur_regime = 0;
	var temps_rapport = new Array();
	var sous_regime_rapport = new Array();
	var sur_regime_rapport = new Array();
	
	
	for(var i = 1;i<nbPointTrajet;i++){
		if(dataConsoReelleVT[i][27]>0){
			temps_phase_motrice++;
		}
	}

	// on
	// compare
	// les
	// rapports
	// utilises
	// avec
	// les
	// rapports
	// optimaux
	for(j=1;j<nbRapportsVitesse+1;j++){
		temps_rapport[j] = 0;
		sous_regime_rapport[j]=0;
		sur_regime_rapport[j]=0;
		for(i=1;i<nbPointTrajet;i++){
			if(dataConsoReelleVT[i][78]==j && dataConsoReelleVT[i][27]>0){
				temps_rapport[j]++;
				if(dataConsoReelleVT[i][78]>dataConsoReelleVT[i][92]){
					sous_regime_rapport[j]++;
					sous_regime++;
				}
				if(dataConsoReelleVT[i][78]<dataConsoReelleVT[i][92]){
					sur_regime_rapport[j]++;
					sur_regime++;
				}
			}
		}
	}

	// Determination
	// du
	// pourcentage
	// de
	// temps
	// pour
	// lequel
	// l'utilisateur
	// est
	// en
	// sur-regime
	// ou
	// en
	// sous-regime
	// (au
	// total
	// et
	// pour
	// chaque
	// rapport)
	
	var pourcentage_sous_regime = new Array();
	var pourcentage_sur_regime = new Array();
	var pourcentage_sous_regime_tot;
	var pourcentage_sur_regime_tot;
	for(j=1;j<nbRapportsVitesse+1;j++){
		pourcentage_sous_regime[j]=sous_regime_rapport[j]/temps_rapport[j]*100;
	    pourcentage_sur_regime[j]=sur_regime_rapport[j]/temps_rapport[j]*100;
	    pourcentage_sous_regime_tot=sous_regime/temps_phase_motrice*100;
	    pourcentage_sur_regime_tot=sur_regime/temps_phase_motrice*100;
	}

	
	boolConseils = false;
	// Affichage
	// des
	// conseils
	// sur
	// la
	// conduite
	var RapportsConseils= new google.visualization.Table(document.getElementById('ChartConseilsVT'));
    dataChart = new google.visualization.DataTable();
    dataChart.addColumn('string', "Conseils pour consommer moins");
    
    if(dataConsoReelleVT[nbPointTrajet][96]<dataConsoReelleVT[nbPointTrajet][82]){
		if(pourcentage_sous_regime_tot >100/3 && pourcentage_sous_regime_tot > pourcentage_sur_regime_tot){
			dataChart.addRow(["Vous avez tendance à retrograder trop tard"]);
			boolConseils = true;
		}
		if (pourcentage_sur_regime_tot > 100/3 && pourcentage_sur_regime_tot > pourcentage_sous_regime_tot){
			dataChart.addRow(["Vous avez tendance à passer vos rapports trop tard"]);
			boolConseils = true;
		}
	
		for(i=1;i<nbRapportsVitesse+1;i++){
			if (pourcentage_sous_regime[i] > 100/3 && pourcentage_sous_regime[i] > pourcentage_sur_regime[i]){
		        dataChart.addRow([" Retrogradez plus tôt au rapport "+i]);
				boolConseils = true;
			}
			if (pourcentage_sur_regime[i] > 100/3 && pourcentage_sur_regime[i] > pourcentage_sous_regime[i]){
				dataChart.addRow([" Passez la vitesse suivante plus tot au rapport "+i]);
				boolConseils = true;
	    	}
		}
		if(!boolConseils){
			dataChart.addRow([" Votre conduite est proche d'une conduite optimale "]);
		}		
		var diffconso = Math.round((Math.round(dataConsoReelleVT[nbPointTrajet][82]*100)/100-Math.round(dataConsoReelleVT[nbPointTrajet][96]*100)/100)*100)/100;
		dataChart.addRow(["En conduisant d'une maniere optimale, vous auriez consomme "+Math.round(dataConsoReelleVT[nbPointTrajet][96]*100)/100+" litres"+
			      			" ("+Math.round(ConsommationMoyenneOpti*100)/100+" litres/100km)."+
			      			" Soit "+diffconso+" litres de moins sur le trajet."]);
	}
    else{
    	dataChart.addRow([" Votre conduite est proche d'une conduite optimale "]);
    }
	dataChart.addRow(["Vous pouvez visualiser les resultats sur les graphiques dans l'onglet Graphique"]);
	
	RapportsConseils.draw(dataChart,{
		width: '100%', 
		height: '100%'	  		
	});
}

// ----------------------------------------------------------------------------------
// Recherche
// de
// donnees
// dans
// les
// tableaux
// -------------------------------------------------------
/**
 * Recherche de la pente au point situe a la distance 'distance'
 * 
 * @param distance :
 *            position ou la pente doit etre recuperee
 * @returns pente: retourne la pente correspondante
 * @returns ecart: retourne l'ecart entre la 'distance' et la position sur les donnees d'elevation
 */
function recherchePente(distance){
	var nbRows = dataxcos.getNumberOfRows();
	var indice = Math.round(nbRows*distance/distanceTotaleRoute);
	if(indice <1){
		indice=1;
	}
	var ecart = Math.abs(distance/1000 - dataxcos.getValue(1,3));
	var pente = 0;
	for (var i = indice; i < nbRows;i++){
		var eTemp = Math.abs(distance/1000 - dataxcos.getValue(i,3));
		if(ecart > eTemp){
			ecart = eTemp;
			pente = dataxcos.getValue(i,5);
		}else{
			break;
		}
	}
	return {
		pente: pente,
		ecart: ecart,
	}
}
/**
 * Recherche des coordonnees du point situe a la distance 'distance'
 * 
 * @param distance :
 *            position ou les coordonnees doivent etre recuperees
 * @returns lat: retourne la latitude correspondante
 * @return long: retourne la longitude correspondante
 * @returns ecart: retourne l'ecart entre la 'distance' et la position sur les donnees d'elevation
 */
function rechercheCoord(distance){
	var ecart = Math.abs(distance/1000 - dataxcos.getValue(1,3));
	var lat = dataxcos.getValue(1,0);
	var long = dataxcos.getValue(1,1);
	var iter = 1;
	var nbRows = dataxcos.getNumberOfRows();
	var indice = Math.round(nbRows*distance/distanceTotaleRoute);
	if(indice <1){
		indice=1;
	}
	for (var i = indice; i < nbRows;i++){ 
		var eTemp = Math.abs(distance/1000 - dataxcos.getValue(i,3));
		if(ecart > eTemp){
			ecart = eTemp;
			lat = dataxcos.getValue(i,0);
			long = dataxcos.getValue(i,1);
			iter = i;
		}else{
			break;
		}
	}
	return {
		lat: lat,
		long: long,
		ecart: ecart,
		iter: iter,
	}
}
/**
 * Recherche des bornes 'accessibles' a partir du point de la panne jusqu'a trouver au moins une borne accessible
 */
function rechercheBorne(){
	var boolBorne = false; // Boolean
							// vrai
							// lorsque
							// le
							// programme
							// a
							// trouve
							// une
							// borne
	var indiceTampon = indiceP;
	for(var iterConso = indiceP;iterConso>1;iterConso--){
		for(var iterBorne = 1; iterBorne < nbPointBorne;iterBorne++){
			var dist = calDistLatLg(dataConso[iterConso][33],dataConso[iterConso][34],dataBorne[iterBorne][1],dataBorne[iterBorne][2]);
			if(dist<dataConso[iterConso][35] && !dataBorne[iterBorne][3]){
				boolBorne = true;
				dataBorne[iterBorne][3] = true;
				addMarker(dataBorne[iterBorne][1],dataBorne[iterBorne][2],'Borne accessible',ctxPath + '/images/borne-recharge-electrique.png',true,markersRecharge);		
				cptborne +=1;
			}
		}
		indiceTampon = iterConso;
		if(boolBorne){
			indiceP = iterConso;
			break;
		}
	}
	if (cptborne == 0){
		alert("Aucunes bornes n'est accessibles à proximité (vérifier si un fichier borne a été lu)");
	}else if(indiceTampon < 3){
		alert("Toutes les bornes accessibles sont affichées");
	}
	
	for(var i = 0;i<markersRecharge.length;i++){
		google.maps.event.clearListeners(markersRecharge[i], 'click');			// Suppression
																				// des
																				// listeners
																				// existants
																				// pour
																				// eviter
																				// d'avoir
																				// 2 ou
																				// plus
																				// listener
																				// sur
																				// la
																				// meme
																				// borne
		google.maps.event.addListener( markersRecharge[i], 'click', function() {
				if(confirm("Aller a cette borne ?")){
					latB = this.getPosition().lat();
					longB = this.getPosition().lng();
					latBtab.push(latB);
					longBtab.push(longB);
					boolChoixBorne = true;
					calcRoute(departure_place.geometry.location,{lat:this.getPosition().lat(),lng:this.getPosition().lng()});
					document.getElementById("calculBorne"+typeVehicule).style.visibility = '';
				}
		    });
	}
	
}
/**
 * Recherche de l'indice de la ligne correspondant au temps 't'
 * 
 * @param t :
 *            temps a rechercher
 * @returns indice de la ligne correspondante
 */
function rechTemps(t,tab){
	var temp 	= 1;
	var abs = document.getElementById("abs"+typeVehicule+"1").value;
	while(t>tab[temp][abs]){
		temp +=1;
	}
	return temp;
}

function rechTemps2(t,tab){
	var temp 	= 1;
	var abs = document.getElementById("abs"+typeVehicule+"2").value;
	//console.log(abs);
	while(t>tab[temp][abs]){
		temp +=1;
	}
	return temp;
}
function rechTemps3(t,tab){
	var temp 	= 1;
	var abs = document.getElementById("abs"+typeVehicule+"3").value;
	//console.log(abs);
	while(t>tab[abs][temp]){
		temp +=1;
	}
	return temp;
}
/**
 * Recherche de l'indice de la ligne correspondant au temps 't' pour un VPU
 * @param t : temps a rechercher
 * @returns indice de la ligne correspondante
 */
function rechTempsVPU(t,tab){
	var temp 	= 1;
	var abs = document.getElementById("abs"+typeVehicule+"1").value;
	while(t>tab[abs][temp]){
		temp +=1;
	}
	return temp;
}

/**
 * Recherche de l'indice de la ligne correspondant au temps 't' pour un VPU
 * @param t : temps a rechercher
 * @returns indice de la ligne correspondante
 */
function rechTempsVPU2(t,tab){
	var temp 	= 1;
	var abs = document.getElementById("abs"+typeVehicule+"2").value;
	while(t>tab[abs][temp]){
		temp +=1;
	}
	return temp;
}

/**
 * Recherche de l'indice de la ligne correspondant au temps 't' pour un VPU
 * @param t : temps a rechercher
 * @returns indice de la ligne correspondante
 */
function rechTempsVPU3(t,tab){
	var temp 	= 1;
	var abs = document.getElementById("abs"+typeVehicule+"3").value;
	while(t>tab[abs][temp]){
		temp +=1;
	}
	return temp;
}

/**
 * Recherche de l'indice de la ligne correspondant au temps 't' dans la table dataConso
 * 
 * @param t :
 *            temps a rechercher
 * @returns indice de la ligne correspondante
 */
function rechTempsdataConso(t,tab){
	var temp 	= 1;
	var abs = document.getElementById("abs"+typeVehicule+"2").value;
	while(t>tab[temp][abs]){
		temp +=1;
	}
	return temp;
}

/**
 * Verifie si une borne est deja dans le tableau
 * 
 * @param lat :
 *            latitude de la borne a chercher
 * @param long :
 *            longitude de la borne a chercher
 * @param tabmarker :
 *            tableau dans lequel la recherche se fait
 * @returns : vrai si la borne est dans le tableau et faux sinon
 */
function rechercheBorneTab(lat,long,tabmarker){
	for(var iter = 0; iter < tabmarker.length;iter++){
		if(Math.abs(tabmarker[iter].getPosition().lat()- lat)<0.00001 && Math.abs(tabmarker[iter].getPosition().lng()- long)<0.00001){
			return true;
			break;
		}
	}
	return false;
}
// ----------------------------------------------------------------------------------
// Maj de
// parametres
// avec
// donnees
// interface
// -----------------------------------------------------
/**
 * Actualisation des parametres quand l'utilisateur selectionne un type de vehicule dans le menu deroulant.
 */
function carTypeSelection() {
	// document.getElementById("saveVehButton"+typeVehicule).style.display
	// =
	// 'none';
	var carTypeId = parseFloat(document.getElementById("carType"+typeVehicule).value);
	console.info("selected index in carTypeSelection : " + carTypeId);
    if (carTypeId > 0)
    	{
    	if(typeVehicule=="VE"){
    		document.getElementById("vehTypeId"+typeVehicule).setAttribute("readonly", true);
        	document.getElementById('vehTypeId'+typeVehicule).value = dataVehiculesE[carTypeId][paramTypeEnum.VEH_TYPE];
        	document.getElementById('Masse'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.VEH_MASS]);
        	document.getElementById('SCx'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.DRAG_COEFF]);
        	document.getElementById('RayonRoue'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.WHEEL_RADIUS]);
        	document.getElementById('res_roulement'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.ROLL_RES]);
        	document.getElementById('Paux'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.AUX_CONS]);
        	document.getElementById('rend_gear'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.TR_YIELD]);
    		document.getElementById('Pmotorrated'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.NOMINAL_ENG_POW]);
    		document.getElementById('PmotorMax'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.MAX_ENG_POW]);
    		document.getElementById('trRatio'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.TR_RATIO]);
        	document.getElementById('rend_elec'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.BATT_EFF]);
        	document.getElementById('capBatterie'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.BATT_CAPA]);
        	document.getElementById('vmin'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.REC_MIN_SPD]);
        	document.getElementById('vmax'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.REC_OPT_SPD]);
        	document.getElementById('seuilDec'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.REC_MAX_DEC]);
        	document.getElementById('coefInertie'+typeVehicule).value = parseFloat(dataVehiculesE[carTypeId][paramTypeEnum.COEFF_INERTIA]);
        	
        	document.getElementById('idParamVeh'+typeVehicule).value = dataVehiculesE[carTypeId][paramTypeEnum.VEH_ID];
    	}
    	else if(typeVehicule=="VT"){
    		document.getElementById("vehTypeId"+typeVehicule).setAttribute("readonly", true);
        	document.getElementById('vehTypeId'+typeVehicule).value = dataVehiculesT[carTypeId][0];
        	document.getElementById('Masse'+typeVehicule).value = parseFloat(dataVehiculesT[carTypeId][1]);
        	document.getElementById('SCx'+typeVehicule).value = parseFloat(dataVehiculesT[carTypeId][3]);
        	document.getElementById('RayonRoue'+typeVehicule).value = parseFloat(dataVehiculesT[carTypeId][2]);
        	document.getElementById('res_roulement'+typeVehicule).value = parseFloat(dataVehiculesT[carTypeId][4]);
        	document.getElementById('rend_gear'+typeVehicule).value = parseFloat(dataVehiculesT[carTypeId][5]);
        	document.getElementById('coefInertie'+typeVehicule).value = parseFloat(dataVehiculesT[carTypeId][7]);
        	document.getElementById('nbRapportsVitesse'+typeVehicule).value = parseFloat(dataVehiculesT[carTypeId][8]);
        	
        	document.getElementById('idParamVeh'+typeVehicule).value = dataVehiculesT[carTypeId][9];
    	}
    	else if(typeVehicule=="VP"){
    		document.getElementById("vehTypeId"+typeVehicule).setAttribute("readonly", true);
        	document.getElementById('vehTypeId'+typeVehicule).value = dataVehiculesP[carTypeId][paramTypeEnum.VEH_TYPE];
        	document.getElementById('Masse'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.VEH_MASS]);
        	document.getElementById('SCx'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.DRAG_COEFF]);
        	document.getElementById('RayonRoue'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.WHEEL_RADIUS]);
        	document.getElementById('res_roulement'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.ROLL_RES]);
        	document.getElementById('Paux'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.AUX_CONS]);
        	document.getElementById('rend_gear'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.TR_YIELD]);
    		document.getElementById('Pmotorrated'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.NOMINAL_ENG_POW]);
    		document.getElementById('PmotorMax'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.MAX_ENG_POW]);
    		document.getElementById('trRatio'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.TR_RATIO]);
        	document.getElementById('rend_elec'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.BATT_EFF]);
        	document.getElementById('capBatterie'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.BATT_CAPA]);
        	document.getElementById('vmin'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.REC_MIN_SPD]);
        	document.getElementById('vmax'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.REC_OPT_SPD]);
        	document.getElementById('seuilDec'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.REC_MAX_DEC]);
        	document.getElementById('seuilP'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.POW_THRE]);
        	document.getElementById('seuilV'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.SPD_THRE]);
        	document.getElementById('seuilA'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.ACC_THRE]);
        	document.getElementById('socLow'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.SOC_LOW]);
        	document.getElementById('socUp'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.SOC_UP]);
        	document.getElementById('coefInertie'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.COEFF_INERTIA]);
        	
        	document.getElementById('idParamVeh'+typeVehicule).value = dataVehiculesP[carTypeId][paramTypeEnum.VEH_ID];
    	}
    	else if(typeVehicule=="VPU"){
    		document.getElementById("vehTypeId"+typeVehicule).setAttribute("readonly", true);
        	document.getElementById('vehTypeId'+typeVehicule).value = dataVehiculesP[carTypeId][paramTypeEnum.VEH_TYPE];
        	document.getElementById('Masse'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.VEH_MASS]);
        	document.getElementById('SCx'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.DRAG_COEFF]);
        	document.getElementById('RayonRoue'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.WHEEL_RADIUS]);
        	document.getElementById('res_roulement'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.ROLL_RES]);
        	document.getElementById('Paux'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.AUX_CONS]);
        	document.getElementById('rend_gear'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.TR_YIELD]);
    		document.getElementById('Pmotorrated'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.NOMINAL_ENG_POW]);
    		document.getElementById('PmotorMax'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.MAX_ENG_POW]);
    		document.getElementById('trRatio'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.TR_RATIO]);
        	document.getElementById('rend_elec'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.BATT_EFF]);
        	document.getElementById('capBatterie'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.BATT_CAPA]);
        	document.getElementById('vmin'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.REC_MIN_SPD]);
        	document.getElementById('vmax'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.REC_OPT_SPD]);
        	document.getElementById('seuilDec'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.REC_MAX_DEC]);
        	document.getElementById('seuilP'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.POW_THRE]);
        	document.getElementById('seuilV'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.SPD_THRE]);
        	document.getElementById('seuilA'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.ACC_THRE]);
        	document.getElementById('socLow'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.SOC_LOW]);
        	document.getElementById('socUp'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.SOC_UP]);
        	document.getElementById('coefInertie'+typeVehicule).value = parseFloat(dataVehiculesP[carTypeId][paramTypeEnum.COEFF_INERTIA]);
        	document.getElementById('idParamVeh'+typeVehicule).value = dataVehiculesP[carTypeId][paramTypeEnum.VEH_ID];
    	}
    	}
    else
    	{
    	document.getElementById("vehTypeId"+typeVehicule).removeAttribute("readonly");
    	emptyParamFields();													 
    	}
}
/**
 * Completion des parametres lies a chaque vehicule dans l'onglet Vehicule (electrique). Vide les data, les options du select, les fields parametres avant de lire le fichier charge.
 */
function remplirParamVehiculesE(){
	dataVehiculesE.splice(0,dataVehiculesE.length);
	emptyCarTypeOptions();
	emptyParamFields();
	document.getElementById("vehTypeId"+typeVehicule).removeAttribute("readonly");
	// dataVehiculeE
	// =
	// lectureParam("#carsParam"+typeVehicule,
	// dataVehiculesE,nbParamParVehiculeE,"VehE");
	paramFileVehELoaded = true;
	document.getElementById("toggleParamButton"+typeVehicule).style.display = 'inline-block';
	document.getElementById("addVehButton"+typeVehicule).style.display = 'inline-block';
}
/**
 * Completion des parametres lies a chaque vehicule dans l'onglet Vehicule (thermique). Vide les data, les options du select, les fields parametres avant de lire le fichier charge.
 */
function remplirParamVehiculesT(){
	dataVehiculesT.splice(0,dataVehiculesT.length);
	emptyCarTypeOptions();
	emptyParamFields();
	document.getElementById("vehTypeId"+typeVehicule).removeAttribute("readonly");
	// dataVehiculeT
	// =
	// lectureParam("#carsParam"+typeVehicule,
	// dataVehiculesT,nbParamParVehiculeT,"VehT");
	paramFileVehTLoaded = true;
	document.getElementById("toggleParamButton"+typeVehicule).style.display = 'inline-block';
	document.getElementById("addVehButton"+typeVehicule).style.display = 'inline-block';
}
function remplirParamVehiculesP(){
	dataVehiculesP.splice(0,dataVehiculesP.length);
	emptyCarTypeOptions();
	emptyParamFields();
	document.getElementById("vehTypeId"+typeVehicule).removeAttribute("readonly");
	// dataVehiculeP
	// =
	// lectureParam("#carsParam"+typeVehicule,
	// dataVehiculesP,nbParamParVehiculeP,"VehP");
	paramFileVehPLoaded = true;
	document.getElementById("toggleParamButton"+typeVehicule).style.display = 'inline-block';
	document.getElementById("addVehButton"+typeVehicule).style.display = 'inline-block';
}
/**
 * Completion des parametres lies aux donnes OBD dans l'onglet OBD. Vide les data, les options du select, les fields parametres avant de lire le fichier charge.
 */
function remplirParamObd(){
    dataObd.splice(0,dataObd.length);
    lectureParam("#donneesObd"+typeVehicule,dataObd,nbParamObd,"Obd");
    paramFileObdLoaded = true;
}

/**
 * Completion des parametres lies aux donnes réelles dans l'onglet Donées réelles. Vide les data, les options du select, les fields parametres avant de lire le fichier charge.
 */
function remplirDonneesReellesVT(){
	// dataReellesVT.splice(0,dataReellesVT.length);
	
	// Acquisition
	// des
	// données
	// réelles
	dataReellesVT = lectureParam("#donneesReelles"+typeVehicule,dataReellesVT,nbParamReelles,"Reelles");
	paramFileReellesLoaded = true;
	
	// Partie
	// pour
	// le
	// bilan
	// de
	// conduite
	// réel
	dataReellesVTBC = reversetab(dataReellesVT,dataReellesVT.length,nbParamReelles+1);
	for (var ter = 0; ter<dataReellesVTBC[0].length; ter++){
		dataReellesVTBC[1][ter] = dataReellesVTBC[2][ter];
		dataReellesVTBC[2][ter] = 0;
		dataReellesVTBC[3][ter] = 0;
		dataReellesVTBC[4][ter] = 0;
	}
	AnalyseTrajetVTBC();
	
	// Partie
	// pour
	// les
	// conseils
	// réels
	dataReellesVTCbis = reversetab(dataReellesVT,dataReellesVT.length,nbParamReelles+1);
	dataReellesVTC[0] = new Array();
	dataReellesVTC[1] = new Array();
	dataReellesVTC[2] = new Array();
	dataReellesVTC[3] = new Array();
	dataReellesVTC[4] = new Array();
	dataReellesVTC[5] = new Array();
	for (var port = 0; port<dataReellesVTCbis[0].length; port++){
		dataReellesVTC[0][port] = dataReellesVTCbis[0][port];
		dataReellesVTC[1][port] = dataReellesVTCbis[3][port];
		dataReellesVTC[2][port] = dataReellesVTCbis[1][port];
		dataReellesVTC[3][port] = dataReellesVTCbis[2][port];
		dataReellesVTC[4][port] = dataReellesVTCbis[4][port];
		dataReellesVTC[5][port] = 50;
	}
	ResumeTrajetVTC();
	
	// Calcul
	// des
	// données
	// qui
	// ne
	// sont
	// pas
	// récupéré
	// par
	// l'Obd
	dataReellesVTDonnees = reversetab(dataReellesVT,dataReellesVT.length,nbParamReelles+1);	
	Resultat = calcDonneesReelles(dataReellesVTDonnees);
	rendMotReel = Resultat;
	
	// Partie
	// pour
	// le
	// bilan
	// de
	// consommation
	dataReellesVTBCO[0] = new Array();
	dataReellesVTBCO[1] = new Array();
	dataReellesVTBCO[2] = new Array();
	dataReellesVTBCO[0][0] = ("Consommation réelle (L)");
	dataReellesVTBCO[0][1] = ConsommationReelleVT;
	dataReellesVTBCO[1][0] = ("Consommation réelle moyenne (L/100km)");
	dataReellesVTBCO[1][1] = ConsommationMoyenneReelleVT;
	dataReellesVTBCO[2][0] = ("Rendement moteur (%)");
	dataReellesVTBCO[2][1] = rendMotReel*100;
	
	affichage_RapportVehTChartBCO();
		
	// Affichage
	// des
	// graphiques
	// réelles
	dataReellesVTGraph = reversetab(dataReellesVT,dataReellesVT.length,nbParamReelles+1);
	dataReellesVTGraph[5] = dataReellesVTDonnees[11];
	dataReellesVTGraph[6] = dataReellesVTDonnees[12];
	dataReellesVTGraph[7] = dataReellesVTDonnees[9];
	dataReellesVTGraph[8] = dataReellesVTDonnees[10];
	dataReellesVTGraph[9] = dataReellesVTDonnees[13];
	dataReellesVTGraph = reversetab(dataReellesVTGraph,10,dataReellesVTGraph[0].length);
	document.getElementById('chart_energieVT2').style.display = '';
	document.getElementById('Affichage_courbesVT2').style.display = '';
	AffConsoReelles(0,2,"chart_energieVT2",0,nbLignesParamReelles,dataReellesVTGraph,myChartVT2);
}
/**
 * Completion des parametres lies aux donnes OBD dans l'onglet OBD. Vide les data, les options du select, les fields parametres avant de lire le fichier charge.
 */
function remplirParamTrajet(){
	dataObdLu.splice(0,dataObdLu.length);
	dataObdLu = lectureParam("#donneesTrajet"+typeVehicule, dataObdLu,nbParamObd,"Obd");
	paramFileTrajetLoaded = true;
}
/**
 * Completion des parametres lies aux donnes OBD dans l'onglet OBD. Vide les data, les options du select, les fields parametres avant de lire le fichier charge.
 */
function remplirParamConseilsVE(){
	dataConseilsVE.splice(0,dataConseilsVE.length);
	dataConseilsVE = lectureParam("#donneesConseils"+typeVehicule, dataConseilsVE,nbParamConseilsVE,"ConseilsVE");
	paramFileConseilsVELoaded = true;
}
/**
 * Completion des parametres lies aucouple max par rapport au regime moteur de chaque vehicule dans l'onglet OBD. Vide les data, les options du select, les fields parametres avant de lire le fichier charge.
 */
/*function remplirParamCouple(){
	dataCouple.splice(0,dataCouple.length);
	dataCouple = lectureParam("#CoupleParam"+typeVehicule, dataCouple,nbParamCouple,"Couple");
	paramFileCoupleLoaded = true;
}*/
/**
 * Mise a jour de la pente valeur fixe ou pente reelle
 */
function updatePente(){
	var temp = parseFloat(document.getElementById("pente"+typeVehicule).value);
	if(temp == 1 || temp == 0){ // Pente
								// relle
		boolPenteFixe = false;
		document.getElementById('penteValeur'+typeVehicule).style.visibility = 'hidden';
	}else if(temp == 2){
		document.getElementById('penteValeur'+typeVehicule).style.visibility = '';
		((document.getElementById('penteValeur'+typeVehicule).value) != '')? penteFixe 		= parseFloat(document.getElementById('penteValeur'+typeVehicule).value)/100 			: penteFixe 	  = 0;
		boolPenteFixe = true;
	}
	return temp;
}

/**
 * Mise a jour de la pente pour le calcul du cycle standard valeur fixe ou pente reelle
 */
function updatePenteStandard(){
	var temp = parseFloat(document.getElementById("penteStandard"+typeVehicule).value);
	if(temp == 1 || temp == 0){ // Pente
								// relle
		boolPenteFixeStandard = false;
		document.getElementById('PenteFixe'+typeVehicule).style.visibility = 'hidden';
	}else if(temp == 2){
		document.getElementById('PenteFixe'+typeVehicule).style.visibility = '';
		((document.getElementById('PenteFixe'+typeVehicule).value) != '')? penteFixeStandard 		= parseFloat(document.getElementById('PenteFixe'+typeVehicule).value)/100 			: penteFixeStandard 	  = 0;
		boolPenteFixeStandard = true;
	}
}

/**
 * Mise a jour du SOC_low pour le calcul avec la loi de commande 2
 */
function updateLoiVitesse(){
	var temp = parseFloat(document.getElementById("loiCommande").value);
	if(temp == 2){		//Loi de vitesse
		document.getElementById('Soc_Low').style.visibility = '';
		((document.getElementById('Soc_Low').value) != '')? Soc_low 		= parseFloat(document.getElementById('Soc_Low').value)			: Soc_Low 	  = 40;
	}
	else { 
		document.getElementById('Soc_Low').style.visibility = 'hidden';
		Soc_low = 40;
	}
}

/**
 * Mise a jour du SOC_low pour le calcul avec la loi de commande 2
 */
function updateSOC(){
	((document.getElementById('Soc_Low').value) != '')? Soc_low 		= parseFloat(document.getElementById('Soc_Low').value)			: Soc_Low 	  = 40;
}


/**
 * Mise a jour du type de resolution 1 : repetition cycle 2 : vitesse constante
 */
function updateResolution(){
	typeResolution = parseFloat(document.getElementById("resolution"+typeVehicule).value);
	if(typeResolution == 1){
		document.getElementById('cycleRepetition'+typeVehicule).style.visibility = '';
	}else{
		document.getElementById('cycleRepetition'+typeVehicule).style.visibility = 'hidden';
	}
}

/**
 * Mise a jour du type de la fonction de mémoire des résulats
 */
var memory=false;
function updateMemory(){
	memory = parseFloat(document.getElementById("memory"+typeVehicule).value);
}

var intervalDeTempsCalcul=15;
function updateIntervalDeTemps(){
	((document.getElementById("intervalDeTemps"+typeVehicule).value) != '')? intervalDeTempsCalcul 		= parseFloat(document.getElementById("intervalDeTemps"+typeVehicule).value)			: intervalDeTempsCalcul 	  = 15;
}

/**
 * Mise à jour du type de calculs et affichage des informations correspondantes
 */

function updateModeCalcul(){
	typeCalcul = parseFloat(document.getElementById("modeCalcul"+typeVehicule).value);
	
	if(typeVehicule == "VE"){
		if(typeCalcul == 1){ //segmentation
			document.getElementById("donneesGPS"+typeVehicule).style.display = 'none';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = '';
			
			document.getElementById("resolution"+typeVehicule).value = 2;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 1;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = '';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = 'none';
			document.getElementById("conduite"+typeVehicule).style.display = '';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = 'none';
		}
		else if(typeCalcul==2){ //profile vitesse
			document.getElementById("donneesGPS"+typeVehicule).style.display = '';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = 'none';
			
			document.getElementById("resolution"+typeVehicule).value = 1;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 1;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = '';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = 'none';
			document.getElementById("conduite"+typeVehicule).style.display = 'none';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = 'none';
		}
		else if(typeCalcul==3){ //calcul en ligne
			document.getElementById("donneesGPS"+typeVehicule).style.display = '';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = 'none';
			
			document.getElementById("resolution"+typeVehicule).value = 1;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 2;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = 'none';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = '';
			document.getElementById("conduite"+typeVehicule).style.display = 'none';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = '';
		}
	}
	
	
	else if(typeVehicule=="VPU"){
		if(typeCalcul == 1){ //segmentation
			document.getElementById("donneesGPS"+typeVehicule).style.display = 'none';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = '';
			document.getElementById("MenuHighways"+typeVehicule).style.display = '';
			document.getElementById("resolution"+typeVehicule).value = 2;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 1;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = '';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = 'none';
			document.getElementById("conduite"+typeVehicule).style.display = '';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = 'none';
		}
		else if(typeCalcul==2){ //profile vitesse
			document.getElementById("donneesGPS"+typeVehicule).style.display = '';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = 'none';
			document.getElementById("MenuHighways"+typeVehicule).style.display = 'none';
			document.getElementById("resolution"+typeVehicule).value = 1;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 1;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = '';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = 'none';
			document.getElementById("conduite"+typeVehicule).style.display = 'none';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = 'none';
		}
		else if(typeCalcul==3){ //calcul en ligne
			document.getElementById("donneesGPS"+typeVehicule).style.display = '';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = 'none';
			document.getElementById("MenuHighways"+typeVehicule).style.display = 'none';
			document.getElementById("resolution"+typeVehicule).value = 1;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 2;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = 'none';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = '';
			document.getElementById("conduite"+typeVehicule).style.display = 'none';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = '';
		}
	}
	else if(typeVehicule=="VT"){
		if(typeCalcul == 1){ //segmentation
			document.getElementById("donneesGPS"+typeVehicule).style.display = 'none';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = '';
			document.getElementById("MenuHighways"+typeVehicule).style.display = '';
			document.getElementById("resolution"+typeVehicule).value = 2;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 1;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = '';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = 'none';
			document.getElementById("conduite"+typeVehicule).style.display = '';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = 'none';
		}
		else if(typeCalcul==2){ //profile vitesse
			document.getElementById("donneesGPS"+typeVehicule).style.display = '';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = 'none';
			document.getElementById("MenuHighways"+typeVehicule).style.display = 'none';
			document.getElementById("resolution"+typeVehicule).value = 1;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 1;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = '';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = 'none';
			document.getElementById("conduite"+typeVehicule).style.display = 'none';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = 'none';
		}
		else if(typeCalcul==3){ //calcul en ligne
			document.getElementById("donneesGPS"+typeVehicule).style.display = '';
			document.getElementById("tableAutocomplete"+typeVehicule).style.display = 'none';
			document.getElementById("MenuHighways"+typeVehicule).style.display = 'none';
			document.getElementById("resolution"+typeVehicule).value = 1;
			updateResolution();
			document.getElementById("memory"+typeVehicule).value = 2;
			updateMemory();
			document.getElementById("BoutonsCalculStandard"+typeVehicule).style.display = 'none';
			document.getElementById("BoutonsCalculEnLigne"+typeVehicule).style.display = '';
			document.getElementById("conduite"+typeVehicule).style.display = 'none';
			document.getElementById("intervalTempsCalcul"+typeVehicule).style.display = '';
		}
	}
	
	
	
}

/*
 * Mise à jour de l'existence d'un calcul avec un autre type de véhicul pour pour comparer
 */
var calcComp=false;
var typeVehiculeComp;
function updateModeComparaison(){
	window.typeVehiculeInit =typeVehicule;
	
	if(document.getElementById("modeComparaison"+typeVehicule).value== 0){
		//set les valeurs par defaut ...
		calcComp= false;
		typeVehicule = window.typeVehiculeInit;
		majChoixVehicule();
		document.getElementById("tabbertabsComparaison").style.display = 'none';
		document.getElementById("tabbertabsProfile"+typeVehicule).style.display = '';
	
		
	}
	else if(document.getElementById("modeComparaison"+typeVehicule).value== 1){
		calcComp = true;
		typeVehiculeComp="VE";
		//set valeurs par défaut des parmètre de VE (pente)
		document.getElementById("tabbertabsProfile"+typeVehicule).style.display = 'none';
		document.getElementById("tabbertabsComparaison").style.display = '';
		document.getElementById("resolutionVE").value =document.getElementById("resolution"+typeVehicule).value ;
		document.getElementById("penteVE").value = 1;
		
		
	}
	else if(document.getElementById("modeComparaison"+typeVehicule).value== 2){
		calcComp = true;
		typeVehiculeComp="VT";
		//set valeurs par défaut des parmètre de VE (pente)
		document.getElementById("tabbertabsProfile"+typeVehicule).style.display = 'none';
		document.getElementById("tabbertabsComparaison").style.display = '';
		document.getElementById("resolutionVT").value =document.getElementById("resolution"+typeVehicule).value ;
		//updateResolution();
		document.getElementById("penteVT").value = 1;
		
		
	}
	else if(document.getElementById("modeComparaison"+typeVehicule).value== 3){
		calcComp = true;
		typeVehiculeComp="VPU";
		//set valeurs par défaut des parmètre de VE (mode puissance, model pac, pente, loi de Commande, ...)
		document.getElementById("tabbertabsProfile"+typeVehicule).style.display = 'none';
		document.getElementById("tabbertabsComparaison").style.display = '';
		document.getElementById("resolutionVPU").value =document.getElementById("resolution"+typeVehicule).value ;
		document.getElementById("modePuissancePACU").value = 3;
		document.getElementById("penteVPU").value = 1;
		document.getElementById("memoryVPU").value = 1;
		document.getElementById("modelePACU").value = 2;
		updateModelePac();
		document.getElementById("tpsReponsePACU").value = 1;
		document.getElementById("rendPACU").value = 2;
		document.getElementById("loiCommande").value = 4;
		document.getElementById("modeleMoteur").value = 2;
		document.getElementById("modeleBatterie").value = 2;
		updateTemp();
		document.getElementById("temperatureBatterie").value = 0;
	}
	
}






/**
 * Mise a jour du type de cycle unitaire a afficher
 */
function updateCycle(){
	cycle = document.getElementById("cycleUnitaire").value;
}
/**
 * Mise à jour de a ligne de la donnée à afficher pour le VE
 */
function updateAbsVE1(){
	absVE1 = (document.getElementById("absVE1").value);
}
function updateAbsVE2(){
	absVE2 = (document.getElementById("absVE2").value);
}
function updateAbsVE3(){
	absVE3 = (document.getElementById("absVE3").value);
}
/**
 * Mise à jour de a colonne de la donnée à afficher pour le VE
 */
function updateOrdVE1(){
	ordVE1 = (document.getElementById("ordVE1").value);
}
function updateOrdVE2(){
	ordVE2 = (document.getElementById("ordVE2").value);
}
function updateOrdVE3(){
	ordVE3 = (document.getElementById("ordVE3").value);
}
/**
 * Recuperation des parametres souhaites pour l'affichage pour le VE
 */
function AffVE1(){
	if(ordVE1==38 && tabvlim.length==0){
		alert("Les vitesses limites ne sont pas définies");
		return;
	}
	//console.log(dataConso);
	var t1;
	var t2;
	var nbPointConsoInstant = dataConso.length-1;
	((document.getElementById("temps1"+typeVehicule+"1").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"1").value) : t1 = 0;
	var point1 = rechTemps2(t1,dataConso);
	((document.getElementById("temps2"+typeVehicule+"1").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"1").value) : t2 = 0;
	if(t1 > nbPointConsoInstant){
		t1 = nbPointConsoInstant;
	}	
	if(t2 == 0 || t2 >nbPointConsoInstant){
		var point2 = nbPointConsoInstant;
	}
	else{
		var point2 = rechTemps2(t2,dataConso);
	}
	AffConso(absVE1,ordVE1,"chart_energieVE1",point1,point2,dataConso,myChartVE1);
}
function AffVE2(){
	/*var t1;
	var t2;
	((document.getElementById("temps1"+typeVehicule+"2").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"2").value) : t1 = 0;
	var point1 = rechTemps2(t1,dataConso);
	((document.getElementById("temps2"+typeVehicule+"2").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"2").value) : t2 = 0;
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTemps2(t2,dataConso);
	}*/
	point1 = 0;
	point2 = nbPointConso;
	AffConso(absVE2,ordVE2,"chart_energieVE2",point1,point2,dataConso,myChartVE2);
	
}

function AffVE3(){
	var t1;
	var t2;
	nbPointTotal=tabMemory[0].length-1;
	((document.getElementById("temps1"+typeVehicule+"3").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"3").value) : t1 = 0;
	var point1 = rechTempsVPU(t1,tabMemory);
	((document.getElementById("temps2"+typeVehicule+"3").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"3").value) : t2 = 0;
	if(t1 > nbPointTotal){
		t1 = nbPointTotal;
	}	
	if(t2 == 0 || t2 >nbPointTotal){
		var point2 = nbPointTotal;
	}
	else{
		var point2 = rechTempsVPU(t2,tabMemory);
	}
	AffConsoRev(absVE3,ordVE3,"chart_energieVE3",point1,point2,tabMemory,myChartVE3);
	
}
/**
 * Mise à jour de a ligne de la donnée à afficher pour le VT
 */
function updateAbsVT1(){
	absVT1 = (document.getElementById("absVT1").value);
}
function updateAbsVT2(){
	absVT2 = (document.getElementById("absVT2").value);
}
function updateAbsVT3(){
	absVT3 = (document.getElementById("absVT3").value);
}
/**
 * Mise à jour de a colonne de la donnée à afficher pour le VT
 */
function updateOrdVT1(){
	ordVT1 = (document.getElementById("ordVT1").value);
}
function updateOrdVT2(){
	ordVT2 = (document.getElementById("ordVT2").value);
}
function updateOrdVT3(){
	ordVT3 = (document.getElementById("ordVT3").value);
}
/**
 * Recuperation des parametres souhaites pour l'affichage pour le VT
 */
function AffVT1(){
	var t1;
	var t2;
	((document.getElementById("temps1"+typeVehicule+"1").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"1").value) : t1 = 0;
	var point1 = rechTemps(t1,dataConso);
	((document.getElementById("temps2"+typeVehicule+"1").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"1").value) : t2 = 0;
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTemps(t2,dataConso);
	}
	AffConso(absVT1,ordVT1,"chart_energieVT1",point1,point2,dataConso,myChartVT1);
}
function AffVT2(){
	/*var t1;
	var t2;
	((document.getElementById("temps1"+typeVehicule+"2").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"2").value) : t1 = 0;
	var point1 = rechTemps(t1,dataReellesVT);
	((document.getElementById("temps2"+typeVehicule+"2").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"2").value) : t2 = 0;
	if(t1 > nbLignesParamReelles){
		t1 = nbLignesParamReelles;
	}	
	if(t2 == 0 || t2 > nbLignesParamReelles){
		var point2 = nbLignesParamReelles;
	}
	else{
		var point2 = rechTemps(t2,dataReellesVT);
	}*/
	var point1 = 0;
	var point2 = nbLignesParamReelles;
	AffConso(absVT2,ordVT2,"chart_energieVT2",point1,point2,dataConso,myChartVT2);
	
}
function AffVT3(){
	var t1;
	var t2;
	var nbPointTotal=tabMemory[0].length-1;
	((document.getElementById("temps1"+typeVehicule+"3").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"3").value) : t1 = 0;
	if(t1 > nbPointTotal){
		t1 = nbPointTotal;
	}
	var point1 = rechTemps3(t1,tabMemory);
	((document.getElementById("temps2"+typeVehicule+"3").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"3").value) : t2 = 0;
		
	if(t2 == 0 || t2 >nbPointTotal){
		var point2 = nbPointTotal;
	}
	else{
		var point2 = rechTemps3(t2,tabMemory);
	}
	AffConsoRev(absVT3,ordVT3,"chart_energieVT3",point1,point2,tabMemory,myChartVT3);
}
/**
 * Mise à jour de a ligne de la donnée à afficher pour le VH
 */
function updateAbsVH1(){
	absVH1 = (document.getElementById("absVH1").value);
}
function updateAbsVH2(){
	absVH2 = (document.getElementById("absVH2").value);
}
/**
 * Mise à jour de a colonne de la donnée à afficher pour le VH
 */
function updateOrdVH1(){
	ordVH1 = (document.getElementById("ordVH1").value);
}
function updateOrdVH2(){
	ordVH2 = (document.getElementById("ordVH2").value);
}
/**
 * Recuperation des parametres souhaites pour l'affichage pour le VH
 */
function AffVH1(){
	var t1;
	var t2;
	((document.getElementById("temps1"+typeVehicule+"1").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"1").value) : t1 = 0;
	var point1 = rechTemps(t1,dataConso);
	((document.getElementById("temps2"+typeVehicule+"1").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"1").value) : t2 = 0;
	nbPointConso = dataBusHybride[0].length;
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTemps(t2,dataConso);
	}
	AffConsoVH(absVH1,ordVH1,"chart_energieVH1",point1,point2,dataBusHybridereverse,myChartVH1);
}
function AffVH2(){
	var t1;
	var t2;
	((document.getElementById("temps1"+typeVehicule+"2").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"2").value) : t1 = 0;
	var point1 = rechTemps(t1,dataConso);
	((document.getElementById("temps2"+typeVehicule+"2").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"2").value) : t2 = 0;
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTemps(t2,dataConso);
	}
	AffConso(absVH2,ordVH2,"chart_energieVH2",point1,point2,dataConso,myChartVH2);
	
}




/**
 * Mise à jour de la ligne de la donnée à afficher pour le VP
 */
function updateAbsVP1(){
	absVP1 = (document.getElementById("absVP1").value);
}
function updateAbsVP2(){
	absVP2 = (document.getElementById("absVP2").value);
}
/**
 * Mise à jour de la colonne de la donnée à afficher pour le VPU
 */
function updateOrdVPU1(){
	ordVPU1 = (document.getElementById("ordVPU1").value);
	if((ordVPU1==24 || ordVPU1==26) && document.getElementById("modeleBatterie").value=="2"){
		alert("Le modèle choisi de la batterie (UTBM) ne calcule ni ne prend en compte cette donnée.")
	}
}
function updateOrdVPU2(){
	ordVPU2 = (document.getElementById("ordVPU2").value);
}

function updateOrdVPU3(){
	ordVPU3 = (document.getElementById("ordVPU3").value);
	if((ordVPU3==24 || ordVPU3==26) && document.getElementById("modeleBatterie").value=="2"){
		alert("Le modèle choisi de la batterie (UTBM) ne calcule ni ne prend en compte cette donnée.")
	}
}



/**
 * Mise à jour de la ligne de la donnée à afficher pour le VPU
 */
function updateAbsVPU1(){
	absVPU1 = (document.getElementById("absVPU1").value);
}
function updateAbsVPU2(){
	absVPU2 = (document.getElementById("absVPU2").value);
}
function updateAbsVPU3(){
	absVPU3 = (document.getElementById("absVPU3").value);
}
/**
 * Mise à jour de la colonne de la donnée à afficher pour le VP
 */
function updateOrdVP1(){
	ordVP1 = (document.getElementById("ordVP1").value);
}
function updateOrdVP2(){
	ordVP2 = (document.getElementById("ordVP2").value);
}

/**
 * Recuperation des parametres souhaites pour l'affichage pour le VP
 */
var dataConsoPac1;

function AffVP1(){
	var t1;
	var t2;
	for( var i =1; i<dataConso.length;i++){               //rempli le tableau 36 (distance en km) avec les données du tableau 3(distance en m)
	    dataConso[i][36] = dataConso[i][3]/1000;				// car elles ne sont pas calculées avant.
	 }
	((document.getElementById("temps1"+typeVehicule+"1").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"1").value) : t1 = 0;
	var point1 = rechTempsVPU(t1,dataConsoPac);
	((document.getElementById("temps2"+typeVehicule+"1").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"1").value) : t2 = 0;
	nbPointConso = dataConsoPac[0].length;
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTempsVPU(t2,dataConsoPac);
	}
	dataConsoPac1 = reversetab(dataConsoPac,dataConsoPac.length,dataConsoPac[0].length);
	AffConsoPac(absVP1,ordVP1,"chart_energieVP1",point1,point2,dataConsoPac1,myChartVP1);
	dataConsoPac1 = reversetab(dataConsoPac1,dataConsoPac1[0].length,dataConsoPac.length);
}

function AffVP2(){
	var t1;
	var t2;
	for( var i =1; i<dataConso.length;i++){               //rempli le tableau 36 (distance en km) avec les données du tableau 3(distance en m)
	    dataConso[i][36] = dataConso[i][3]/1000;				// car elles ne sont pas calculées avant.
	 }
	((document.getElementById("temps1"+typeVehicule+"2").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"2").value) : t1 = 0;
	var point1 = rechTempsDataConso(t1,dataConso);
	((document.getElementById("temps2"+typeVehicule+"2").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"2").value) : t2 = 0;
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTempsDataConso(t2,dataConso);
	}
	var dataConsoBis = reversetab(dataConso,dataConso.length,dataConso[0].length);
	AffConsoRev(absVP2,ordVP2,"chart_energieVP2",point1,point2,dataConsoBis,myChartVP2);
}

/**
 * Recuperation des parametres souhaites pour l'affichage pour le VPU
 */
var dataConsoPacUTBM1;

function AffVPU1(){
	
	
	//console.log(dataConsoPacUTBM);
	var t1;
	var t2;
	((document.getElementById("temps1"+typeVehicule+"1").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"1").value) : t1 = 0;
	((document.getElementById("temps2"+typeVehicule+"1").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"1").value) : t2 = 0;
	nbPointConso = dataConsoPacUTBM[0].length;
	var point1 = rechTempsVPU(t1,dataConsoPacUTBM);
	
	
	
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTempsVPU(t2,dataConsoPacUTBM);
	}
	dataConsoPacUTBM1 = reversetab(dataConsoPacUTBM,dataConsoPacUTBM.length,dataConsoPacUTBM[0].length);
	AffConsoPacUTBM(absVPU1,ordVPU1,"chart_energieVPU1",point1,point2,dataConsoPacUTBM1,myChartVPU1);
	//dataConsoPacUTBM1 = reversetab(dataConsoPacUTBM1,dataConsoPacUTBM1[0].length,dataConsoPacUTBM.length);


}


/*
 * La function affVPU sert a afficher les données lors d'un calcul avec mise en mémoire des informations
 * 
 * initialement cette fonction permettait d'affciher en même temps les données calculées pour un trajet avec un profil et avec GMaps. la partie avec le 
 * profile vitesse restait en mémoire dans le graph VPU2
 * 
 * aujourd'hui cette fonctionnalité n'est plus utilisée 
 * la fonction affiche simplement la dernière portion de trajet calculée.
 * 
 */
function AffVPU2(){
	/**
	 * Initialement, AffVPU2 permattait de visualiser les données de dataConso
	 * Cependant j'ai décidé temporairement de modifier cette partie et d'afficher les valeurs d'un nouveau tableau 
	 * temporaire : dataConsoPacUTBMProfile
	 * Cela va permettre de visualiser en même temps les valeurs calculées à partir du profil généré par HEVES et celles avec le profil de vitesses réelles 
	 * JE laisse le code antérieur en commentaire, ce dernier est fonctionnel
	 * 
	 * 
	 * Attention il faut également modifier le fichier index pour les bonnes valeurs de sélection ainsi que la méthode absVP2() et ordVPU2()
	 * 
	 */
	var t1=0;
	var t2=0;
	/*for( var i =1; i<dataConsoPacUTBM.length;i++){               //rempli le tableau 36 (distance en km) avec les données du tableau 3(distance en m)
	    dataConso[i][36] = dataConso[i][3]/1000;				// car elles ne sont pas calculées avant.
	 }*/
	//((document.getElementById("temps1"+typeVehicule+"2").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"2").value) : t1 = 0;
	var point1 = rechTempsdataConso(t1,dataConsoPacUTBM);
	//((document.getElementById("temps2"+typeVehicule+"2").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"2").value) : t2 = 0;
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTempsVPU2(t2,dataConsoPacUTBM);
	}
	var dataConsoPacUTBM2 = reversetab(dataConsoPacUTBM,dataConsoPacUTBM.length,dataConsoPacUTBM[0].length);
	AffConsoPacUTBM(absVPU2,ordVPU2,"chart_energieVPU2",point1,point2,dataConsoPacUTBM2,myChartVPU2);
	
	
	// avec ce bout de code, le graph d'acquisition VPU ne sert plus qu'a afficher le résultat du précédant calcul avec profile vitesse
	// Il faut donc commencer par calculer avec le profil vitesse, puis calculer avec la segmentation pour comparer les deux methodes.
	
	
	/*
	window.tabResultant = JSON.parse(JSON.stringify(dataConsoPacUTBM));				//on utilise cette commande pour 'deepCopy' (copier intégralement pour avoir deux objets différents ne referant pas au même pointeur)
																					// sans ça, les variables contenant le tableau dataConsoPacUTBM seraient tous liée au même pointeur et on ne pourrait pas afficher deux tableaux différent en même temps
																					
																					// source : http://www.oranlooney.com/post/deep-copy-javascript/
	if(typeResolution==1){    				//Si on utilise le profile vitesse on stock le résultat pour le comparer avec celui de HEVES
		var tabUtilise = tabResultant;
		window.tabStocke=tabResultant; 
	}else{									// si on utilise la segmentation, on récupère le tableau stocké temporairement
		var tabUtilise = tabStocke;
	}
	window.tableauTamponProfile=tabUtilise;
	
	var t1=0;
	var t2=0;
	//((document.getElementById("temps1"+typeVehicule+"2").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"2").value) : t1 = 0;
	//((document.getElementById("temps2"+typeVehicule+"2").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"2").value) : t2 = 0;
	nbPointConso = tableauTamponProfile[0].length;
	var point1 = rechTempsVPU2(t1,tableauTamponProfile);
	
	
	
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTempsVPU2(t2,tableauTamponProfile);
	}
	var tableauTamponProfilerev = reversetab(tableauTamponProfile,tableauTamponProfile.length,tableauTamponProfile[0].length);
	AffConsoPacUTBM(absVPU2,ordVPU2,"chart_energieVPU2",point1,point2,tableauTamponProfilerev,myChartVPU2);
	//dataConsoPacUTBMProfile1 = reversetab(dataConsoPacUTBMProfile1,dataConsoPacUTBMProfile1[0].length,dataConsoPacUTBMProfile.length);
*/
}
var tabMemory=[];
/*
 * La function affVPU3 sert a afficher les données lors d'un calcul avec mise en mémoire des informations
 * Lors du calcul en ligne les information sont ajoutées au fur et mesure dans tabMemory
 * On affiche alors les informations de ce tableau
 */


function AffVPU3(){
	//console.log("tabMemory inti", tabMemory,tabMemory.lenght)
	
	
		
	
	//console.log("dataPacUTBM",dataConsoPacUTBMClone);
	//console.log("tabMemory ",tabMemory);
	
	var t1;
	var t2;
	((document.getElementById("temps1"+typeVehicule+"3").value) != "")	? t1 = (document.getElementById("temps1"+typeVehicule+"3").value) : t1 = 0;
	((document.getElementById("temps2"+typeVehicule+"3").value) != "")	? t2 = (document.getElementById("temps2"+typeVehicule+"3").value) : t2 = 0;
	nbPointConso = tabMemory[0].length;
	var point1 = rechTempsVPU3(t1,tabMemory);
	
	
	
	if(t1 > nbPointConso){
		t1 = nbPointConso;
	}	
	if(t2 == 0 || t2 >nbPointConso){
		var point2 = nbPointConso;
	}
	else{
		var point2 = rechTempsVPU3(t2,tabMemory);
	}
	tabMemory1 = reversetab(tabMemory,tabMemory.length,tabMemory[0].length);
	AffConsoPacUTBM(absVPU3,ordVPU3,"chart_energieVPU3",point1,point2,tabMemory1,myChartVPU3);
	//tabMemory1 = reversetab(tabMemory1,tabMemory1[0].length,tabMemory1.length);
	

}


/**
 * Desactivation des gradients et retrace du graphique
 */
function disableClimbs(){
	var elone = document.getElementById("disone");
	var eltwo = document.getElementById("distwo");
	var elthree = document.getElementById("disthree");
	var elfour = document.getElementById("disfour");
	var eleone = document.getElementById("gradeone");
	var eletwo = document.getElementById("gradetwo");
	var elethree = document.getElementById("gradethree");
	var elefour = document.getElementById("gradefour");
	elone.disabled = !elone.disabled;
	eltwo.disabled = !eltwo.disabled;
	elthree.disabled = !elthree.disabled;
	elfour.disabled = !elfour.disabled;
	eleone.disabled = !eleone.disabled;
	eletwo.disabled = !eletwo.disabled;
	elethree.disabled = !elethree.disabled;
	elefour.disabled = !elefour.disabled;
	disabled = !disabled;
	if(disabled){
		disone = 0;
	}
	else{
		disone = parseFloat(document.getElementById("disone").value);
	}
	plotElevation(elevations, elevation_status);
}
/**
 * Mise a jour de l'elevation et du graphique d'elevation
 */
function updateClimbs(){
	disone = parseFloat(document.getElementById("disone").value);
	distwo = parseFloat(document.getElementById("distwo").value);
	disthree = parseFloat(document.getElementById("disthree").value);
	disfour = parseFloat(document.getElementById("disfour").value);
	gradeone = parseFloat(document.getElementById("gradeone").value);
	gradetwo = parseFloat(document.getElementById("gradetwo").value);
	gradethree = parseFloat(document.getElementById("gradethree").value);
	gradefour = parseFloat(document.getElementById("gradefour").value);
	plotElevation(elevations, elevation_status);
}
/**
 * Suppression des minimums et maximum de gradient mise a jour du graphique
 */
function hideMinMax() {
	hideminmax = !hideminmax;
	plotElevation(elevations, elevation_status);
}
/**
 * Mise a jour des gradients
 */
function updateGradient(){
	gradient_orange = parseFloat(document.getElementById("orange").value);
	gradient_red = parseFloat(document.getElementById("red").value);
	plotElevation(elevations, elevation_status);
}
/**
 * Mise a jour du mode de transport et recalcul de l'itineraire
 */
function changeTravelMode(){
	mode = document.getElementById("mode"+typeVehicule).value;
	saveWaypoints();
	calcRoute();
}	
/**
 * Mise a jour du systeme de mesure Attention tous les calculs energetiques sont fait dans le systeme metrique
 */
function changeSystem() {
	if(document.getElementById("system"+typeVehicule).value == "METRIC") {
		system_mi = 1;
		system_ft = 1;
		str_mi = "km";
		str_ft = "m";
		for(var i = 5; i < 11; i++) {
			document.getElementById("disone")[i - 5].text = "> "+ i + "km";
			document.getElementById("disone")[i - 5].value = i * 1000;
		}
		for(var i = 2; i < 8; i++) {
			document.getElementById("distwo")[i - 2].text = "> "+ i + "km";
			document.getElementById("distwo")[i - 2].value = i * 1000;
		}
		for(var i = 1; i < 7; i++) {
			document.getElementById("disthree")[i - 1].text = "> "+ i + "km";
			document.getElementById("disthree")[i - 1].value = i * 1000;
		}
		document.getElementById("disfour")[0].text = "> "+ "500m";
		document.getElementById("disfour")[0].value = 500;
		for(var i = 1; i < 6; i++) {
			document.getElementById("disfour")[i].text = "> "+ i + "km";
			document.getElementById("disfour")[i].value = i * 1000;
		}
		document.getElementById("disone").options[1].selected = true;
		document.getElementById("distwo").options[2].selected = true;
		document.getElementById("disthree").options[1].selected = true;
		document.getElementById("disfour").options[1].selected = true;
	}
	else {
		system_mi = 1 / 1.609344;
		system_ft = 1 / 0.3048;
		str_mi = "mi";
		str_ft = "ft";
		for(var i = 3; i < 9; i++) {
			document.getElementById("disone")[i - 3].text = "> "+ i + "mi";
			document.getElementById("disone")[i - 3].value = i * 1000 * 1.609344;
		}
		for(var i = 2; i < 8; i++) {
			document.getElementById("distwo")[i - 2].text = "> "+ i + "mi";
			document.getElementById("distwo")[i - 2].value = i * 1000 * 1.609344;
		}
		document.getElementById("disthree")[0].text = "> "+ "0.5mi";
		document.getElementById("disthree")[0].value = 0.5 * 1000 * 1.609344;
		for(var i = 1; i < 6; i++) {
			document.getElementById("disthree")[i].text = "> "+ i + "mi";
			document.getElementById("disthree")[i].value = i * 1000 * 1.609344;
		}
		document.getElementById("disfour")[0].text = "> "+ "0.25mi";
		document.getElementById("disfour")[0].value = 0.25 * 1000 * 1.609344;
		document.getElementById("disfour")[1].text = "> "+ "0.5mi";
		document.getElementById("disfour")[1].value = 0.5 * 1000 * 1.609344;
		for(var i = 1; i < 5; i++) {
			document.getElementById("disfour")[i + 1].text = "> "+ i + "mi";
			document.getElementById("disfour")[i + 1].value = i * 1000 * 1.609344;
		}
		document.getElementById("disone")[1].selected = true;
		document.getElementById("distwo")[0].selected = true;
		document.getElementById("disthree")[1].selected = true;
		document.getElementById("disfour")[1].selected = true;	
	}
	plotElevation(elevations, elevation_status);

}
/**
 * Definition de la nouvelle origine par clic droit sur la carte
 */
function setOrigin() {
	if(start == null && end!= null) {
		directionsDisplay.setMap(map);	
	}
	else if(end != null && start != null){
		saveWaypoints();
	}
	start = rightclicklatlng;
	calcRoute();
	infowindow.close();
}
/**
 * Definition de la nouvelle arrivee par clic droit sur la carte
 */
function setDestination() {
	if(end == null && start!= null) {
		directionsDisplay.setMap(map);	
	}	
	else if(end != null && start != null){
		saveWaypoints();
	}
	end = rightclicklatlng;
	calcRoute();	
	infowindow.close();
}
/**
 * Inversion du depart et de l'arrivee
 */
function invertRoute(){
	saveWaypoints();
	waypoints.reverse();
	
	var temp = document.getElementById('autocompleteDeparture'+typeVehicule).value;
	document.getElementById('autocompleteDeparture'+typeVehicule).value = document.getElementById('autocompleteArrival'+typeVehicule).value;
	document.getElementById('autocompleteArrival'+typeVehicule).value = temp;
	
	var temp2 = start;
	start = end;
	end = temp2;
	
	var temp3 = departure_place;
	departure_place = arrival_place;
	arrival_place = temp3;
	
	calcRoute();
}

/**
 * Mise a jour des parametres
 */
function parameterUpdate (){
	waypoints = [];
	if(get_url_param('slat') != '') {
	start = new google.maps.LatLng(get_url_param('slat'),get_url_param('slng'));
	end = new google.maps.LatLng(get_url_param('elat'),get_url_param('elng'));
	mode = get_url_param('mode');
	if (mode == "BICYCLING") {
 		 document.getElementById("mode"+typeVehicule).options[2].selected = true;
	}
	if (mode == "WALKING") {
 		 document.getElementById("mode"+typeVehicule).options[1].selected = true;
	}
	}
	for(var i = 0; i < 10; i++) {
		if(get_url_param('wp' + i + 'lat') != '') {
			waypoints.push({
			location: new google.maps.LatLng(get_url_param('wp' + i + 'lat'),get_url_param('wp' + i + 'lng')),
			stopover:false
		});
		}
	}
}



function majChoixVehicule(choix){
	
	if(choix == 1 && typeVehicule != "VE"){	// Véhicule
											// électrique
		typeVehicule = 'VE';
		initialize(); // on
						// réinitialise
						// les
						// variables
						// pour
						// un
						// véhicule
						// electrique
		// on
		// affiche
		// les
		// onglets,
		// les
		// tableaux
		// qui
		// concernent
		// les
		// véhicules
		// electriques
		document.getElementById("messageChoixVehicule").style.display='none';
		document.getElementById("tabbertabsVT").style.display='none';
		document.getElementById("tabbertabsVE").style.display='';
		document.getElementById("tabbertabsVH").style.display='none';
		document.getElementById("tabbertabsVP").style.display='none';
		document.getElementById("tabbertabsVPU").style.display='none';
		document.getElementById("tabbertabsVT2").style.display='none';
		document.getElementById("tabbertabsVE2").style.display='';
		document.getElementById("tabbertabsVH2").style.display='none';
		document.getElementById("tabbertabsVP2").style.display='none';
		document.getElementById("tabbertabsVPU2").style.display='none';
		document.getElementById("tabbertabsVT3").style.display='none';
		document.getElementById("tabbertabsVE3").style.display='';
		document.getElementById("tabbertabsVH3").style.display='none';
		document.getElementById("tabbertabsVP3").style.display='none';
		document.getElementById("tabbertabsVPU3").style.display='none';
		document.getElementById("table_elevation_divVT").style.display='none';
		document.getElementById("table_elevation_divVE").style.display='';
		document.getElementById("table_elevation_divVH").style.display='none';
		document.getElementById("table_elevation_divVPU").style.display='none';
		// document.getElementById("table_elevation_divVP").style.display='none';
		document.getElementById("table_etapes_divVT").style.display='none';
		document.getElementById("table_etapes_divVE").style.display='';
		document.getElementById("table_etapes_divVH").style.display='none';
		document.getElementById("table_etapes_divVPU").style.display='none';
		// document.getElementById("table_etapes_divVP").style.display='none';
		document.getElementById("syntheseEtapeVE").style.display = '';
		document.getElementById("syntheseEtapeVT").style.display = 'none';
		document.getElementById("syntheseEtapeVH").style.display = 'none';
		document.getElementById("syntheseEtapeVP").style.display = 'none';
		document.getElementById("syntheseEtapeVPU").style.display = 'none';
		
		document.getElementById("tabbertabsVALL").style.display = 'none';
		document.getElementById("tabbertabsProfileVE").style.display = '';
		document.getElementById("tabbertabsProfileVT").style.display = 'none';
		document.getElementById("tabbertabsProfileVH").style.display = 'none';
		document.getElementById("tabbertabsProfileVP").style.display = 'none';
		document.getElementById("tabbertabsProfileVPU").style.display = 'none';
		
		// On
		// récupère
		// dans
		// la
		// base
		// de
		// données
		// la
		// liste
		// des
		// paramètres
		// des
		// voitures
		// électriques
		loadCarElecParam();
		// directionsDisplay.setDirections({routes:
		// []});
	}
	if(choix == 2 && typeVehicule != "VT"){	// Véhicule
											// thermique
		typeVehicule='VT';
		initialize();	// on
						// réinitialise
						// les
						// variables
						// pour
						// un
						// véhicule
						// thermique
		// on
		// affiche
		// les
		// onglets,
		// les
		// tableaux
		// qui
		// concernent
		// les
		// véhicules
		// thermiques
		document.getElementById("messageChoixVehicule").style.display='none';
		document.getElementById("tabbertabsVE").style.display='none';
		document.getElementById("tabbertabsVT").style.display='';
		document.getElementById("tabbertabsVH").style.display='none';
		document.getElementById("tabbertabsVP").style.display='none';
		document.getElementById("tabbertabsVPU").style.display='none';
		document.getElementById("tabbertabsVE2").style.display='none';
		document.getElementById("tabbertabsVT2").style.display='';
		document.getElementById("tabbertabsVH2").style.display='none';
		document.getElementById("tabbertabsVP2").style.display='none';
		document.getElementById("tabbertabsVPU2").style.display='none';
		document.getElementById("tabbertabsVE3").style.display='none';
		document.getElementById("tabbertabsVT3").style.display='';
		document.getElementById("tabbertabsVH3").style.display='none';
		document.getElementById("tabbertabsVP3").style.display='none';
		document.getElementById("tabbertabsVPU3").style.display='none';
		document.getElementById("table_elevation_divVE").style.display='none';
		document.getElementById("table_elevation_divVH").style.display='none';
		document.getElementById("table_elevation_divVT").style.display='';
		document.getElementById("table_elevation_divVPU").style.display='none';
		// document.getElementById("table_elevation_divVP").style.display='none';
		document.getElementById("table_etapes_divVE").style.display='none';
		document.getElementById("table_etapes_divVT").style.display='';
		document.getElementById("table_etapes_divVH").style.display='none';
		document.getElementById("table_etapes_divVPU").style.display='none';
		// document.getElementById("table_etapes_divVP").style.display='none';
		document.getElementById("syntheseEtapeVE").style.display = 'none';
		document.getElementById("syntheseEtapeVT").style.display = '';	
		document.getElementById("syntheseEtapeVH").style.display = 'none';
		document.getElementById("syntheseEtapeVP").style.display = 'none';
		document.getElementById("syntheseEtapeVPU").style.display = 'none';
		
		document.getElementById("tabbertabsVALL").style.display = 'none';
		document.getElementById("tabbertabsProfileVE").style.display = 'none';
		document.getElementById("tabbertabsProfileVT").style.display = '';
		document.getElementById("tabbertabsProfileVH").style.display = 'none';
		document.getElementById("tabbertabsProfileVP").style.display = 'none';
		document.getElementById("tabbertabsProfileVPU").style.display = 'none';


		// On
		// récupère
		// dans
		// la
		// base
		// de
		// données
		// la
		// liste
		// des
		// paramètres
		// des
		// voitures
		// thermiques
		loadCarThermParam();
		
		// boolOBD
		// =
		// false;
		// majDonneesSortie();
		// directionsDisplay.setDirections({routes:
		// []});
	}
	if(choix == 3 && typeVehicule != "VH"){
		typeVehicule = 'VH';
		initialize();	// on
						// réinitialise
						// les
						// variables
						// pour
						// un
						// véhicule
						// hybride
		// on
		// affiche
		// les
		// onglets,
		// les
		// tableaux
		// qui
		// concernent
		// les
		// véhicules
		// hybrides
		document.getElementById("messageChoixVehicule").style.display='none';
		document.getElementById("tabbertabsVE").style.display='none';
		document.getElementById("tabbertabsVT").style.display='none';
		document.getElementById("tabbertabsVH").style.display='';
		document.getElementById("tabbertabsVP").style.display='none';
		document.getElementById("tabbertabsVPU").style.display='none';
		document.getElementById("tabbertabsVE2").style.display='none';
		document.getElementById("tabbertabsVT2").style.display='none';
		document.getElementById("tabbertabsVH2").style.display='';
		document.getElementById("tabbertabsVP2").style.display='none';
		document.getElementById("tabbertabsVPU2").style.display='none';
		document.getElementById("tabbertabsVE3").style.display='none';
		document.getElementById("tabbertabsVT3").style.display='none';
		document.getElementById("tabbertabsVH3").style.display='';
		document.getElementById("tabbertabsVP3").style.display='none';
		document.getElementById("tabbertabsVPU3").style.display='none';
		document.getElementById("table_elevation_divVE").style.display='none';
		document.getElementById("table_elevation_divVT").style.display='none';
		document.getElementById("table_elevation_divVH").style.display='';
		document.getElementById("table_elevation_divVPU").style.display='none';
		// document.getElementById("table_elevation_divVP").style.display='none';
		document.getElementById("table_etapes_divVE").style.display='none';
		document.getElementById("table_etapes_divVT").style.display='none';
		document.getElementById("table_etapes_divVH").style.display='';
		document.getElementById("table_etapes_divVPU").style.display='none';
		// document.getElementById("table_etapes_divVP").style.display='none';
		document.getElementById("syntheseEtapeVE").style.display = 'none';
		document.getElementById("syntheseEtapeVT").style.display = 'none';	
		document.getElementById("syntheseEtapeVH").style.display = '';
		document.getElementById("syntheseEtapeVP").style.display = 'none';
		document.getElementById("syntheseEtapeVPU").style.display = 'none';
		
		document.getElementById("tabbertabsVALL").style.display = 'none';
		document.getElementById("tabbertabsProfileVE").style.display = 'none';
		document.getElementById("tabbertabsProfileVT").style.display = 'none';
		document.getElementById("tabbertabsProfileVH").style.display = '';
		document.getElementById("tabbertabsProfileVP").style.display = 'none';
		document.getElementById("tabbertabsProfileVPU").style.display = 'none';

		
				
		// On
		// récupère
		// dans
		// la
		// base
		// de
		// données
		// la
		// liste
		// des
		// paramètres
		// des
		// voitures
		// hybrides
		// loadCarHybridParam();
	
		// directionsDisplay.setDirections({routes:
		// []});
	}
	if(choix == 4 && typeVehicule != "VP"){
		typeVehicule = 'VP';
		initialize();	// on
						// réinitialise
						// les
						// variables
						// pour
						// un
						// véhicule
						// hybride
						// pac
		// on
		// affiche
		// les
		// onglets,
		// les
		// tableaux
		// qui
		// concernent
		// les
		// véhicules
		// hybrides
		document.getElementById("messageChoixVehicule").style.display='none';
		document.getElementById("tabbertabsVE").style.display='none';
		document.getElementById("tabbertabsVT").style.display='none';
		document.getElementById("tabbertabsVH").style.display='none';
		document.getElementById("tabbertabsVPU").style.display='none';
		document.getElementById("tabbertabsVP").style.display='';
		document.getElementById("tabbertabsVE2").style.display='none';
		document.getElementById("tabbertabsVT2").style.display='none';
		document.getElementById("tabbertabsVH2").style.display='none';
		document.getElementById("tabbertabsVPU2").style.display='none';
		document.getElementById("tabbertabsVP2").style.display='';
		document.getElementById("tabbertabsVE3").style.display='none';
		document.getElementById("tabbertabsVT3").style.display='none';
		document.getElementById("tabbertabsVH3").style.display='none';
		document.getElementById("tabbertabsVPU3").style.display='none';
		document.getElementById("tabbertabsVP3").style.display='';
		document.getElementById("table_elevation_divVE").style.display='none';
		document.getElementById("table_elevation_divVT").style.display='none';
		document.getElementById("table_elevation_divVH").style.display='none';
		document.getElementById("table_elevation_divVPU").style.display='none';
		// document.getElementById("table_elevation_divVP").style.display='';
		document.getElementById("table_etapes_divVE").style.display='none';
		document.getElementById("table_etapes_divVT").style.display='none';
		document.getElementById("table_etapes_divVH").style.display='none';
		document.getElementById("table_etapes_divVPU").style.display='none';
		// document.getElementById("table_etapes_divVP").style.display='';
		document.getElementById("syntheseEtapeVE").style.display = 'none';
		document.getElementById("syntheseEtapeVT").style.display = 'none';	
		document.getElementById("syntheseEtapeVH").style.display = 'none';
		document.getElementById("syntheseEtapeVPU").style.display = 'none';
		document.getElementById("syntheseEtapeVP").style.display = '';
		
		document.getElementById("tabbertabsVALL").style.display = 'none';
		document.getElementById("tabbertabsProfileVE").style.display = 'none';
		document.getElementById("tabbertabsProfileVT").style.display = 'none';
		document.getElementById("tabbertabsProfileVH").style.display = 'none';
		document.getElementById("tabbertabsProfileVP").style.display = '';
		document.getElementById("tabbertabsProfileVPU").style.display = 'none';
						
		// On
		// récupère
		// dans
		// la
		// base
		// de
		// données
		// la
		// liste
		// des
		// paramètres
		// des
		// voitures
		// hybrides
		loadCarPacParam();
	
		// directionsDisplay.setDirections({routes:
		// []});
	}
	if(choix == 5 && typeVehicule != "VPU"){
		typeVehicule = 'VPU';
		initialize();	// on
						// réinitialise
						// les
						// variables
						// pour
						// un
						// véhicule
						// hybride
						// pac
						// utbm
		// on
		// affiche
		// les
		// onglets,
		// les
		// tableaux
		// qui
		// concernent
		// les
		// véhicules
		// hybrides
		// PAC
		// UTBM
		document.getElementById("messageChoixVehicule").style.display='none';
		document.getElementById("tabbertabsVE").style.display='none';
		document.getElementById("tabbertabsVT").style.display='none';
		document.getElementById("tabbertabsVH").style.display='none';
		document.getElementById("tabbertabsVP").style.display='none';
		document.getElementById("tabbertabsVPU").style.display='';
		document.getElementById("tabbertabsVE2").style.display='none';
		document.getElementById("tabbertabsVT2").style.display='none';
		document.getElementById("tabbertabsVH2").style.display='none';
		document.getElementById("tabbertabsVP2").style.display='none';
		document.getElementById("tabbertabsVPU2").style.display='';
		document.getElementById("tabbertabsVE3").style.display='none';
		document.getElementById("tabbertabsVT3").style.display='none';
		document.getElementById("tabbertabsVH3").style.display='none';
		document.getElementById("tabbertabsVP3").style.display='none';
		document.getElementById("tabbertabsVPU3").style.display='';
		document.getElementById("table_elevation_divVE").style.display='none';
		document.getElementById("table_elevation_divVT").style.display='none';
		document.getElementById("table_elevation_divVH").style.display='none';
		// document.getElementById("table_elevation_divVP").style.display='none';
		document.getElementById("table_elevation_divVPU").style.display='';
		document.getElementById("table_etapes_divVE").style.display='none';
		document.getElementById("table_etapes_divVT").style.display='none';
		document.getElementById("table_etapes_divVH").style.display='none';
		// document.getElementById("table_etapes_divVP").style.display='none';
		document.getElementById("table_etapes_divVPU").style.display='';
		document.getElementById("syntheseEtapeVE").style.display = 'none';
		document.getElementById("syntheseEtapeVT").style.display = 'none';	
		document.getElementById("syntheseEtapeVH").style.display = 'none';
		document.getElementById("syntheseEtapeVP").style.display = 'none';
		document.getElementById("syntheseEtapeVPU").style.display = '';
		
		document.getElementById("tabbertabsVALL").style.display = 'none';
		document.getElementById("tabbertabsProfileVE").style.display = 'none';
		document.getElementById("tabbertabsProfileVT").style.display = 'none';
		document.getElementById("tabbertabsProfileVH").style.display = 'none';
		document.getElementById("tabbertabsProfileVP").style.display = 'none';
		document.getElementById("tabbertabsProfileVPU").style.display = '';
						
		// On
		// récupère
		// dans
		// la
		// base
		// de
		// données
		// la
		// liste
		// des
		// paramètres
		// des
		// voitures
		// hybrides
		loadCarPacParam();
	
		// directionsDisplay.setDirections({routes:
		// []});
	
	}
	else{document.getElementById("tabbertabsVALL").style.display='';}
	document.getElementById("tabbertabsComparaison").style.display='none';
	tabMemory=[];
	lastVitesseMemory=[];
	E_batt_r_U_REF=undefined;
	lastAltitude=undefined;
}
/**
 * Affichage des boutons hybrides
 */
function AffichageBouttons(){
	if(boolAffichageBouttons){
		document.getElementById("buttonlecture11").style.visibility='visible';
		document.getElementById("buttonlecture22").style.visibility='visible';
		document.getElementById("buttonlecture33").style.visibility='visible';
		document.getElementById("buttonlecture44").style.visibility='visible';
		boolAffichageBouttons = false;
	}
	else{
		document.getElementById("buttonlecture11").style.visibility='hidden';
		document.getElementById("buttonlecture22").style.visibility='hidden';
		document.getElementById("buttonlecture33").style.visibility='hidden';
		document.getElementById("buttonlecture44").style.visibility='hidden';
		boolAffichageBouttons = true;
	}
	
}

function DesaffichageBoutons(){
	document.getElementById("buttonlecture11").style.visibility='hidden';
	document.getElementById("buttonlecture22").style.visibility='hidden';
	document.getElementById("buttonlecture33").style.visibility='hidden';
	document.getElementById("buttonlecture44").style.visibility='hidden';
	boolAffichageBouttons = true;
}

/**
 * Mise a jour des variables accessibles dans les graphiques
 */
/*
 * function majDonneesSortie(){ for(i=32;i<=40;i++){ document.getElementById('ord').options[i].removeAttribute("disabled"); } if ( (dataConso.length>0 && nbPointConso != undefined) && (dataConso[nbPointConso][73]>dataConso[nbPointConso][54]) ){ for(i=41;i<=49;i++){ document.getElementById('ord').options[i].setAttribute("disabled",""); } } else{ for(i=41;i<=49;i++){
 * document.getElementById('ord').options[i].removeAttribute("disabled"); } } if(boolTrajet){ for(i=50;i<=56;i++){ document.getElementById('ord').options[i].removeAttribute("disabled"); } /*if(dataConso[nbPointConso][96]>dataConso[nbPointConso][82]){ for(i=57;i<=63;i++){ document.getElementById('ord').options[i].setAttribute("disabled",""); } } else{
 *//*
	 * for(i=57;i<=63;i++){ document.getElementById('ord').options[i].removeAttribute("disabled"); } //} } else{ for(i=50;i<=63;i++){ document.getElementById('ord').options[i].setAttribute("disabled",""); } } }
	 */
// ----------------------------------------------------------------------------------
// Gestion
// des
// donnees
// vehicule
// -----------------------------------------------------------------
/**
 * Pour verifier la validite des champs avant de sauvegarder le set de parametre
 * 
 * @returns true si valide, false sinon
 */
function checkParamFields(){
	if(typeVehicule=="VE"){
		if (document.getElementById('vehTypeId'+typeVehicule).value != ""
			&& document.getElementById('Masse')+typeVehicule.value != ""
			&& document.getElementById('MasseSup')+typeVehicule.value != ""
			&& document.getElementById('SCx'+typeVehicule).value != ""
			&& document.getElementById('RayonRoue'+typeVehicule).value != ""
			&& document.getElementById('res_roulement'+typeVehicule).value != ""
			&& document.getElementById('Paux'+typeVehicule).value != ""
			&& document.getElementById('rend_gear'+typeVehicule).value != ""
			&& document.getElementById('Pmotorrated'+typeVehicule).value != ""
			&& document.getElementById('rend_elec'+typeVehicule).value != ""
			&& document.getElementById('capBatterie'+typeVehicule).value != ""
			&& document.getElementById('PmotorMax'+typeVehicule).value != ""
			&& document.getElementById('trRatio'+typeVehicule).value != ""	
			&& document.getElementById('vmin'+typeVehicule).value != ""
			&& document.getElementById('vmax'+typeVehicule).value != ""
			&& document.getElementById('seuilDec'+typeVehicule).value != ""
			&& document.getElementById('coefInertie'+typeVehicule).value != ""
		    && isNumeric(document.getElementById('Masse'+typeVehicule).value)
		    && isNumeric(document.getElementById('MasseSup'+typeVehicule).value)
			&& isNumeric(document.getElementById('SCx'+typeVehicule).value)
			&& isNumeric(document.getElementById('RayonRoue'+typeVehicule).value) 
			&& isNumeric(document.getElementById('res_roulement'+typeVehicule).value) 
			&& isNumeric(document.getElementById('Paux'+typeVehicule).value)
			&& isNumeric(document.getElementById('rend_gear'+typeVehicule).value)
			&& isNumeric(document.getElementById('Pmotorrated'+typeVehicule).value)
			&& isNumeric(document.getElementById('PmotorMax'+typeVehicule).value)
			&& isNumeric(document.getElementById('trRatio'+typeVehicule).value)
			&& isNumeric(document.getElementById('rend_elec'+typeVehicule).value)
			&& isNumeric(document.getElementById('capBatterie'+typeVehicule).value)
			&& isNumeric(document.getElementById('vmin'+typeVehicule).value)
			&& isNumeric(document.getElementById('vmax'+typeVehicule).value)
			&& isNumeric(document.getElementById('seuilDec'+typeVehicule).value)
			&& isNumeric(document.getElementById('coefInertie'+typeVehicule).value))
				{
				return true;
				}
			else
				return false;
	}
	else if(typeVehicule=="VT"){
		if (document.getElementById('vehTypeId'+typeVehicule).value != ""
			&& document.getElementById('Masse'+typeVehicule).value != ""
			&& document.getElementById('MasseSup'+typeVehicule).value != ""
			&& document.getElementById('SCx'+typeVehicule).value != ""
			&& document.getElementById('RayonRoue'+typeVehicule).value != ""
			&& document.getElementById('res_roulement'+typeVehicule).value != ""
			&& document.getElementById('rend_gear'+typeVehicule).value != ""
			&& document.getElementById('coefInertie'+typeVehicule).value != ""
			&& document.getElementById('nbRapportsVitesse'+typeVehicule).value != ""				
			&& isNumeric(document.getElementById('Masse'+typeVehicule).value)
			&& isNumeric(document.getElementById('MasseSup'+typeVehicule).value)
			&& isNumeric(document.getElementById('SCx'+typeVehicule).value)
			&& isNumeric(document.getElementById('RayonRoue'+typeVehicule).value) 
			&& isNumeric(document.getElementById('res_roulement'+typeVehicule).value) 
			&& isNumeric(document.getElementById('rend_gear'+typeVehicule).value)
			&& isNumeric(document.getElementById('coefInertie'+typeVehicule).value)
			&& isNumeric(document.getElementById('nbRapportsVitesse'+typeVehicule).value))
			
		{
			return true;
			}
		else
			return false;					
	}
	else if(typeVehicule=="VP"){
		if (document.getElementById('vehTypeId'+typeVehicule).value != ""
			&& document.getElementById('Masse')+typeVehicule.value != ""
			&& document.getElementById('MasseSup')+typeVehicule.value != ""
			&& document.getElementById('powerThreshold')+typeVehicule.value != ""
			&& document.getElementById('speedThreshold')+typeVehicule.value != ""
			&& document.getElementById('acceleratorThreshold')+typeVehicule.value != ""
			&& document.getElementById('SCx'+typeVehicule).value != ""
			&& document.getElementById('RayonRoue'+typeVehicule).value != ""
			&& document.getElementById('res_roulement'+typeVehicule).value != ""
			&& document.getElementById('Paux'+typeVehicule).value != ""
			&& document.getElementById('rend_gear'+typeVehicule).value != ""
			&& document.getElementById('Pmotorrated'+typeVehicule).value != ""
			&& document.getElementById('rend_elec'+typeVehicule).value != ""
			&& document.getElementById('capBatterie'+typeVehicule).value != ""
			&& document.getElementById('PmotorMax'+typeVehicule).value != ""
			&& document.getElementById('trRatio'+typeVehicule).value != ""	
			&& document.getElementById('vmin'+typeVehicule).value != ""
			&& document.getElementById('vmax'+typeVehicule).value != ""
			&& document.getElementById('seuilDec'+typeVehicule).value != ""
			&& document.getElementById('coefInertie'+typeVehicule).value != ""
		    && isNumeric(document.getElementById('Masse'+typeVehicule).value)
		    && isNumeric(document.getElementById('MasseSup'+typeVehicule).value)	
		    && isNumeric(document.getElementById('powerThreshold'+typeVehicule).value)
		    && isNumeric(document.getElementById('speedThreshold'+typeVehicule).value)
		    && isNumeric(document.getElementById('acceleratorThreshold'+typeVehicule).value)
			&& isNumeric(document.getElementById('SCx'+typeVehicule).value)
			&& isNumeric(document.getElementById('RayonRoue'+typeVehicule).value) 
			&& isNumeric(document.getElementById('res_roulement'+typeVehicule).value) 
			&& isNumeric(document.getElementById('Paux'+typeVehicule).value)
			&& isNumeric(document.getElementById('rend_gear'+typeVehicule).value)
			&& isNumeric(document.getElementById('Pmotorrated'+typeVehicule).value)
			&& isNumeric(document.getElementById('PmotorMax'+typeVehicule).value)
			&& isNumeric(document.getElementById('trRatio'+typeVehicule).value)
			&& isNumeric(document.getElementById('rend_elec'+typeVehicule).value)
			&& isNumeric(document.getElementById('capBatterie'+typeVehicule).value)
			&& isNumeric(document.getElementById('vmin'+typeVehicule).value)
			&& isNumeric(document.getElementById('vmax'+typeVehicule).value)
			&& isNumeric(document.getElementById('seuilDec'+typeVehicule).value)
			&& isNumeric(document.getElementById('coefInertie'+typeVehicule).value))
				{
				return true;
				}
			else
				return false;
	}
	
	else if(typeVehicule=="VPU"){
		if (document.getElementById('vehTypeId'+typeVehicule).value != ""
			&& document.getElementById('Masse')+typeVehicule.value != ""
			&& document.getElementById('MasseSup')+typeVehicule.value != ""
			&& document.getElementById('powerThreshold')+typeVehicule.value != ""
			&& document.getElementById('speedThreshold')+typeVehicule.value != ""
			&& document.getElementById('acceleratorThreshold')+typeVehicule.value != ""
			&& document.getElementById('SCx'+typeVehicule).value != ""
			&& document.getElementById('RayonRoue'+typeVehicule).value != ""
			&& document.getElementById('res_roulement'+typeVehicule).value != ""
			&& document.getElementById('Paux'+typeVehicule).value != ""
			&& document.getElementById('rend_gear'+typeVehicule).value != ""
			&& document.getElementById('Pmotorrated'+typeVehicule).value != ""
			&& document.getElementById('rend_elec'+typeVehicule).value != ""
			&& document.getElementById('capBatterie'+typeVehicule).value != ""
			&& document.getElementById('PmotorMax'+typeVehicule).value != ""
			&& document.getElementById('trRatio'+typeVehicule).value != ""	
			&& document.getElementById('vmin'+typeVehicule).value != ""
			&& document.getElementById('vmax'+typeVehicule).value != ""
			&& document.getElementById('seuilDec'+typeVehicule).value != ""
			&& document.getElementById('coefInertie'+typeVehicule).value != ""
		    && isNumeric(document.getElementById('Masse'+typeVehicule).value)
		    && isNumeric(document.getElementById('MasseSup'+typeVehicule).value)	
		    && isNumeric(document.getElementById('powerThreshold'+typeVehicule).value)
		    && isNumeric(document.getElementById('speedThreshold'+typeVehicule).value)
		    && isNumeric(document.getElementById('acceleratorThreshold'+typeVehicule).value)
			&& isNumeric(document.getElementById('SCx'+typeVehicule).value)
			&& isNumeric(document.getElementById('RayonRoue'+typeVehicule).value) 
			&& isNumeric(document.getElementById('res_roulement'+typeVehicule).value) 
			&& isNumeric(document.getElementById('Paux'+typeVehicule).value)
			&& isNumeric(document.getElementById('rend_gear'+typeVehicule).value)
			&& isNumeric(document.getElementById('Pmotorrated'+typeVehicule).value)
			&& isNumeric(document.getElementById('PmotorMax'+typeVehicule).value)
			&& isNumeric(document.getElementById('trRatio'+typeVehicule).value)
			&& isNumeric(document.getElementById('rend_elec'+typeVehicule).value)
			&& isNumeric(document.getElementById('capBatterie'+typeVehicule).value)
			&& isNumeric(document.getElementById('vmin'+typeVehicule).value)
			&& isNumeric(document.getElementById('vmax'+typeVehicule).value)
			&& isNumeric(document.getElementById('seuilDec'+typeVehicule).value)
			&& isNumeric(document.getElementById('coefInertie'+typeVehicule).value))
				{
				return true;
				}
			else
				return false;
	}
	
}
/**
 * Sauvegarde dans dataVehicules des parametres entres par l'utilisateur, avec demande de confirmation.
 */
function saveVehParam(test) {
	alert(choix),
	alert("TEST");
	if (checkParamFields())
	{
		if (confirm('Voulez-vous sauvegarder ces parametres pour le vehicule : ' + document.getElementById('vehTypeId'+typeVehicule).value + '?')) 
		    {
			writeInDataVeh();
			document.getElementById("saveVehButton"+typeVehicule).style.display = 'none';
		    }
	}
	else
		alert("Un ou plusieurs champs ne sont pas correctement renseignes");
}
/**
 * Suppression du set de parametre selectionne par l'utilisateur. Actualisation en consequence des index lies aux options et aux data.
 */
function deleteVehParam() {
	alert("TEST");
	var select = document.getElementById("carType"+typeVehicule);
	var tempSelectedIndex;
	
	if (document.getElementById("carType"+typeVehicule).selectedIndex != 0 && confirm('Voulez-vous supprimer les parametres lies au vehicule : ' + document.getElementById('vehTypeId').value + '?'))
		{
		if(typeVehicule=="VE"){
			dataVehiculesE.splice(document.getElementById("carType"+typeVehicule).selectedIndex, 1);
		}
		else if(typeVehicule=="VT"){
			dataVehiculesT.splice(document.getElementById("carType"+typeVehicule).selectedIndex, 1);
		}
		tempSelectedIndex = select.selectedIndex;
		select.remove(select.selectedIndex);
		
		if(typeVehicule=="VE"){
			for (var j = tempSelectedIndex; j < nbLignesParamVehE ; j++)
			{
				select.options[j].value = j;
			}
			nbLignesParamVehE-=1;	
		}
		else if(typeVehicule=="VT"){
			for (var j = tempSelectedIndex; j < nbLignesParamVehT ; j++)
			{
				select.options[j].value = j;
			}
			nbLignesParamVehT=1;	
		}
		
		emptyParamFields();
		select.selectedIndex = 0;
		
		}
}
/**
 * Ecriture d'un set de parametres dans dataVehiculesE : la nouvelle ligne est composee des parametres entres par l'utilisateur, les champs non renseignes sont des valeurs par defaut presentes dans defaultParamValue. Ajout d'une option en consequence dans le select carType.
 */
function writeInDataVeh() {
	var option = document.createElement("option");
    option.text = document.getElementById('vehTypeId'+typeVehicule).value;
    
    if(typeVehicule =="VE"){
    	nbLignesParamVehE+=1;
        option.value = nbLignesParamVehE;
        document.getElementById("carType"+typeVehicule).add(option);     
    	dataVehiculesE[nbLignesParamVehE] = new Array();
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.VEH_TYPE] = document.getElementById('vehTypeId'+typeVehicule).value;
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.VEH_MASS] = parseFloat(document.getElementById('Masse'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.DRAG_COEFF] = parseFloat(document.getElementById('SCx'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.WHEEL_RADIUS] = parseFloat(document.getElementById('RayonRoue'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.ROLL_RES] = parseFloat(document.getElementById('res_roulement'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.AUX_CONS] = parseFloat(document.getElementById('Paux'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.TR_YIELD] = parseFloat(document.getElementById('rend_gear'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.TR_RATIO] = parseFloat(document.getElementById('trRation'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.ENG_TYPE] = (defaultParamValue[paramTypeEnum.ENG_TYPE]);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.NOMINAL_ENG_POW] = parseFloat(document.getElementById('Pmotorrated'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.MAX_ENG_POW] = parseFloat(document.getElementById('PmotorMax'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.MAX_ENG_TQ] = parseFloat(defaultParamValue[paramTypeEnum.MAX_ENG_TQ]);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.BATT_TYPE] = (defaultParamValue[paramTypeEnum.BATT_TYPE]);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.BATT_EFF] = parseFloat(document.getElementById('rend_elec'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.BATT_CAPA] = parseFloat(document.getElementById('capBatterie'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.REC_MIN_SPD] =parseFloat( document.getElementById('vmin'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.REC_OPT_SPD] = parseFloat(document.getElementById('vmax'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.REC_MAX_DEC] = parseFloat(document.getElementById('seuilDec'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.COEFF_INERTIA] = parseFloat(document.getElementById('coefInertie'+typeVehicule).value);
    	dataVehiculesE[nbLignesParamVehE][paramTypeEnum.VEH_MASS_SUP] = parseFloat(document.getElementById('MasseSup'+typeVehicule).value);
    	document.getElementById("carType").selectedIndex = nbLignesParamVehE;
    }
    else if(typeVehicule=="VT"){
    	nbLignesParamVehT+=1;
        option.value = nbLignesParamVehT;
        document.getElementById("carType"+typeVehicule).add(option);
        
    	dataVehiculesT[nbLignesParamVehT] = new Array();
    	dataVehiculesT[nbLignesParamVehT][0] = document.getElementById('vehTypeId'+typeVehicule).value;
    	dataVehiculesT[nbLignesParamVehT][1] = parseFloat(document.getElementById('Masse'+typeVehicule).value);
    	dataVehiculesT[nbLignesParamVehT][2] = parseFloat(document.getElementById('RayonRoue'+typeVehicule).value);
    	dataVehiculesT[nbLignesParamVehT][3] = parseFloat(document.getElementById('SCx'+typeVehicule).value);
    	dataVehiculesT[nbLignesParamVehT][4] = parseFloat(document.getElementById('res_roulement'+typeVehicule).value);
    	dataVehiculesT[nbLignesParamVehT][5] = parseFloat(document.getElementById('rend_gear'+typeVehicule).value);
    	dataVehiculesT[nbLignesParamVehT][7] = parseFloat(document.getElementById('coefInertie'+typeVehicule).value);
    	dataVehiculesT[nbLignesParamVehT][8] = parseFloat(document.getElementById('nbRapportsVitesse'+typeVehicule).value);
    	dataVehiculesT[nbLignesParamVehT][9] = parseFloat(document.getElementById('MasseSup'+typeVehicule).value);
    	document.getElementById("carType").selectedIndex = nbLignesParamVehT;
    }
}
/**
 * Pour vider les options du select carType, excepte le premier choix
 */
function emptyCarTypeOptions () {
	var select = document.getElementById("carType"+typeVehicule);
	while (select.options[1] != null)
		select.remove(1);
}

// ----------------------------------------------------------------------------------
// Calcul
// sur les
// etapes
// ------------------------------------------------------------------------
/**
 * Affectation dans le tableau de donnees dataStep des differentes profils de conduite a utiliser pour chaque etape du parcours
 */
function calcVStep(){
	nbCycle = 1;
	for (var i = 1; i < nbStep+1;i++){
		dataStep[i][2] = dataStep[i][0]/dataStep[i][1]
		dataStep[i][3] = dataStep[i][2]*3.6 // Calcul
											// de
											// la
											// vitesse
											// moyenne
											// sur
											// l'etape
		if(dataStep[i][3] < 20){			// affectation
											// des
											// types
											// de
											// cycle
			dataStep[i][4] = 'VilleLent'
		} else if(dataStep[i][3] >= 20 && dataStep[i][3] < 50){
			dataStep[i][4] = 'VilleRapide'
		}else if(dataStep[i][3] >= 50 && dataStep[i][3]<70){
			dataStep[i][4] = 'RuralLent'
		}else if(dataStep[i][3] >= 70 && dataStep[i][3] < 90){
			dataStep[i][4] = 'RuralRapide'
		}else if(dataStep[i][3] >= 90){
			dataStep[i][4] = 'Autoroute'
		}
	}
	dataStep[1][5] = dataStep[1][0];
	var ID=1;
	for(var i = 2; i<nbStep+1;i++){			// Calcul
											// de
											// la
											// distance
											// cumulee
		dataStep[i][5] = dataStep[i][0] + dataStep[i-1][5];	
		if(dataStep[i][4] != dataStep[i-1][4]){ // Calcul
												// du
												// nombre
												// de
												// cycles
												// different
												// utilise
												// sur
												// le
												// parcours
			nbCycle+=1;
			ID+=1;
		}
		dataStep[i][6]=ID
	}
	affSyntheseEtape();
}
/**
 * Recherche du type de cycle au point situe a la distance 'distance'
 * 
 * @param distance :
 *            position ou la le cycle doit etre recuperee
 * @returns cycle: retourne le cycle correspondant
 */
function rechercheStep(distance){
	var cycle = dataStep[1][4];
	for (var i = 1; i < nbStep;i++){
		if(distance > dataStep[i][5]){
			cycle = dataStep[i+1][4]
		}else{
			break;
		}
	}
	return cycle;
}

// ----------------------------------------------------------------------------------
// Calcul
// sur les
// cycle
// standards
// ---------------------------------------------------------------
/**
 * Fonction pour comparer les resultats des calculs avec FastSim, elle n'est pas utilise dans le code pour la resolution
 */
function cumulEnergieFastSim(tab){
	var mae=0;
	var mse=0;
	var mape =0;
	var cycle;
	var nbPoint;
	var fileInput = document.querySelector('#cumulativeEnergie'+typeVehicule);
	    var reader = new FileReader();
	    reader.readAsText(fileInput.files[0]);
	    reader.addEventListener('load', function() {
	    	cycle=reader.result
	 });
	alert("Le fichier " + fileInput.files[0].name + " a ete lu")
	cycle=reader.result;
	nbPoint 	= cycle.match(/;/g).length;	// Compte
											// le
											// nombre
											// de
											// ';'
											// dans
											// le
											// fichier
											// txt
											// et
											// donc
											// le
											// nombre
											// de
											// points
											// des
											// donnees
											// energetique
	var iter 	= 1;	// iterateur
						// pour
						// le
						// parcours
						// des
						// donnees
						// lues
	var iterTab = 1;	// iterateur
						// pour
						// le
						// parcours
						// du
						// tableau
						// du
						// cycle
						// calcule
	while(iterTab < nbPoint+1){
		// Lecture
		// de
		// l'energie
		var lu 	= lectureNb(iter,cycle)
		var t 	= lu.nb;
		iter 	= lu.iter1 + 2;
		var etemp = dataCycle[iterTab][29]/1000;
		mae += Math.abs(t-etemp);
		mse +=Math.abs((t-etemp)*(t-etemp));
		mape = Math.abs((t-etemp)/t);
		iterTab+=1;
	}	
	mae = mae/nbPoint;
	mse = mse/nbPoint;
	mape = mape / nbPoint * 100;
	alert("mae = " + mae + "\n mse = " + mse + "\n mape = " + mape);
	return nbPoint;
}
/**
 * Fonction appelee lors du chargement d'un fichier
 */
function remplirCycleData() {
	lectureCycles("#cycleStandard"+typeVehicule);
}
/**
 * Pour vider les options du select cycleType, excepte le premier choix
 */
function emptyCycleTypeOptions() {
	var select = document.getElementById("cycleType");
	while (select.options[1] != null)
		select.remove(1);
}
/**
 * Fonction appelee lors de la selection d'une option de cycle. Utilisation ulterieure.
 */
function cycleTypeSelection() {
}

// ----------------------------------------------------------------------------------
// Gestion
// de
// l'affichage
// des
// onglets
// -----------------------------------------------------------
/**
 * Affiche un item html dont la visibilite est controlee par sa propriete visibility
 * 
 * @param id :
 *            le string id de l'item
 */

function test(){
	document.getElementById("vehTypeParamVE").style.visibility='visible';
}

function toggleVisById(id) {
	var item = document.getElementById(id);
	if (item.style.visibility == 'hidden')
		item.style.visibility = 'visible';
	else
		item.style.visibility = 'hidden';
}
/**
 * Affiche un item html dont la visibilite est controlee par sa propriete display
 * 
 * @param id :
 *            le string id de l'item
 */
function toggleDisplayById(id) {
	var item = document.getElementById(id);
	if (item.style.display == 'none')
		item.style.display = 'inline-block';
	else
		item.style.display = 'none';
}
/**
 * Switch l'affichage des items html correspondant aux fields de parametres.
 * 
 */
function toggleParam() {
	
	if (typeVehicule==undefined) typeVehicule = 'VE';
	
	toggleVisById("vehTypeParam"+typeVehicule);
	toggleVisById("paramHead"+typeVehicule);
	toggleVisById("masseParam"+typeVehicule);
	toggleVisById("rayonParam"+typeVehicule);
	toggleVisById("scxParam"+typeVehicule);
	toggleVisById("resRoulementParam"+typeVehicule);
	toggleVisById("lostMassParam"+typeVehicule);		
	toggleVisById("rTractionParam"+typeVehicule);
	toggleVisById("MasseSupParam"+typeVehicule);

	if(typeVehicule=="VE"){	
		toggleVisById("engineNominalPowerParam"+typeVehicule);
		toggleVisById("engineMaxPowerParam"+typeVehicule);
		toggleVisById("trRatioParam"+typeVehicule);	
		toggleVisById("auxConsParam"+typeVehicule);
		toggleVisById("battEffParam"+typeVehicule);
		toggleVisById("battCapaParam"+typeVehicule);
		toggleVisById("minChargeSpdParam"+typeVehicule);
		toggleVisById("optChargeSpdParam"+typeVehicule);
		toggleVisById("maxDecParam"+typeVehicule);
	}
	else if(typeVehicule=="VT"){
		toggleVisById("nbRapportsVitesseParam"+typeVehicule);
	}
	else if(typeVehicule=="VP"){
		toggleVisById("engineNominalPowerParam"+typeVehicule);
		toggleVisById("engineMaxPowerParam"+typeVehicule);
		toggleVisById("trRatioParam"+typeVehicule);	
		toggleVisById("auxConsParam"+typeVehicule);
		toggleVisById("battEffParam"+typeVehicule);
		toggleVisById("battCapaParam"+typeVehicule);
		toggleVisById("minChargeSpdParam"+typeVehicule);
		toggleVisById("optChargeSpdParam"+typeVehicule);
		toggleVisById("maxDecParam"+typeVehicule);
		toggleVisById("powerThresholdParam"+typeVehicule);
		toggleVisById("speedThresholdParam"+typeVehicule);
		toggleVisById("acceleratorThresholdParam"+typeVehicule);
		// toggleVisById("socLowThresholdParam"+typeVehicule);
		// toggleVisById("socUpThresholdParam"+typeVehicule);
	}
	else if(typeVehicule=="VPU"){
		toggleVisById("engineNominalPowerParam"+typeVehicule);
		toggleVisById("engineMaxPowerParam"+typeVehicule);
		toggleVisById("trRatioParam"+typeVehicule);	
		toggleVisById("auxConsParam"+typeVehicule);
		toggleVisById("battCapaParam"+typeVehicule);
		toggleVisById("minChargeSpdParam"+typeVehicule);
		toggleVisById("optChargeSpdParam"+typeVehicule);
		toggleVisById("maxDecParam"+typeVehicule);
		toggleVisById("powerThresholdParam"+typeVehicule);
		toggleVisById("speedThresholdParam"+typeVehicule);
		toggleVisById("acceleratorThresholdParam"+typeVehicule);
		toggleVisById("puissanceMaxPACU");
		toggleVisById("rendConvDCPACU");
		toggleVisById("capaciteReservoirHydrogne");
		toggleVisById("coefficientFrottementvisqueux");
		toggleVisById("tensionMoteur");
		toggleVisById("resistancePhaseMoteur");
		toggleVisById("ageBatterie");
		toggleVisById("CyclageBatterie");
		toggleVisById("TempératureExterieure");
		toggleVisById("rapportTrans");
		toggleVisById("CellBatt");
		toggleVisById("prixH2");
		toggleVisById("prixElec")
		toggleVisById("puissCharge")
		// toggleVisById("socLowThresholdParam"+typeVehicule);
		// toggleVisById("socUpThresholdParam"+typeVehicule);
	}
	
// toggleDisplayById("deleteVehButton"+typeVehicule);
	if(document.getElementById("toggleParamButton"+typeVehicule).value == "Afficher paramètres")
		document.getElementById("toggleParamButton"+typeVehicule).value = "Cacher paramètres";
	else
		document.getElementById("toggleParamButton"+typeVehicule).value = "Afficher paramètres";

	if (!paramVisibleE)
		paramVisibleE = true;
	else
		{
		// if
		// (document.getElementById("saveVehButton"+typeVehicule).style.display
		// ==
		// "inline-block")
		// document.getElementById("saveVehButton"+typeVehicule).style.display
		// =
		// "none";
		paramVisibleE = false;
		}

}
/**
 * Switch l'affichage du bouton save et appelle toggleParam() Les champs param sont vides et le type de vehicule n'est plus read only.
 */
function toggleParamByAddButton () {
	document.getElementById("vehTypeId"+typeVehicule).removeAttribute("readonly");
	emptyParamFields();
	if (document.getElementById("saveVehButton"+typeVehicule).style.display == "none")
		document.getElementById("saveVehButton"+typeVehicule).style.display = "inline-block";
	if (!paramVisibleE)
		toggleParam();
}
/**
 * Pour vider les champs de parametres dans le formulaire html
 */
function emptyParamFields (){
	document.getElementById('vehTypeId'+typeVehicule).value = "";
	document.getElementById('Masse'+typeVehicule).value = "";
	document.getElementById('SCx'+typeVehicule).value = "";
	document.getElementById('RayonRoue'+typeVehicule).value = "";
	document.getElementById('res_roulement'+typeVehicule).value = "";
	document.getElementById('rend_gear'+typeVehicule).value = "";
	document.getElementById('coefInertie'+typeVehicule).value = "";
	document.getElementById('MasseSup'+typeVehicule).value = "";
	
	if(typeVehicule=="VE"){
		document.getElementById('Pmotorrated'+typeVehicule).value = "";
		document.getElementById('PmotorMax'+typeVehicule).value = "";	
		document.getElementById('Paux'+typeVehicule).value = "";
		document.getElementById('trRatio'+typeVehicule).value = "";
		document.getElementById('rend_elec'+typeVehicule).value = "";
		document.getElementById('capBatterie'+typeVehicule).value = "";
		document.getElementById('vmin'+typeVehicule).value = "";
		document.getElementById('vmax'+typeVehicule).value = "";
		document.getElementById('seuilDec'+typeVehicule).value = "";
	}
	else if(typeVehicule=="VT"){
		document.getElementById('nbRapportsVitesse'+typeVehicule).value = "";
	}
	else if(typeVehicule=="VP"){
		document.getElementById('Pmotorrated'+typeVehicule).value = "";
		document.getElementById('PmotorMax'+typeVehicule).value = "";	
		document.getElementById('Paux'+typeVehicule).value = "";
		document.getElementById('trRatio'+typeVehicule).value = "";
		document.getElementById('rend_elec'+typeVehicule).value = "";
		document.getElementById('capBatterie'+typeVehicule).value = "";
		// document.getElementById('capH2'+typeVehicule).value
		// =
		// "";
		document.getElementById('vmin'+typeVehicule).value = "";
		document.getElementById('vmax'+typeVehicule).value = "";
		document.getElementById('seuilDec'+typeVehicule).value = "";
		document.getElementById('powerThreshold'+typeVehicule).value = "";
		document.getElementById('speedThreshold'+typeVehicule).value = "";
		document.getElementById('acceleratorThreshold'+typeVehicule).value = "";
		// document.getElementById('seuilSocLow'+typeVehicule).value
		// =
		// "";
		// document.getElementById('seuilSocUp'+typeVehicule).value
		// =
		// "";
	}
	else if(typeVehicule=="VPU"){
		document.getElementById('Pmotorrated'+typeVehicule).value = "";
		document.getElementById('PmotorMax'+typeVehicule).value = "";	
		document.getElementById('Paux'+typeVehicule).value = "";
		document.getElementById('trRatio'+typeVehicule).value = "";
		document.getElementById('rend_elec'+typeVehicule).value = "";
		document.getElementById('capBatterie'+typeVehicule).value = "";
		// document.getElementById('capH2'+typeVehicule).value
		// =
		// "";
		document.getElementById('vmin'+typeVehicule).value = "";
		document.getElementById('vmax'+typeVehicule).value = "";
		document.getElementById('seuilDec'+typeVehicule).value = "";
		document.getElementById('powerThreshold'+typeVehicule).value = "";
		document.getElementById('speedThreshold'+typeVehicule).value = "";
		document.getElementById('acceleratorThreshold'+typeVehicule).value = "";
		// document.getElementById('seuilSocLow'+typeVehicule).value
		// =
		// "";
		// document.getElementById('seuilSocUp'+typeVehicule).value
		// =
		// "";
	}
}
/**
 * Mise a jour du taux de charge initial de la batterie avec la dragbar
 * 
 * @param valeur
 */
function updatesocInitial(valeur){
	if(typeVehicule=="VE"){socBatterieVE = parseInt(valeur)}
	if(typeVehicule=="VPU"){socBatterieVPU = parseInt(valeur)}
	if(typeVehicule=="VP"){socBatterieVP = parseInt(valeur)}
	document.getElementById("socInitial"+typeVehicule).innerHTML = "Taux de charge initial " + parseInt(valeur) + " %"
}
/**
 * Mise a jour du taux de charge minimum de la batterie avec la dragbar
 * 
 * @param valeur
 */
function updatesocMin(valeur){
	if(typeVehicule=="VE"){socMinVE = parseInt(valeur)}
	if(typeVehicule=="VPU"){socMinVPU = parseInt(valeur)}
	if(typeVehicule=="VP"){socMinVP = parseInt(valeur)}
	document.getElementById("socMin"+typeVehicule).innerHTML = "Taux de charge minimum " + parseInt(valeur) + " %"
}

/**
 * Mise a jour du niveau initial du réservoir à hydrogène avec la dragbar
 * 
 * @param valeur
 */
function updateniveauInitial(valeur){
	niveauH2 = parseInt(valeur);
	document.getElementById("niveauInitial"+typeVehicule).innerHTML = "Niveau initial " + niveauH2 + " %"
}
/**
 * Mise a jour du niveau minimum du réservoir à hydrogène avec la dragbar
 * 
 * @param valeur
 */
function updateniveauMin(valeur){
	niveauMin = parseInt(valeur);
	document.getElementById("niveauMin"+typeVehicule).innerHTML = "Niveau minimum " + niveauMin + " %"
}

// ----------------------------------------------------------------------------------
// Gestion
// de
// l'affichage
// de la
// carte
// -----------------------------------------------------------
/**
 * Ajout d'un marquer sur la carte GoogleMaps
 * 
 * @param lat :
 *            latitude du marqueur
 * @param long :
 *            longitude du marqueur
 * @param label :
 *            label du marqueur
 * @param image :
 *            logo du marqueur
 * @param tabmarker :
 *            tableau dans lequel le marker est stockee
 */
function addMarker(lat,long,label,logo,bool,tabmarker){
	var myLatlng = new google.maps.LatLng(lat,long);
	var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
    });	
	if(bool){ // si
				// false,
				// le
				// marqueur
				// est
				// celui
				// par
				// defaut
		marker.setIcon(logo);
	}
	tabmarker.push(marker)
}
/**
 * Cache le marqueur situe a la latitude et longitude donnees
 * 
 * @param lat :
 *            latitude du marqueur
 * @param long :
 *            longitude du marqueur
 */
function removeMarker(lat,long,tabmarker){
	for(var iter = 0; iter < tabmarker.length;iter++){
		if(Math.abs(tabmarker[iter].getPosition().lat()- lat)<0.00001 && Math.abs(tabmarker[iter].getPosition().lng()- long)<0.00001){
			tabmarker[iter].setMap(null);
			break;
		}
	}
}
/**
 * Affichage ou non des bornes
 */
function AffichageBornes(){
	if(document.getElementById('affichageBornes'+typeVehicule).value == 'Cacher'){
		markerCluster.clearMarkers();
		document.getElementById('affichageBornes'+typeVehicule).value = 'Afficher';
	}else{
		markerCluster = new MarkerClusterer(map, markersBornes,
		        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
		document.getElementById('affichageBornes'+typeVehicule).value = 'Cacher';
	}
}
/**
 * Centre la carte sur la position voulu a partir d'une latitute et longitude
 * 
 * @param pPos :
 *            position ou la carte doit etre centree
 */
function moveOnMapLttLgt(pPos){
	marker.setVisible(false);
	map.setZoom(17);
	map.setCenter(pPos);
	
    marker.setPosition(pPos);
    marker.setVisible(true);
}
/**
 * Centre la carte sur la position voulu a partir d'un point d'interet
 * 
 * @param pPlace
 */
function moveOnMap(pPlace){
	marker.setVisible(false);
    if (!pPlace.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }
    // If
	// the
	// place
	// has
	// a
	// geometry,
	// then
	// present
	// it
	// on a
	// map.
    if (pPlace.geometry.viewport) {
      map.fitBounds(pPlace.geometry.viewport);
    } else {
      map.setCenter(pPlace.geometry.location);
      map.setZoom(17);  // Why
						// 17?
						// Because
						// it
						// looks
						// good.
    }
    
    marker.setPosition(pPlace.geometry.location);
    marker.setVisible(true);
}
/**
 * Affiche ou cache le calque du traffic
 */
function affichageTraffic(){
	if(document.getElementById('affichageTraffic'+typeVehicule).value == 'Afficher le traffic'){
		trafficLayer.setMap(map);// Ajout
									// du
									// calque
									// contenant
									// les
									// informations
									// trafic
		document.getElementById('affichageTraffic'+typeVehicule).value = 'Cacher le traffic'
	}else{
		trafficLayer.setMap(null);
		document.getElementById('affichageTraffic'+typeVehicule).value = 'Afficher le traffic'
	}
	
}
/**
 * Affichage d'une portion du trajet dans la couleur choisi
 * 
 * @param number :
 *            numero de l'etape a afficher en couleur
 * @param color :
 *            couleur d'affichage
 */
function setColorStep(number,color,weight,opacity){
	var steps = directionsDisplay.getDirections().routes[0].legs[0].steps;
	var polylineOptions = {
		strokeColor: color,
		strokeOpacity: opacity,
		strokeWeight: weight
	};
	var polylines = [];
	var stepPolyline = new google.maps.Polyline(polylineOptions);
	stepPolyline.setOptions({
      strokeColor: color
    })
	var step = number;
	for(var iterStep = 1; iterStep < nbStep+1;iterStep++){
		if(dataStep[iterStep][6] == step){
			var nextSegment = steps[iterStep-1].path;
		    for (k = 0; k < nextSegment.length; k++) {
		      stepPolyline.getPath().push(nextSegment[k]);
		    }
		    polylines.push(stepPolyline);
		}
	}
    stepPolyline.setMap(map);
}
// ----------------------------------------------------------------------------------
// Gestion
// de
// l'affichage
// des
// graphiques
// --------------------------------------------------------
/**
 * Fonction globale regroupant les differentes fonctions utilisees pour l'affichage
 */
function affichage(){
	document.getElementById('Affichage_courbesVE1').style.display = '';
	document.getElementById('Affichage_courbesVE2').style.display = '';
	document.getElementById('Affichage_courbesVE3').style.display = '';
	document.getElementById('Affichage_courbesVT1').style.display = '';
	document.getElementById('Affichage_courbesVT2').style.display = '';
	document.getElementById('Affichage_courbesVH1').style.display = '';
	document.getElementById('Affichage_courbesVH2').style.display = '';
	document.getElementById('Affichage_courbesVP1').style.display = '';
	document.getElementById('Affichage_courbesVP2').style.display = '';
	document.getElementById('Affichage_courbesVPU1').style.display = '';
	document.getElementById('Affichage_courbesVPU2').style.display = '';
	document.getElementById('Affichage_courbesVPU3').style.display = '';
	if(affichageComp){
		return
	}
	if(!boolTrajet){	
		affSynthese();		// Affichage
							// de
							// la
							// synthese
							// des
							// calculs
	}
	if(typeVehicule=="VE"){
		//AffConso(36,1,"chart_energieVE1",1,nbPointConso,dataConso,myChartVE1); 		// affichage
		AffVE1();																			// du
																					// graphique
																					// VE1
																					// colonne
																					// 36
																					// (distance
																					// cumulée)
																					// en
																					// fonction
																					// de
																					// la
																					// colonne
																					// 1
																					// (vitesse)
		//AffConso(36,1,"chart_energieVE2",1,nbPointConso,dataConso,myChartVE2); 		// affichage
		AffVE2();																			// du
																					// graphique
																					// VE2
																					// colonne
																					// 36
																					// (distance
																					// cumulée)
																					// en
																					// fonction
																					// de
																					// la
																					// colonne
																					// 1
		if(memory == 2){
			var nbPointTotal = tabMemoryRev.length-1;
			//AffConso(36,1,"chart_energieVE3",1,nbPointTotal,tabMemoryRev,myChartVE3);
			AffVE3();
		}																			// (vitesse)
		
	}
	if(typeVehicule=="VT"){
		AffVT1();
		//AffConso(36,1,"chart_energieVT1",1,nbPointConso,dataConso,myChartVT1); 		// affichage
																					// du
																					// graphique
																					// VT1
																					// colonne
																					// 36
																					// (distance
																					// cumulée)
																					// en
																					// fonction
																					// de
																					// la
																					// colonne
																					// 1
																					// (vitesse)
		//AffConso(36,1,"chart_energieVT2",1,nbPointConso,dataConso,myChartVT2);
		AffVT2();
		if(memory == 2){
			var nbPointTotal = tabMemoryRev.length-1;
			//AffConso(36,1,"chart_energieVT3",1,nbPointConso,dataConso,myChartVT3);
			AffVT3();
		}
	}
	if(typeVehicule=="VH"){
		AffConsoVH(0,1,"chart_energieVH1",1,dataBusHybridereverse.length,dataBusHybridereverse,myChartVH1); 		// affichage
																													// du
																													// graphique
																													// VH1
																													// colonne
																													// 36
																													// (distance
																													// cumulée)
																													// en
																													// fonction
																													// de
																													// la
																													// colonne
																													// 1
																													// (vitesse)
		AffConso(36,1,"chart_energieVH2",1,nbPointConso,dataConso,myChartVH2); 		// affichage
																					// du
																					// graphique
																					// VH2
																					// colonne
																					// 36
																					// (distance
																					// cumulée)
																					// en
																					// fonction
																					// de
																					// la
																					// colonne
																					// 1
																					// (vitesse)
	}
	if(typeVehicule=="VP"){
		AffConsoPac(7,15,"chart_energieVP1",1,nbPointConso,dataConso,myChartVP1); 		// affichage
																						// du
																						// graphique
																						// VP1
																						// colonne
																						// 7
																						// (distance
																						// cumulée)
																						// en
																						// fonction
																						// de
																						// la
																						// colonne
																						// 1
																						// (vitesse)
		AffConsoPac(7,15,"chart_energieVP2",1,nbPointConso,dataConso,myChartVP2); 		// affichage
																						// du
																						// graphique
																						// VP2
																						// colonne
																						// 7
																						// (distance
																						// cumulée)
																						// en
																						// fonction
																						// de
																						// la
																						// colonne
																						// 1
																						// (vitesse)
	}
	if(typeVehicule=="VPU"){
		//AffConsoPacUTBM(7,15,"chart_energieVPU1",1,nbPointConso,dataConso,myChartVPU1); 		// affichage
																								// du
																								// graphique
																								// VP1
																								// colonne
																								// 7
																								// (distance
																								// cumulée)
																								// en
																								// fonction
																								// de
																								// la
																								// colonne
																								// 1
																								// (vitesse)
		//AffConsoPacUTBM(7,15,"chart_energieVPU2",1,nbPointConso,dataConso,myChartVPU2); 		// affichage
																								// du
																								// graphique
																								// VP2
																								// colonne
																								// 7
																								// (distance
																								// cumulée)
																								// en
																								// fonction
																								// de
																								// la
																								// colonne
																								// 1
																								// (vitesse)
		//AffConsoPacUTBM(7,15,"chart_energieVPU3",1,nbPointConso,tabMemory1,myChartVPU3); 
		AffVPU1();
		AffVPU2();
		if(memory==2){
			AffVPU3();
		}
		
		
	}
}
/**
 * Affichage du cycle de conduite unitaire utilise
 */
function AffCycleUnit() {
	document.getElementById('cycle_chart').style.display = '';
	var data = [];
	var tab;
	var titre;
	var nbPoint;
	// Copie
	// des
	// donnees
	// du
	// cycle
	if(cycle == 0){
		tab 	= dataCycleVilleLent;
		nbPoint = nbPointCycleVilleLent;
		titre 	= 'Cycle unitaire ville lent'
	}else if(cycle == 1){
		tab		= dataCycleVilleRapide;
		nbPoint	= nbPointCycleVilleRapide;
		titre 	= 'Cycle unitaire ville rapide'
	}else if(cycle == 2){
		tab		= dataCycleRuralLent;
		nbPoint = nbPointCycleRuralLent;
		titre 	= 'Cycle unitaire rural lent'
	}else if (cycle == 3){
		tab		= dataCycleRuralRapide;
		nbPoint	= nbPointCycleRuralRapide;
		titre 	= 'Cycle unitaire rural rapide'
	}else if(cycle == 4){
		tab		= dataCycleAutoroute;
		nbPoint = nbPointCycleAutoroute;
		titre 	= 'Cycle unitaire autoroute'
	}
	
	for(var i = 1; i < nbPoint+1;i++){
		data.push({x:tab[i][0],y:tab[i][1]});
	}
	// Parametres
	// du
	// grahique
    var ctx = document.getElementById("cycle_chart");
    if(ChartCycleUnit != null){
    	ChartCycleUnit.destroy();
    }
    ChartCycleUnit = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Cycle de conduite',
                data: data,
                backgroundColor: 'rgba(0,0,0,0.1)',
                fill: false,
                pointRadius: 0,
                borderColor: window.chartColors.blue,
                borderWidth: 3
            }]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text: titre,
            },
            scales: {
                xAxes: [{
                    display: true,
                    type: 'linear',
                    position: 'bottom',
                    scaleLabel: {
                        display: true,
                        labelString: 'Temps (s)'
                    },
                }],
                yAxes: [{
                    display: true,
                    type: 'linear',
                    scaleLabel: {
                        display: true,
                        labelString: 'Vitesse (km/h)'
                    }
                }]
            }
        }
    });
    ChartCycleUnit.render();
};
/**
 * Affichage de la colonne 'colonne2' en fonction de la colonne 'colonne1' entre les temps t1 et t2
 * 
 * @param colonne1 :
 *            colonne en absisse
 * @param colonne2 :
 *            colonne en ordonnee
 */
function AffConso(colonne1,colonne2,canvas,p1,p2,tab,mychart) {
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', tab[0][colonne1]);
	dataChart.addColumn('number', tab[0][colonne2]);
	dataChart.addColumn({'type': 'string', 'role': 'tooltip'});
	var point1 = p1;
	var point2 = p2;
	// Recuperation
	// de
	// l'intervale
	// de
	// temps
	// de
	// l'affichage
	if(isNaN(point1) || point1==0){
		point1 = 1;
	}
	if(isNaN(point2) || point2 == 0){
		point2 = nbPointConso;
	}
	if(point2 < point1){
		var temp 	= point2;
		point2 		= point1;
		point1		= temp;
	}
    /*var tabBis = reversetab(tab,tab.length, tab[0].length);   //affichage des tables contenant les valeurs, a supprimer plus tard
    for( var i =0; i<tabBis.length;i++){
    	console.log(tabBis[i]);
    }
    console.log(tabBis[colonne1]);						//affichage de différents logs
    console.log(tabBis[colonne2]);
    console.log("point1 et 2 : "+ point1 + " / "+ point2 + " Colonne 1 et 2 : "+ colonne1 + " / "+ colonne2);*/
    
	//dataChart.addRow([parseFloat(tabBis[colonne1][i]),parseFloat(tabBis[colonne2][i]),tooltip(tabBis[colonne1][0], tabBis[colonne1][i],tabBis[colonne2][0], tabBis[colonne2][i])])
	for(var i = point1;i<point2+1;i++){
		dataChart.addRow([parseFloat(tab[i][colonne1]),parseFloat(tab[i][colonne2]),tooltip(tab[0][colonne1], tab[i][colonne1],tab[0][colonne2], tab[i][colonne2])]);
	}
	  document.getElementById(canvas).style.display = 'block';
	  mychart.draw(dataChart, {
	  	vAxis: {title: tab[0][colonne1]},
	  	hAxis: {title: tab[0][colonne2]},
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},
	  	bar: {groupWidth: '100%'},
	  	chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
	  	axisTitlesPosition: "in",
	  	isHtml: true,
	  	lineWidth: 2,
	  	areaOpacity: 0.4,
	  // height:300,
	  	colors: ['#295DBC', '#F29D00', '#ED0300'],
	  	
	  });
	  document.getElementById(canvas).style.height = '100%'   
};
function AffConsoRev(colonne1,colonne2,canvas,p1,p2,tab,mychart) {
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', tab[0][colonne1]);
	dataChart.addColumn('number', tab[0][colonne2]);
	dataChart.addColumn({'type': 'string', 'role': 'tooltip'});
	var point1 = p1;
	var point2 = p2;
	// Recuperation
	// de
	// l'intervale
	// de
	// temps
	// de
	// l'affichage
	if(isNaN(point1) || point1==0){
		point1 = 1;
	}
	if(isNaN(point2) || point2 == 0){
		point2 = nbPointConso;
	}
	if(point2 < point1){
		var temp 	= point2;
		point2 		= point1;
		point1		= temp;
	}
    /*var tabBis = reversetab(tab,tab.length, tab[0].length);   //affichage des tables contenant les valeurs, a supprimer plus tard
    for( var i =0; i<tabBis.length;i++){
    	console.log(tabBis[i]);
    }
    console.log(tabBis[colonne1]);						//affichage de différents logs
    console.log(tabBis[colonne2]);
    console.log("point1 et 2 : "+ point1 + " / "+ point2 + " Colonne 1 et 2 : "+ colonne1 + " / "+ colonne2);*/
    
	//dataChart.addRow([parseFloat(tabBis[colonne1][i]),parseFloat(tabBis[colonne2][i]),tooltip(tabBis[colonne1][0], tabBis[colonne1][i],tabBis[colonne2][0], tabBis[colonne2][i])])
	 //console.log(tab[colonne1]);						//affichage de différents logs
	 //console.log(tab[colonne2]);
	for(var i = point1;i<point2+1;i++){
		dataChart.addRow([parseFloat(tab[colonne1][i]),parseFloat(tab[colonne2][i]),tooltip(tab[colonne1][0], tab[colonne1][i],tab[colonne2][0], tab[colonne2][i])]);
	}
	  document.getElementById(canvas).style.display = 'block';
	  mychart.draw(dataChart, {
	  	vAxis: {title: tab[colonne1][0]},
	  	hAxis: {title: tab[colonne2][0]},
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},
	  	bar: {groupWidth: '100%'},
	  	chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
	  	axisTitlesPosition: "in",
	  	isHtml: true,
	  	lineWidth: 2,
	  	areaOpacity: 0.4,
	  // height:300,
	  	colors: ['#295DBC', '#F29D00', '#ED0300'],
	  	
	  });
	  document.getElementById(canvas).style.height = '100%'   
};
/**
 * Affichage de la colonne 'colonne2' en fonction de la colonne 'colonne1' entre les temps t1 et t2
 * 
 * @param colonne1 :
 *            colonne en absisse
 * @param colonne2 :
 *            colonne en ordonnee
 */
function AffConsoVH(colonne1,colonne2,canvas,p1,p2,tab,mychart) {
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', tab[0][colonne1]);
	dataChart.addColumn('number', tab[0][colonne2]);
	dataChart.addColumn({'type': 'string', 'role': 'tooltip'});
	var point1 = p1;
	var point2 = p2;
	// Recuperation
	// de
	// l'intervale
	// de
	// temps
	// de
	// l'affichage
	if(isNaN(point1) || point1==0){
		point1 = 1;
	}
	if(isNaN(point2) || point2 == 0){
		point2 = nbPointConso;
	}
	if(point2 < point1){
		var temp 	= point2;
		point2 		= point1;
		point1		= temp;
	}
	for(var i = point1;i<point2;i++){
		dataChart.addRow([parseFloat(tab[i][colonne1]),parseFloat(tab[i][colonne2]),tooltip(tab[0][colonne1], tab[i][colonne1],tab[0][colonne2], tab[i][colonne2])]);
	}
	  document.getElementById(canvas).style.display = 'block';
	  mychart.draw(dataChart, {
	  	vAxis: {title: tab[0][colonne1]},
	  	hAxis: {title: tab[0][colonne2]},
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},
	  	bar: {groupWidth: '100%'},
	  	chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
	  	axisTitlesPosition: "in",
	  	isHtml: true,
	  	lineWidth: 2,
	  	areaOpacity: 0.4,
	  // height:300,
	  	colors: ['#295DBC', '#F29D00', '#ED0300'],
	  	
	  });
	  document.getElementById(canvas).style.height = '100%'   
}
function AffConsoPac(colonne1,colonne2,canvas,p1,p2,tab,mychart) {
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', tab[0][colonne1]);
	dataChart.addColumn('number', tab[0][colonne2]);
	dataChart.addColumn({'type': 'string', 'role': 'tooltip'});
	var point1 = p1;
	var point2 = p2;
	// Recuperation
	// de
	// l'intervale
	// de
	// temps
	// de
	// l'affichage
	if(isNaN(point1) || point1==0){
		point1 = 1;
	}
	if(isNaN(point2) || point2 == 0){
		point2 = nbPointConso;
	}
	if(point2 < point1){
		var temp 	= point2;
		point2 		= point1;
		point1		= temp;
	}
	for(var i = point1;i<point2;i++){
		dataChart.addRow([parseFloat(tab[i][colonne1]),parseFloat(tab[i][colonne2]),tooltip(tab[0][colonne1], tab[i][colonne1],tab[0][colonne2], tab[i][colonne2])]);
	}
	  document.getElementById(canvas).style.display = 'block';
	  mychart.draw(dataChart, {
	  	vAxis: {title: tab[0][colonne1]},
	  	hAxis: {title: tab[0][colonne2]},
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},
	  	bar: {groupWidth: '100%'},
	  	chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
	  	axisTitlesPosition: "in",
	  	isHtml: true,
	  	lineWidth: 2,
	  	areaOpacity: 0.4,
	  // height:300,
	  	colors: ['#295DBC', '#F29D00', '#ED0300'],
	  	
	  });
	  document.getElementById(canvas).style.height = '100%'   
};

function AffConsoPacUTBM(colonne1,colonne2,canvas,p1,p2,tab,mychart) {
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', tab[0][colonne1]);
	dataChart.addColumn('number', tab[0][colonne2]);
	dataChart.addColumn({'type': 'string', 'role': 'tooltip'});
	var point1 = p1;
	var point2 = p2;
	// Recuperation
	// de
	// l'intervale
	// de
	// temps
	// de
	// l'affichage
	if(isNaN(point1) || point1==0){
		point1 = 1;
	}
	if(isNaN(point2) || point2 == 0){
		point2 = nbPointConso;
	}
	if(point2 < point1){
		var temp 	= point2;
		point2 		= point1;
		point1		= temp;
	}
	for(var i = point1;i<point2;i++){
		dataChart.addRow([parseFloat(tab[i][colonne1]),parseFloat(tab[i][colonne2]),tooltip(tab[0][colonne1], tab[i][colonne1],tab[0][colonne2], tab[i][colonne2])]);
	}

	  document.getElementById(canvas).style.display = 'block';
	  mychart.draw(dataChart, {
	  	vAxis: {title: tab[0][colonne1]},
	  	hAxis: {title: tab[0][colonne2]},
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},
	  	bar: {groupWidth: '100%'},
	  	chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
	  	axisTitlesPosition: "in",
	  	isHtml: true,
	  	lineWidth: 2,
	  	areaOpacity: 0.4,
	  // height:300,
	  	colors: ['#295DBC', '#F29D00', '#ED0300'],
	  	
	  });
	  document.getElementById(canvas).style.height = '100%'   
};

/**
 * Affichage de la colonne 'colonne2' en fonction de la colonne 'colonne1' entre les temps t1 et t2
 * 
 * @param colonne1 :
 *            colonne en absisse
 * @param colonne2 :
 *            colonne en ordonnee
 */
function AffConsoReelles(colonne1,colonne2,canvas,p1,p2,tab,mychart) {
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', tab[0][colonne1]);
	dataChart.addColumn('number', tab[0][colonne2]);
	dataChart.addColumn({'type': 'string', 'role': 'tooltip'});
	var point1 = p1;
	var point2 = p2;
	// Recuperation
	// de
	// l'intervale
	// de
	// temps
	// de
	// l'affichage
	if(isNaN(point1) || point1==0){
		point1 = 1;
	}
	if(isNaN(point2) || point2 == 0){
		point2 = nbLignesParamReelles;
	}
	if(point2 < point1){
		var temp 	= point2;
		point2 		= point1;
		point1		= temp;
	}
	for(var i = point1;i<point2+1;i++){
		dataChart.addRow([parseFloat(tab[i][colonne1]),parseFloat(tab[i][colonne2]),tooltip(tab[0][colonne1], tab[i][colonne1],tab[0][colonne2], tab[i][colonne2])]);
	}
	  document.getElementById(canvas).style.display = 'block';
	  mychart.draw(dataChart, {
	  	vAxis: {title: tab[0][colonne1]},
	  	hAxis: {title: tab[0][colonne2]},
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},
	  	bar: {groupWidth: '100%'},
	  	chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"},
	  	axisTitlesPosition: "in",
	  	isHtml: true,
	  	lineWidth: 2,
	  	areaOpacity: 0.4,
	  // height:300,
	  	colors: ['#295DBC', '#F29D00', '#ED0300'],
	  	
	  });
	  document.getElementById(canvas).style.height = '100%'   
};
/**
 * Recherche et affichages sur le graphique des gradients d'elevation
 */
function findtops(temptops, gradients, distances, altitudes, length, length_stop, avggrade) {
	var intervall_start = 0;		
	var tempdistance = 0;	
	var tempgradient = 0;		
	var tops = [];
	for(var i = 0; i < distances.length; i++) {
		if(temptops[i]) {
			tempdistance = 0;
			tempgradient = 0;
			for(var z = i - 1; z >= 0; z--) {
				tempdistance = tempdistance + distances[z];
				if(tempdistance > length && tempdistance < length_stop) {
					tempgradient = (altitudes[i] - altitudes[z]) / tempdistance;
					if(tempgradient > avggrade) {

						intervall_start = z;
						
						tops[i] = true;
					}
				}
			}
			if(tops[i]){
				for(var x = intervall_start; x < i; x++) {
					if(altitudes[x] > altitudes[i] && temptops[x]) {
						tops[i] = false;
					}
					if(altitudes[x] < altitudes[i] && temptops[x]) {
						tops[x] = false;
					}
				}
			}
		}
	}
	return tops;
}
/**
 * Affichage des labels sur les points des graphiques
 * 
 * @param temp_distance
 * @param temp_elevation
 * @returns {String}
 */
function tooltip(yLabel,yValue,xLabel ,xValue) {
	return yLabel+': ' + Math.round(yValue*10)/10 + '\n' + xLabel+': ' + Math.round(xValue*10)/10;
}
/**
 * Trace du pieChart du bilan energetique
 */
function affEnergieChart(){	
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Energie');
	data.addColumn('number', 'wh');
	data.addColumn({type: 'string', role: 'tooltip'});
	
	var data2 = new google.visualization.DataTable();
	data2.addColumn('string', 'Energie');
	data2.addColumn('number', 'wh');
	data2.addColumn({type: 'string', role: 'tooltip'});
	
	
	if(typeVehicule=="VE"){
		var eAux = Paux*nbPointConso*inter/3600;
		var eAbsDissip = Math.abs(dataConso[nbPointConso][37]);
		var eTotal = dataConso[nbPointConso][29] + eAbsDissip+dataConso[nbPointConso][30]+eAux;
		var eConso = dataConso[nbPointConso][29];
		var p1 = Math.round((eConso)/eTotal*10*100)/10	;					// Pourcentage
																			// d'energie
																			// consommee
		var p2 = Math.round((dataConso[nbPointConso][30])/eTotal*10*100)/10;	// Pourcentage
																			// d'energie
																			// recuperee
		var p3 = Math.round((eAbsDissip)/eTotal*10*100)/10 ;					// Pourcentage
																			// d'energie
																			// dissipee
		var p4 = Math.round((eAux)/eTotal*10*100)/10;						// Pourcentage
																			// d'energie
																			// auxiliaires
		data.addRows([
		              ['Energie recuperee ' + Math.round(dataConso[nbPointConso][30]) + ' Wh', dataConso[nbPointConso][30], 'Energie recupere ' + Math.round(dataConso[nbPointConso][30]) + 'Wh (' + p2 + '%)'],
		              ['Energie consommee ' + Math.round(eConso) + ' Wh', eConso, 'Energie consommee ' + Math.round(eConso) + 'Wh (' + p1 + '%)'],
		              ['Energie non recuperee ' + Math.round(eAbsDissip) + ' Wh', eAbsDissip, 'Energie non recuperee ' + Math.round(eAbsDissip) + 'Wh (' + p3 + '%)'],// en
																																										// fait
																																										// energie
																																										// non
																																										// recuperee
																																										// lors
																																										// du
																																										// passage
																																										// en
																																										// generatrice
		              ['Energie auxiliaires ' + Math.round(p4) + ' Wh', p4, 'Energie auxilaires ' + Math.round(p4) + 'Wh (' + p4 + '%)'],
		            ]);
		
		
		if(memory == 2 && tabMemory != undefined && tabMemory[0].length != 0){
			
			var nbPointTotal=tabMemoryRev.length-1;
			var eAux = Paux*nbPointTotal*inter/3600;
			var eAbsDissip = Math.abs(tabMemoryRev[nbPointTotal][37]);
			var eTotal = tabMemoryRev[nbPointTotal][29] + eAbsDissip+tabMemoryRev[nbPointTotal][30]+eAux;
			var eConso = tabMemoryRev[nbPointTotal][29];
			var p1 = Math.round((eConso)/eTotal*10*100)/10	;					// Pourcentage
																				// d'energie
																				// consommee
			var p2 = Math.round((tabMemoryRev[nbPointTotal][30])/eTotal*10*100)/10;	// Pourcentage
																				// d'energie
																				// recuperee
			var p3 = Math.round((eAbsDissip)/eTotal*10*100)/10 ;					// Pourcentage
																				// d'energie
																				// dissipee
			var p4 = Math.round((eAux)/eTotal*10*100)/10;						// Pourcentage
																				// d'energie
																				// auxiliaires
			data2.addRows([
			              ['Energie recuperee ' + Math.round(tabMemoryRev[nbPointTotal][30]) + ' Wh', tabMemoryRev[nbPointTotal][30], 'Energie recupere ' + Math.round(tabMemoryRev[nbPointTotal][30]) + 'Wh (' + p2 + '%)'],
			              ['Energie consommee ' + Math.round(eConso) + ' Wh', eConso, 'Energie consommee ' + Math.round(eConso) + 'Wh (' + p1 + '%)'],
			              ['Energie non recuperee ' + Math.round(eAbsDissip) + ' Wh', eAbsDissip, 'Energie non recuperee ' + Math.round(eAbsDissip) + 'Wh (' + p3 + '%)'],// en
																																											// fait
																																											// energie
																																											// non
																																											// recuperee
																																											// lors
																																											// du
																																											// passage
																																											// en
																																											// generatrice
			              ['Energie auxiliaires ' + Math.round(p4) + ' Wh', p4, 'Energie auxilaires ' + Math.round(p4) + 'Wh (' + p4 + '%)'],
			            ]);
		}
		
		
	}
	else if(typeVehicule=="VT"){
		var p1 = Math.round(dataConso[nbPointConso][29]/dataConso[nbPointConso][57]*10*100)/10;	// Pourcentage
																								// d'energie
																								// consommee
		var p2 = Math.round(dataConso[nbPointConso][58]/dataConso[nbPointConso][57]*10*100)/10;	// Pourcentage
																								// d'energie
																								// dissipee
		data.addRows([
            ['Energie consommee ' + Math.round(dataConso[nbPointConso][29]) + ' Wh', dataConso[nbPointConso][29], 'Energie consommee ' + Math.round(dataConso[nbPointConso][29]) + 'Wh (' + p1 + '%)'],
            ['Energie dissipee ' + Math.round(dataConso[nbPointConso][58]) + ' Wh', dataConso[nbPointConso][58], 'Energie dissipee ' + Math.round(dataConso[nbPointConso][58]) + 'Wh (' + p2 + '%)'],
          ]);
		
		if(memory == 2 && tabMemory != undefined && tabMemory[0].length != 0){
			var nbPointTotal=tabMemoryRev.length-1;
			var p3 = Math.round(tabMemoryRev[nbPointTotal][29]/tabMemoryRev[nbPointTotal][57]*10*100)/10;	// Pourcentage
						// d'energie
						// consommee
			var p4 = Math.round(tabMemoryRev[nbPointTotal][58]/tabMemoryRev[nbPointTotal][57]*10*100)/10;	// Pourcentage
						// d'energie
						// dissipee
			data2.addRows([
			['Energie consommee ' + Math.round(tabMemoryRev[nbPointTotal][29]) + ' Wh', tabMemoryRev[nbPointTotal][29], 'Energie consommee ' + Math.round(tabMemoryRev[nbPointTotal][29]) + 'Wh (' + p3 + '%)'],
			['Energie dissipee ' + Math.round(tabMemoryRev[nbPointTotal][58]) + ' Wh', tabMemoryRev[nbPointTotal][58], 'Energie dissipee ' + Math.round(tabMemoryRev[nbPointTotal][58]) + 'Wh (' + p4 + '%)'],
			]);
						
		}
	}
	else if(typeVehicule == "VH"){
		var p1 = Math.round(dataBusHybride[20][nbPointHybride]/dataBusHybride[24][nbPointHybride]*10*100)/10	// Pourcentage
																												// d'énergie
																												// batterie
		var p2 = Math.round(dataBusHybride[22][nbPointHybride]/dataBusHybride[24][nbPointHybride]*10*100)/10	// Pourcentage
																												// d'énergie
																												// moteur
																												// thermique

		data.addRows([
            ['Énergie moteur thermique ' + Math.round(dataBusHybride[22][nbPointHybride]) + ' W.h', dataBusHybride[22][nbPointHybride], 'Énergie moteur thermique ' + Math.round(dataBusHybride[22][nbPointHybride]) + 'W.h (' + p2 + '%)'],
            ['Énergie batterie ' + Math.round(dataBusHybride[20][nbPointHybride]) + ' W.h', dataBusHybride[20][nbPointHybride], 'Énergie batterie ' + Math.round(dataBusHybride[20][nbPointHybride]) + 'W.h (' + p1 + '%)']
            ]);
	}
	else if(typeVehicule=="VP"){
		var p1 = Math.abs(Math.round(dataConsoPac[2][dataConsoPac[2].length - 1]/(dataConsoPac[2][dataConsoPac[2].length - 1]+dataConsoPac[5][dataConsoPac[5].length - 1])*10*100)/10)	// Pourcentage
																																														// d'energie
																																														// consommee
		var p2 = Math.abs(Math.round(dataConsoPac[5][dataConsoPac[5].length - 1]/(dataConsoPac[2][dataConsoPac[2].length - 1]+dataConsoPac[5][dataConsoPac[5].length - 1])*10*100)/10)	// Pourcentage
																																														// d'energie
																																														// dissipee
		data.addRows([
            ['Energie batterie ' + Math.abs(Math.round(dataConsoPac[2][dataConsoPac[2].length - 1])) + ' Wh', Math.abs(dataConsoPac[2][dataConsoPac[2].length - 1]), 'Energie consommee ' + Math.abs(Math.round(dataConsoPac[2][dataConsoPac[2].length - 1])) + 'Wh (' + p1 + '%)'],
            ['Energie pile a combustible ' + Math.abs(Math.round(dataConsoPac[5][dataConsoPac[5].length - 1])) + ' Wh', Math.abs(dataConsoPac[5][dataConsoPac[5].length - 1]), 'Energie dissipee ' + Math.abs(Math.round(dataConsoPac[5][dataConsoPac[5].length - 1])) + 'Wh (' + p2 + '%)']
            ]);
	}
	else if(typeVehicule=="VPU"){
		//chart consommation instantanée
		var p1 = Math.abs(Math.round(dataConsoPacUTBM[2][dataConsoPacUTBM[2].length - 1]/(dataConsoPacUTBM[2][dataConsoPacUTBM[2].length - 1]+dataConsoPacUTBM[5][dataConsoPacUTBM[5].length - 1])*10*100)/10)	// Pourcentage
																																																				// d'energie
																																																				// batterie
		var p2 = Math.abs(Math.round(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length - 1]/(dataConsoPacUTBM[2][dataConsoPacUTBM[2].length - 1]+dataConsoPacUTBM[5][dataConsoPacUTBM[5].length - 1])*10*100)/10)	// Pourcentage
																																																				// d'energie
																																																				// PAC
		data.addRows([
		    ['Energie batterie ' + Math.abs(Math.round(dataConsoPacUTBM[2][dataConsoPacUTBM[2].length - 1])) + ' Wh', Math.abs(dataConsoPacUTBM[2][dataConsoPacUTBM[2].length - 1]), 'Energie consommee ' + Math.abs(Math.round(dataConsoPacUTBM[2][dataConsoPacUTBM[2].length - 1])) + 'Wh (' + p1 + '%)'],
            ['Energie pile a combustible ' + Math.abs(Math.round(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length - 1])) + ' Wh', Math.abs(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length - 1]), 'Energie dissipee ' + Math.abs(Math.round(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length - 1])) + 'Wh (' + p2 + '%)']
            ]);
		
		//chart consommation totale
		
		if(memory == 2 && tabMemory != undefined && tabMemory[0].length != 0){
			var percent1 = Math.abs(Math.round(tabMemory[2][tabMemory[2].length - 1]/(tabMemory[2][tabMemory[2].length - 1]+tabMemory[5][tabMemory[5].length - 1])*10*100)/10);   //% énergie batterie
			
			var percent2 = Math.abs(Math.round(tabMemory[5][tabMemory[5].length - 1]/(tabMemory[2][tabMemory[2].length - 1]+tabMemory[5][tabMemory[5].length - 1])*10*100)/10);   //% énergie PAC
			
			// PAC
			data2.addRows([
			    ['Energie batterie ' + Math.abs(Math.round(tabMemory[2][tabMemory[2].length - 1])) + ' Wh', Math.abs(tabMemory[2][tabMemory[2].length - 1]), 'Energie consommee ' + Math.abs(Math.round(tabMemory[2][tabMemory[2].length - 1])) + 'Wh (' + percent1 + '%)'],
	            ['Energie pile a combustible ' + Math.abs(Math.round(tabMemory[5][tabMemory[5].length - 1])) + ' Wh', Math.abs(tabMemory[5][tabMemory[5].length - 1]), 'Energie dissipee ' + Math.abs(Math.round(tabMemory[5][tabMemory[5].length - 1])) + 'Wh (' + percent2 + '%)']
	            ]);
		
		}
		
	}
    var options = {
      title: 'Bilan des énergies (Wh)',
      titleTextStyle: { color: '#365F91',
    	  fontSize: 13,
    	  bold: true,
    	  italic: false },
      is3D: false,
      slices: {  
    	  0: {offset: 0,color: 'green'},
    	  1:{color:'blue'},
      },
      pieSliceText:'percentage',
      tooltip :{
    	trigger:'selection',
    	ignoreBounds:true,
      },
      chartArea:{left:"2%",top:"20%",width:'60%',height:'60%'},
      sliceVisibilityThreshold: .0002,
    };
  
    var chartenergie = new google.visualization.PieChart(document.getElementById('piechart_energie'+typeVehicule));
    chartenergie.draw(data, options);
    
    if(memory == 2 && tabMemory != undefined && tabMemory[0].length != 0){
    	var chartenergie2 = new google.visualization.PieChart(document.getElementById('piechart_energie'+typeVehicule+"3"));
        chartenergie2.draw(data2, options);
    }
}
/**
 * Affichage du taux de charge reel dans le rapport
 */
function affTauxCharge(){
	var dataChart;
	var colonne1 = 0;
	var colonne2 = 32;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', dataConso[0][colonne1]);
	dataChart.addColumn('number', dataConso[0][colonne2]);
	dataChart.addColumn({'type': 'string', 'role': 'tooltip'});
	dataChart.addColumn({'type': 'string', 'role': 'style'});
	var tpsTampon = 0;
	var tauxTampon = 0;
	var tauxmin = 100-dataConso[nbPointConso][32]+socMin;
	var tempsChargeMin = capBatterie*(tauxmin-tauxDeChargeBorne)/100/puissanceBorne*3600; // s
	for(var i = 1;i<nbPointConso+1;i++){
		if(boolBorne && i == tpsBorne){		
			var taux = dataConso[i][32];
			
			var pente = (tauxmin-taux)/(tempsChargeMin);
			while(taux<tauxmin){
				tpsTampon +=1;
				taux += pente; 
				dataChart.addRow([parseFloat((dataConso[i][colonne1]+tpsTampon)/60),parseFloat(taux),tooltip("Temps (min) ", (dataConso[i][colonne1]+tpsTampon)/60,dataConso[0][colonne2], taux),'color: green']);
			}
			tauxTampon = -(dataConso[nbPointConso][32]-socMin);
		}else{
			dataChart.addRow([parseFloat((dataConso[i][colonne1]+tpsTampon)/60),parseFloat((dataConso[i][colonne2]+tauxTampon)),tooltip("Temps (min) ", (dataConso[i][colonne1]+tpsTampon)/60,dataConso[0][colonne2], dataConso[i][colonne2]+tauxTampon),'color: blue']);
		}
		
	}
	  document.getElementById("tauxChargeChart"+typeVehicule).style.display = 'block';
	  chartCharge.draw(dataChart, {
	  	vAxis: {title: 'Temps (min)'},
	  	hAxis: {title: dataConso[0][colonne2]},
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},
	  	bar: {groupWidth: '100%'},
	  	width:"100%",
	  	chartArea:{left:"10%", right:0, top:"10%", width:"100%", height:"60%"},
	  	axisTitlesPosition: "in",
	  	isHtml: true,
	  	lineWidth: 2,
	  	areaOpacity: 0.4,
	  	// height:300,
	  	colors: ['#295DBC', '#F29D00', '#ED0300'],
	  });	
}
/**
 * affichage de la map cse VT
 */
function AffMapCseVT() {

    dataessai = new google.visualization.DataTable();
    dataessai.addColumn('number', 'Regime moteur (tours/min)');
    dataessai.addColumn('number', 'Charge moteur (%)');
    dataessai.addColumn('number', 'Consommation specifique d\'energie (g/kWh)');

    for (var i = 1; i < longueurvecNmot; i++) {
    	for (var j = 1; j < longueurvecCmot; j++) {
    		dataessai.addRow([vecNmot[i],vecCmot[j],Map_cse_ex[i][j]]);
    	}
    }


    options = {width:  "100%",
    		   height:"100%",
               style: "surface",
               showPerspective: true,
               showGrid: true,
               showShadow: false,
               keepAspectRatio: false,
               verticalRatio: 0.5,
               };

    document.getElementById('tableCseVT').style.display="";
    graph = new links.Graph3d(document.getElementById('MapCseChartVT'));
    graph.draw(dataessai, options); 
}
/**
 * affichage de la map cse VH
 */
function AffMapCseVH() {

    dataessai = new google.visualization.DataTable();
    dataessai.addColumn('number', 'Regime moteur (tours/min)');
    dataessai.addColumn('number', 'Charge moteur (%)');
    dataessai.addColumn('number', 'Consommation specifique d\'energie (g/kWh)');

    for (var i = 1; i < longueurvecNmot; i++) {
    	for (var j = 1; j < longueurvecCmot; j++) {
    		dataessai.addRow([vecNmot[i],vecCmot[j],Map_cse_ex[i][j]]);
    	}
    }



    options = {width:  "100%",
    		   height:"100%",
               style: "surface",
               showPerspective: true,
               showGrid: true,
               showShadow: false,
               keepAspectRatio: false,
               verticalRatio: 0.5,
               };


    document.getElementById('tableCseVH').style.display="";
    graph = new links.Graph3d(document.getElementById('MapCseChartVH'));
    graph.draw(dataessai, options); 
}
/**
 * affichage des differents rapports de reduction VT
 */
function affichage_VitessesChartVT(){
	var ChartVitesses = new google.visualization.ScatterChart(document.getElementById('ChartVitessesVT'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', dataObd[3][0]);
	dataChart.addColumn('number', dataObd[2][0]);
	for(i=1;i<dataObd[2].length;i++){
		dataChart.addRow([dataObd[3][i] , dataObd[2][i]]);
	}
      ChartVitesses.draw(dataChart, {
    	title: "Evolution du regime moteur en fonction de la vitesse",
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},  	
	  	// chartArea:{left:chartleft,
		// right:chartright,
		// top:charttop,
		// width:chartwidth,
		// height:"80%"},
	  	isHtml: true,
	  	lineWidth: 0,
	  	pointShape:{ type: 'star', sides: 4 },
	    pointSize: 4,
	    hAxis: {title:'Vitesse (km/h)'},
	  	vAxis: {title:'Regime moteur (tours/min)',
	  		viewWindow: {
	  			min: 100*Math.round(rechercheMin(dataObd[2])/100)},
	  			baseline : 100*Math.round(rechercheMin(dataObd[2])/100)
	  		},
	    axisTitlesPosition: "in"
	  });
      var RapportsVitesses= new google.visualization.Table(document.getElementById('RapportsVitessesVT'));
      dataChart = new google.visualization.DataTable();
      dataChart.addColumn('number', "Numero");
      dataChart.addColumn('number', "Rapport de reduction");
      for(i=1;i<dataVitesses.length;i++){
  		dataChart.addRow([i,dataVitesses[i]]);
      }
      RapportsVitesses.draw(dataChart,{
	  	width: '100%', 
	  	height: '100%'
	  		
	  });
}
/**
 * affichage des differents rapports de reduction VH
 */
function affichage_VitessesChartVH(){
	var ChartVitesses = new google.visualization.ScatterChart(document.getElementById('ChartVitessesVH'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('number', dataObd[3][0]);
	dataChart.addColumn('number', dataObd[2][0]);
	for(i=1;i<dataObd[2].length;i++){
		dataChart.addRow([dataObd[3][i] , dataObd[2][i]]);
	}
      ChartVitesses.draw(dataChart, {
    	title: "Evolution du regime moteur en fonction de la vitesse",
	  	legend: {position:'none'},
	  	backgroundColor: '#FFFFF',
	  	animation: {duration: 2000, easing: 'linear'},  	
	  	// chartArea:{left:chartleft,
		// right:chartright,
		// top:charttop,
		// width:chartwidth,
		// height:"80%"},
	  	isHtml: true,
	  	lineWidth: 0,
	  	pointShape:{ type: 'star', sides: 4 },
	    pointSize: 4,
	    hAxis: {title:'Vitesse (km/h)'},
	  	vAxis: {title:'Regime moteur (tours/min)',
	  		viewWindow: {
	  			min: 100*Math.round(rechercheMin(dataObd[2])/100)},
	  			baseline : 100*Math.round(rechercheMin(dataObd[2])/100)
	  		},
	    axisTitlesPosition: "in"
	  });
      var RapportsVitesses= new google.visualization.Table(document.getElementById('RapportsVitessesVH'));
      dataChart = new google.visualization.DataTable();
      dataChart.addColumn('number', "Numero");
      dataChart.addColumn('number', "Rapport de reduction");
      for(i=1;i<dataVitesses.length;i++){
  		dataChart.addRow([i,dataVitesses[i]]);
      }
      RapportsVitesses.draw(dataChart,{
	  	width: '100%', 
	  	height: '100%'
	  		
	  });
     
}



/**
 * affichage des resultats dans l'onglet Bilan consommation VE (prédiction)
 */
function affichage_RapportVehEChart(){
	var consoMoy = (dataConso[nbPointConso][29]-dataConso[nbPointConso][30])/dataConso[nbPointConso][36] // wh/km
	
	var ChartVehE = new google.visualization.Table(document.getElementById('RapportVehEChart'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	dataChart.addRow(["Durée du trajets (minutes)",(dataConso[nbPointConso][0])/60])
	dataChart.addRow(["Distance du trajet (km)",(dataConso[nbPointConso][36])])
	dataChart.addRow(["Energie totale fournie par la batterie pendant le trajet (Wh)" , Math.round(dataConso[nbPointConso][29]*100)/100]);
	dataChart.addRow(["Energie totale theorique generee pendant le trajet (Wh)", Math.round(dataConso[nbPointConso][30]*100)/100]);
	var temp = Math.round(dataConso[nbPointConso][30]/dataConso[nbPointConso][29]*100*100)/100;
	(temp>100)? temp=100:
	dataChart.addRow(["Energie regeneree pendant le trajet (%)",temp]);
	dataChart.addRow(["Energie totale requise pour le trajet (Wh)",Math.round(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])]);
	dataChart.addRow(["Energie requise sans regeneratif pour 100 km (kWh)",Math.round(10*((100*dataConso[nbPointConso][29]/dataConso[nbPointConso][3])))/10]);
	dataChart.addRow(["Energie requise avec regeneratif pour 100 km (kWh)",Math.round(10*100*(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])/dataConso[nbPointConso][3])/10]);
	dataChart.addRow(["Consommation normalisee (Wh/km)", Math.round(consoMoy)]);
	if(boolBorne){	
		dataChart.addRow(['Temps de recharge de la batterie (100%) (heures)', Math.round(capBatterie*(100-tauxDeChargeBorne)/100/puissanceBorne*10)/10]);
		dataChart.addRow(['Temps de recharge de la batterie (minimum) (heures)',Math.round(capBatterie*(100-dataConso[nbPointConso][32]-tauxDeChargeBorne+socMin)/100/puissanceBorne*10)/10]);
		dataChart.addRow(['Taux de charge necessaire (minimum) apres la recharge (%)',Math.round((100-dataConso[nbPointConso][32]+socMin)*10)/10]);
		dataChart.addRow(['Charge totale nécessaire pour le trajet (%)', Math.round((100-dataConso[nbPointConso][32]+socBatterie-tauxDeChargeBorne)*10)/10]);
		dataChart.addRow(['Energie totale consommée (Wh)',(1/rend_elec)*Math.round(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])]);
	}
	else{
		dataChart.addRow(['Charge totale nécessaire pour le trajet (%)', Math.round((socBatterie-dataConso[nbPointConso][32])*10)/10]);
		dataChart.addRow(['Energie totale consommée du secteur (Wh)',(1/rend_elec)*Math.round(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])]);
		dataChart.addRow(['Prix du trajet (€)',Math.round((1/rend_elec)*(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])*prixElec/1000*100)/100 ]);
	}
	
	ChartVehE.draw(dataChart,{
		width: '95%', 
		height: '100%'
	}); 
	boolRapport = true;
	document.getElementById("synthese"+typeVehicule).style.display = '';
	affTauxCharge();
}


var tabMemoryRev=[];
function affichage_RapportVehEChartTotal(){
	
	window.tabMemoryRev = reversetab(tabMemory,tabMemory.length,tabMemory[0].length)
	
	var ChartVehE = new google.visualization.Table(document.getElementById('RapportVehEChartTotal'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	var nbPointTotal= tabMemoryRev.length-1;
	var consoMoyTT = (tabMemoryRev[nbPointTotal][29]-tabMemoryRev[nbPointTotal][30])/tabMemoryRev[nbPointTotal][36] // wh/km
	
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	dataChart.addRow(["Durée du trajets (minutes)",(tabMemoryRev[nbPointTotal][0])/60])
	dataChart.addRow(["Distance du trajet (km)",(tabMemoryRev[nbPointTotal][36])])
	dataChart.addRow(["Energie totale fournie par la batterie pendant le trajet (Wh)" , Math.round(tabMemoryRev[nbPointTotal][29]*100)/100]);
	dataChart.addRow(["Energie totale theorique generee pendant le trajet (Wh)", Math.round(tabMemoryRev[nbPointTotal][30]*100)/100]);
	var temp = Math.round(tabMemoryRev[nbPointTotal][30]/tabMemoryRev[nbPointTotal][29]*100*100)/100;
	(temp>100)? temp=100:
	dataChart.addRow(["Energie regeneree pendant le trajet (%)",temp]);
	dataChart.addRow(["Energie totale requise pour le trajet (Wh)",Math.round(tabMemoryRev[nbPointTotal][29]-tabMemoryRev[nbPointTotal][30])]);
	dataChart.addRow(["Energie requise sans regeneratif pour 100 km (kWh)",Math.round(10*((100*tabMemoryRev[nbPointTotal][29]/tabMemoryRev[nbPointTotal][3])))/10]);
	dataChart.addRow(["Energie requise avec regeneratif pour 100 km (kWh)",Math.round(10*100*(tabMemoryRev[nbPointTotal][29]-tabMemoryRev[nbPointTotal][30])/tabMemoryRev[nbPointTotal][3])/10]);
	dataChart.addRow(["Consommation normalisee (Wh/km)", Math.round(consoMoyTT)]);
	if(boolBorne){	
		dataChart.addRow(['Temps de recharge de la batterie (100%) (heures)', Math.round(capBatterie*(100-tauxDeChargeBorne)/100/puissanceBorne*10)/10]);
		dataChart.addRow(['Temps de recharge de la batterie (minimum) (heures)',Math.round(capBatterie*(100-tabMemoryRev[nbPointTotal][32]-tauxDeChargeBorne+socMin)/100/puissanceBorne*10)/10]);
		dataChart.addRow(['Taux de charge necessaire (minimum) apres la recharge (%)',Math.round((100-tabMemoryRev[nbPointTotal][32]+socMin)*10)/10]);
		dataChart.addRow(['Charge totale nécessaire pour le trajet (%)', Math.round((100-tabMemoryRev[nbPointTotal][32]+socBatterie-tauxDeChargeBorne)*10)/10]);
		dataChart.addRow(['Energie totale consommée (Wh)',(1/rend_elec)*Math.round(tabMemoryRev[nbPointTotal][29]-tabMemoryRev[nbPointTotal][30])]);
	}
	else{
		dataChart.addRow(['Charge totale nécessaire pour le trajet (%)', Math.round((tabMemoryRev[1][32]-tabMemoryRev[nbPointTotal][32])*10)/10]);
		dataChart.addRow(['Energie totale consommée du secteur (Wh)',(1/rend_elec)*Math.round(tabMemoryRev[nbPointTotal][29]-tabMemoryRev[nbPointTotal][30])]);
		dataChart.addRow(['Prix du trajet (€)',Math.round((1/rend_elec)*(tabMemoryRev[nbPointTotal][29]-tabMemoryRev[nbPointTotal][30])*prixElec/1000*100)/100 ]);
	}
	
	ChartVehE.draw(dataChart,{
		width: '95%', 
		height: '100%'
	}); 
	boolRapport = true;
	document.getElementById("synthese"+typeVehicule).style.display = '';
	//affTauxCharge();			//affiche de la courbe de charge dans l'onglet 'recharche' du VE
}





/**
 * affichage des resultats dans l'onglet Bilan consommation VT (prédiction)
 */
function affichage_RapportVehTChart(){
	
	
	
	var ChartVehT = new google.visualization.Table(document.getElementById('RapportVehTChart'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	dataChart.addRow(["Durée du trajets (minutes)",(dataConso[nbPointConso][0])/60])
	dataChart.addRow(["Distance du trajet (km)",(dataConso[nbPointConso][36])])
	dataChart.addRow([dataConso[0][54] , Math.round(dataConso[nbPointConso][54]*100)/100]); //Conso litre tt
	dataChart.addRow([dataConso[0][55] , Math.round(dataConso[nbPointConso][55]*100)/100]);	//Conso litre/100km
	dataChart.addRow([dataConso[0][56] , Math.round(dataConso[nbPointConso][56]*100)/100]);	//CO2
	dataChart.addRow([dataConso[0][57] , Math.round(dataConso[nbPointConso][57]*100)/100]);	//Eprod
	dataChart.addRow([dataConso[0][58] , Math.round(dataConso[nbPointConso][58]*100)/100]);	//Edissip
	dataChart.addRow([dataConso[0][29] , Math.round(dataConso[nbPointConso][29]*100)/100]);	//Econsomée
	var prixEssence = 1.20;
	//dataChart.addRow(["Rendement du moteur (%)",rendementmoteur]);
	dataChart.addRow(["Prix du trajet (€)" , (Math.round(dataConso[nbPointConso][54]*100)/100)*prixEssence]); //prix
	dataChart.addRow([dataConso[0][73] , Math.round(dataConso[nbPointConso][73]*100)/100]);					 //Conso tt opti
	dataChart.addRow([dataConso[0][74] , Math.round(dataConso[nbPointConso][74]*100)/100]);					 // Conso moy TT opti
	//dataChart.addRow(["Rendement du moteur optimal (%)",rendementmoteurOpti]);
	
	// dataChart.addRow([dataConso[0][73]
	// ,
	// Math.round(dataConso[nbPointConso][73]*100)/100]);
	// dataChart.addRow([dataConso[0][74]
	// ,
	// Math.round(dataConso[nbPointConso][74]*100)/100]);
	// dataChart.addRow(["Rendement
	// du
	// moteur
	// optimal
	// (%)",rendementmoteurOpti]);
	ChartVehT.draw(dataChart,{
		width: '95%', 
		height: '100%'
	}); 
}

function affichage_RapportVehTChartTotal(){
	
	window.tabMemoryRev = reversetab(tabMemory,tabMemory.length,tabMemory[0].length);
	var nbPointTotal=tabMemoryRev.length-1;
	
	var ChartVehT = new google.visualization.Table(document.getElementById('RapportVehTChartTotal'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	dataChart.addRow(["Durée du trajets (minutes)",(tabMemoryRev[nbPointTotal][0])/60])
	dataChart.addRow(["Distance du trajet (km)",(tabMemoryRev[nbPointTotal][36])])
	dataChart.addRow([tabMemoryRev[0][54] , Math.round(tabMemoryRev[nbPointTotal][54]*100)/100]); //Conso litre tt
	dataChart.addRow([tabMemoryRev[0][55] , Math.round(tabMemoryRev[nbPointTotal][55]*100)/100]);	//Conso litre/100km
	dataChart.addRow([tabMemoryRev[0][56] , Math.round(tabMemoryRev[nbPointTotal][56]*100)/100]);	//CO2
	dataChart.addRow([tabMemoryRev[0][57] , Math.round(tabMemoryRev[nbPointTotal][57]*100)/100]);	//Eprod
	dataChart.addRow([tabMemoryRev[0][58] , Math.round(tabMemoryRev[nbPointTotal][58]*100)/100]);	//Edissip
	dataChart.addRow([tabMemoryRev[0][29] , Math.round(tabMemoryRev[nbPointTotal][29]*100)/100]);	//Econsomée
	var prixEssence = 1.20;
	//dataChart.addRow(["Rendement du moteur (%)",rendementmoteur]);
	dataChart.addRow(["Prix du trajet (€)" , (Math.round(tabMemoryRev[nbPointTotal][54]*100)/100)*prixEssence]); //prix
	dataChart.addRow([tabMemoryRev[0][73] , Math.round(tabMemoryRev[nbPointTotal][73]*100)/100]);					 //Conso tt opti
	dataChart.addRow([tabMemoryRev[0][74] , Math.round(tabMemoryRev[nbPointTotal][74]*100)/100]);					 // Conso moy TT opti
	//dataChart.addRow(["Rendement du moteur optimal (%)",rendementmoteurOpti]);
	
	
	ChartVehT.draw(dataChart,{
		width: '95%', 
		height: '100%'
	}); 
}
/**
 * affichage des resultats dans l'onglet Bilan consommation VH (prédiction)
 */
function affichage_RapportVehHChart(){
	var ChartVehT = new google.visualization.Table(document.getElementById('RapportVehHChart'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	dataChart.addRow([dataBusHybride[31][0] , Math.round(dataBusHybride[31][nbPointHybride]*100)/100]); // Concommation
																										// totale
																										// en L
	dataChart.addRow(["Consommation moyenne (L/100km)" , Math.round((dataBusHybride[31][nbPointHybride]/dataBusHybride[3][nbPointHybride]*100)*100)/100]); // Consommation
																																							// totale
																																							// en
																																							// L/100km
	// dataChart.addRow(["Rendement
	// du
	// moteur
	// (%)",rendementmoteur]);
	dataChart.addRow([dataBusHybride[22][0] , Math.round(dataBusHybride[22][nbPointHybride]*100)/100]); // Énergie
																										// moteur
																										// thermique
	dataChart.addRow([dataBusHybride[20][0] , Math.round(dataBusHybride[20][nbPointHybride]*100)/100]); // Énergie
																										// batterie
																										// fournie
	dataChart.addRow([dataBusHybride[34][0] , Math.round(dataBusHybride[34][nbPointHybride]*100)/100]); // Énergie
																										// batterie
																										// récupérée
	


	
	ChartVehT.draw(dataChart,{
		width: '95%', 
		height: '100%'
	}); 
}

function affichage_RapportVehPChart(){
	var ChartVehT = new google.visualization.Table(document.getElementById('RapportVehPChart'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	dataChart.addRow([dataConsoPac[6][0] , Math.round(dataConsoPac[6][dataConsoPac[6].length - 1]*100)/100]);
	// dataChart.addRow([dataConsoPac[5][0]
	// ,
	// Math.round(dataConsoPac[5][dataConsoPac[5].length
	// -
	// 1]*100)/100]);
	
	ChartVehT.draw(dataChart,{
		width: '95%', 
		height: '100%'
	}); 
}

function affichage_RapportVehPUChart(){
	//console.log(dataConso);
	var ChartVehT = new google.visualization.Table(document.getElementById('RapportVehPUChart'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	// document.body.innerHTML=dataConsoPacUTBM[8]
	var valDuree=0;
	for (var i=1;i<dataConsoPacUTBM[12].length-1;i++){
		valDuree += dataConsoPacUTBM[12][i];
	}
// for(var
// i = 1;
// i <
// nbPoint+1;i++){
// tab[i][0]
// =
// (i-1)*inter;
// tab[i][1]
// = Vmoy;
// tab[i][2]
// =
// Vmoy/3.6;
// }
	dataChart.addRow(["Durée du trajets (minutes)",(dataConsoPacUTBM[12][dataConsoPacUTBM[12].length-1])/60])
	dataChart.addRow(["Distance du trajet (km)",(dataConsoPacUTBM[7][dataConsoPacUTBM[7].length-1])])
	dataChart.addRow([dataConsoPacUTBM[6][0] , Math.round(dataConsoPacUTBM[6][dataConsoPacUTBM[6].length - 1]*1000)/1000]);
	dataChart.addRow(["Consommation moyenne d'hydrogène (kg/100km)", ((dataConsoPacUTBM[6][dataConsoPacUTBM[6].length - 1])/(dataConsoPacUTBM[7][dataConsoPacUTBM[7].length-1])*100)]);
	dataChart.addRow(["Energie fournie par la PAC (Wh)", Math.round(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length-1]*1000)/1000])
	dataChart.addRow(["Energie fournie par la batterie (Wh)", Math.round(dataConsoPacUTBM[2][dataConsoPacUTBM[2].length-1]*1000)/1000])
	dataChart.addRow(["Energie récupérée par la batterie (Wh)", Math.round(dataConsoPacUTBM[21][dataConsoPacUTBM[21].length-1]*1000)/1000])
	dataChart.addRow(["Energie totale consommée (Wh)", Math.round(dataConsoPacUTBM[25][dataConsoPacUTBM[25].length-1])])
	dataChart.addRow(["Coût du trajet (euros)",Math.round(dataConsoPacUTBM[27][dataConsoPacUTBM[27].length-1]*100)/100])
	var EBatt = (dataConsoPacUTBM[11][1]-dataConsoPacUTBM[11][dataConsoPacUTBM[11].length-1])
	if (EBatt<0){EBatt=0}
// var
// EPaC =
// dataConsoPacUTBM[6][dataConsoPacUTBM[6].length
// - 1];
// var
// consoPaC
// =
// EPaC/((dataStep[nbStep][5]/1000));
// var
// consoBatt
// =
// EBatt/((dataStep[nbStep][5]/1000));
// var
// conso
// if(document.getElementById("loiCommande").value=="3"){
// dataChart.addRow(["Autonomie
// du
// véhicule
// (km)",((dataStep[nbStep][5]/1000)+(capH2-dataConsoPacUTBM[6][dataConsoPacUTBM[6].length
// -
// 1])/((dataConsoPacUTBM[6][dataConsoPacUTBM[6].length
// -
// 1])/(dataStep[nbStep][5]/1000)))+((0.6*capBatterie*1000-EBatt)/(EBatt/(dataStep[nbStep][5]/1000)))])
// }
	if (document.getElementById("loiCommande").value!="3"){
		dataChart.addRow(["Autonomie du véhicule (km)",(capH2/((dataConsoPacUTBM[6][dataConsoPacUTBM[6].length - 1])/(dataConsoPacUTBM[7][dataConsoPacUTBM[7].length-1])))])
	}
	dataChart.addRow(["Temps de recharge minimal nécessaire (minutes)",(EBatt/puissanceCharge)*60+dataConsoPacUTBM[6][dataConsoPacUTBM[6].length - 1]])

	// dataChart.addRow([dataConsoPacUTBM[5][0]
	// ,
	// Math.round(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length
	// -
	// 1]*100)/100]);
	

	ChartVehT.draw(dataChart,{
		width: '95%', 
		height: '100%'
	});
}

/*
 * Cette fonction permet d'afficher le résumé des données calculées lors d'un calcul avec mise en mémoire des résulats
 * Il fonctionne de la même façon que le autre fonction du même type, on utilise simplement tabMemory pour les calculs.
 */



function affichage_RapportVehPUChartTotal(){
	
	tabMemory;
	
	var ChartVehT = new google.visualization.Table(document.getElementById('RapportVehPUChartTotal'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	// document.body.innerHTML=dataConsoPacUTBM[8]
	var valDuree=0;
	for (var i=1;i<nbStep;i++){
		valDuree += dataStep[i][1];
	}
// for(var
// i = 1;
// i <
// nbPoint+1;i++){
// tab[i][0]
// =
// (i-1)*inter;
// tab[i][1]
// = Vmoy;
// tab[i][2]
// =
// Vmoy/3.6;
// }
	dataChart.addRow(["Durée du trajets (minutes)",(tabMemory[12][tabMemory[12].length-1])/60])
	dataChart.addRow(["Distance du trajet (km)",(tabMemory[7][tabMemory[7].length-1])])
	dataChart.addRow([tabMemory[6][0] , Math.round(tabMemory[6][tabMemory[6].length - 1]*1000)/1000]);
	dataChart.addRow(["Consommation moyenne d'hydrogène (kg/100km)", ((tabMemory[6][tabMemory[6].length - 1])/(tabMemory[7][tabMemory[7].length-1])*100)]);
	dataChart.addRow(["Energie fournie par la PAC (Wh)", Math.round(tabMemory[5][tabMemory[5].length-1]*1000)/1000])
	dataChart.addRow(["Energie fournie par la batterie (Wh)", Math.round(tabMemory[2][tabMemory[2].length-1]*1000)/1000])
	dataChart.addRow(["Energie récupérée par la batterie (Wh)", Math.round(tabMemory[21][tabMemory[21].length-1]*1000)/1000])
	dataChart.addRow(["Energie totale consommée (Wh)", Math.round(tabMemory[25][tabMemory[25].length-1])])
	dataChart.addRow(["Coût du trajet (euros)",Math.round(tabMemory[27][tabMemory[27].length-1]*100)/100])
	var EBatt = (tabMemory[11][1]-tabMemory[11][tabMemory[11].length-1])
	if (EBatt<0){EBatt=0}
// var
// EPaC =
// dataConsoPacUTBM[6][dataConsoPacUTBM[6].length
// - 1];
// var
// consoPaC
// =
// EPaC/((dataStep[nbStep][5]/1000));
// var
// consoBatt
// =
// EBatt/((dataStep[nbStep][5]/1000));
// var
// conso
// if(document.getElementById("loiCommande").value=="3"){
// dataChart.addRow(["Autonomie
// du
// véhicule
// (km)",((dataStep[nbStep][5]/1000)+(capH2-dataConsoPacUTBM[6][dataConsoPacUTBM[6].length
// -
// 1])/((dataConsoPacUTBM[6][dataConsoPacUTBM[6].length
// -
// 1])/(dataStep[nbStep][5]/1000)))+((0.6*capBatterie*1000-EBatt)/(EBatt/(dataStep[nbStep][5]/1000)))])
// }
	if (document.getElementById("loiCommande").value!="3"){
		dataChart.addRow(["Autonomie du véhicule (km)",(capH2/((tabMemory[6][tabMemory[6].length - 1])/(tabMemory[7][tabMemory[7].length-1])))])
	}
	dataChart.addRow(["Temps de recharge minimal nécessaire (minutes)",(EBatt/puissanceCharge)*60+tabMemory[6][tabMemory[6].length - 1]])

	// dataChart.addRow([dataConsoPacUTBM[5][0]
	// ,
	// Math.round(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length
	// -
	// 1]*100)/100]);
	

	ChartVehT.draw(dataChart,{
		width: '95%', 
		height: '100%'
	});
}





function affichage_RapportVehChartComparaison(){
	document.getElementById("tabbertabsComparaison").style.display=''
	var ChartVehT = new google.visualization.Table(document.getElementById('RapportVehChartComparaison'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	
	//dataChart.addRow(["Type de véhicule",typeVehiculeComp,typeVehiculeInit])
	
	if(typeVehiculeComp == "VE" && typeVehiculeInit == "VPU"){
		dataChart.addColumn('number',typeVehiculeInit);
		dataChart.addColumn('number',typeVehiculeComp);
		dataChart.addRow(["Coût du trajet (€)" , Math.round(dataConsoRecup[27][dataConsoRecup[27].length-1]*100)/100 , Math.round((1/rend_elec)*Math.round(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])*prixElec/1000*100)/100 ]);
		dataChart.addRow(["Rejet CO2 (kg)", Math.round(dataConsoRecup[6][dataConsoRecup[6].length - 1]*ratioH2*10)/10 , Math.round((1/rend_elec)*(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])*ratioElec*10)/10 ]);
		dataChart.addRow(["Energie totale consommée (Wh)", (Math.round(dataConsoRecup[5][dataConsoRecup[5].length-1]) + Math.round(dataConsoRecup[2][dataConsoRecup[2].length-1])) ,  Math.round((dataConso[nbPointConso][29]-dataConso[nbPointConso][30])) ]);
		dataChart.addRow(["Energie totale produite (Wh)", Math.round(dataConsoRecup[25][dataConsoRecup[25].length-1]) , Math.round((1/rend_elec)*(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])*1/1) ])
		dataChart.addRow(["Energie totale regénerée pendant le trajet (Wh)", Math.round(dataConsoRecup[21][dataConsoRecup[21].length-1]) , Math.round(dataConso[nbPointConso][30]) ]);
		
	}
	else if(typeVehiculeComp == "VPU" && typeVehiculeInit == "VE"){
		dataChart.addColumn('number',typeVehiculeInit);
		dataChart.addColumn('number',typeVehiculeComp);
		dataChart.addRow(["Coût du trajet (€)" , Math.round((1/rend_elec)*Math.round(dataConsoRecup[nbPointConso][29]-dataConsoRecup[nbPointConso][30])*prixElec/1000*100)/100 , Math.round(dataConsoPacUTBM[27][dataConsoPacUTBM[27].length-1]*100)/100 ]);
		dataChart.addRow(["Rejet CO2 (kg)", Math.round((1/rend_elec)*(dataConsoRecup[nbPointConso][29]-dataConsoRecup[nbPointConso][30])*ratioElec*10)/10 , Math.round(dataConsoPacUTBM[6][dataConsoPacUTBM[6].length - 1]*ratioH2*10)/10 ]);
		dataChart.addRow(["Energie totale consommée (Wh)", Math.round((dataConsoRecup[nbPointConso][29]-dataConsoRecup[nbPointConso][30])*1)/1 , Math.round((dataConsoPacUTBM[5][dataConsoPacUTBM[5].length-1] + dataConsoPacUTBM[2][dataConsoPacUTBM[2].length-1])*1)/1 ]);
		dataChart.addRow(["Energie totale produite (Wh)", Math.round((1/rend_elec)*(dataConsoRecup[nbPointConso][29]-dataConsoRecup[nbPointConso][30])*1)/1 , Math.round(dataConsoPacUTBM[25][dataConsoPacUTBM[25].length-1]*1)/1 ]);
		dataChart.addRow(["Energie totale regénerée pendant le trajet (Wh)", Math.round(dataConsoRecup[nbPointConso][30]*1)/1 , Math.round(dataConsoPacUTBM[21][dataConsoPacUTBM[21].length-1]*1)/1 ]);
		
	}
	else if(typeVehiculeComp == "VT" && typeVehiculeInit == "VE"){
		dataChart.addColumn('number',typeVehiculeInit);
		dataChart.addColumn('number',typeVehiculeComp);
		dataChart.addRow(["Coût du trajet (€)" , Math.round((1/rend_elec)*Math.round(dataConsoRecup[nbPointConso][29]-dataConsoRecup[nbPointConso][30])*prixElec/1000*100)/100 , Math.round(dataConso[nbPointConso][54]*prixEssence*100)/100 ]);
		dataChart.addRow(["Rejet CO2 (kg)",  Math.round((1/rend_elec)*(dataConsoRecup[nbPointConso][29]-dataConsoRecup[nbPointConso][30])*ratioElec*10)/10 , Math.round(dataConso[nbPointConso][56]*10)/10 ]);
		dataChart.addRow(["Energie totale consommée (Wh)", Math.round((dataConsoRecup[nbPointConso][29]-dataConsoRecup[nbPointConso][30])*10)/10 , Math.round(dataConso[nbPointConso][29]*1)/1 ]);
		dataChart.addRow(["Energie totale produite (Wh)", Math.round((1/rend_elec)*(dataConsoRecup[nbPointConso][29]-dataConsoRecup[nbPointConso][30])*10)/10 , Math.round(dataConso[nbPointConso][57]*1)/1 ])
		dataChart.addRow(["Energie totale regénerée pendant le trajet (Wh)", Math.round(dataConsoRecup[nbPointConso][30]*1)/1 , 0 ]);
		
	}
	else if(typeVehiculeComp == "VE" && typeVehiculeInit == "VT"){
		dataChart.addColumn('number',typeVehiculeInit);
		dataChart.addColumn('number',typeVehiculeComp);
		dataChart.addRow(["Coût du trajet (€)" , Math.round(dataConsoRecup[nbPointConso][54]*prixEssence*100)/100 , Math.round((1/rend_elec)*Math.round(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])*prixElec/1000*100)/100 ]);
		dataChart.addRow(["Rejet CO2 (kg)", Math.round(dataConsoRecup[nbPointConso][56]*10)/10 , Math.round((1/rend_elec)*(dataConso[nbPointConso][29]-dataConso[nbPointConso][30])*ratioElec*10)/10  ]);
		dataChart.addRow(["Energie totale consommée (Wh)", Math.round(dataConsoRecup[nbPointConso][29]*1)/1 , Math.round((dataConso[nbPointConso][29]-dataConso[nbPointConso][30])*10)/10 ]);
		dataChart.addRow(["Energie totale produite (Wh)", Math.round(dataConsoRecup[nbPointConso][57]*1)/1 , (1/rend_elec)*Math.round((dataConso[nbPointConso][29]-dataConso[nbPointConso][30])*10)/10 ])
		dataChart.addRow(["Energie totale regénerée pendant le trajet (Wh)", 0 , Math.round(dataConso[nbPointConso][30]*1)/1 ]);
		
	}
	else if(typeVehiculeComp == "VT" && typeVehiculeInit == "VPU"){
		dataChart.addColumn('number',typeVehiculeInit);
		dataChart.addColumn('number',typeVehiculeComp);
		dataChart.addRow(["Coût du trajet (€)" , Math.round(dataConsoRecup[27][dataConsoRecup[27].length-1]*100)/100 , Math.round(dataConso[nbPointConso][54]*prixEssence*100)/100 ]);
		dataChart.addRow(["Rejet CO2 (kg)", Math.round(dataConsoRecup[6][dataConsoRecup[6].length - 1]*ratioH2*10)/10 , Math.round(dataConso[nbPointConso][56]*10)/10 ]);
		dataChart.addRow(["Energie totale consommée (Wh)", Math.round(dataConsoRecup[5][dataConsoRecup[5].length-1]) , Math.round(dataConso[nbPointConso][29]*1)/1 ]);
		dataChart.addRow(["Energie totale produite (Wh)", Math.round(dataConsoRecup[25][dataConsoRecup[25].length-1]) , Math.round(dataConso[nbPointConso][57]*1)/1 ])
		dataChart.addRow(["Energie totale regénerée pendant le trajet (Wh)", Math.round(dataConsoRecup[21][dataConsoRecup[21].length-1]), 0 ]);
		
	}
	else if(typeVehiculeComp == "VPU" && typeVehiculeInit == "VT"){
		dataChart.addColumn('number',typeVehiculeInit);
		dataChart.addColumn('number',typeVehiculeComp);
		dataChart.addRow(["Coût du trajet (€)" , Math.round(dataConsoRecup[nbPointConso][54]*prixEssence*100)/100 , Math.round(dataConsoPacUTBM[27][dataConsoPacUTBM[27].length-1]*100)/100]);
		dataChart.addRow(["Rejet CO2 (kg)", Math.round(dataConsoRecup[nbPointConso][56]*10)/10 , Math.round(dataConsoPacUTBM[6][dataConsoPacUTBM[6].length - 1]*ratioH2*10)/10 ]);
		dataChart.addRow(["Energie totale consommée (Wh)", Math.round(dataConsoRecup[nbPointConso][29]*1)/1 , Math.round((dataConsoPacUTBM[5][dataConsoPacUTBM[5].length-1] + dataConsoPacUTBM[2][dataConsoPacUTBM[2].length-1])*1)/1 ]);
		dataChart.addRow(["Energie totale produite (Wh)", Math.round(dataConsoRecup[nbPointConso][57]*1)/1  , Math.round(dataConsoPacUTBM[25][dataConsoPacUTBM[25].length-1]*1)/1 ])
		dataChart.addRow(["Energie totale regénerée pendant le trajet (Wh)", 0 , Math.round(dataConsoPacUTBM[21][dataConsoPacUTBM[21].length-1]*1)/1 ]);
		
	}
	
	ChartVehT.draw(dataChart,{
		width: '95%', 
		height: '100%'
	});
	
}



function affichage_RapportVehPUChartAcqui(){
	var ChartVehT = new google.visualization.Table(document.getElementById('RapportVehPUChartAcqui'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	// document.body.innerHTML=dataConsoPacUTBM[8]
	var valDuree=0;
	for (var i=1;i<nbStep;i++){
		valDuree += dataStep[i][1];
	}
// for(var
// i = 1;
// i <
// nbPoint+1;i++){
// tab[i][0]
// =
// (i-1)*inter;
// tab[i][1]
// = Vmoy;
// tab[i][2]
// =
// Vmoy/3.6;
// }
	dataChart.addRow(["Durée du trajets (minutes)",(tableauTamponProfile[12][tableauTamponProfile[12].length-1])/60])
	dataChart.addRow(["Distance du trajet (km)",(tableauTamponProfile[7][tableauTamponProfile[7].length-1])])
	dataChart.addRow([tableauTamponProfile[6][0] , Math.round(tableauTamponProfile[6][tableauTamponProfile[6].length - 1]*1000)/1000]);
	dataChart.addRow(["Consommation moyenne d'hydrogène (kg/100km)", ((tableauTamponProfile[6][tableauTamponProfile[6].length - 1])/(dataStep[nbStep][5]/1000)*100)]);
	dataChart.addRow(["Energie fournie par la PAC (Wh)", Math.round(tableauTamponProfile[5][tableauTamponProfile[5].length-1]*1000)/1000])
	dataChart.addRow(["Energie fournie par la batterie (Wh)", Math.round(tableauTamponProfile[2][tableauTamponProfile[2].length-1]*1000)/1000])
	dataChart.addRow(["Energie récupérée par la batterie (Wh)", Math.round(tableauTamponProfile[21][tableauTamponProfile[21].length-1]*1000)/1000])
	dataChart.addRow(["Energie totale consommée (Wh)", Math.round(tableauTamponProfile[25][tableauTamponProfile[25].length-1])])
	dataChart.addRow(["Coût du trajet (euros)",Math.round(tableauTamponProfile[27][tableauTamponProfile[27].length-1]*100)/100])
	var EBatt = (tableauTamponProfile[11][1]-tableauTamponProfile[11][tableauTamponProfile.length-1]);
	if (EBatt<0){EBatt=0}

	if (document.getElementById("loiCommande").value!="3"){
		dataChart.addRow(["Autonomie du véhicule (km)",(capH2/((tableauTamponProfile[6][tableauTamponProfile[6].length - 1])/(dataStep[nbStep][5]/1000)))])
	}
	dataChart.addRow(["Temps de recharge minimal nécessaire (minutes)",(EBatt/puissanceCharge)*60+tableauTamponProfile[6][tableauTamponProfile[6].length - 1]])

	// dataChart.addRow([dataConsoPacUTBM[5][0]
	// ,
	// Math.round(dataConsoPacUTBM[5][dataConsoPacUTBM[5].length
	// -
	// 1]*100)/100]);
	

	ChartVehT.draw(dataChart,{
		width: '95%', 
		height: '100%'
	});
}




/**
 * affichage des resultats dans l'onglet Bilan consommation (acquisition)
 */
function affichage_RapportVehTChartBCO(){
	var ChartVehTBCO = new google.visualization.Table(document.getElementById('RapportVehTChartBCO'));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string',"Resultats");
	dataChart.addColumn('number',"");
	dataChart.addRow([dataReellesVTBCO[0][0] , Math.round(dataReellesVTBCO[0][1]*100)/100]);
	dataChart.addRow([dataReellesVTBCO[1][0] , Math.round(dataReellesVTBCO[1][1]*100)/100]);
	dataChart.addRow([dataReellesVTBCO[2][0], dataReellesVTBCO[2][1]]);
	/*
	 * dataChart.addRow([dataConso[0][73] , Math.round(dataConso[nbPointConso][73]*100)/100]); dataChart.addRow([dataConso[0][74] , Math.round(dataConso[nbPointConso][74]*100)/100]); dataChart.addRow(["Rendement du moteur optimal (%)",rendementmoteurOpti]);
	 */
	ChartVehTBCO.draw(dataChart,{
		width: '95%', 
		height: '100%'
	}); 
}
function affichage_ConseilsVE(pourcentage_acceleration_faible,pourcentage_acceleration_excessive,pourcentage_freinage_faible,
		pourcentage_freinage_excessif,pourcentage_vitesse_constante,pourcentage_distance_acceleration_faible,
		pourcentage_distance_acceleration_excessive,pourcentage_distance_freinage_faible,pourcentage_distance_freinage_excessif,
		pourcentage_distance_vitesse_constante){
	
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string','')
	dataChart.addColumn('number','pourcentage du temps')
	dataChart.addColumn('number','pourcentage de la distance')
	dataChart.addRows([
		['freinage excessif', pourcentage_freinage_excessif, pourcentage_distance_freinage_excessif],
		['freinage modéré', pourcentage_freinage_faible, pourcentage_distance_freinage_faible],
		['vitesse quasi-constante', pourcentage_vitesse_constante, pourcentage_distance_vitesse_constante],
		['accélération modérée', pourcentage_acceleration_faible, pourcentage_distance_acceleration_faible],
		['accélération excessive',pourcentage_acceleration_excessive, pourcentage_distance_acceleration_excessive]
	])
	var options = {
			width: '95%', 
			height: '100%',
			vAxis:{format: 'percent'},
			legend:{position : 'top'}
	        };
	var ChartConseilsVE = new google.visualization.ColumnChart(document.getElementById('GraphConseilsVE'));
	ChartConseilsVE.draw(dataChart,options);
}

function affichage_ConseilsVT(pourcentage_acceleration_faible,pourcentage_acceleration_excessive,pourcentage_freinage_faible,
		pourcentage_freinage_excessif,pourcentage_vitesse_constante,pourcentage_distance_acceleration_faible,
		pourcentage_distance_acceleration_excessive,pourcentage_distance_freinage_faible,pourcentage_distance_freinage_excessif,
		pourcentage_distance_vitesse_constante){
	
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string','')
	dataChart.addColumn('number','pourcentage du temps')
	dataChart.addColumn('number','pourcentage de la distance')
	dataChart.addRows([
		['freinage excessif', pourcentage_freinage_excessif, pourcentage_distance_freinage_excessif],
		['freinage modéré', pourcentage_freinage_faible, pourcentage_distance_freinage_faible],
		['vitesse quasi-constante', pourcentage_vitesse_constante, pourcentage_distance_vitesse_constante],
		['accélération modérée', pourcentage_acceleration_faible, pourcentage_distance_acceleration_faible],
		['accélération excessive',pourcentage_acceleration_excessive, pourcentage_distance_acceleration_excessive]
	])
	var options = {
			width: '100%', 
			height: '100%',
			vAxis:{format: 'percent'},
			legend:{position : 'top'}
	        };
	var ChartConseilsVT = new google.visualization.ColumnChart(document.getElementById('GraphConseilsVT'));
	ChartConseilsVT.draw(dataChart,options);
}
/*
 * /** affichage de la courbe de couple max
 */
/*
 * function affichage_CoupleChart(dataObd){ var ChartCouple = new google.visualization.ScatterChart(document.getElementById('ChartCoupleVT')); var dataChart; dataChart = new google.visualization.DataTable(); dataChart.addColumn('number', dataObd[2][0]); dataChart.addColumn('number', dataObd[8][0]); for(i=1;i<dataObd[2].length;i++){ dataChart.addRow([dataObd[2][i] , dataObd[8][i]]); }
 * ChartCouple.draw(dataChart, { title: "Courbe de couple", legend: {position:'none'}, backgroundColor: '#FFFFF', animation: {duration: 2000, easing: 'linear'}, //chartArea:{left:chartleft, right:chartright, top:charttop, width:chartwidth, height:"80%"}, isHtml: true, lineWidth: 0, pointShape:{ type: 'star', sides: 4 }, pointSize: 4, hAxis: {title:'Regime moteur (tours/min)'}, vAxis:
 * {title:'Couple max (N.m)', viewWindow: { min: 100*Math.round(rechercheMin(dataObd[8])/100)}, baseline : 100*Math.round(rechercheMin(dataObd[8])/100) }, axisTitlesPosition: "in" }); }
 */
// ----------------------------------------------------------------------------------
// Gestion
// de
// l'affichage
// des
// rapports
// de
// calcul
// ------------------------------------------------
/**
 * Affichage du rapport de calcul
 * et stockage de l'information dans le cas d'un calcul en ligne. L'organisation du stockage est le même pour tous les types
 * de veh à la différence que le VPU privilégie le tableau dataConsoPacUTBM pour ses résultat c'est donc celui que l'on copie
 * Les autre veh utilisent dataConso qui n'a pas la même format, c'est donc à cause de ça que 'lon doit passer par dataConsoRev pour ne pas se tromper d'indice
 */
function affSynthese(){
	
	
	if(typeVehicule=="VE"){
		//partie spécifique à VE où l'on calculs sa conso moyenne
		var consoMoy = (dataConso[nbPointConso][29]-dataConso[nbPointConso][30])/dataConso[nbPointConso][36] // wh/km
		
		var autonomieTotale = capBatterie/consoMoy*1000; // km
		// var
		// autonomieTotale
		// =
		// dataConso[1][32]*capBatterie/consoMoy/100*1000;
		// //km


		//On récupère ici l'information calculée de dataConso dans dataConsoVEcloneRev pour qu'il soit compatible avec les indices
		var dataConsoVECloneRev = JSON.parse(JSON.stringify(dataConso));
		window. dataConsoVEClone = reversetab(dataConsoVECloneRev,dataConsoVECloneRev.length,dataConsoVECloneRev[0].length);
		affichage_RapportVehEChart();	//on affiche les informations de la conso instantané contenus dans dataConso
		
		if(memory==2){
			
			if(tabMemory.length == 0){
				window.tabMemory = JSON.parse(JSON.stringify(dataConsoVEClone));		//on vérifi s'il y a déja de l'information stockée
			}
			else{																		//On gère ici les éventuelles discontinuité dans le stockages des informations 
																						//et on les stock dans tabMemory
				var indexFinMemory= tabMemory[0].length;
				for(var i = 0;i <= tabMemory.length-1;i++){
					for(var k=2;k < dataConsoVEClone[0].length;k++){
						if(i==0 || i==3 ||i==36 || i==27 || i== 28 || i==29  ||i==30 || i==37){				//les i ici correspondent aux valeurs de dataConso qui sont continues
							
							tabMemory[i].push((tabMemory[i][indexFinMemory-1]+dataConsoVEClone[i][k]));
						}
						else{
							tabMemory[i].push(dataConsoVEClone[i][k]);
						}
						
					}
				}
				
				
			}
			
			
			affichage_RapportVehEChartTotal();						//on affiache alors les résulats de la conso totale en bas
		}
		else{tabMemory=[];}
		
		// Suppression
		// de
		// l'ancien
		// rapport
		// s'il
		// y en
		// a un
		if(boolRapport){
			var parent = document.getElementById("syntheseParcours"+typeVehicule);
			var child = document.getElementById("progressAutonomie"+typeVehicule);
			parent.removeChild(child);					// Suppression
														// du
														// paragraphe
			var para = document.createElement("p");
			para.id="progressAutonomie"+typeVehicule;
			var element = document.getElementById("syntheseParcours"+typeVehicule);
			element.appendChild(para);					// Creation
														// d'un
														// paragraphe
														// avec
														// le
														// meme
														// nom
			var parent2 = document.getElementById("syntheseParcours"+typeVehicule);
			var child2 = document.getElementById("progressBatterie"+typeVehicule);
			parent2.removeChild(child2);
			var para2 = document.createElement("p");
			para2.id="progressBatterie"+typeVehicule;
			var element2 = document.getElementById("syntheseParcours"+typeVehicule);
			element2.appendChild(para2);
		}	
		
		//var tabMemoryRev = reversetab(tabMemory,tabMemory.length,tabMemory[0].length)
        var nbPointTrajetInter = dataConso.length-1;

		var bar = new tinyProgressbar(document.getElementById("progressAutonomie"+typeVehicule));
		bar.progress(Math.round(dataConso[nbPointTrajetInter][35]/autonomieTotale*100),'Autonomie restante ' + Math.round(dataConso[nbPointTrajetInter][35]) + ' km ');
		bar = new tinyProgressbar(document.getElementById("progressBatterie"+typeVehicule));
		bar.progress(Math.round(dataConso[nbPointTrajetInter][32]),' Batterie restante ');
		
	}
	else if(typeVehicule=="VT"){
		//affichage_RapportVehTChart();
		
		
		var dataConsoVTCloneRev = JSON.parse(JSON.stringify(dataConso));
		window. dataConsoVTClone = reversetab(dataConsoVTCloneRev,dataConsoVTCloneRev.length,dataConsoVTCloneRev[0].length);	
		affichage_RapportVehTChart();
		
		if(memory==2){
			
			if(tabMemory.length == 0){
				window.tabMemory = JSON.parse(JSON.stringify(dataConsoVTClone));		
			}
			else{
				var indexFinMemory= tabMemory[0].length;
				for(var i = 0;i <= tabMemory.length-1;i++){
					for(var k=1;k < dataConsoVTClone[0].length;k++){
						if(i==0 || i==36 ||i==56 ||i==54 || i==57 || i==58  || i==29 ||i==73  ||i==75 || i==76 || i==77){
							
							tabMemory[i].push((tabMemory[i][indexFinMemory-1]+dataConsoVTClone[i][k]));
						}
						else{
							tabMemory[i].push(dataConsoVTClone[i][k]);
						}
						
					}
				}
				
				
			}
			
			
			affichage_RapportVehTChartTotal();
		}
		else{tabMemory=[];}
	
	}
	else if(typeVehicule=="VH"){
		affichage_RapportVehHChart();
	}
	else if(typeVehicule=="VP"){
		affichage_RapportVehPChart();
	}
	else if(typeVehicule=="VPU"){
		window.dataConsoPacUTBMClone = JSON.parse(JSON.stringify(dataConsoPacUTBM));	
		affichage_RapportVehPUChart();
		//AffVPU2();									//informations redondantes pour l'instant donc enlevées
		//affichage_RapportVehPUChartAcqui();
		//console.log("memory",memory);
		if(memory==2){
			
			if(tabMemory.length == 0){
				window.tabMemory = JSON.parse(JSON.stringify(dataConsoPacUTBMClone));		
			}
			else{
				var indexFinMemory= tabMemory[0].length;
				for(var i = 0;i <= tabMemory.length-1;i++){
					for(var k=1;k < dataConsoPacUTBMClone[0].length;k++){
						if(i==2 ||i==7 || i==12 || i== 5 || i==6  ||i==21 || i==25 ||i==27 ||i==30 || i==34||i==36){
							//console.log("tabMemory[7][tabMemory[7].length-1]",tabMemory[7][tabMemory[7].length-1],+,"dataConsoPacUTBMClone[7][k]",dataConsoPacUTBMClone[7][k]);
							tabMemory[i].push((tabMemory[i][indexFinMemory-1]+dataConsoPacUTBMClone[i][k]));
						}
						else{
							tabMemory[i].push(dataConsoPacUTBMClone[i][k]);
						}
						
					}
				}
				
				
			}
			
			//AffVPU3();
			affichage_RapportVehPUChartTotal();
		}
		else{tabMemory=[];}
	}
	affEnergieChart();
}
/**
 * Affichage de la synthese des Etapes du parcours
 */
function affSyntheseEtape(){
// document.getElementById('gifV3R').style.display='none';
	var statusCol = "";
    var table = '<table id="tableEtape'+typeVehicule+'" style="width:100%;"><tr><th>Numero de l\'etape</th><th>Distance</th><th>Duree</th><th>Vitesse moyenne prevue</th></tr>';
    var rowUnit = '<tr> <th></th> <th>(km)</th> <th>(s)</th> <th>(km/h)</th> </tr>'
    	table +=rowUnit;
    var ID = 1;
    
    var dist  = dataStep[1][5];
	var duree = dataStep[1][1];
	var vmoy  = dataStep[1][3];
	var distTampon = 0;
	var dureeTampon = 0;
	// dataStep[1][6] = 1;
	for(iterEtape=1;iterEtape<nbCycle+1;iterEtape++){
		for(iterStep=1;iterStep<nbStep+1;iterStep++){
			if(dataStep[iterStep][6]==iterEtape){
				dist = dataStep[iterStep][5];
				duree += dataStep[iterStep][1];
			}
		}
		vmoy=(dist - distTampon)/(duree - dureeTampon)*3.6;		
		var row = "<tr num='"+ID+"' class='staff-row' id='" +ID +"'>";
		row += '<td id="Cell1' + typeVehicule + ID + '">' + ID + '</td>';
		row += '<td id="Cell2' + typeVehicule + ID + '">' + (dist - distTampon)/1000.0 + '</td>';
        row += '<td id="Cell3' + typeVehicule + ID + '">' + Math.round(duree - dureeTampon) + '</td>'
        row += '<td id="Cell4' + typeVehicule + ID + '">' + Math.round(vmoy) + '</td>'
        row+="</tr>"
        table+=row;
        ID++;
        distTampon = dist;
        dureeTampon = duree;
	}
	var rowTotal = '<tr> <th>Total</th> <th colspan="2">Duree '+ Math.round(duree/60) +' min</th> <th>Distance '+Math.round(dataStep[nbStep][5]/1000*10)/10 +' km</th> </tr>'
    	table +=rowTotal;
    table += '</table>';
    $('#syntheseEtape'+typeVehicule).html(table);
    for(i=0;i<polylines.length;i++){
    	polylines[i].setVisible(false);
    }
    generatePolyline('yellow',1,4,ID); // probleme
										// avec
										// les
										// polylines
										// jaunes
										// lorsqu'on
										// rajoute
										// des
										// points
										// de
										// parcours
										// qui
										// se
										// retrouvent
										// sous
										// le
										// trajet
    $('.staff-row').click(function(){
    	var $this = $(this);
    	var value = $this.attr('id');
    	var color = "yellow"
    		// Ici
			// le
			// changement
			// de
			// couleur
			// se
			// fait
			// sur
			// chaque
			// cell
			// car
			// je
			// n'arrive
			// pas
			// a le
			// faire
			// directement
			// sur
			// toute
			// la
			// ligne
    	if(document.getElementById('Cell1' + typeVehicule + value).style.backgroundColor == 'white' || document.getElementById('Cell1' + typeVehicule +value).style.backgroundColor == ''){
    		document.getElementById('Cell1'+ typeVehicule + value).style.backgroundColor=color;
    		document.getElementById('Cell2'+ typeVehicule + value).style.backgroundColor=color;
    		document.getElementById('Cell3'+ typeVehicule + value).style.backgroundColor=color;
    		document.getElementById('Cell4'+ typeVehicule + value).style.backgroundColor=color;
    		polylines[value-1].setVisible(true);
    	}else{
    		document.getElementById('Cell1'+ typeVehicule + value).style.backgroundColor="white"; 
    		document.getElementById('Cell2'+ typeVehicule + value).style.backgroundColor="white";
    		document.getElementById('Cell3'+ typeVehicule + value).style.backgroundColor="white";
    		document.getElementById('Cell4'+ typeVehicule + value).style.backgroundColor="white";
    		polylines[value-1].setVisible(false);
    	}
    });
    document.getElementById("syntheseEtape"+typeVehicule).style.display = '';
}
/**
 * Affichage de la synthese du cycle standard
 */
function affSyntheseCycle(){
	
	var ChartSynthese = new google.visualization.Table(document.getElementById('SyntheseCycle'+typeVehicule));
	var dataChart;
	dataChart = new google.visualization.DataTable();
	dataChart.addColumn('string','Synthese pour le cycle ' + fileName);
	dataChart.addColumn('number',"");
	dataChart.addRow(["Distance parcourue sur le cycle (km)",Math.round(dataCycle[nbPointCycle][3]/10)*10/1000]);
	if(typeVehicule =="VE"){
		dataChart.addRow(["Energie totale consommee pendant le cycle (Wh)" , Math.round(dataCycle[nbPointCycle][29]*100)/100]);
		dataChart.addRow(["Energie totale theorique generee pendant le cycle (Wh)", Math.round(dataCycle[nbPointCycle][30]*100)/100]);
		var temp = Math.round(dataCycle[nbPointCycle][30]/dataCycle[nbPointCycle][29]*100*100)/100;
		(temp>100)? temp=100:
		dataChart.addRow(["Energie regeneree pendant le cycle (%)",temp]);
		dataChart.addRow(["Energie totale requise pendant le cycle (Wh)",Math.round(dataCycle[nbPointCycle][29]-dataCycle[nbPointCycle][30])]);
		// dataChart.addRow(["Autonomie
		// visee
		// (km)",100]);
		dataChart.addRow(["Energie requise sans regeneratif pour 100 km (kWh) ",Math.round(10*((100*dataCycle[nbPointCycle][29]/dataCycle[nbPointCycle][3])))/10]);
		dataChart.addRow(["Energie requise avec regeneratif pour 100 km (kWh) ",Math.round(10*100*(dataCycle[nbPointCycle][29]-dataCycle[nbPointCycle][30])/dataCycle[nbPointCycle][3])/10]);
	}
	else if(typeVehicule =="VPU"){
		dataChart.addRow(["Energie totale fournie par la batterie (Wh)",dataConsoPacUTBM_CS[2][dataConsoPacUTBM_CS[2].length-1]]);
		dataChart.addRow(["Energie totale fournie par la PAC (Wh)",dataConsoPacUTBM_CS[5][dataConsoPacUTBM_CS[5].length-1]]);
		dataChart.addRow(["Consommation d'hydrogène (kg)",dataConsoPacUTBM_CS[6][dataConsoPacUTBM_CS[6].length-1]]);
		dataChart.addRow(["Consommation d'hydrogène moyenne (kg/100km)",dataConsoPacUTBM_CS[6][dataConsoPacUTBM_CS[6].length-1]/(Math.round(dataCycle[nbPointCycle][3]/10)*10/1000)*100])
		
	}
	else if(typeVehicule=="VT"){
		if(dataCycle[nbPointCycle][73]<dataCycle[nbPointCycle][54]){
			// On
			// affiche
			// les
			// resulats
			// pour
			// une
			// consommation
			// optimale
			// de
			// carburant
			dataChart.addRow(["Consommation d'essence (litres)",Math.round(dataCycle[nbPointCycle][73]*100)/100]);
			dataChart.addRow(["Consommation d'essence moyenne (litres/100km)",Math.round(dataCycle[nbPointCycle][74]*100)/100]);
			dataChart.addRow(["Emissions de Co2 (kg)",Math.round(dataCycle[nbPointCycle][75]*100)/100]);
			dataChart.addRow(["Energie consommee (Wh)",Math.round(dataCycle[nbPointCycle][29])]);
			dataChart.addRow(["Energie dissipee (Wh)",Math.round(dataCycle[nbPointCycle][77])]);
			dataChart.addRow(["Energie produite par le moteur (Wh)",Math.round(dataCycle[nbPointCycle][76])]);
			dataChart.addRow(["Rendement moteur",rendementmoteurOptiCycle]);
		}
		else{
			dataChart.addRow(["Consommation d'essence (litres)",Math.round(dataCycle[nbPointCycle][54]*100)/100]);
			dataChart.addRow(["Consommation d'essence moyenne (litres/100km)",Math.round(dataCycle[nbPointCycle][55]*100)/100]);
			dataChart.addRow(["Emissions de Co2 (kg)",Math.round(dataCycle[nbPointCycle][56]*100)/100]);
			dataChart.addRow(["Energie consommee (Wh)",Math.round(dataCycle[nbPointCycle][29])]);
			dataChart.addRow(["Energie dissipee (Wh)",Math.round(dataCycle[nbPointCycle][58])]);
			dataChart.addRow(["Energie produite par le moteur (Wh)",Math.round(dataCycle[nbPointCycle][57])]);
			dataChart.addRow(["Rendement moteur",rendementmoteurCycle]);
		}
	}
	
	ChartSynthese.draw(dataChart,{
		width: '95%', 
		height: '100%'
	}); 
	
}
/**
 * Ajout d'une ligne dans une table HTML
 * 
 * @param ElementID :
 *            id de la table ou la ligne doit etre ajoutee
 * @param col1 :
 *            contenu de la premiere colonne
 * @param col2 :
 *            contenu de la deuxieme colonne (alignee a droite)
 * @param col3 :
 *            contenu de la troisieme colonne
 * @param bool :
 *            boolean pour l'ajout de deux colonnes supplementaires
 * @param col4 :
 *            contenu de la quatrieme colonne (alignee a droite)
 * @param col5 :
 *            contenu de la cinquieme colonne
 */
function affichageTabSynthese(ElementID,col1,col2,col3,bool,col4,col5){
	var nouvelleLigne = document.getElementById(ElementID).insertRow(-1);	
	var colonne1 = nouvelleLigne.insertCell(0);
	colonne1.innerHTML += col1;
	colonne1.setAttribute('style','text-align:left;');
	var colonne2 = nouvelleLigne.insertCell(1);
	colonne2.innerHTML += col2;
	colonne2.setAttribute('style','text-align:right;');
	var colonne3 = nouvelleLigne.insertCell(2);
	colonne3.innerHTML += col3;
	if(bool){
		var colonne4 = nouvelleLigne.insertCell(3);
		colonne4.innerHTML += col4;
		colonne4.setAttribute('style','text-align:right;');
		var colonne5 = nouvelleLigne.insertCell(4);
		colonne5.innerHTML += col5;
	}

}

// ----------------------------------------------------------------------------------
// Fonctions
// de test
// -------------------------------------------------------------------------

/**
 * Execution de la fonction au clic sur le bouton print
 */
function Print(){
	var value = parseInt(document.getElementById('printHolder').value);
/*
 * google.charts.load('current', {'packages':['gauge']}); var data = google.visualization.arrayToDataTable([ ['Label', 'Value'], ['Batterie', 80], ]);
 * 
 * var options = { width: 400, height: 120, redFrom: 0, redTo: 15, greenFrom:85, greenTo:100, minorTicks: 5 }; var chart = new google.visualization.Gauge(document.getElementById('test_chart_div')); chart.draw(data, options);
 */
	alert(nbCycle)
	alert(polylines)
	alert(dataStep)
}

/**
 * Affiche la valeur contenue dans la case de la ligne 'getAbs' et de la colonne 'getOrd'
 */
function GetValue(){
	var abs = parseFloat(document.getElementById("getAbs").value);
	var ord = parseFloat(document.getElementById("getOrd").value);
	alert(dataConso[0][ord] + " : " + dataConso[abs][ord]);
}
function move() {
	alert(masse)
}
// ----------------------------------------------------------------------------------
// Fonctions
// inutilisees
// ------------------------------------------------------------------------
/**
 * Reset du trajet, des variables associees et l'ensemble des parametres de calcul
 */
function reset() {

	waypoints=[];
	document.getElementById("tablesummary"+typeVehicule).style.visibility ="hidden";
	document.getElementById("distance"+typeVehicule).innerHTML=0;
	document.getElementById("differenceup"+typeVehicule).innerHTML=0;
	document.getElementById("differencedown"+typeVehicule).innerHTML=0;
	document.getElementById("max"+typeVehicule).innerHTML=0;
	document.getElementById("min"+typeVehicule).innerHTML=0;
	end = null;
	start = null;
	if(mousemarker != null) {	
		mousemarker.setMap(null);
		mousemarker = null;
	}
	document.getElementById("autocompleteDeparture"+typeVehicule).value = "";document.getElementById("autocompleteArrival"+typeVehicule).value = "";
	
	if(typeVehicule=="VE"){
		document.getElementById('elevation_chartVE').style.display = 'none';
		document.getElementById('chart_energieVE1').style.display = 'none';
		document.getElementById('chart_energieVE2').style.display = 'none';
		document.getElementById('chart_energieVE3').style.display = 'none';
	}
	if(typeVehicule=="VT"){
		document.getElementById('elevation_chartVT').style.display = 'none';
		document.getElementById('chart_energieVT1').style.display = 'none';
		document.getElementById('chart_energieVT2').style.display = 'none';
		document.getElementById('chart_energieVT3').style.display = 'none';
	}
	if(typeVehicule=="VH"){
		document.getElementById('elevation_chartVH').style.display = 'none';
		document.getElementById('chart_energieVH1').style.display = 'none';
		document.getElementById('chart_energieVH2').style.display = 'none';
		//document.getElementById('chart_energieVH2').style.display = 'none';
	}
	if(typeVehicule=="VP"){
		document.getElementById('elevation_chartVP').style.display = 'none';
		document.getElementById('chart_energieVP1').style.display = 'none';
		document.getElementById('chart_energieVP2').style.display = 'none';
		//document.getElementById('chart_energieVP3').style.display = 'none';
	}
	if(typeVehicule=="VPU"){
		document.getElementById('elevation_chartVPU').style.display = 'none';
		document.getElementById('chart_energieVPU1').style.display = 'none';
		document.getElementById('chart_energieVPU2').style.display = 'none';
		document.getElementById('chart_energieVPU3').style.display = 'none';
	}
	
	
	document.getElementById("carsParam"+typeVehicule).value = "";
	document.getElementById("dataBorne"+typeVehicule).value = "";
	document.getElementById("cycleVilleLent"+typeVehicule).value = "";document.getElementById("vitesseVilleLent"+typeVehicule).value = "";
	document.getElementById("cycleVilleRapide"+typeVehicule).value = "";document.getElementById("vitesseVilleRapide"+typeVehicule).value = "";
	document.getElementById("cycleRuralLent"+typeVehicule).value = "";document.getElementById("vitesseRuralLent"+typeVehicule).value = "";
	document.getElementById("cycleRuralRapide"+typeVehicule).value = "";document.getElementById("vitesseRuralRapide"+typeVehicule).value = "";
	document.getElementById("cycleAutoroute"+typeVehicule).value = "";document.getElementById("vitesseAutoroute"+typeVehicule).value = "";
	document.getElementById("accMax"+typeVehicule).value = "";document.getElementById("decMax"+typeVehicule).value = "";
	document.getElementById("pente"+typeVehicule).value = 0;document.getElementById("resolution"+typeVehicule).value = 0;
	document.getElementById("penteValeur"+typeVehicule).style.visibility = 'hidden';document.getElementById("cycleRepetition"+typeVehicule).style.visibility = 'hidden';
	document.getElementById("syntheseEtape"+typeVehicule).style.display = 'none';
	document.getElementById("Affichage_courbesVE1").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVE2").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVE3").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVT1").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVT2").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVT3").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVH1").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVH2").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVP1").style.visibility = 'hidden';
	document.getElementById("Affichage_courbesVP2").style.visibility = 'hidden';

	if(typeVehicule=="VE"){
		document.getElementById("socMin"+typeVehicule).value = "";document.getElementById("socInitial"+typeVehicule).value = "";
		document.getElementById("rechercheBorne"+typeVehicule).style.visibility = 'hidden';document.getElementById("calculBorne"+typeVehicule).style.visibility = 'hidden';
		document.getElementById("synthese"+typeVehicule).style.visibility = 'hidden';
	}
	markersBornes = [];
	markersRecharge = [];
	boolChoixBorne = false;
	boolReDo = false;
	typeResolution = 0;
	dataConso = []; nbPointConso = 0;
	abs = 36; ord = 1;
	boolRapport = false;
	boolRapportEtape = false;
	boolRapportCycle = false;
	boolCalcRoute = false;
	for (var i = 0; i < markersRecharge.length; i++) {
        markersRecharge[i].setMap(null);
    }
	markersRecharge = [];
	for (var i = 0; i < markersBornes.length; i++) {
        markersBornes[i].setMap(null);
    }	
	directionsDisplay.setDirections({routes: []});
	
	if(typeVehicule=="VPU"){
		document.getElementById("socMin"+typeVehicule).value = "";document.getElementById("socInitial"+typeVehicule).value = "";
	}
}
/**
 * Enregistrement de l'image de la carte en url
 */
function saveMapToDataUrl() {
    var element = $("#map-canvas");
    var bool = false;
    var dataUrl;
    html2canvas(element, {
        useCORS: true,
        onrendered: function(canvas) {
            dataUrl= canvas.toDataURL("image/png");
            bool = true;
            document.write('<img src="' + dataUrl + '"/>');
        }
    });
    alert(dataUrl);
    var img = document.createElement("img");
    img.src = dataUrl;
    var src = document.getElementById("test");
 // src.appendChild(img);
    

    window.open(dataUrl, "toDataURL() image", "width=600, height=200");
// alert("fin")
}
/**
 * Calcul de l'energie mecanique consommee entre T1 et T2
 * 
 * @param V1 :
 *            vitesse en T1
 * @param V2 :
 *            vitesse en T2
 * @param duree :
 *            delta T
 * @param pente :
 *            pente de la route
 * @param acc :
 *            acceleration
 * @returns energie mecanique
 */
function energieMec(V1,V2,duree,pente,acc,alpha_moy){
	return ((pes*masse*(res_roulement*Math.cos(pente) + Math.sin(Math.atan(pente + alpha_moy))))*intV(V1,V2,duree,acc) + 0.5*rho*SCx*intV3(V1,V2,duree,acc) + (masse+coefInertie*masse)*intAcc(V1,V2,duree,acc))/3600;
// return
// ((pes*masse*(res_roulement*Math.cos(pente)
// +
// Math.sin(Math.atan(pente
// +
// alpha_moy))))*intV(V1,V2,duree,acc)
// +
// 0.5*rho*SCx*intV3(V1,V2,duree,acc)
// +
// masse*intAcc(V1,V2,duree,acc))/3600;
}
/**
 * Calcul numerique de la valeur de l'integrale de V entre T1 et T2 cette fonction n'est plus utilise dans la nouvelle methode de resolution
 * 
 * @param V1 :
 *            vitesse en T1
 * @param V2 :
 *            vitesse en T2
 * @param duree :
 *            delta T
 * @param acc :
 *            acceleration
 * @returns valeur approchee de l'integrale
 */
function intV(V1,V2,duree,acc){
	return acc*Math.pow(duree,2)/2+V1*duree		// Approximation
												// par
												// les
												// rectangles
												// plus
												// correction
												// erreur
												// (pertinance
												// de
												// la
												// correction
												// d'erreur?)
												// (cftableau
												// excel)
// return
// (V1+V2)*duree/2;
// //
// Approximation
// par un
// trapez
// (cf
// equation
// excel)
}
/**
 * Calcul numerique de la valeur de l'integrale de V*V' entre T1 et T2 cette fonction n'est plus utilise dans la nouvelle methode de resolution
 * 
 * @param V1 :
 *            vitesse en T1
 * @param V2 :
 *            vitesse en T2
 * @param duree :
 *            delta T
 * @param acc :
 *            acceleration
 * @returns valeur approchee de l'integrale
 */
function intAcc(V1,V2,duree,acc){
	return (acc*Math.pow(duree,2)/2 + V1*duree)*acc;	// Approximation
														// par
														// les
														// rectangles
														// (cftableau
														// excel)
// return
// (Math.pow(V2,2)-Math.pow(V1,2))/2;
// //
// Approximation
// par les
// trapeze
// (cf
// equation
// excel)s
}	
/**
 * Calcul numerique de la valeur de l'integrale de V^3 entre T1 et T2 cette fonction n'est plus utilise dans la nouvelle methode de resolution
 * 
 * @param V1 :
 *            vitesse en T1
 * @param V2 :
 *            vitesse en T2
 * @param duree :
 *            delta T
 * @param acc :
 *            acceleration
 * @returns valeur approchee de l'integrale
 */
function intV3(V1,V2,duree,acc){
	return (duree/4)*(Math.pow(V2,3)+V1*Math.pow(V2,2)+V2*Math.pow(V1,2)+Math.pow(V1,3));
// return
// Math.pow(acc,3)*Math.pow(duree,4)/4
// +
// V1*Math.pow(acc,2)*Math.pow(duree,3)
// + 3*acc
// *
// Math.pow(V1,2)
// *
// Math.pow(duree,2)/2
// +
// Math.pow(V1,2)*duree;
// //
// Approximation
// par les
// rectangles
// (cf
// tableau
// excel);
// return
// (duree/4)*(Math.pow(V2,3)
// +
// Math.pow(V2,2)*V1
// +
// V2*Math.pow(V1,2)
// +
// Math.pow(V1,3))
// //
// Approximation
// par les
// trapeze
// (cf
// equation
// excel)s
}
/**
 * Recherche des extremums des gradients
 */
function findtemptops(gradients) {

	var count = 0;
	var temp = false;
	var temptops = [];

	for(var i = 0; i < gradients.length; i++) {
		if(gradients[i] > 0) {
			count = 0;
			temp = true;
		}
		else if (temp){
			count++;
			temptops[i] = true;
			temp = false;
			count = 0;
		}
	}
	temptops[gradients.length - 1] = true;
	return temptops;
}	
/**
 * Suppression des categories de gradient d'elevation
 */
function clearcats(one, disone, two, distwo, three, disthree, four, disfour, distances) {
	var z;
	var tempdistance;
	for(var i = 0; i < distances.length; i++) {
		if(one[i]) {
			z = i - 1;
			tempdistance = 0;
			while(tempdistance <= disone * 1.5) {
				tempdistance = tempdistance + distances[z];
				two[z] = false;
				three[z] = false;
				four[z] = false;
				z--;
			}
		}
		if(two[i]) {
			z = i - 1;
			tempdistance = 0;
			while(tempdistance <= disone) {
				if(one[z]) {
					two[i] = false;
				}				
				tempdistance = tempdistance + distances[z];
				three[z] = false;
				four[z] = false;
				z--;
			}
		}
		if(three[i]) {
			z = i - 1;
			tempdistance = 0;
			while(tempdistance <= distwo) {
				if(one[z] || two[z]) {
					three[i] = false;
				}
				tempdistance = tempdistance + distances[z];	
				four[z] = false;
				z--;
			}
		}
		if(four[i]) {
			z = i - 1;
			tempdistance = 0;
			while(tempdistance <= disthree) {
				if(one[z] || two[z] || three[z]) {
					four[i] = false;
				}
				tempdistance = tempdistance + distances[z];
				z--;
			}
		}
		if(disone == 0){
			one[i] = false;
			two[i] = false;
			three[i] = false;
			four[i] = false;
		}

	}
}
/**
 * Mise a jour des gradients et du graphique d'elevation
 */
function updateGradient2(){
	gradient_orange = document.getElementById("orangefull").value;
	gradient_red = document.getElementById("redfull").value;
	plotElevation(elevations, elevation_status);
}
/**
 * Creation de l'url
 */
function createUrl() {
	var link = "?slat=" + start.lat() + "&slng=" + start.lng() + "&elat=" + end.lat() + "&elng=" + end.lng() + "&mode=" + mode;
	for(var i = 0; i < waypoints.length; i++) {
		link+= "&wp" + i + "lat=" + waypoints[i].location.lat() + "&wp" + i + "lng=" + waypoints[i].location.lng();
	}
	history.pushState("", "Title", link);
}
/**
 * Generation
 * 
 * @param csvExport
 */
function downloadCSV (csvExport) {
  if(csvExport){
    var blob = new Blob([csvExport], {type: 'text/csv;charset=utf-8'});
    var url  = window.URL || window.webkitURL;
    var link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
    link.href = url.createObjectURL(blob);
    link.download = "dataConsoPac.csv"; 
    
    var event = document.createEvent("MouseEvents");
    event.initEvent("click", true, false);
    link.dispatchEvent(event); 
  }  
}

/**
 * Generre un fichier Json comportant les données du tableau dataConsoPacUTBM
 * 
 * @param tab
 * 		// tableau de données , pour l'instant seulement pour dataConsoPacUTBM
 * @param filename
 * 		// nom du ficher . extension du fichier
 * @param contenttype
 * 		// format des données, à laisser 
 */
//createJSONFile(dataConsoPacUTBM, 'Data_heves.json', 'text/plain');

function createJSONFile() {
	tab = dataConso;
	//tabRev = reversetab(dataConso,dataConso.length,dataConso[0].length);
	//filename = 'Data_HEVES.json';
	//contentType = 'text/plain';
	alert("Fichier créé");
	myJson = {								//creation du tableau de base 
				Param : {
				}
	};
	
    for(var i = 0;i<tab[0].length;i++){			
    	if( i ==0 || i== 1 ||i ==2 || i== 3 ||i ==4 || i== 5 ||i ==6 || i== 38 )
    	var newParam = tab[0][i];
    	myJson.Param[newParam] = {};      // pour chaque itération, on créé une liste de nom tab[k][0]
    	for(var k = 1; k<=tab.length-1;k++){
    		var newId = k;
    		//console.log(tab[k][i]);
    		var newValue = tab[k][i];
    		myJson.Param[newParam][newId] = newValue;  // on ajoute ensuite la valeur avec un id i pour localiser les valeurs par la suite
    	}
    	
    }
	var data = JSON.stringify(myJson);
    var a = document.createElement("a");
    var file = new Blob([data], {type: 'text/plain'});    //'text/plain à remplacer par contenttype
    a.href = URL.createObjectURL(file);
    a.download = 'Data_Heves.json';    // à remplacer par fileName
    a.click();
    

}


/**
 * Generre un fichier Json comportant les données GPS pour SATAN
 * 
 * @param json
 * 		fichier JSON existant
 * 
 * @param dataVlim
 * 		données vitesses limites à ajouter au fichier 
 */

function createJSONFileSATAN() {
	var json=JSON.parse(textFileGPS);				//on récupère le fichier JSON lu
	
	
	var dataVlim=tabvlim;

	//tab = dataConsoPacUTBM; 
	//filename = 'Data_HEVES.json';
	//contentType = 'text/plain';
	//console.log(arrayHeureGPS);
    //console.log(json);
    //console.log(dataVlim);
	
	
	
    /*for(var i=0;i<dataVlim.length;i++){				//On modifie les valeurs dans dataVlim car elle sont en string et il est plus facile de les manipluer en float
    	dataVlim[i]=parseFloat(dataVlim[i]);
    	if(dataVlim[i]==0){							// on retire les vitesses limites 0 car elles correspondent à un résulats nul de la requête overpass
    		dataVlim[i]=dataVlim[i-1];				//on remplce ce zéro par la vlim précédente
    	}
    }

    for(var i=0;i<dataVlim.length-1;i++){								//ici, on filtre 'grossièrement' les valeurs pour éviter les pics ou erreurs trop excessvives. Par exemple les valeurs retournant 50 sur une portion d'autoroute.
    	if(Math.abs(dataVlim[i-1]-dataVlim[i]) >29 ){					// il serait négatif d'enlever toutes les valeurs car il peut y avoir des changement brusques de vitesse légitime (ex: passage de 130 à 70 en sortie d'autoroute)
    		dataVlim[i]=(dataVlim[i-1]+dataVlim[i+1])/2;				// il ne faut pas perdre à l'esprit qu'on point un point toutes les x (ici 13) secondes
    	}   
    }*/



    var newParam="Vitesses_lim";							//on créer les tableaux dans le fichier JSON
    var newParam2="Heure_effective";
    json[newParam]=[];
    json[newParam2]=[];
    
    
    var indexVit = 1;
    var index = 1
    json[newParam][0]=dataVlim[0].toString();
    json[newParam2][0]=json.heure[0];
    for(var k = 1; k<=json.heure.length-1;k++){								//on ajoute ici les valeurs aux indices correspondant.
    																		// comme on a prit un point sur 13 pour les vitesse limites, on comble les trous par rapport aux vraies vitesses avec la vitesse limites précédantes. 
    	if(index != 13){													// le tableau est donc composé comme suit : [13xVitesse_limite_1 ; 13xVitesse_limite_2 ; ....  ; 13xVitesse_Limite_n-1]
    		json[newParam2][k]=json.heure[k];
    		json[newParam][k] = json[newParam][k-1];
    	}
    	else{
    		json[newParam2][k]=json.heure[k];
    		json[newParam][k]=dataVlim[indexVit].toString();
    		indexVit++;
    		index=0;
    	}
    	index++;
    }
    
    
	
    
    //console.log(json);
    alert("Fichier créé");													//on créer l'objet qui servira de fichier
	var data = JSON.stringify(json);										// on transforme le tableau JSON en string compatible JSON 
    var a = document.createElement("a");
    var file = new Blob([data], {type: 'text/plain'});    //'text/plain à remplacer par contenttype
    a.href = URL.createObjectURL(file);
    a.download = 'Data_HEVES.json';    // à remplacer par fileName
    a.click();
	
}





var tTotal=0;
/**function requeteHTTP
 * cette fonction permet d'effectuer une requête HTTP à la base de données OpenStreetMap qui permet de récupérer 
 * la vitesse limite en un point GPS
 * La requete renvoie un formet JSON, et retourne une liste du nom de la route et de la vitesse limite 
 * il y a un radius réglable mit a 1500
 * 
 * 
 * 
 * Focntion non utilisée. Elle à été remplacée par rqtHTTPOSM()
 * 
 *
 *
 *@params	lat
 *			latitude du point étudié
 *@parmas   lng
 *			Longitude du point étudié
 *
 *   
 * @returns
 * 			la vitesse limite au point
 */
function requeteHTTP(lat,lng){
    //var test= ["80","80"];
    //console.log(test);
    //console.log(mostFreq(test));

	var tabTampon= [];
	var vlim;
	var tTotal=0;
	//var lat=47.680892;
	//var lng=6.939710;
	var t1 = new Date().getTime();
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "http://overpass-api.de/api/interpreter?"+
        	"[out:json];way[maxspeed](around:100,"+lat+","+lng+");out;"
        	, true);
	xhr.responseType = 'json';
	xhr.send();

	xhr.onload = function() {
		//console.log('DONE: ', xhr.status);
	    for(var i=0; i<xhr.response.elements.length;i++){
	    	//console.log(i);
	    	 //console.log(xhr.response.elements[i].tags.maxspeed);
	    	 tabTampon.push(xhr.response.elements[i].tags.maxspeed);
	    }
	    console.log(tabTampon);
	    var t2 = new Date().getTime();
	    tTotal=tTotal+(t2-t1);
        console.log(lat,lng);
        console.log("Temps cumulé des requetes en ms",tTotal);
	    console.log("vitesse limite = "+ mostFreq(tabTampon));
	    if(mostFreq(tabTampon) =! 0){
	    	return mostFreq(tabTampon);
	    }
	    else{
	    	console.log("Aucune vitesse limite trouvée pour ce point");
	    	return 0;
	    }
	    
	}
}

/**					version csv, à completer

	  
	    	   var t1 = new Date().getTime();
	    var xhr = new XMLHttpRequest();
	    xhr.open('GET', "http://overpass-api.de/api/interpreter?"+
	        	"data=[out:csv(name,ref,%22maxspeed%22;false)];" +
	        	"way[maxspeed](around:5,47.680892,6.939710);" +
	        	"out;"
	        	, true);
	    xhr.onload = function () {
	    	var rsp = "";
	    	rsp = xhr.responseText;
	    	rsp.split('\t');
	      console.log(xhr.responseText); // http://example.com/test
	      console.log(rsp);
	      var t2 = new Date().getTime();
	    console.log("time in ms",t2-t1);
	    };
	    xhr.send(null);


**/

/* Function mostFreq renvoie l'élement avec le plus d'occurence dans une liste
 * NB pour une liste [1,2], la fonction renverra 1
 * NB pour une liste [2,1], la fonction renverra 2
 * 
 * 
 * @params tab
 * 			Liste contenant les élements à étudier
 * 
 * @return	mostFrequent
 * 				Element présent dans tab qui a le plus d'occurence dans la liste
 * 
 */
function mostFreq(tab){
	var counts = {};
	var compare = 0;
	var mostFrequent
	if(tab.length == 0){
		mostFrequent ="0";
	}
   for(var i = 0, len = tab.length; i < len; i++){
       var element = tab[i];
       
       if(counts[element] === undefined){
           counts[element] = 1;
       }else{
           counts[element] = counts[element] + 1;
       }
       if(counts[element] > compare){
             compare = counts[element];
             mostFrequent = tab[i];
       }
    }
  return mostFrequent;
}










/* fonction rqtHTTPOSM permettant de lancer les requêtes HTTP vers OverStreetMap
 * cette fonction est accompagnée de 3 autre fonction qui assurent que les requêtes ne s'envoie pas en même temps,
 * ce que gènere des erreur au niveau du server OSM
 *   
 *
 *Ces fonctions sont en partie tiré d'une solution trouvée sur StackOverflow d'où certains commentaires en anglais
 *
 * @param
 * 		URLS : liste contenant les URL des requêtes à envoyer.
 * 
 */
function rqtHTTPOSM(URLS){
	 tabvlim=[];
	 //initialisation du json contenant les rqt
	 var items = [{
		    url: "http://overpass-api.de/api/interpreter?[out:json];way[maxspeed](around:100,47.651190,6.824945);out;",//ext Belfort 70/50
		    name: "point1"
		  },
		];
	 //assignation des rqt
	 for(var i = 0;i<URLS.length;i++){
			items[i] = {
					url : URLS[i],
					name : "point"+i
			}
		}
        //var t1= new Date().getTime();
		 							//First prepare array of requests that later will be send 
		var requests = [];
		for (var i = 0; i < items.length; i++) {
		  requests.push(new Request(items[i].url));
		}

										 //Initialize the object that will be responsible for 
										 // request sending and process the requests  
		var manager = new RequestManager(requests);
		manager.process();
		manager.onSend(function(url, response) {
									//  this code will fire once a request is completed, no matter if success of failed 
		  //console.log('request to ' + url + ' completed ....');
		  //console.log('----------------------------------------');
			
			console.log("nombre de requetes restante :",requests.length);
		        if(requests.length == 0){				//Termine si on a bien la liste requests vide
		        	httpvlim=true;
		        }
		})
		
}



var tabvlim=[];
var httpvlim = false;
/*
 * This class is a wrapper that will hold the request
 * and will be responsible to send it with a delay of 50 mseconds
 * 
 * @param {string} url - this is the url which is going to be requested
 * @returns {Request} - new Request object
 */
function Request(url) {
  //var tabvlim=[]; 
  var tabTampon=[];
  var that = this, resolve, reject;
  that.url = url;

  that.promise = new Promise((res, rej) => {
    resolve = res;
    reject = rej;
  });

  that.xhr = new XMLHttpRequest();
  that.xhr.onreadystatechange = function() {
    if (that.xhr.readyState == 4) {
      if (that.xhr.status == 200) {
        var data = null;
        var tabTampVit=[];
        try {
        	//console.log(xhr.response.elements[i].tags.maxspeed)
          data = JSON.parse(that.xhr.responseText);						//on lis la reponse de la requ^tes et on le transforme en JSON
          for(var i=1; i<data.elements.length;i++){
        	     
              if(data.elements[i].type === "count"){					//grâce à la mention count dans la requête, on peut séprarer les résultats des req
            	  var vlim = mostFreq(tabTampon);						// dès qu'on crois la mention count, on sait q"on change de point. On cacul dont la vitesse limite pour ce point
            	  //console.log(vlim);
            	  if(parseFloat(vlim)> 130){
            		  vlim="130";
            	  }//On utilise mostFreq qui ressort la vitesse la plsu présente dans la liste des résulats.
            	  
            	  tabvlim.push(vlim);
            	  var nbResultQuery = data.elements[i].total;
            	  vlim=0;
            	  tabTampon=[];
              }
              else{
            	  tabTampon.push(data.elements[i].tags.maxspeed);			//on récupère la vitesse limite dans l'élément JSON
              }

	    	
	    }
          vlim = mostFreq(tabTampon);
          if(parseFloat(vlim)> 130){
    		  vlim="130";
    	  }
		  tabvlim.push(vlim);
		  vlim=0;
		  tabTampon=[];

          resolve(data);
        } catch (e) {
          reject(e);
        }
      } else {
        reject({
          statusText: that.xhr.statusText,
          response: that.xhr.response
        });
      }
    }
  };

  that.send = function() {
     //send request after 50 milisecondes 
    setTimeout(function() {
      that.xhr.open("GET", that.url, true);
      that.xhr.send();
    }, 1500)

    return this;
  }
}


/**
 * This class is responsible for processing all the request
 * The main role is to call the Request's send method, 
 * wait the request to complete and then call the next request from the queue
 * until all the requests are processed
 * 
 * @param {Array} requests - this is an array of Request objects
 * @returns {RequestManager} new RequestManager object
 */
function RequestManager(requests) {
  var that = this,
    callback;
  that.requests = requests;

  that.onSend = function(cb) {
    callback = cb;
  }

  that.process = function() {
    console.log('Starting requests sending .......');
    doRequestRecursive(that.requests.shift());
  }

  function doRequestRecursive(request) {
    request.send().promise.then(function(data) {
      console.log('request ' + request.url + ' success ...');				// a ajouter si on veut s'assurer que la requête à bien réussi
      callback(request.url, data);
    }, function(err) {
      console.log('request ' + request.url + ' failed ...');
      callback(request.url, err);
    }).then(function() {
      var nextRequest = that.requests.shift();
      if (nextRequest) {
        doRequestRecursive(nextRequest);
      }
    });
  }
}
 

/**
 *la fonction updateBoutonStop permet de mettre à jour la valeur d'un booleen permettant de continuer ou non le calcule en ligne en cour.
 *
 */

function updateBoutonStop(){
	stopCalcEnLigne= !stopCalcEnLigne;
}



var ratioElec =0.000065 ; //kg CO2/kWh elec produit
var ratioH2 =10  ; //  kg CO2/kg H2 produit
var prixElec =0.25 ; // €/kWh
var prixEssence =1.20 ; //  €/L

/*function updateRatios
 * Cette fonction sert à modifier les paramètres pour les calcs de prix/consommation lors des comparaisons
 */
function udpateRatios(){
	((document.getElementById("PrixE"+typeVehicule).value) != '')? prixElec 		= parseFloat(document.getElementById("PrixE"+typeVehicule).value)			: prixElec 	 	  = 0.25;
	((document.getElementById("RatioH2"+typeVehicule).value) != '')? ratioH2 		= parseFloat(document.getElementById("RatioH2"+typeVehicule).value)			: ratioH2 	 	  = 010;
	((document.getElementById("PrixEssence"+typeVehicule).value) != '')? prixEssence= parseFloat(document.getElementById("PrixEssence"+typeVehicule).value)		: prixEssence 	  = 1.20;
	((document.getElementById("RatioE"+typeVehicule).value) != '')? ratioElec 		= parseFloat(document.getElementById("RatioE"+typeVehicule).value)			: ratioElec 	  = 0.000065;
}





//Partie logique floue liée à la loi de commande 11 dans les calculs de la PAC calculConsoElecPACUTBM

function SOC_FLC(x){
	 var VectSOC=[]
//case low : 
			 
	 if(x <= 0.5){
		 VectSOC.push(1);
	 }
	 else if( (0.5 < x) && (x <= 0.65)){
		 VectSOC.push((0.65-x)/(0.65-0.5));
	 }
	 else if( x > 0.65){
		 VectSOC.push(0);
	 }
			 
//case medium :
			 
	 if(x <= 0.5){
		 VectSOC.push(0);
	 }
	 else if( (0.5 < x) && (x <= 0.65)){
		 VectSOC.push((x-0.5)/(0.65-0.5));
	 }
	 else if( (0.65 < x) && (x <= 0.8)){
		 VectSOC.push((0.8-x)/(0.8-0.65));
	 }
	 else if( x > 0.8){
		 VectSOC.push(0);
	 }
		
//		 case high :
			 
	 if(x <= 0.65){
		 VectSOC.push(0);
	 }
	 else if( (0.65 < x) && (x <= 0.8)){
		 VectSOC.push((x-0.65)/(0.8-0.65));
	 }
	 else if( x > 0.8){
		 VectSOC.push(1);
	 }

	 return VectSOC;
}



//TEST
function SOC_VAR_LOW(a1,b1,x){
	 var VectSOC=[]
//case low : 
			 
	 if(x <= a1){
		 VectSOC.push(1);
	 }
	 else if( (a1 < x) && (x <= b1)){
		 VectSOC.push((b1-x)/(b1-a1));
	 }
	 else if( x > b1){
		 VectSOC.push(0);
	 }
	 return VectSOC;
}
			 
//case medium :
function SOC_VAR_MEDIUM(a2,b2,c2,x){
	var VectSOC=[]
	 if(x <= a2){
		 VectSOC.push(0);
	 }
	 else if( (a2 < x) && (x <= b2)){
		 VectSOC.push((x-a2)/(b2-a2));
	 }
	 else if( (b2 < x) && (x <= c2)){
		 VectSOC.push((c2-x)/(c2-b2));
	 }
	 else if( x > c2){
		 VectSOC.push(0);
	 }
	
	 return VectSOC;
}
//		 case high :
function SOC_VAR_HIGH(a3,b3,x){		
	var VectSOC=[]
	 if(x <= a3){
		 VectSOC.push(0);
	 }
	 else if( (a3 < x) && (x <= b3)){
		 VectSOC.push((x-a3)/(b3-a3));
	 }
	 else if( x > b3){
		 VectSOC.push(1);
	 }

	 return VectSOC;
}


function Pmot_VAR_NEG_HIGH(a1,b1,c1,d1,x){

	 var VectPmot=[];
//case negativeHigh : 
		 
	 if((x <=a1) || (x > -0.6)){
		 VectPmot.push(0);
	 }
	 else if( (a1 < x) && (x <= b1)){
		 VectPmot.push((x-(a1))/(b1-(a1)));
	 }
	 else if( (b1 < x) && (x <= c1)){
		 VectPmot.push(1);
	 }
	 else if( (c1 < x) && (x <= d1)){
		 VectPmot.push((x-(d1))/(d1-(c1)));
	 }
	 return VectPmot;
}
		 
//case negativeMedium :
	 
function Pmot_VAR_NEG_MED(a1,b1,c1,d1,x){

	 var VectPmot=[];
//case negativeHigh : 
		 
	 if((x <=a1) || (x > -0.6)){
		 VectPmot.push(0);
	 }
	 else if( (a1 < x) && (x <= b1)){
		 VectPmot.push((x-(a1))/(b1-(a1)));
	 }
	 else if( (b1 < x) && (x <= c1)){
		 VectPmot.push(1);
	 }
	 else if( (c1 < x) && (x <= d1)){
		 VectPmot.push((x-(d1))/(d1-(c1)));
	 }
	 return VectPmot;
}
	
//case negativeLow  :
		 
function Pmot_VAR_NEG_LOW(a1,b1,c1,d1,x){

	 var VectPmot=[];
//case negativeHigh : 
		 
	 if((x <=a1) || (x > -0.6)){
		 VectPmot.push(0);
	 }
	 else if( (a1 < x) && (x <= b1)){
		 VectPmot.push((x-(a1))/(b1-(a1)));
	 }
	 else if( (b1 < x) && (x <= c1)){
		 VectPmot.push(1);
	 }
	 else if( (c1 < x) && (x <= d1)){
		 VectPmot.push((x-(d1))/(d1-(c1)));
	 }
	 return VectPmot;
}
//case positiveLow : 
		 
function Pmot_VAR_POS_LOW(a1,b1,c1,d1,x){

	 var VectPmot=[];
//case negativeHigh : 
		 
	 if((x <=a1) || (x > -0.6)){
		 VectPmot.push(0);
	 }
	 else if( (a1 < x) && (x <= b1)){
		 VectPmot.push((x-(a1))/(b1-(a1)));
	 }
	 else if( (b1 < x) && (x <= c1)){
		 VectPmot.push(1);
	 }
	 else if( (c1 < x) && (x <= d1)){
		 VectPmot.push((x-(d1))/(d1-(c1)));
	 }
	 return VectPmot;
}
		 
//case positiveMedium :
		 
function Pmot_VAR_POS_MED(a1,b1,c1,d1,x){

	 var VectPmot=[];
//case negativeHigh : 
		 
	 if((x <=a1) || (x > -0.6)){
		 VectPmot.push(0);
	 }
	 else if( (a1 < x) && (x <= b1)){
		 VectPmot.push((x-(a1))/(b1-(a1)));
	 }
	 else if( (b1 < x) && (x <= c1)){
		 VectPmot.push(1);
	 }
	 else if( (c1 < x) && (x <= d1)){
		 VectPmot.push((x-(d1))/(d1-(c1)));
	 }
	 return VectPmot;
}
//case positiveHigh :
		 
function Pmot_VAR_POS_HIGH(a1,b1,c1,d1,x){

	 var VectPmot=[];
//case negativeHigh : 
		 
	 if((x <=a1) || (x > -0.6)){
		 VectPmot.push(0);
	 }
	 else if( (a1 < x) && (x <= b1)){
		 VectPmot.push((x-(a1))/(b1-(a1)));
	 }
	 else if( (b1 < x) && (x <= c1)){
		 VectPmot.push(1);
	 }
	 else if( (c1 < x) && (x <= d1)){
		 VectPmot.push((x-(d1))/(d1-(c1)));
	 }
	 return VectPmot;
}





function Pmot_FLC(x){

	 var VectPmot=[];
//case negativeHigh : 
		 
	 if(x > -0.6){
		 VectPmot.push(0);
	 }
	 else if( (-1 <= x) && (x <= -0.9)){
		 VectPmot.push((x-(-1))/(-0.9-(-1)));
	 }
	 else if( (-0.9 < x) && (x <= -0.7)){
		 VectPmot.push(1);
	 }
	 else if( (-0.7 < x) && (x <= -0.6)){
		 VectPmot.push((x-(-0.6))/(-0.6-(-0.7)));
	 }
	
		 
//case negativeMedium :
	 
	 if((x <=-0.7) || (x > -0.3)){
		 VectPmot.push(0);
	 }
	 else if( (-0.7 < x) && (x <= -0.6)){
		 VectPmot.push((x-(-0.7))/(-0.6-(-0.7)));
	 }
	 else if( (-0.6 < x) && (x <= -0.4)){
		 VectPmot.push(1);
	 }
	 else if( (-0.4 < x) && (x <= -0.3)){
		 VectPmot.push((-0.3-x)/(-0.3-(-0.4)));
	 }
	
//case negativeLow  :
		 
	 if((x <= -0.4) || (x > 0.005)){
		 VectPmot.push(0);
	 }
	 else if( (-0.4 < x) && (x <= -0.3)){
		 VectPmot.push((x-(-0.4))/(-0.3-(-0.4)));
	 }
	 else if( (-0.3 < x) && (x <= -0.01)){
		 VectPmot.push(1);
	 }
	 else if( (-0.01 < x) && (x <= 0.005)){
		 VectPmot.push((0.05-x)/(0.05-(-0.01)));
	 }

//case positiveLow : 
		 
	 if((x < -0.01) || (x > 0.4)){
		 VectPmot.push(0);
	 }
	 else if( (-0.01 <= x) && (x <= 0.01)){
		 VectPmot.push((x-0)/(0.01-0));
	 }
	 else if( (0.01 < x) && (x <= 0.3)){
		 VectPmot.push(1);
	 }
	 else if( (0.3 < x) && (x <= 0.4)){
		 VectPmot.push((0.4-x)/(0.4-0.3));
	 }
	
		 
//case positiveMedium :
		 
	 if((x < 0.3) || (x > 0.7)){
		 VectPmot.push(0);
	 }
	 else if( (0.3 <= x) && (x <= 0.4)){
		 VectPmot.push((x-0.3)/(0.4-0.3));
	 }
	 else if( (0.4 < x) && (x <= 0.6)){
		 VectPmot.push(1);
	 }
	 else if( (0.6 < x) && (x <= 0.7)){
		 VectPmot.push((0.7-x)/(0.7-0.6));
	 }
	
//case positiveHigh :
		 
	 if((x < 0.6)){
		 VectPmot.push(0);
	 }
	 else if( (0.6 <= x) && (x <= 0.7)){
		 VectPmot.push((x-0.6)/(0.7-0.6));
	 }
	 else if( (0.7 < x) && (x <= 0.9)){
		 VectPmot.push(1);
	 }
	 else if( (0.9 < x) && (x <= 1)){
		 VectPmot.push((1-x)/(1-0.9));
	 }
	 
	 return VectPmot;
}


function operationRègles(VectSoc, VectPmot){
	 var vectInter = []
	 
	 //partie opérations, on obtient un vecteur de taille 18 avec toute les appartenaces de tous les cas possible 
	 for(var i=0;i < VectSoc.length;i++ ){
		 for(var k=0; k < VectPmot.length ; k++){
			 
			 vectInter.push(Math.min(VectSoc[i],VectPmot[k]));
			
		 }
	 }
	 //console.log(vectInter);
	 //partie règles: on répartis les résualts dans les différentes loi associée aux cas 
	 // On sait que le vecteur vectInter est sous forme [L.NH , L.NM, L.NL, L.PL,...., H.PM, H.PH], on répartis alors les appartenances selon les règles
	 var vectLoi = [0,0,0,0,0,0];
	 
	 //Loi 0
	 vectLoi[0]= Math.max(vectInter[0],vectInter[6],vectInter[7],vectInter[12],vectInter[13],vectInter[14]);
	 //Loi 1
	 vectLoi[1]= Math.max(vectInter[1],vectInter[8],vectInter[15]);
	 vectLoi[2]= Math.max(vectInter[2],vectInter[9]);
	 vectLoi[3]= Math.max(vectInter[3],vectInter[10],vectInter[16]);
	 vectLoi[4]= Math.max(vectInter[4],vectInter[17]);
	 vectLoi[5]= Math.max(vectInter[5],vectInter[11]);
	 
	 return vectLoi;
}

//Aire des courbes d'appartenaces des lois
//vecteur aires sous la forme [aire0, aire1,...,aire5]
//vecteur plage sous la forme [a,b,c,d] ou a,b,c,d sont les abscisses du trapèze


var plageL0=[0, 0.005, 0.015, 0.02];
var plageL1=[0.09, 0.0905, 0.105, 0.11];
var plageL2=[0.11, 0.12, 0.29, 0.31];
var plageL3=[0.29, 0.31, 0.47, 0.49];
var plageL4=[0.47, 0.49, 0.68, 0.7];
var plageL5=[0.69, 0.695, 0.705, 0.71];

var intervals =[plageL0, plageL1, plageL2, plageL3, plageL4, plageL5];

function calculAires(vectLoi){
	 //l'aire d'un trapèze est : 0.5*(Base+base)*hauteur
	 //La grande Base est invariante, la hauteur correspond au degrès d'appartenance
	 //la petite base varie elle en fonction de la hauteur, on la déterminer via le théorème de Thales
	 var aires = [0,0,0,0,0,0]
	 for(var i=0; i< aires.length ; i++){
		 var a = intervals[i][0];
		 var b = intervals[i][1];
		 var c = intervals[i][2];
		 var d = intervals[i][3];
		 var h = vectLoi[i];
		 
		 aires[i] = 0.5*( (d-a) +2*(1-h)*(d-c) + (c-b) )*h
	 }
	 
	 return aires
}


//Pour le calcul on a besoin du centre de masse des courbes qui sont invariantes
var centreDeMasse= [0.001, 0.1, 0.2, 0.39, 0.585, 0.7];

/**la function defuzzification utilise la méthode des centre de sommes (COS)
 * 
 * @param vectLoi
 * @returns
 */

function deffuzzification(vectLoi){
	 var aireTotale = 0;
	 var airePondere=0;
	
	 var aires = calculAires(vectLoi)
	 
	 for(var i=0; i< vectLoi.length ; i++){
		 airePondere = airePondere + aires[i]*centreDeMasse[i];
		 aireTotale = aireTotale+ aires[i];
	 }
	 
	 var crisp = airePondere/aireTotale;
	 return crisp
}






function initialize_Fuzzylogic_Modif(){
	chart_membership_function = new google.visualization.AreaChart(document.getElementById('chart_membership_function'));
}


/*var x= 0.75;
var y= -0.32;
var vectSOC = SOC(x);
console.log('vectSOC :' + vectSOC);
//vectSOC=[0.4,0.6,0];
var vectPmot =Pmot(y);
console.log('vectPmot :' +vectPmot);
var vectLoi =operationRègles(vectSOC, vectPmot);
console.log('vectLoi :' +vectLoi);
//vectLoi =[0,0,0.6,0.4,0.2,0]
var crisp = deffuzzification(vectLoi);
console.log('crisp =' +crisp);*/





//nouvelles fonctions apportés pas Huijan H. pour le vehicule thermique





/**
* Lecture des donnees du fichier de parametres et affectation dans le tableau de donnees correspondant
* 
 * @param nomFichier :
*            nom du fichier de parametres a lire
* @param tabData :
*            tableau de donnee ou doivent être copie les parametres
* @param nbParam :
*            nombre de parametres dans le tableau
* @param typeTableau :
*            chaine de caractere indiquant comment le tableau doit être rempli
* @returns nbLignesParamVehT : var globale, nombre de types de vehicules differents aka nombre de lignes du fichier
*/

function lectureParam(nomFichier,tabData,nbParam,typeTableau){
                var textString =""; // texte pour les options a ajouter dans le select
                var params =""; // le fichier de parametres sera rentre dans cette string params
                // Check for the various File API support.
                if (window.File && window.FileReader) {
                               // L'API  FileReader est prise/ en charge.
                } else {
                               alert('Les FileReader APIs ne sont pas totalement prises en charge par ce navigateur');
                               return;
                }
                var fileInput = document.querySelector(nomFichier); // manipulation fichier
                var reader = new FileReader(); // lecture fichier

                // controle presence d'un fichier de parametres
                if($(nomFichier).val() == ''){
                               alert("Aucun fichier detecte");
                               return;
                }
                var ParamFileName = fileInput.files[0].name; // nom rentre dans la variable locale
                var ready = false;
                
                var check = function() {
                    if (ready === true) {
                               console.log("lecture fichier data obd fini")
                               
//                           dataObd = traitementTabData(tabData,params,nbParam,typeTableau)
                               
                               var dataTest = JSON.parse(params);

                        var keys = Object.keys(dataTest.Param);
                        keys.forEach(function(key){
                            dataObd.push(dataTest.Param[key]);
                        });
                        
                         return ;
                    }
                    setTimeout(check, 200);
                }

                check();
                
                
                // lecture des donnees et stockage dans la string params
                reader.onload = function(evt) {
                               params = evt.target.result; 
                               ready = true;

                   
                };
                
                
                reader.onerror = function() {
                               alert('Error : Failed to read file');
                };
                
                reader.onabort = function() {
                               alert('Error : read file abort');
                };
                
                
                reader.readAsText(fileInput.files[0]);
                alert("Fichier de parametres utilises : " + ParamFileName);

                return tabData;
}

/*
* HHJ: Fonctions du Model VT
* 
*/
/**
* creer un Array de 3 dimension
* @param x: 1d
* @param y: 2d
* @param z: 3d
*/
function initArray3D(x,y,z){
                var a = []
                var b = []
                var c = []
                for(i=0;i<z;i++){
                                               c[i] = 0;
                }              
                for(i=0;i<y;i++){
                                               b[i] = c.slice(0);
                }              
                for(i=0;i<x;i++){
                                               a[i] = b.slice(0);
                }              
                return a;
}

/**
* convertir un object en Arr
*/
function Object2Arr(){
    try{
        return Array.prototype.slice.call(s);
    } catch(e){
        var arr = [];
        for(var i = 0,len = s.length; i < len; i++){
               arr[i] = s[i];
        }
         return arr;
    }
}

/**
* recuperer les données obd du fichier json  
 */
function getJsonFileFromApp(){
                dataObd.splice(0,dataObd.length); //reset la valeur à 0
                
                //Convertir l'Object myDataObd du fichier 'DataObd_Heves_formatted.js' en Array
    var keys = Object.keys(myDataObd.Param);
    keys.forEach(function(key){
               dataObd.push(myDataObd.Param[key]);
    });
               
}

function traitementTabData(tabData,params,nbParam,typeTableau){
                // compte  du nombre de lignes ,en fonction du nombres de parametres par ligne
                nbLignesParamObd = 1/(nbParam+4)*params.match(/;/g).length;

                                               // split  du string params en fonction des ';'.
                                               var tempArray = new Array();
                                               params = params.split(';');
                                               
                                               console.log("avant splice")
                                               // creation du tableau de donnees en fonction du nombre de lignes
                                                               creaTableauParam(tabData, nbLignesParamObd,typeTableau);
                                                               while(params[0]) {
                                                                               tempArray.push(params.splice(0,1));
                                                                               tempArray.push(params.splice(0,1));
                                                                               params.splice(0,1);
                                                                               tempArray.push(params.splice(0,1));
                                                                               params.splice(0,1);
                                                                               tempArray.push(params.splice(0,1));
                                                                               params.splice(0,1);
                                                                               tempArray.push(params.splice(0,1));
                                                                               params.splice(0,1);
                                                                               tempArray.push(params.splice(0,1));
                                                               }
                                                               console.log("apres splice")
                                                                               if(typeTableau=="Obd" && typeTableau!="Reelles"){
                                                                                              for(var i = 1; i < tabData.length; i++) {
                                                                                                              tabData[i][0] = parseFloat(tempArray[(i - 1) * nbParam].toString().trim().substring(6)); 
                                                                                                              for(var j = 1; j < nbParam; j++) {
                                                                                                                              tabData[i][j] = parseFloat(tempArray[(i - 1) * nbParam + j]); 
                                                                                                              }
                                                                                              }
                                                                                              console.log("avant reverse")
                                                                                              tabData = reversetab(tabData,tabData.length,nbParamObdTotal);
                                                                                              console.log("apres reverse")
                                                                               }
                                                               
                                                               // stockage dans tabData, tableau bidimensionnel. En i = 0 on trouve le nom du type de donnee stockee.
                                               
                                               return tabData;
}

/**
* convertir un fichier Obd .txt en .json
* 
 */
function createJSONFileVT() {
                
                //si dataObd est null
                if(dataObd.length==0){
                               alert("Error: No data obd")
                }
                //si dataObd n'est pas null
                else{
                    tab = dataObd;                       //on récupère les données que l’on veut stocker
                    
                    alert("Fichier créé");
                    myJson = {                                     //creation du tableau de base 
                                        Param : {
                                        }
                    };
                    
                    var nbDonnees = nbLignesParamObd;                                                             
                    for(var i = 0;i<tab.length-1;i++){                     
                               var newKey = tab[i][0];
                               myJson.Param[newKey] = [];                    // pour chaque itération, on créé une liste de nom tab[k][0]
                               for(var k = 0; k<= nbDonnees;k++){              // le nbDonnees ici correspond au nombre de données que l’on enregistre
                          var newId = k;
                          var newValue = tab[i][k];
                          myJson.Param[newKey][newId] = newValue;  // on ajoute ensuite la valeur avec un id i pour localiser les valeurs par la suite
                               }
                    }

                               var data = JSON.stringify(myJson);                  //transforme le JSON object en string compatible JSON
                               var a = document.createElement("a");
                               var file = new Blob([data], {type: 'text/plain'});    //'text/plain à remplacer par contenttype si jamais on veut changer l’output
                               a.href = URL.createObjectURL(file);
                               a.download = 'DataObd_Heves.json';    // à remplacer par fileName si on veut faire varier le nom du fichier en sortie
                               a.click();
                }

}

var tabCSE = []; //tableau CSE de 2 dimensions à retourner par la fonction qui stocke les params separees: i = params, j = ligne (0 = couple, 1 = regime, 2 = CSE)
function lectureFichierCSE(){
                var nomFichier= "#donneesCSE";
                var params =""; // le fichier de parametres sera rentre dans cette string params
                var ready = false;
                               
                               // Check for the various File API support.
                               if (window.File && window.FileReader) {
                                               // L'API  FileReader est prise/ en charge.
                               } else {
                                               alert('Les FileReader APIs ne sont pas totalement prises en charge par ce navigateur');
                                               return;
                               }
                               var fileInput = document.querySelector(nomFichier); // manipulation fichier
                               var reader = new FileReader(); // lecture fichier

                               // controle presence d'un fichier de parametres
                               if($(nomFichier).val() == ''){
                                               alert("Aucun fichier detecte");
                                               return;
                               }
                               
                               
        var check = function() {
            if (ready === true) {
               ready = false;
               tabCSE = lectureDonneeCSE(params); //recupere les donnees du fichier MapCSE.dat
              
                 return;
            }
            setTimeout(check, 200);
        }
        check();

                               // lecture des donnees et stockage dans la string params
                               reader.onload = function(event) {
                                               params = event.target.result;  
                                               ready = true;
                               };
                               reader.readAsText(fileInput.files[0]);
                               //alert("Fichier de parametres utilises : " + fileInput.files[0].name);
                }

/**
* lecture de données d'une map CSE du fichier importé 
 */
function lectureDonneeCSE(params){

                var nbParam = 3;                                                            // 3 params (couple, regime, cse) dans le fichier mapCSE.dat
                var tabParamsSplit = [];                                // declaration d'un tableau qui stocke les string params split en fonction des espaces '' ou entrée
                var tabData = [];                                                              // declaration d'un tableau de 2 dimensions à retourner par la fonction qui stocke les params separees:  (0 = couple, 1 = regime, 2 = CSE)

                // compte  du nombre de lignes ,en fonction du nombres de touche entrée par ligne
                var nbLignesCSE = params.match(/\n/g).length;

                // split du string params en fonction des espaces '' ou entrée.
                tabParamsSplit = params.split(/\s+/);

                // i = 0 represente les strings des noms de params
                tabData[0] = ['couple', 'regime', 'cse'];
                for(var i = 1; i < nbLignesCSE; i++) {
                               tabData[i] = [];
                                               for(var j = 0; j < nbParam; j++) {
                                                                               tabData[i][j] = parseFloat(tabParamsSplit[(i - 1) * nbParam + j]); 
                                               }
                }

                tabData = reversetab(tabData,tabData.length,nbParam); //inverser les lignes et les colonnes du tableau: i<->j 

                return tabData;
}

//fonction qui traiter les tableaux Map CSE: interpolation des donnees.
function traitement_CSE(){
                console.log("traitement cse started");
                minvecNmot = pasNmot*Math.round(rechercheMin(tabCSE[1])/pasNmot); //tab[1] = tab de regime,  (0 = couple, 1 = regime, 2 = CSE)
                maxvecNmot = pasNmot*Math.round(rechercheMax(tabCSE[1])/pasNmot);
                longueurvecNmot = (maxvecNmot-minvecNmot)/pasNmot+1;
                for(i=0;i<longueurvecNmot;i++){
                               vecNmot[i] = minvecNmot+i*pasNmot; 
                }
                minvecCmot = 0;
                maxvecCmot=pasCmot*Math.round(100/pasCmot);
                longueurvecCmot = (maxvecCmot-minvecCmot)/pasCmot+1;
                for(i=0;i<longueurvecCmot;i++){
                               vecCmot[i] = minvecCmot+i*pasCmot; 
                }

                for(i=0;i<longueurvecNmot;i++){
                               matNb[i] = new Array();
                               Map_cse_init[i] = new Array();
                               Map_cse[i]= new Array();            
                               for(j=0;j<longueurvecCmot;j++){
                                               matNb[i][j] = 0;
                                               Map_cse_init[i][j] = 0;
                                               Map_cse[i][j] = 0;
                               }
                }
                
                // Partie cartographie Remplissage de la map cse (moyenne)
                var a;
                var b;
                for (i=1;i<tabCSE[0].length;i++){
                               //si tab regime && tab couple >0
                               if (tabCSE[1][i]>=0 && tabCSE[0][i]>0){ 
                                               a = findIndex(vecNmot,tabCSE[1][i],pasNmot); //trouver l'indice de Regime dans le tab vecNmot
                                               b = findIndex(vecCmot,tabCSE[0][i],pasCmot); //trouver l'indice de Couple dans le tab vecCmot
                                               matNb[a][b] = matNb[a][b]+1; //counter pour calculer le moyen de cse
                                               Map_cse_init[a][b]= tabCSE[2][i];
                               }
                }
                
                
                
                // Remplissage des trous par interpolation / extrapolation Partie interpolation Determination de la taille maximale
                // des trous de la matrice de la matrice (dans les deux directions et dans l'intervalle de definition)
                var k;
                var l;
                for (i=1;i<vecNmot.length;i++){
                    for (j=1;j<vecCmot.length;j++){
                        if (Map_cse_init[i][j]==0){
                            k=j;
                            l=j;
                            while (Map_cse_init[i][l]==0 && l<vecCmot.length-1){
                                l++;
                            }
                            if(Map_cse_init[i][l]==0)
                            {
                              l++;
                            }
                            while (Map_cse_init[i][k]==0 && k>1){
                                k--;
                            }
                            if (l-k>=taille_trou_max && k>1 && l<vecCmot.length){
                              taille_trou_max = l-k-1;
                            }
                        }
                    }
                }

                for (i=1;i<vecNmot.length;i++){
                    for (j=1;j<vecCmot.length;j++){
                        if (Map_cse_init[i][j]==0){
                            k=i;
                            l=i;
                            while (Map_cse_init[l][j]==0 && l<vecNmot.lenght-1){
                                l++;
                            }
                            if(Map_cse_init[l][j]==0)
                            {
                              l++;
                            }
                            while (Map_cse_init[k][j]==0 && k>1){
                                k--;
                            }
                            if (l-k>=taille_trou_max && k>1 && l<vecNmot.length){
                              taille_trou_max = l-k-1;
                            }
                        }
                    }
                }              

                // Remplissage  des trous de la map cse par interpolations lineaires 
                // On remplit en priorite les trous de petite dimension, suivant i et suivant j
                copieTableau(Map_cse_init,Map_cse);
                taille_trou=1;
                while (taille_trou_max > 0){
                    copieTableau(Map_cse,Map_cse_t);
                    var indice=1;
                    while (indice > 0){
                        if (indice==1){
                            for (i=1;i<longueurvecNmot;i++){
                                for (j=1;j<longueurvecCmot;j++){
                                    if (Map_cse[i][j]==0 ){
                                        k=i;
                                        l=i;
                                        while (k>1 && Map_cse[k][j]==0){
                                            k=k-1;
                                        }
                                        while ( l<longueurvecNmot-1 && Map_cse[l][j]==0){                        
                                            l=l+1;
                                        }
                                        if(Map_cse[l][j]==0)
                                           {
                                              l++;
                                           }
                                        if(l<longueurvecNmot){
                                             penteCse = (Map_cse[l][j]-Map_cse[k][j])/(l-k);
                                             if (l-k-1 == taille_trou ){
                                                             for (a=i;a<l;a++){
                                                                            if (a>1 && Map_cse[k][j]>0 && Map_cse[l][j]>0){
                                                                                            Map_cse[a][j]=Map_cse[a-1][j]+penteCse;
                                                                            }
                                                             }
                                             }
                                        }
                                        
                                    }
                                }
                            }
                            indice=2;
                        }
                        if (indice==2){
                            copieTableau(Map_cse,Map_cse_i);
                            for (i=1;i<vecNmot.length;i++){
                                for (j=1;j<vecCmot.length;j++){
                                    if (Map_cse[i][j]==0){
                                        k=j;
                                        l=j;
                                        while (Map_cse[i][k]==0 && k>1){
                                            k=k-1;
                                        }
                                        while (Map_cse[i][l]==0 && l<vecCmot.length-1){
                                            l=l+1;
                                        }
                                        if(Map_cse[i][l]==0)
                                           {
                                              l++;
                                           }
                                        if(l<longueurvecCmot){
                                             penteCse = (Map_cse[i][l]-Map_cse[i][k])/(l-k);
                                             if (l-k-1 == taille_trou){
                                                             for (a=j;a<l;a++){
                                                                            if (a>1 && Map_cse[i][k]>0 && Map_cse[i][l]>0){
                                                                                            Map_cse[i][a]=Map_cse[i][a-1]+penteCse;
                                                                            }
                                                             }
                                             }
                                        }
                                        
                                    }
                                }
                            }
                            if (tabIsEqual(Map_cse,Map_cse_i)){
                                indice=0;
                                if (taille_trou==taille_trou_max){
                                    taille_trou_max=taille_trou_max-1;
                                }
                                if (taille_trou >= 1 && tabIsEqual(Map_cse,Map_cse_t) && taille_trou < taille_trou_max){
                                    taille_trou=taille_trou+1;
                                }
                                else{
                                    taille_trou=1;
                                }
                            }
                            else{
                                indice=1;
                            }
                        }
                        
                    }          
                }

                // Partie  extrapolation

                // Determination de la taille maximale des trous de/ la matrice de la matrice (dans les deux directions et
                // hors  de l'intervalle de definition)

    for (i=1;i<longueurvecNmot;i++){
        for (j=1;j<longueurvecCmot;j++){
                        if (Map_cse_init[i][j]==0){
                            k=j;
                            l=j;
                            while (Map_cse_init[i][l]==0 && l<longueurvecCmot-1){
                                l=l+1;
                            }
                            if(Map_cse_init[i][l]==0){
                              l++;
                            }
                            while (Map_cse_init[i][k]==0 && k>1){
                                k=k-1;
                            }
                            if (l-k-1>=taille_trou_max_ex){
                                taille_trou_max_ex = l-k-1;
                            }
                        }
        }
    }
    for (i=1;i<longueurvecNmot;i++){
        for (j=1;j<longueurvecCmot;j++){
               if (Map_cse_init[i][j]==0){
                            k=i;
                            l=i;
                            while (Map_cse_init[l][j]==0 && l<longueurvecNmot-1){
                                l=l+1;
                            }
                            if(Map_cse_init[l][j]==0){
                              l++;
                            }
                            while (Map_cse_init[k][j]==0 && k>1){
                                k=k-1;
                            }
                            if (l-k-1>=taille_trou_max_ex){
                                taille_trou_max_ex = l-k-1;
                            }
               }
        }
    }

    // Remplissage  des trous parextrapolation lineaire

    copieTableau(Map_cse,Map_cse_ex);
    taille_trou_ex=1;
    while (taille_trou_max_ex > 0){
               Map_cse_t=Map_cse_ex ;
               indice=1;
               while (indice > 0){
                               if (indice==1 ){
                                               for (i=1;i<longueurvecNmot;i++){
                                                               for (j=1;j<longueurvecCmot;j++){
                                                                              if (Map_cse_ex[i][j]==0){
                                                                                              k=i;
                                                                                              l=i;
                                                                                              while (Map_cse_ex[k][j]==0 && k>0){
                                                                                                              k=k-1;
                                                                                              }
                                                                                              while (Map_cse_ex[l][j]==0 && l<longueurvecNmot-1){
                                                                                                              l=l+1;
                                                                                              }
                                                                                              if(Map_cse_ex[l][j]==0){
                                                             l++;
                                                           }
                                                                                              if (k==0 && l < longueurvecNmot-1){
                                                                                                              penteCse = (Map_cse_ex[l][j]-Map_cse_ex[l+1][j])
                                                                                                              if (l-k-1 == taille_trou_ex){
                                                                                                                              b=taille_trou_ex;
                                                                                                                              for (a=i;a<l;a++){
                                                                                                                                             if(Map_cse_ex[l][j]+penteCse*b>0){
                                                                                                                                                             Map_cse_ex[a][j]=Map_cse_ex[l][j]+penteCse*b;
                                                                                                                                             }
                                                                                                                                             b=b-1;
                                                                                                                              }
                                                                                                              }
                                                                                              }
                                                                                              if (k > 1 && l == longueurvecNmot){
                                                                                                              penteCse = (Map_cse_ex[k][j]-Map_cse_ex[k-1][j])
                                                                                                              if (l-k-1 == taille_trou_ex ){
                                                                                                                              for (a=i;a<l;a++){
                                                                                                                                             if(Map_cse_ex[a-1][j]+penteCse>0){
                                                                                                                                                             Map_cse_ex[a][j]=Map_cse_ex[a-1][j]+penteCse;
                                                                                                                                             }
                                                                                                                              }
                                                                                                              }
                                                                                              }
                                                                              }
                                                               }
                  }
                                               indice=2;
                               }
                               if (indice==2){
                                               copieTableau(Map_cse_ex,Map_cse_i);
                                               for (i=1;i<longueurvecNmot;i++){
                                                               for (j=1;j<longueurvecCmot;j++){
                                                                              if (Map_cse_ex[i][j]==0){
                             k=j;
                             l=j;
                             while (Map_cse_ex[i][k]==0 && k>0){
                            k=k-1;
                             }
                             while (Map_cse_ex[i][l]==0 && l<longueurvecCmot-1){
                            l=l+1;
                             }
                             if(Map_cse_ex[i][l]==0){
                            l++;
                          }
                             if (k==0 && l <longueurvecCmot-1){
                            penteCse = (Map_cse_ex[i][l]-Map_cse_ex[i][l+1]);
                            if (l-k-1 == taille_trou_ex){
                                            b=taille_trou_ex;
                                            for (a=j;a<l;a++){
                                                           if(Map_cse_ex[i][l]+penteCse*b>0){
                                                                           Map_cse_ex[i][a]=Map_cse_ex[i][l]+penteCse*b;
                                                           }                                         
                                           b=b-1;
                                            }
                            }
                             }
                             if (k > 1 && l == longueurvecCmot){
                             penteCse = (Map_cse_ex[i][k]-Map_cse_ex[i][k-1]);
                            if (l-k-1 == taille_trou_ex){
                                            for (a=j;a<l;a++){
                                                            if(Map_cse_ex[i][a-1]+penteCse>0){
                                                                           Map_cse_ex[i][a]=Map_cse_ex[i][a-1]+penteCse;
                                                            }
                                            }
                            }
                             }
                                                                              }
                                                               }
                                               }
              if (tabIsEqual(Map_cse_ex,Map_cse_i)){
                              indice=0;
                              if (taille_trou_ex==taille_trou_max_ex){
                             taille_trou_max_ex=taille_trou_max_ex-1;
                              }
                              if (taille_trou_ex >= 1 && tabIsEqual(Map_cse_ex,Map_cse_t) && taille_trou_ex < taille_trou_max_ex){
                             taille_trou_ex=taille_trou_ex+1;
                              }
                              else{
                             taille_trou_ex=1;
                              }
              }
              else{
                              indice=1;
              }
              
                               }
               }
                }
    console.log("traitement cse fini")
                
}//fin de fonction traitement CSE

function AffMapCSE() {
                
                boolOBD = true; 

                var dataessai = new google.visualization.DataTable();
    dataessai.addColumn('number', 'Regime moteur (tours/min)');
    dataessai.addColumn('number', 'Couple (Nm)');
    dataessai.addColumn('number', 'Consommation specifique d\'energie (g/kWh)');


    for (var i = 1; i < longueurvecNmot; i++) {
               for (var j = 1; j < longueurvecCmot; j++) {
                               dataessai.addRow([vecNmot[i],vecCmot[j],Map_cse_ex[i][j]]);
               }
    }

    options = {width:  "100%",
                                  height:"100%",
               style: "surface",
               showPerspective: true,
               showGrid: true,
               showShadow: false,
               keepAspectRatio: false,
               verticalRatio: 0.5,
               };

    document.getElementById('tableCseVT').style.display="";
    graph = new links.Graph3d(document.getElementById('MapCseChartVT'));
    graph.draw(dataessai, options); 
}

/*
* fin de nouvelles fonctions de la map CSE HHJ 
 */
/**
* Fonction globale du traitement des donnees OBD VT
*/
function traitementOBDVT(){

       initialisationParam();    
       
       getJsonFileFromApp(); //recuperer dataObd du fichier local DataObd_Heves_formatted.js
       
       if(boolOBD != true){
             calcMapVitesses();
             affichage_VitessesChartVT();
             remplirParamCouple();
       }
       
       //calcCse();
       //AffMapCseVT(); //HHJ: plus utile ici
       
       
       traitement_CSE();
       AffMapCSE();
       
    boolOBD = true;
    return;
}
/**
* Completion des parametres lies aucouple max par rapport au regime moteur de chaque vehicule dans l'onglet OBD. Vide les data, les options du select, les fields parametres avant de lire le fichier charge.
*/
function remplirParamCouple(){
       dataCouple.splice(0,dataCouple.length);      //initialisation du tableau dataCouple
       //dataCouple = lectureParam("#CoupleParam"+typeVehicule, dataCouple,nbParamCouple,"Couple"); //recuperer le tableau couple par format fichier
       
       //tableau couple avec le meme format que le fichier couple.txt HHJ
       dataCouple = [
             ["Couple max (Nm)", 0, 30, 100, 155, 195, 210, 220, 230, 230, 200, 170],
             ["Regime moteur (tours/min)",750, 800, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000]
       ];
       
       paramFileCoupleLoaded = true;
}

            
/**
* Coefficient pour le calcul de la map cse
*/
var coef = [-173.75613154855455, 0.31619451147527755, -0.000076864935670733, 5.529798712107248e-9];

















/*
 * TODO - la mention 'ADD' est ajoutee aux endroits ou il faut ajouter ou enlever des lignes si on choisi d'augmenter ou de reduire la segmentation des cycles
 * 
 * 
 * A faire apres validation du modele - limite de vitesse de rotation du moteur (necessite le rapport de reduction) - limitateur de puissance - limitateur de couple - prise en compte de la consommation au demarrage - optimisation du code
 * 
 */