var submitRun = false;

var listCarElecParam;
var listCarThermParam;
var listCarHybridParam;
var listCarPacParam;

//Fonction chargant la liste des paramètres des véhicules électriques lors du clic sur le bouton Véhicule électrique
function loadCarElecParam(){
	remplirParamVehiculesE();
	
	// on récupère la liste des paramètres pour les voitures électriques
	$.getJSON("vehicule/electrique/getListCarParam", function(result) {
		listCarElecParam = result.carParams;
		dataVehiculesE[0] = new Array();
		
		var $selectTypeVE = $('#carTypeVE');
		$selectTypeVE.find('option').remove().end();
		$selectTypeVE.append('<option value="0">Param&egrave;tres &agrave; d&eacute;finir</option>');
		
		$.each(listCarElecParam, function(i, val){
			$selectTypeVE.append($('<option />', { value: i+1, text: val.vehicleType }));
			
			var data = new Array();
			data[paramTypeEnum.VEH_TYPE] = val.vehicleType;
			data[paramTypeEnum.VEH_MASS] = val.vehicleWeight;
			data[paramTypeEnum.WHEEL_RADIUS] = val.wheelRadius;
			data[paramTypeEnum.DRAG_COEFF] = val.dragCoefficient;
			data[paramTypeEnum.ROLL_RES] = val.rollingResistance;
			data[paramTypeEnum.TR_YIELD] = val.transmissionYield;
			data[paramTypeEnum.TR_RATIO] = val.transmissionRatio;
			data[paramTypeEnum.ENG_TYPE] = (defaultParamValue[paramTypeEnum.ENG_TYPE]);
			data[paramTypeEnum.NOMINAL_ENG_POW] = val.nominalEnginePower;
			data[paramTypeEnum.MAX_ENG_POW] = val.maxEnginePower;
			data[paramTypeEnum.MAX_ENG_TQ] = val.maxEngineTQ;
			data[paramTypeEnum.BATT_TYPE] = (defaultParamValue[paramTypeEnum.BATT_TYPE]);
			data[paramTypeEnum.BATT_EFF] = val.batteryEfficiency;
			data[paramTypeEnum.BATT_CAPA] = val.batteryCapacity;
			data[paramTypeEnum.REC_MIN_SPD] = val.recoveryMinSpeed;
			data[paramTypeEnum.REC_OPT_SPD] = val.recoveryOptSpeed;
			data[paramTypeEnum.REC_MAX_DEC] = val.recoveryMaxDec;
			data[paramTypeEnum.AUX_CONS] = val.auxiliaryConsumption;
			data[paramTypeEnum.COEFF_INERTIA] = val.inertialLossCoefficient;
			data[paramTypeEnum.VEH_MASS_SUP] = "";
			data[paramTypeEnum.VEH_ID] = val.id;
			
			dataVehiculesE[i+1] = data;
		});
    });
}

//Fonction chargant la liste des paramètres des véhicules thermiques lors du clic sur le bouton Véhicule thermique
function loadCarThermParam(){
	remplirParamVehiculesT();
	
	// on récupère la liste des paramètres pour les voitures thermiques
	$.getJSON("vehicule/thermique/getListCarParam", function(result) {
		listCarThermParam = result.carParams;
		dataVehiculesT[0] = new Array();
		
		var $selectTypeVT = $('#carTypeVT');
		$selectTypeVT.find('option').remove().end();
		$selectTypeVT.append('<option value="0">Param&egrave;tres &agrave; d&eacute;finir</option>');
		
		$.each(listCarThermParam, function(i, val){
			$selectTypeVT.append($('<option />', { value: i+1, text: val.vehicleType }));
			
			var data = new Array();;
			data[0] = val.vehicleType;
			data[1] = val.vehicleWeight;
			data[2] = val.wheelRadius;
			data[3] = val.dragCoefficient;
			data[4] = val.rollingResistance;
			data[5] = val.transmissionYield;
			data[6] = "";
			data[7] = val.inertialLossCoefficient;
			data[8] = val.numberGearbox;
			data[9] = val.id;
			
			dataVehiculesT[i+1] = data;
		});
    });
}

//Fonction chargant la liste des paramètres des véhicules hybrides lors du clic sur le bouton Véhicule hybride
function loadCarHybridParam(){
	// on récupère la liste des paramètres pour les voitures thermiques
	$.getJSON("vehicule/hybrid/getListCarParam", function(result) {
		listCarHybridParam = result.carParams;
		
		var $selectTypeVH = $('#carTypeVH');
		$selectTypeVH.find('option').remove().end();
		$selectTypeVH.append('<option value="0">Param&egrave;tres &agrave; d&eacute;finir</option>');
		
		$.each(listCarHybridParam, function(i, val){
			$selectTypeVH.append($('<option />', { value: i+1, text: val.vehicleType }));
			
			var data = new Array();;
			data[0] = val.vehicleType;
			
			//dataVehicules[i+1] = data;
		});
    });
}


//Fonction chargeant la liste des paramètres des véhicules hydrogènes lors du clic sur le bouton Véhicule hydrogène
function loadCarPacParam(){
	remplirParamVehiculesP();
	
	// on récupère la liste des paramètres pour les voitures à pac
	$.getJSON("vehicule/pac/getListCarParam", function(result) {
		listCarPacParam = result.carParams;
		dataVehiculesP[0] = new Array();
		
		var $selectTypeVP = $('#carTypeVP');
		$selectTypeVP.find('option').remove().end();
		$selectTypeVP.append('<option value="0">Param&egrave;tres &agrave; d&eacute;finir</option>');
		
		$.each(listCarPacParam, function(i, val){
			$selectTypeVP.append($('<option />', { value: i+1, text: val.vehicleType }));
			
			var data = new Array();
			data[paramTypeEnum.VEH_TYPE] = val.vehicleType;
			data[paramTypeEnum.VEH_MASS] = val.vehicleWeight;
			data[paramTypeEnum.WHEEL_RADIUS] = val.wheelRadius;
			data[paramTypeEnum.DRAG_COEFF] = val.dragCoefficient;
			data[paramTypeEnum.ROLL_RES] = val.rollingResistance;
			data[paramTypeEnum.TR_YIELD] = val.transmissionYield;
			data[paramTypeEnum.TR_RATIO] = val.transmissionRatio;
			data[paramTypeEnum.ENG_TYPE] = (defaultParamValue[paramTypeEnum.ENG_TYPE]);
			data[paramTypeEnum.NOMINAL_ENG_POW] = val.nominalEnginePower;
			data[paramTypeEnum.MAX_ENG_POW] = val.maxEnginePower;
			data[paramTypeEnum.MAX_ENG_TQ] = val.maxEngineTQ;
			data[paramTypeEnum.BATT_TYPE] = (defaultParamValue[paramTypeEnum.BATT_TYPE]);
			data[paramTypeEnum.BATT_EFF] = val.batteryEfficiency;
			data[paramTypeEnum.BATT_CAPA] = val.batteryCapacity;
			data[paramTypeEnum.REC_MIN_SPD] = val.recoveryMinSpeed;
			data[paramTypeEnum.REC_OPT_SPD] = val.recoveryOptSpeed;
			data[paramTypeEnum.REC_MAX_DEC] = val.recoveryMaxDec;
			data[paramTypeEnum.AUX_CONS] = val.auxiliaryConsumption;
			data[paramTypeEnum.COEFF_INERTIA] = val.inertialLossCoefficient;
			data[paramTypeEnum.VEH_MASS_SUP] = "";
			data[paramTypeEnum.VEH_ID] = val.id;
			
			dataVehiculesP[i+1] = data;
		});
    });
}

function selectCarParam(){
	
}

//Création des paramètres de véhicules en base
function addVehParam(typeVehParam){
	var idVehParam = "";
	var data = "";
	var url = ctxPath + "/vehicule";
	
	if (typeVehParam == 'E'){
		url += "/carElecParam/add";
		data = $("#idFormVehVE").serialize();
		idVehParam = formVehVE.id.value;
	} else if (typeVehParam == 'T'){
		url += "/carThermParam/add";
		data = $("#idFormVehVT").serialize();
		idVehParam = formVehVT.id.value;
	} else if (typeVehParam == 'H'){
		url += "/carHybridParam/add";
		data = $("#idFormVehVH").serialize();
		idVehParam = formVehVH.id.value;
	} else if (typeVehParam == 'P'){
		url += "/carPacParam/add";
		data = $("#idFormVehVP").serialize();
		idVehParam = formVehVP.id.value;
	} else if (typeVehParam == 'PU'){
		url += "/carPacUTBMParam/add";
		data = $("#idFormVehVPU").serialize();
		idVehParam = formVehVPU.id.value;
	}
	
	postAjax(data, url, successCallback, failureCallback);
	
	function successCallback(){
		if (typeVehParam == 'E'){
			loadCarElecParam();
		} else if (typeVehParam == 'T'){
			loadCarThermParam();
		} else if (typeVehParam == 'H'){
			loadCarHybridParam();
		} else if (typeVehParam == 'P'){
			loadCarPacParam();
		} else if (typeVehParam == 'PU'){
			loadCarPacUTBMParam();
		}
	}
	function failureCallback(){
	}
}

//Mise à jour des paramètres de véhicules en base
function saveVehParam(typeVehParam){
	var idVehParam = "";
	var data = "";
	var url = ctxPath + "/vehicule";
	
	if (typeVehParam == 'E'){
		url += "/carElecParam/update";
		data = $("#idFormVehVE").serialize();
		idVehParam = formVehVE.id.value;
	} else if (typeVehParam == 'T'){
		url += "/carThermParam/update";
		data = $("#idFormVehVT").serialize();
		idVehParam = formVehVT.id.value;
	} else if (typeVehParam == 'H'){
		url += "/carHybridParam/update";
		data = $("#idFormVehVH").serialize();
		idVehParam = formVehVH.id.value;
	} else if (typeVehParam == 'P'){
		url += "/carPacParam/update";
		data = $("#idFormVehVP").serialize();
		idVehParam = formVehVP.id.value;
	} else if (typeVehParam == 'PU'){
		url += "/carPacUTBMParam/update";
		data = $("#idFormVehVPU").serialize();
		idVehParam = formVehVPU.id.value;
	}
	
	postAjax(data, url, successCallback, failureCallback);
	
	function successCallback(){
		if (typeVehParam == 'E'){
			loadCarElecParam();
		} else if (typeVehParam == 'T'){
			loadCarThermParam();
		} else if (typeVehParam == 'H'){
			loadCarHybridParam();
		} else if (typeVehParam == 'P'){
			loadCarPacParam();
		} else if (typeVehParam == 'PU'){
			loadCarPacUTBMParam();
		} 
	}
	function failureCallback(){
	}
}

//Suppression des paramètres de véhicules en base
function deleteVehParam(typeVehParam){
	var idVehParam = "";
	var data = "";
	var url = ctxPath + "/vehicule";
	
	if (typeVehParam == 'E'){
		url += "/carElecParam";
		data = $("#idFormVehVE").serialize();
		idVehParam = formVehVE.id.value;
	} else if (typeVehParam == 'T'){
		url += "/carThermParam";
		data = $("#idFormVehVT").serialize();
		idVehParam = formVehVT.id.value;
	} else if (typeVehParam == 'H'){
		url += "/carHybridParam";
		data = $("#idFormVehVH").serialize();
		idVehParam = formVehVH.id.value;
	} else if (typeVehParam == 'P'){
		url += "/carPacParam";
		data = $("#idFormVehVP").serialize();
		idVehParam = formVehVP.id.value;
	} else if (typeVehParam == 'PU'){
		url += "/carPacParam";
		data = $("#idFormVehVPU").serialize();
		idVehParam = formVehVPU.id.value;
	}
	
	url += "/delete/"+idVehParam;
	postAjax(data, url, successCallback, failureCallback);
	
	function successCallback(){
		if (typeVehParam == 'E'){
			loadCarElecParam();
		} else if (typeVehParam == 'T'){
			loadCarThermParam();
		} else if (typeVehParam == 'H'){
			loadCarHybridParam();
		} else if (typeVehParam == 'P'){
			loadCarPacParam();
		} else if (typeVehParam == 'PU'){
			loadCarPacParam();
		}
	}
	function failureCallback(){
	}
}

function loadDataBorne(){
	// on récupère la liste des paramètres pour les voitures électriques
	$.getJSON(ctxPath+"/dataBorne/getListDataBorne", function(result) {
		listDataBorne = result.dataBornes;
		dataBorne = new Array();
		
		dataBorne[0] = new Array();
		dataBorne[0][0] = 'Nom du fournisseur'
		dataBorne[0][1] = 'Latitude'
		dataBorne[0][2] = 'Longitude'
		dataBorne[0][3] = 'Borne accessible' // vrai si oui et faux sinon, faux par defaut
		dataBorne[0][4] = 'Id';
			
		$.each(listDataBorne, function(i, val){
			var data = new Array();
			data[0]	= val.providerName;
			data[1]	= val.latitude;		
			data[2]	= val.longitude;
			data[3] = false;
			data[4] = val.id;
						
			dataBorne[i+1] = data;
			
			addMarker(dataBorne[i+1][1],dataBorne[i+1][2],'Borne de recharge','images/borne-recharge-electrique.png',true,markersBornes)
		});
		
		// Compteur du nombre de bornes
		nbPointBorne = dataBorne.length;
		
		markerCluster = new MarkerClusterer(map, markersBornes, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
		markerCluster.clearMarkers();
		document.getElementById('affichageBornes'+typeVehicule).style.visibility='';
    });
}

function selectCycleStandard(){
	var $selectCycleStandardVE = $('#cycleStandardVE');
	
	var typeCycle = $selectCycleStandardVE.val();
	
	$.getJSON(ctxPath+"/cycleStandard/selectCycleStandard/"+typeCycle, function(result) {
		var listCycles = result.cycleStandards;
	});
}