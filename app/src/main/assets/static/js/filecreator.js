
		function displayContent() {
			var type = document.getElementById("fileType").value;
			if(type == 1 || type == 2 || type == 3){
			  content = createContent();		
			  alert(content);
			  document.getElementById("donnees_route").innerHTML = content;
			} else  if (type == 4){
				downloadCSV(csvElevationExport);
			} else if (type == 5){
				downloadCSV(csvDistanceExport);
			}
		}	
			
		function createContent() {

			var name = "myroute";
			var type = document.getElementById("fileType").value;
			var content = "";
			content += "Début des données";
			
			var trackString = "\n\n<trk>\n<name>" + name + "</name>";
			var temp1; // latitude du WP
			var temp2; // longitude du WP
			var temp3; // longueur de la portion entre les 2 WP
			var temp4; // durée de la portion entre les 2 wp
			var temp5 = 0; // distance cumulée entre les wp
			var temp6; //altitude
			var routeString = "\n\n<rte>\n<name>" + name + "</name>";
			var nedcString = "\n\ntime(s)\tspeed(m/s)\t\tslope\trapport\n";
			var path;
			var dataLength = dataxcos.getNumberOfRows();
			var dataWidth = dataxcos.getNumberOfColumns();
			
			if (type == 3) {
			for ( var lin = 1; lin < dataLength; lin++) {
				temp1 = dataxcos.getValue(lin,1); // latitude
				temp2 = dataxcos.getValue(lin,2); // longitude
				temp3 = dataxcos.getValue(lin,3); // altitude
				temp4 = dataxcos.getValue(lin,4);	// distance cumulee 
				temp5 = dataxcos.getValue(lin,5);	// durée cumulée
				temp6 = dataxcos.getValue(lin,5);	// pente
				nedcString += temp5 + "\t" + temp3/temp4 + "\t" + temp6 + "\t" + "rapport" + "\n";
				}
			}

			if (type == 1 || type == 2) {
			directionsResult = directionsDisplay.getDirections();
			wayPoints = [];
			var route = directionsResult.routes[0];
			var steps = route.legs[0].steps;
				for( var i = 0; i < steps.length; i++) {
					path = steps[i].path;
					temp1 = steps[i].start_location.lat();
					temp2 = steps[i].start_location.lng();
					temp3 = steps[i].distance.value;
					temp4 = steps[i].duration.value;
					temp5 += steps[i].duration.value;
					routeString += "\n<rtept lat =\"" + temp1 + "\" lon=\"" + temp2 + "\" distance=\"" + temp3 + "\" durée=\"" + temp4 + "\"></rtept>";
					trackString += "\n<trkseg>"
					for (var z = 0; z < path.length; z++){
						temp1 = path[z].lat();
						temp2 = path[z].lng();
						trackString += "\n<trkpt lat =\"" + temp1 + "\" lon=\"" + temp2 + "\"></trkpt>";
					}
					trackString += "\n</trkseg>";
				}		
			trackString += "\n</trk>";
			routeString += "\n</rte>";					
			}

			if(type == 1 || type == 2) {
				content += routeString;
			}	
			if(type == 0 || type == 2) {
				content += trackString;
			}
			if(type == 3) {
				content += nedcString;
			}
			content += "\n Fin des données";
			return content;
		}




		